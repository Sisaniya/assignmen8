var port = process.env.PORT || 2410;
const { convertArrayToCSV } = require('convert-array-to-csv');
const csv=require('csvtojson');
var multer = require('multer');
var upload = multer();
const converter=require('json-2-csv');
var express = require("express");
const fs=require("fs");
const BodyParser=require("body-parser");
var jwt=require("jsonwebtoken");
var passport=require("passport");
var passportJWT=require("passport-jwt");
const Strategy=passportJWT.Strategy;
const ExtractJwt=passportJWT.ExtractJwt;
let secretKey="my_passport_token_yatra";
let app=express();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true })); 
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers,Authorization,Access-Control-Request,Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "PUT,HEAD,POST, GET, DELETE, OPTIONS");
  res.setHeader("Access-Control-Expose-Headers","*");
  next();
});
app.listen(port,()=>console.log("Listening at the post =>",port));
var params={
  secretOrKey: secretKey,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};
let data=require("./flights.js").data;
let users=require("./usersData").users;
let bookings=require("./usersData").bookings;

passport.initialize();
var strategy=new Strategy(params,function(payload,done){
  console.log(payload,"enterr in gettting varifying");
  let user=users.find(ele=>ele.id == payload.id);
  user=user?user:null;
  if(user)
  {
   
     console.log("user varified",user);
      done(null,{id:user.id})
    
  }
  else
  {
   done(new Error("Invalid Request!"),null);
  }
});
passport.use("local.one",strategy);



app.get("/",function(req,res){
  res.send("welcome to  server");
});
let jwtExpiresSeconds=300000;
app.post("/login",function(req,res,payload){

  let body={...req.body};
  if(body.email && body.password)
  {
        let user=users.find(ele=>ele.email == body.email && ele.password == body.password);
         if(user)
         {
          
          var payload={id:user.id};
          console.log(user,payload);
          let token=jwt.sign(payload,secretKey,{algorithm:"HS256",expiresIn:jwtExpiresSeconds});
           res.setHeader("X-Auth-Token","bearer "+token);
           let obj={name:user.fname,email:user.email,mobileNo:user.mobileNo,id:user.id,fullname:`${user.title} ${user.fname} ${user.lname}`};
           res.json(obj);
         }
       else
       {
        res.sendStatus(400);
       }
    
       
     
  }
     else {
         res.sendStatus(400);
     }

});
app.get("/user",passport.authenticate("local.one",{session:false}),function(req,res){
  let user=users.find(ele=>ele.id == req.user.id);
 console.log(user,req.user);
  res.send(user);
});

app.get("/eCash",passport.authenticate("local.one",{session:false}),function(req,res){
  let arr=bookings.filter(ele=>ele.userId == req.user.id);
   narr=arr.map(ele=>{
     let obj={date:ele.departure.date,ref:ele.ref,ecash:ele.ecash};
     obj.des="Use Ecash using promocode (Before Jul 1,2021 )";
     return obj;
   })
res.send(narr);
});

app.get("/bookings",passport.authenticate("local.one",{session:false}),function(req,res){
    let arr=bookings.filter(ele=>ele.userId == req.user.id);

  res.send(arr);
});
app.post("/bookings",function(req,res){
  let obj={...req.body};

  obj.id=bookings.length+1;

  let refn=Math.random().toString(36).substring(2,6).toUpperCase()+Math.random().toString(36).substring(2,7)+Math.random().toString(36).substring(2,4).toUpperCase()+Math.random().toString(36).substring(2,5);
  
  obj.ref=refn;
  bookings.push(obj);
 
  res.send(obj);
});
app.put("/user",function(req,res){
  let user={...req.body};
  let index=users.findIndex(ele=>ele.id == user.id);
  if(index>=0)
  {
    users.splice(index,1,user);
    res.send(user);
  }
  else
  {
    res.status(400).send("Error");
  }
})
app.get("/flights/:departFrom/:goingTo",function(req,res){
   let list=[...data];
   let {departFrom,goingTo}=req.params;
   console.log(departFrom,goingTo,list.length);
   let {price,time,flight,airbus,sort}=req.query;
   list=list.filter(ele=>ele.desDept == departFrom && ele.desArr == goingTo);
   console.log(departFrom,goingTo,list.length);
 
   list=getPriceFilter(price,list);
   if(time){
     list=getTimefilter(time,list);
   }
   if(flight)
   {
    list=list.filter(ele=>flight.split(",").findIndex(e=>e == ele.name)>=0?true:false);
   }
   if(airbus)
   {
    list=list.filter(ele=>airbus.split(",").findIndex(e=>e == ele.airBus)>=0?true:false);
   }
   list=getSortArray(sort,list);
   res.send(list);
  
   });
   app.get("/returnflights/:departFrom/:goingTo",function(req,res){
    let list=[...data];
    let list2=[...data];
    let {departFrom,goingTo}=req.params;
    console.log(departFrom,goingTo,list.length);
    let {price,time,flight,airbus,sort}=req.query;
    list=list.filter(ele=>ele.desDept == departFrom && ele.desArr == goingTo);
    list2=list2.filter(ele=>ele.desDept == goingTo&& ele.desArr == departFrom);
    console.log(departFrom,goingTo,list.length);
  
    list=getPriceFilter(price,list);
    if(time){
      list=getTimefilter(time,list);
    }
    if(flight)
    {
     list=list.filter(ele=>flight.split(",").findIndex(e=>e == ele.name)>=0?true:false);
    }
    if(airbus)
    {
     list=list.filter(ele=>airbus.split(",").findIndex(e=>e == ele.airBus)>=0?true:false);
    }
    list=getSortArray(sort,list);
    list2=getPriceFilter(price,list2);
    if(time){
      list2=getTimefilter(time,list2);
    }
    if(flight)
    {
     list2=list2.filter(ele=>flight.split(",").findIndex(e=>e == ele.name)>=0?true:false);
    }
    if(airbus)
    {
     list2=list2.filter(ele=>airbus.split(",").findIndex(e=>e == ele.airBus)>=0?true:false);
    }
    list2=getSortArray(sort,list2);
    let obj={list1:list,list2:list2};
    res.send(obj);
   
    });
 
   function getSortArray(sort,list){
    if(sort)
    {
      if(sort=="departure")list.sort((f1,f2)=>{
       let timearr1=(f1.timeDept.split(":"));
       let t1= + (timearr1[0]+timearr1[1]);
       let timearr2=(f2.timeDept.split(":"));
       let t2= + (timearr2[0]+timearr2[1]);
       return t1-t2;
      });
      if(sort=="arrival")list.sort((f1,f2)=>{
       let timearr1=(f1.timeArr.split(":"));
       let t1= + (timearr1[0]+timearr1[1]);
       let timearr2=(f2.timeArr.split(":"));
       let t2= + (timearr2[0]+timearr2[1]);
       return t1-t2;
      });
      if(sort=="price")list.sort((f1,f2)=>(+f1.Price) - (+f2.Price));
    }
     return list; 
   }
function getPriceFilter(price,list) 
{
  if(price)
   {
     if(price=="0-5000") list=list.filter(ele=>ele.Price < 5000);
     if(price=="5000-10000") list=list.filter(ele=>ele.Price >= 5000 && ele.Price < 10000);
     if(price=="10000-15000") list=list.filter(ele=>ele.Price >= 10000 && ele.Price < 15000);
     if(price=="15000-20000") list=list.filter(ele=>ele.Price >= 15000 && ele.Price < 20000);
   
   }
   return list;

}
function getTimefilter(time,arr){
  if(time=="0-6")
  {arr=arr.filter(ele=>{
    let timearr=(ele.timeDept.split(":"));
    let t= + timearr[0];
   
    if(t<6) return true;
    else return false;
  });
   }
  if(time=="6-12")
  {
  arr=arr.filter(ele=>{
    let timearr=(ele.timeDept.split(":"));
    let t= + timearr[0];
   
    if(t>=6 && t<12) return true;
    else return false;
  });
    }
  if(time=="12-18")
  {arr=arr.filter(ele=>{
    let timearr=(ele.timeDept.split(":"));
    let t= + timearr[0];
   
    if(t>=12 && t<18) return true;
    else return false;
  });
   }
  if(time=="18-00")
  {arr=arr.filter(ele=>{
    let timearr=(ele.timeDept.split(":"));
    let t= + timearr[0];
   
    if(t>=18) return true;
    else return false;
  });}
  return arr;

}
var logout=require("express-passport-logout");

app.delete("/logout",passport.authenticate("local.one",{session:false}),logout());

