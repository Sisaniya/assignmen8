import React,{Component} from "react";
class InputTDetails extends Component
{
    state={};
    render() {
        let {email,mobileNo,travellersInfo}=this.props.cDetails;
        let {Adult=[],Child=[],Infant=[]}=travellersInfo;
        let {uinfo,error}=this.props;
        return (
            <React.Fragment>
            <div className="row bg-white " id="box2">
            <div className="col-12">
                <div className="row bg-white ">
                    <div className="col-lg-2 col-4" id="fs14">
                        <b>Contact :</b>
                    </div>
                    <form className="ng-invalid ng-dirty ng-touched">
                        <div className="row">
                            <div className="col-6 form-group">
                                <input className="form-control ng-invalid ng-dirty ng-touched" 
                                name="email" id="email" placeholder="Enter email" type="text"
                                readOnly={uinfo?true:false}
                                onChange={this.props.handleDChange}
                                value={email}/>
                                <div className="fontstyle">
                                    {error.email && <span className="text-danger">{error.email}</span>}
                                </div>
                            </div>
                            <div className="col-6 form-group">
                                <input className="form-control ng-untouched ng-pristine ng-invalid"
                                 placeholder="Mobile Number" type="number" name="mobileNo" id="mobileNo" 
                                 onChange={this.props.handleDChange}
                                 readOnly={uinfo?true:false}
                                 value={mobileNo}/>
                                 <div className="fontstyle">
                                    {error.mobileNo && <span className="text-danger">{error.mobileNo}</span>}
                                </div>
                            </div>
                        </div>
                   </form>
               </div>
               <div className="row bg-white idftfs" >
                   <div className="col-2 d-none d-lg-block"></div>
                   <div className="col-lg-10 col-12" id="fs14">
                        Your booking details will be sent to this email address and mobile number.
                   </div>
              </div>
              <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12" id="fs14"> Also send my booking details on WhatsApp 
                  <i className="fab fa-whatsapp-square idftfs1" ></i>
                  </div>
              </div>
              <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12"><hr/>
                  </div>
              </div>
              <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-10" id="fs14"><b>Traveller Information</b></div>
              </div>
              <div className="row bg-white idftfs2" >
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12" id="fs14">
                      <b>Important Note:</b> 
                      Please ensure that the names of the passengers on the travel documents is the same as on their government issued identity proof. 
                  </div>
              </div>
              <form className="ng-untouched ng-pristine ng-invalid">
                  <div className="row bg-white ng-untouched ng-pristine ng-invalid" name="arr">
                      {error.travellersInfo && <div className="col-12 text-center fontstyle text-danger">{error.travellersInfo}</div>}
                      <div className="col-12">
                        {Adult.map((ele,index)=>{
                        
                            return (
                                <div className="row mt-1 ng-untouched ng-pristine ng-invalid">
                              <div className="col-lg-2 col-12" id="fs14"><label>Adult {index+1} :</label></div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid"
                                   name="Adult" placeholder="First Name" title="travellers"
                                   id={index}
                                   onChange={this.props.handleDChange}
                                
                                   type="text" value={ele.fname}/>
                              </div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid" 
                                  placeholder="Last Name"  title="travellers" name="Adult" type="text" 
                                  onChange={this.props.handleDChange}
                                  id={index} value={ele.lname}/>
                              </div>
                          </div>


                            )
                        })}

                     {Child.map((ele,index)=>{
                            return (
                                <div className="row ng-untouched mt-1 ng-pristine ng-invalid">
                              <div className="col-lg-2 col-12" id="fs14"><label>Child {index+1} :</label></div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid"
                                   name="Child" placeholder="First Name" title="travellers"
                                   id={index}
                                   onChange={this.props.handleDChange}
                                
                                   type="text" value={ele.fname}/>
                        
                                   
                              </div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid" 
                                  placeholder="Last Name" title="travellers" name="Child" type="text" 
                                  onChange={this.props.handleDChange}
                                  id={index} value={ele.lname}/>
                              </div>
                          </div>


                            )
                        })}

                   {Infant.map((ele,index)=>{
                            return (
                                <div className="row ng-untouched mt-1 ng-pristine ng-invalid">
                              <div className="col-lg-2 col-12" id="fs14"><label>Infant {index+1} :</label></div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid"
                                   name="Infant" placeholder="First Name" title="travellers"
                                   id={index}
                                   onChange={this.props.handleDChange}
                                
                                   type="text" value={ele.fname}/>
                              </div>
                              <div className="col-5">
                                  <input className="form-control ng-untouched ng-pristine ng-invalid" 
                                  placeholder="Last Name" title="travellers" name="Infant" type="text" 
                                  onChange={this.props.handleDChange}
                                  id={index} value={ele.lname}/>
                              </div>
                          </div>


                            )
                        })}

                          

                      </div>
                  </div>
              </form>
          </div>
      </div>




<div className="row bg-white mt-2 " id="box2">
    <div className="col-12 d-none d-lg-block">
        <div className="row bg-white idftfs3" >
            <div className="col-1 text-center mt-1">
                <i className="fa fa-university idftfs4" ></i>
            </div>
            <div className="col-10 text-left">
                <div className="row" id="fs14"><b>Add your GST Details (Optional)</b></div>
                <div className="row" id="fs14"> Claim credit of GST charges. Your taxes may get updated post submitting your GST details.
                </div>
            </div>
        </div>
        <div className="row bg-white idftfs5">
            <div className="col-1 text-center">
                <svg aria-hidden="true" focusable="false" data-prefix="fas"
                 data-icon="suitcase-rolling" className="svg-inline--fa fa-suitcase-rolling fa-w-12 idftfs6"
                  role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" >
                      <path fill="currentColor" d="M336 160H48c-26.51 0-48 21.49-48 48v224c0 26.51 21.49 48 48 48h16v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h128v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h16c26.51 0 48-21.49 48-48V208c0-26.51-21.49-48-48-48zm-16 216c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zM144 48h96v80h48V48c0-26.51-21.49-48-48-48h-96c-26.51 0-48 21.49-48 48v80h48V48z">
                        </path>
                 </svg>
            </div>
            <div className="col-10 text-left">
                <div className="row" id="fs14"><b>Travelling for work?</b></div>
                <div className="row" id="fs14"> Join Yatra for Business. View Benefits </div>
            </div>
        </div>
    </div>
</div>






                 

<div className="row mt-2">
    <div className="col-1 text-right idftfs7 ">
        <i className="fa fa-umbrella"></i>
    </div>
    <div className="col-11" id="fs10"> Travel Protection
    <span id="fs14">(Recommended)</span>
    </div>
</div>



    </React.Fragment>
        )
    }
}
export default InputTDetails;