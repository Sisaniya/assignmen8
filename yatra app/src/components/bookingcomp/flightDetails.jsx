import React,{Component} from "react";
class FlightDetails extends Component 
{
    state={}
    getFlightRow=(flight={})=>{
        let  {id,logo,name,code,checkin,meal,desDept,desArr,Price,airBus,total,timeDept,timeArr,T1,T2} =flight;
        return (
            <div className="row bg-white sforfds " >
            <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                <div className="row pl-1">
                      <div className="col-lg-12 col-12">
                        <img src="https://i.ibb.co/hBb7Yg8/images-1.jpg" className="sforfds1"/>
                      </div>
                </div>
                <div className="row pl-1">
                    <div className="col-lg-12 col-12">{name}</div>
                </div>
                <div className="row pl-1 text-secondary">
                    <div className="col-lg-12 col-12">{code}</div>
                </div>
            </div>
            <div className="col-lg-2 col-4">
                <div className="row fs-base">{desDept}</div>
                <div className="row lh28 gray-dark fs-24 bold">{timeDept}</div>
                <div className="row text-secondary" id="fs11">{T1}</div>
            </div>
            <div className="col-lg-6 col-3">
                <div className="row">
                    <div className="col-12 text-center no-padding-margin">{total}
                    <span className="gray-lightest">|</span>
                    <span className=""> {meal} </span>
                    <span className="gray-lightest">|</span>
                    <span className="">Economy</span>
                    </div>
                </div>
                 <div className="row two-dots">
                     <div className="col-4 no-padding-margin"><hr/></div>
                     <div className="col-lg-4 text-center no-padding-margin">
                         <span classNamename="type-text">
                             <i className="fa fa-fighter-jet" id="ic1">
                                 </i>Flight</span>
                      </div>
                      <div className="col-4 no-padding-margin"><hr/></div>
                  </div>
                  <div className="row">
                      <div className="col text-center">25 kgs |
                       <span className="text-success">Partially Refundable</span>
                       </div>
                  </div>
             </div>
             <div className="col-lg-2 col-5">
                 <div className="row fs-base">{desArr}</div>
                 <div className="row lh28 gray-dark fs-24 bold">{timeArr}</div>
                 <div className="row text-secondary" id="fs11">{T2}</div>
              </div>
          </div>



            
        )
    }
    render() {
        let {completeD}=this.props;
        let {bflights,searchFlight}=completeD;
        return (
            <div className="row bg-white " id="box2">
                  <div className="col-12">

                      {this.getFlightRow(bflights[0])}
                           
                        {searchFlight.rFlight?
                        <React.Fragment>
                            <div className="row bg-white">
                            <div className="col-2 d-none d-lg-block"></div>
                            <div className="col-lg-10 col-12 sforfds2"
                            >
                            </div>
                           </div>
                            {this.getFlightRow(bflights[1])}
                        </React.Fragment>
                        :""}
                        <div className ="note-block-new full-lrb">
                            <i className="ytfi-info-circled"></i>
                            <span className="noteblocks fs-sm fontstyle ng-binding">Compulsory Guidelines for Passengers</span>
                             <ul className="noteblocks fontstyle">
                                 <li ng-repeat="msg in guidelines.impmsg.msg track by $index" className="ng-scope">
                                     <span className="fs-sm ng-binding">You need to certify your health status through Aarogya Setu app preactivated on your mobile or self-declaration form.
                                     <a href="#" ng-show="msg.link" target="_blank" className="color-blue under-link ng-hide">click here</a>
                                     </span>
                                 </li>
                                 <li ng-repeat="msg in guidelines.impmsg.msg track by $index" classNamae="ng-scope">
                                     <span className="fs-sm ng-binding">Face Mask is mandatory both at the airport &amp; in flight.
                                     <a href="#" ng-show="msg.link" target="_blank" className="color-blue under-link ng-hide">click here</a>
                                     </span>
                                </li>
                                <li ng-repeat="msg in guidelines.impmsg.msg track by $index" className="ng-scope">
                                    <span className="fs-sm ng-binding">Failure to comply with Covid-19 protocols and the directions of ground staff and/or crew may attract penal action against the concerned individual.
                                    <a href="#" ng-show="msg.link" target="_blank" className="color-blue under-link ng-hide">click here</a>
                                    </span>
                                </li>
                                <li ng-repeat="msg in guidelines.impmsg.msg track by $index" className="ng-scope">
                                    <span className="fs-sm ng-binding">Only passengers with confirmed web check-in will be allowed to enter 2 hours prior to the flight departure.
                                    <a href="#" ng-show="msg.link" target="_blank" className="color-blue under-link ng-hide">click here</a>
                                   </span>
                                </li>
                                <li ng-repeat="msg in guidelines.impmsg.msg track by $index" className="ng-scope">
                                    <span className="fs-sm ng-binding">Only one check-in bag and cabin bag will be allowed per customer with a baggage tag affixed on the bag.
                                    <a href="" ng-show="msg.link" target="_blank" className="color-blue under-link ng-hide">click here</a>
                                    </span>
                                </li>
                                <li ng-repeat="msg in guidelines.impmsg.msg track by $index" className="ng-scope">
                                    <span className="fs-sm ng-binding">View all mandatory travel guidelines issued by Govt. of India here
                                    <a href="https://bit.ly/3gjVF7o" ng-show="msg.link" target="_blank" className="color-blue under-link">click here</a>
                                    </span>
                                </li>
                                </ul>
                            </div>

                  </div> 
           
            </div> 
        )
    }
}
export default FlightDetails;