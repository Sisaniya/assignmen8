import React,{Component} from "react";
class ProtectionComp extends Component
{
    state={}
    render() {
        let {insurance}=this.props;
        return (
            

      <div className="row bg-white  fontstyle" id="box2">
      <div className="col-12">
          <div className="row bg-white">
              <div className="col-2 text-center">
                  <input className="form-check-input ng-untouched ng-pristine ng-valid" 
                  name="insurance" 
                  checked={insurance}
                  onChange={this.props.handleChange}
                  type="checkbox" id="insurance"/>
                </div>
                <div className="col-10 mt-1" id="fs14"> Yes, Add Travel Protection to protect my trip (Rs.269 per traveller) </div>
         </div>
         <div className="row bg-white pcsfort" >
             <div className="col-1 d-none d-lg-block"></div>
             <div className="col-11 d-none d-lg-block" id="fs14">
                  6000+ travellers on Yatra protect their trip daily.
                  <span className="text-primary">Learn More</span>
             </div>
        </div>
        <div className="row bg-white">
            <div className="col-12 text-left text-muted d-none d-lg-block" id="fs14"> Cover Includes: </div>
        </div>
        <div className="row bg-white">
            <div className="col-4 text-center d-none d-lg-block" id="fs14">
                <div className="row">
                    <div className="col text-left">
                        <span className="fa-stack fa-2x">
                            <i className="fa fa-circle fa-stack-2x">
                            </i>
                            <svg aria-hidden="true" focusable="false"
                             data-prefix="fas" data-icon="plane-departure" 
                             className="svg-inline--fa fa-plane-departure fa-w-20 fa-stack-1x fa-inverse" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                                 <path fill="currentColor" d="M624 448H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h608c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM80.55 341.27c6.28 6.84 15.1 10.72 24.33 10.71l130.54-.18a65.62 65.62 0 0 0 29.64-7.12l290.96-147.65c26.74-13.57 50.71-32.94 67.02-58.31 18.31-28.48 20.3-49.09 13.07-63.65-7.21-14.57-24.74-25.27-58.25-27.45-29.85-1.94-59.54 5.92-86.28 19.48l-98.51 49.99-218.7-82.06a17.799 17.799 0 0 0-18-1.11L90.62 67.29c-10.67 5.41-13.25 19.65-5.17 28.53l156.22 98.1-103.21 52.38-72.35-36.47a17.804 17.804 0 0 0-16.07.02L9.91 230.22c-10.44 5.3-13.19 19.12-5.57 28.08l76.21 82.97z">
                                </path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div className="row pcsfort1 " > Trip Cancellation </div>
                <div className="row pcsfort1 " > Claim upto Rs.25,000 </div>
            </div>
            <div className="col-4 text-center d-none d-lg-block pcsfort2">
                <div className="row mb-1">
                    <div className="col text-left">
                        <span className="fa-stack fa-2x">
                            <i className="fa fa-circle fa-stack-2x"></i>
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="suitcase-rolling" 
                            className="svg-inline--fa fa-suitcase-rolling fa-w-12 fa-stack-1x fa-inverse" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                <path fill="currentColor" d="M336 160H48c-26.51 0-48 21.49-48 48v224c0 26.51 21.49 48 48 48h16v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h128v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h16c26.51 0 48-21.49 48-48V208c0-26.51-21.49-48-48-48zm-16 216c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zM144 48h96v80h48V48c0-26.51-21.49-48-48-48h-96c-26.51 0-48 21.49-48 48v80h48V48z">
                                </path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div className="row  pcsfort1"> Loss of Baggage </div>
                <div className="row  pcsfort1" > Claim upto Rs.25,000 </div>
                </div>
                <div className="col-4 text-center d-none d-lg-block pcsfort3">
                    <div className="row">
                        <div className="col text-left">
                            <span className="fa-stack fa-2x">
                                <i className="fa fa-circle fa-stack-2x"></i>
                                <i className="fa fa-ambulance fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>
                    </div>
                    <div className="row pcsfort1" > Medical Emergency </div>
                    <div className="row pcsfort1" > Claim upto Rs.25,000 </div>
                </div>
                </div>
                <div className="row bg-white">
                    <div className="col-12 text-center d-none d-lg-block pcsfort1" >
                         Note: Travel Protection is applicable only for Indian citizens below the age of 70 years.
                          <span className="text-primary">Terms Conditions</span>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
export default ProtectionComp