import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import SearchDetailsNav from "./searchDetailsNav";
class ReturnFlightSearch extends Component 
{
    state={
        data:this.props.data,
        showItems:this.props.showItems,
        farerules:false,
       
    }
    setFareRules=(bool)=>{
        let s={...this.state};
        s.farerules=bool;
        this.setState(s);
    }
    setshowItem=(id)=>{
        let s={...this.state};
        let index=s.showItems.findIndex(ele=>ele== id);
        if(index>=0)
        {
            s.showItems.splice(index,1);
        }
        else
        {
            s.showItems.push(id);
        }
        this.setState(s);
    }
   
    setEffect=(e)=>{
        e.currentTarget.className="row mt-1 ml-4 pt-1  onewaybactive"
    }
    removeEffect=(e)=>{
        e.currentTarget.className="row mt-1 ml-4 pt-1  onewaybox"
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let {data}=this.props;
        let find=data.find(ele=>ele.id == input.name);
        console.log(find);
        this.props.setFlight(find);
    }
    render() {
      
        let {data,departFrom,goingTo,flight,showItems}=this.props;
      console.log(data);
      let {farerules}=this.state;

        return (
           <React.Fragment>
               <div class="row  nofthsf mb-4  ml-4">
                   <div class="col-12"> {departFrom} <i class="fa fa-arrow-right"></i> 
                       {goingTo}</div>
                </div>
               {data.map(ele=>{
                    let  {id,logo,name,code,checkin,meal,desDept,desArr,Price,airBus,total,timeDept,timeArr,T1,T2} =ele;
    
                   return (
                    
                   <div className="row mt-1 ml-4 pt-1 onewaybox " onMouseLeave={this.removeEffect}  onMouseEnter={this.setEffect} id="box1">
                       <div className="col fontstyle" id="F41 ">
                          
                           <div className="row  flight-det">
                               <div className="col-lg-2 col-3">
                                   <img src={logo} className="sforowfes"/>
                                </div>
                                <div className="col-lg-1 col-3">
                                    <div className="row sforowfes1" >
                                       {timeDept}
                                    </div>
                                    <div className="row text-muted namestfrf" >{name}</div>
                                </div>
                                
                           
                            <div className="col-lg-2 col-2 text-right sforowfes3" >
                               <hr className="rfbsfs" />
                          
                            </div>

                            <div className="col-lg-1 col-3  text-left" >
                                <div className="row"><span>{timeArr}</span></div>
                                
                            </div>
                            <div className="col-1 pt-1 text-right d-none d-lg-block">
                                <span className="sforowfes5"></span>
                            </div>
                            <div className="col-lg-2 col-6 text-right" id="fs8" >
                                <div className="row"><div className="col">{total}</div></div>
                                <div className="row"><div className="col">Non-Stop</div></div>
                            </div>
                           
                            <div className="col-lg-2 col-3 text-right " id="fs9" >
                                <span>₹{Price}</span>
                            </div>
                            <div className="col-1 text-right">
                            <div class="row">
                                <div class="col text-right">
                                    <input type="radio"
                                     id="F69" name={id}
                                     onChange={this.handleChange}
                                     value={id}
                                      class="ng-pristine ng-valid ng-touched"
                                       checked={id == flight.id}/>
                                </div>
                                </div>
                            </div>
                            </div>
                        
                        <hr className="sforowfes13"/>
                            <div className="row pb-1">
                                <div className="col-7 d-none d-lg-block">
                                    <a className="yt-row-footer" onClick={()=>this.props.setshowItem(id)}>Flight Details
                                    <i className={showItems.findIndex(ele=>ele== id)>=0?"fa fa-chevron-down":"fa fa-chevron-up"}></i></a>
                                </div>
                                <div className="col-5 text-right d-none d-lg-block">
                                    <span id="rect"> eCash</span><span id="rect1">₹ 250</span>
                                </div>
                            </div>
                      {showItems.findIndex(ele=>ele== id)>=0?(

                     
                        <div className="row flight-details" id="F41">
                            <div className="col-7 ">
                                <div className="row  header"> Flight Details </div>
                                <div className="row contents pt-0 pb-0">
                                    <div className="col-3">
                                        <img src={logo} className="sforowfes8"/>
                                    </div>
                                    <div className="col-5 text-left fs-13">
                                        <div className="row">{name}</div>
                                        <div className="row fs-10 font-lightestgrey">{airBus}</div>
                                    </div>
                                    <div className="col-3 text-right">
                                        <img src="https://i.ibb.co/31BTG9K/icons8-food-100.png" className="sforowfes12"/>
                                    </div>
                               </div>
                               <hr/>
                               <div className="row contents pt-1 rfinfosize ">
                                   <div className="col-3 ml-1 sforowfes9" >
                                       <div className="row ml-1">{desDept}</div>
                                       <div className="row ml-1 fs-16 bold"><span>{timeDept}</span></div>
                                       <div className="row ml-1 fs-12">{T1}</div>
                                  </div>
                                  <div className="col-4">
                                      <div className="row fs-12">
                                          <div className="col-9 text-center">Time Taken</div>
                                      </div>
                                      <div className="row">
                                          <div className="col-10">
                                              <hr className="hr-row"/>
                                          </div>
                                          <div className="col-6 pt-2 font-lightestgrey sforowfes10" >
                                              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plane"
                                               className="svg-inline--fa fa-plane fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                  <path fill="currentColor" d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z">
                                                  </path>
                                              </svg>
                                         </div>
                                      </div>
                                 </div>
                                 <div className="col-3 sforowfes9" >
                                     <div className="row">{desArr}</div>
                                     <div className="row fs-16 bold"><span>{timeArr}</span></div>
                                     <div className="row fs-12">{T2}</div>
                                </div>
                                </div>
                                
                                <div className="row amen-details">
                                    <div className="col text-center font-lightestgrey fs-12">Checkin Baggage:
                                        <i className="fa fa-briefcase font-lightestgrey sforowfes10" ></i> 25kg
                                    </div>
                                </div>
                            </div>


                            <div className="col-5 sforowfes11" >
                                {this.props.rf?(
                                      <div className="row pl-1 fsf pt-3 pb-3  bold">
                                      <div onClick={()=>this.setFareRules(false)} className={farerules?"col-6":"col-6 border"}>Fare Summary</div>
                                      <div onClick={()=>this.setFareRules(true)} className={farerules?"col-6 border":"col-6 "}>Fare Rules</div>
                                  </div>
                                ):
                                (
                                    <div className="row pl-1 fsf pt-3 pb-3 bold">
                                    <div className={"col-6 border-bottom"}>Fare Summary</div>
                                    <div className={"col-6"}>Fare Rules</div>
                                </div>

                                )}
                              
                              
                              
                                  <div className="row fs-13 bold mt-1">
                                    <div className="col-4">Fare Summary</div>
                                    <div className="col-4">Base and Fare</div>
                                    <div className="col-4">Fees and Taxes</div>
                                </div><br/>
                                <div className="row fs-11">
                                    <div className="col-4">Adult X 1</div>
                                    <div className="col-4">₹ {Price}</div>
                                    <div className="col-4">₹ 1000</div>
                                </div>
                                <hr/>
                                <div className="row fs-14 bold">
                                    <div className="col-6">You Pay:</div>
                                    <div className="col-6 text-right">₹ {Price}</div>
                                </div>
                                <div className="row fs-12">
                                    <div className="col-12"> Note: Total fare displayed above has been rounded off and may show a slight difference from actual fare. </div>
                                </div>

                                
                            </div>
                        </div>
                         ):""}



                        </div>
                        <br/>
                    </div>
                     
                     )
               })}
           </React.Fragment>
        )
    }
}
export default  ReturnFlightSearch;