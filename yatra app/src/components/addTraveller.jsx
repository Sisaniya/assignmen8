import React,{Component} from "react";
class AddTraveller extends Component
{
    state={}
    render() {
        let {type,handelInc,handelDec,travellers,desc,index}=this.props;
       return (
           <React.Fragment>
             <h6>{type}{desc}</h6>
             <div className="row ml-1 " >
                 <h6 className="col-1 border cps text-center" >
                     <small className="text-muted cps" onClick={()=>handelDec(type,index)}>-</small>
                 </h6>
                 <h6 className="col-1 border bg-light ">
                     <small className="text-muted text-center">{travellers[type]}</small>
                 </h6>
                 <h6 className="col-1 border cps text-center" >
                     <small className="text-muted mx-auto cps" onClick={()=>handelInc(type,index)}>+</small>
                 </h6>
             </div>
            
                
                       

           </React.Fragment>
       )
    }
}
export default AddTraveller;