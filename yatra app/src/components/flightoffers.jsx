import React,{Component} from "react";
import Flight from "./flight";


import NavBar from "./navBar";
class FlightOffers extends Component
{
    state={
        routes:[
        {origin: "New Delhi", dest: "Bengaluru", date: "Wed, 3 Oct", amount: 3590},
        {origin: "New Delhi", dest: "Mumbai", date: "Sun, 13 Oct", amount: 2890},
        {origin: "Hyderabad", dest: "Bengaluru", date: "Mon, 30 Sep", amount: 2150},
        {origin: "Mumbai", dest: "Pune", date: "Sun, 6 Oct", amount: 1850}],
        destinations:[
            {
                img: "https://i.ibb.co/SQ7NSZT/hol1.png",
                place: "Australia",
                price: "177,990",
                days: "9 Nights / 10 Days"
                },
                {
                img: "https://i.ibb.co/Wxj50q1/hol2.png",
                place: "Europe",
                price: "119,990",
                days: "6 Nights / 7 Days"
                },
                {
                img: "https://i.ibb.co/VY3XNZr/hol3.png",
                place: "New Zealand",
                price: "199,990",
                days: "6 Nights / 7 Days"
                },
                {
                img: "https://i.ibb.co/j4NNc35/hol4.jpg",
                place: "Sri Lanka",
                price: "18,999",
                days: "4 Nights / 5 Days"
                },
                {
                img: "https://i.ibb.co/ct6076f/hol5.jpg",
                place: "Kerala",
                price: "12,999",
                days: "4 Nights / 5 Days"
                },
                {
                    img: "https://i.ibb.co/vB0CpYK/hol6.jpg",
                    place: "Char Dham",
                    price: "22,999",
                    days: "4 Nights / 5 Days"
                    }
               
        ],
       
        travelType:"",
       
    }
  
    setTravelType=(str)=>{
        let s={...this.state};
        s.travelType=str;
        s.tclassName="Economy";
       s.travellers={Adult:1,Child:0,Infant:0};
        this.setState(s);
    }
  
    render() {
        let {routes,destinations,travelType}=this.state;
        return (
            <React.Fragment>
              
                 
                       <h6>Flight Discounts for you</h6>
                       <div className="row  bg-light">
                           <div className="col-4">
                               <img src="https://i.ibb.co/qdc2z7Z/ad01.png" alt="" className="hpdis"/>
                            </div>
                            <div className="col-4">
                                <img src="https://i.ibb.co/yp0bbgz/ad02.png" alt="" className="hpdis"/>
                            </div>
                            <div className="col-4">
                                <img src="https://i.ibb.co/DkrVrkY/ad03.png" alt="" className="hpdis1" />
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <img src="https://i.ibb.co/Rc9qLyT/banner1.jpg" alt="" className="m-2 ml-3 hpdis" />
                                </div>
                            </div>
                            <div className="row m-1 mt-1">
                                <div className="col-12 text-muted fontstyle">
                                    <h5>Popular Domestic Flight Routes</h5>
                                </div>
                            </div>
                        </div>
                        <div className="row text14 fontstyle">
                        {routes.map(ele=>{
                            let {origin,dest,date,amount}=ele;
                            return (
                                <div class="col-2 bg-white m-1">
                                    <div class="row">
                                        <div class="col text-dark text-center">
                                            {dest}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col text-muted text-center">{date}</div>
                                    </div><br/>
                                    <div class="row">
                                        <div class="col text-dark text-center">{origin}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col text-muted text-center"> Starting From </div>
                                    </div><div class="row">
                                        <div class="col text-center">
                                            <button class="btn btn-warning" style={{fontSize:"14px"}}>{dest}</button>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            )
                        })}

                        </div>
                        <div class="row ">
                            <div class="col-12 text-muted fontstyle"><h4>Popular Holiday Destinations</h4>
                            </div>
                        </div>
                        <div class="row fontstyle">
                            {destinations.map(ele=>{
                                let {img,place,price,days}=ele;
                                return (
                                    <div class="col-5 bg-white m-1">
                                        <div class="row">
                                            <div class="col-2 mt-3">
                                                <img class="img-fluid" src={img}/></div>
                                            <div class="col-8">
                                                <div class="row">
                                                    <div class="col text-muted text-center">{place}</div>
                                                </div>
                                            <div class="row">
                                                <div class="col text-center">
                                                    <span class="text-danger">
                                                        <strong> Rs.{price}</strong>
                                                    </span>
                                                     <span class="text-dark">per person</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col text-center text-muted">{days}</div>
                                            </div>
                                           </div>
                                           <div class="col-1 mt-3">
                                               <i class="fa fa-arrow-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                
            </React.Fragment>
        )
    }
}
export default FlightOffers;