import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import SearchDetailsNav from "./searchDetailsNav";
import OneWayFlights from "./oneWayFlights.jsx"
import Input from "./input";
import queryString from "query-string";
import ReturnFlightSearch from "./returnFlightSearch";
class SearchFlight extends Component 
{
    state={
        data:[],
        rdata:[],
        showFilters:false,
        goingFlight:{},
        returnFlight:{},
        showItems:[],
    }
    bookRFlight=()=>{
        let {goingFlight,returnFlight}=this.state;
       let bflights=[goingFlight,returnFlight];
       if( returnFlight && goingFlight)
       {
        this.props.setBookFlight(bflights);
        this.props.history.push("/booking");
       }
       
    }
    bookFlight=(flight)=>{
       let bflights=[flight];
        this.props.setBookFlight(bflights);
        this.props.history.push("/booking");

    }
    setGoingFlight=(f)=>{
        let s={...this.state};
        s.goingFlight=f;
        this.setState(s);
    }
    setReturnFlight=(f)=>{
        let s={...this.state};
        s.returnFlight=f;
        this.setState(s);
    }
    setShowFiters=()=>{
        let s={...this.state};
        s.showFilters=s.showFilters?false:true;
        this.setState(s);
    }
    async fetchData() {
        let str=this.props.location.search;
        let {date,rDate,departFrom,goingTo,travellers,rFlight}=this.props.searchdata;
        if(departFrom && goingTo)
        {
            if(rFlight)
            {
                let response=await httpServices.get(`/returnflights/${departFrom}/${goingTo}${str}`);
                let {data}=response;
                console.log(response);
                this.setState({showItems:[],data:data.list1,rdata:data.list2,goingFlight:data.list1[0],returnFlight:data.list2[0]});
            }
            else
            {
                let response=await httpServices.get(`/flights/${departFrom}/${goingTo}${str}`);
                let {data}=response;
                console.log(response);
                this.setState({data:data,showItems:[]});
    
            }
           
        }
        else 
        {
            window.alert("an unexcepted error");
        }
       

    }
     componentDidMount() {
        this.fetchData();
       
    }
    componentDidUpdate(prevProps,prevState) {
        if(prevProps != this.props)
        this.fetchData();
    }
    handelSearch=(options)=>{
        options.page=1;
        let s={...this.state};
        s.showFilters=s.showFilters?false:true;
       
        this.callURL(options);
        this.setState(s);
    }
    clearFilter=()=>{
        let s={...this.state};
        s.showFilters=false;
      
        let options=queryString.parse(this.props.location.search);
        let {sort}=options;
        if(sort)
          this.props.history.push(`/yatra/airSearch?sort=${sort}`);
        else  this.props.history.push(`/yatra/airSearch`);
        this.setState(s);
    }
    setSortFilter=(str)=>{
        let options=queryString.parse(this.props.location.search);
        if(str == options.sort)
        {
            options.sort="";
        }
       else
       {
           options.sort=str;
       }
        this.callURL(options);

    }
    callURL=(options)=>{
        let searchString=this.makeQueryString(options);
        this.props.history.push({
            pathName:"/yatra/airSearch",
            search:searchString,
        });
    }
    makeQueryString=(options)=>{
        let {price,time,flight,airbus,sort}=options;
        let str="";
       str=this.addToString(str,"price",price);
       str=this.addToString(str,"time",time);
       str=this.addToString(str,"flight",flight);
       str=this.addToString(str,"airbus",airbus);
       str=this.addToString(str,"sort",sort);
     

        return str;
    }
    addToString=(str,name,value)=>{
        let s=value?str?`${str}&${name}=${value}`:`${name}=${value}`:str;
        return s;
    }
    setshowItem=(id)=>{
        let s={...this.state};
        let index=s.showItems.findIndex(ele=>ele== id);
        if(index>=0)
        {
            s.showItems.splice(index,1);
        }
        else
        {
            s.showItems.push(id);
        }
        this.setState(s);
    }
    render() {
        let {searchdata}=this.props;
     
        let {data=[],showFilters,goingFlight,returnFlight,rdata=[],showItems}=this.state;
        console.log(data,rdata,showItems);
        let {date,rDate,departFrom,goingTo,travellers,rFlight}=searchdata;
      let options=queryString.parse(this.props.location.search);
      let {price,time,flight,airbus,sort}=options;
           
        return (
            <div className="row bgcolor">
           <div className="container-fluid">
               <SearchDetailsNav searchdata={searchdata}/>
               <div className="mainExpand">
                   {showFilters?(
                        <div class="row pt-2 pb-2 fnavsforf " >
                        <div class="col-1 text-center d-none d-lg-block t14">
                            <i class="fa fa-filter"></i> Filter 
                        </div>
                        <div class="col-lg-8 col-3 text-right" >
                            <button class="btn btn-outline-dark text-secondary" onClick={()=>this.setShowFiters()}><strong>Cancel</strong></button>
                        </div>
                        { (price || time || flight || airbus)?(
                           <div class="col-1" >
                              <button class="btn btn-danger text-white" onClick={()=>this.clearFilter()}><strong>Clear Filters</strong></button>
                         </div>

                       ):""  }
                    </div>




                   ):(
                    <div class="row pt-2 pb-2 fnavsforf" id="id1" >
                    <div class="col-1 text-center d-none d-lg-block t14" >
                        <i class="fa fa-filter"></i> Filter 
                     </div>
                     <div class="col-lg-2 col-3 text-center cps" onClick={()=>this.setShowFiters()}> Price
                      <i class="fa fa-angle-down"></i>
                      </div>
                      <div class="col-lg-2 col-3 text-center cps" onClick={()=>this.setShowFiters()}> Depart
                      <i class="fa fa-angle-down"></i>
                      </div>
                      <div class="col-lg-2 col-3 text-center cps" onClick={()=>this.setShowFiters()}> Airline
                       <i class="fa fa-angle-down"></i>
                       </div>
                       <div class="col-lg-2 col-3 text-center cps" onClick={()=>this.setShowFiters()}> Aircraft
                       <i class="fa fa-angle-down"></i>
                       </div>
                       { (price || time || flight || airbus)?(
                           <div class="col-1" >
                              <button class="btn btn-danger text-white" onClick={()=>this.clearFilter()}><strong>Clear Filters</strong></button>
                         </div>

                       ):""  }
                 </div>
                       
                   )}
                   {showFilters?(
                       
                    <div className="expand ml-3 py-1  fontstyle">
                        <Input options={options} handelSearch={this.handelSearch}/>
                     </div>

                   ):""}
                   
                   

                </div>
               <div className="row mt-3">
                   <div className="col-lg-9 ml-2 col-12">
                   <div className="row ml-4 pt-1 pb-1 mb-4 psfswss" id="id1"> 
                 
                       <div className="col-2 fontstyle t5">Sort By:</div>
                       <div className={"col-lg-2 col-3 text-right psfswss1 "+(sort=="arrival"?"text-primary":"")} onClick={()=>this.setSortFilter("arrival")}>
                           ARRIVE {sort=="arrival"?<i class="fa fa-arrow-up"></i>:""}  </div>
                       <div className={"col-lg-2 col-3 text-right psfswss1 "+(sort=="departure"?"text-primary":"")} onClick={()=>this.setSortFilter("departure")}>
                           DEPART {sort=="departure"?<i class="fa fa-arrow-up"></i>:""}</div>
                       <div className={"col-lg-2 col-3 text-right psfswss1 "+(sort=="price"?"text-primary":"")} onClick={()=>this.setSortFilter("price")}> 
                       PRICE {sort=="price"?<i class="fa fa-arrow-up"></i>:""}</div>
                    </div>

                    {rFlight?(
                        <div className="row">
                            <div className="col-6">
                            <ReturnFlightSearch showItems={showItems} setshowItem={this.setshowItem} flight={goingFlight} setFlight={this.setGoingFlight} rf={true}  departFrom={departFrom} goingTo={goingTo} data={data}/>
                            </div>
                            <div className="col-6">
                            <ReturnFlightSearch showItems={showItems} setshowItem={this.setshowItem}  flight={returnFlight} setFlight={this.setReturnFlight} rf={false} departFrom={goingTo} goingTo={departFrom} data={rdata}/>
                            </div>
                           
                          

                        </div>
                    )
                    :
                      <OneWayFlights showItems={showItems} setshowItem={this.setshowItem} bookFlight={this.bookFlight} data={data}/> }

                   </div>
                   <div className="col-2 d-none d-lg-block">
                   <img className="adsstylefsf" src="https://i.ibb.co/18cngjz/banner-1575268481164-ICICI-Dom-Flight-New-Homepage-SRP-182-X300.png" 
                  />

                   </div>
               </div>
              



               {rFlight?(
               <div className="row fixed-bottom pt-4 pb-4 bottomheaders" >
                   <div className="col-4 d-none d-lg-block">
                       {goingFlight && (
                               <div className="row">
                               <div className="col-3 text-center">
                                   <img src="https://i.ibb.co/g7kySJR/images.png" width="40px" className="bottomheaders1"/>
                                </div>
                                <div className="col-2 d-none d-lg-block bomhrs">{goingFlight.name}</div>
                                <div className="col-2 d-none d-lg-block">{goingFlight.timeDept}</div>
                                <div className="col-3 d-none d-lg-block"><hr className="bomhrs1"/></div>
                                <div className="col-2 d-none d-lg-block">{goingFlight.timeArr}</div>
                            </div>
                       )}
                      
                    </div>
                    <div className="col-4 d-none d-lg-block">
                        {returnFlight && (
                               <div className="row">
                               <div className="col-3 text-center">
                                   <img src="https://i.ibb.co/0FZw9Ps/images.jpg" width="40px" className="bottomheaders1"/>
                               </div>
                               <div className="col-2 bomhrs" >{returnFlight.name}</div>
                               <div className="col-2">{returnFlight.timeDept}</div>
                               <div className="col-3"><hr className="bomhrs1"/></div>
                               <div className="col-2">{returnFlight.timeArr}</div>
                           </div>
                        )}
                       
                    </div>
                    <div className="col-lg-4 col-12">
                        <div className="row">
                            <div className="col-6 ml-lg-0 ml-1">
                                <div className="row bomhrs2 fontstyle">
                                    <div className="col">Total Fare</div>
                                </div>
                                <div className="row bomhrs3 fontstyle">
                                    {goingFlight && returnFlight ? <div className="col"><h3>₹{ goingFlight.Price+returnFlight.Price}</h3></div>:""}
                                   
                                </div>
                            </div>
                            <div className="col-lg-6 col-4 mt-1">
                                <button onClick={()=>this.bookRFlight()} className="btn btn-danger text-white"> Book </button>
                            </div>
                        </div>
                    </div>
                </div>
                ):""}

           </div>
           </div>
        )
    }
}
export default SearchFlight;