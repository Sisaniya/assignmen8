import React,{Component} from "react";
import Flight from "./flight";
import FlightOffers from "./flightoffers";


import NavBar from "./navBar";
class Home extends Component
{
    state={
        routes:[
        {origin: "New Delhi", dest: "Bengaluru", date: "Wed, 3 Oct", amount: 3590},
        {origin: "New Delhi", dest: "Mumbai", date: "Sun, 13 Oct", amount: 2890},
        {origin: "Hyderabad", dest: "Bengaluru", date: "Mon, 30 Sep", amount: 2150},
        {origin: "Mumbai", dest: "Pune", date: "Sun, 6 Oct", amount: 1850}],
        destinations:[
            {
                img: "https://i.ibb.co/SQ7NSZT/hol1.png",
                place: "Australia",
                price: "177,990",
                days: "9 Nights / 10 Days"
                },
                {
                img: "https://i.ibb.co/Wxj50q1/hol2.png",
                place: "Europe",
                price: "119,990",
                days: "6 Nights / 7 Days"
                },
                {
                img: "https://i.ibb.co/VY3XNZr/hol3.png",
                place: "New Zealand",
                price: "199,990",
                days: "6 Nights / 7 Days"
                },
                {
                img: "https://i.ibb.co/j4NNc35/hol4.jpg",
                place: "Sri Lanka",
                price: "18,999",
                days: "4 Nights / 5 Days"
                },
                {
                img: "https://i.ibb.co/ct6076f/hol5.jpg",
                place: "Kerala",
                price: "12,999",
                days: "4 Nights / 5 Days"
                },
                {
                    img: "https://i.ibb.co/vB0CpYK/hol6.jpg",
                    place: "Char Dham",
                    price: "22,999",
                    days: "4 Nights / 5 Days"
                    }
               
        ],
       
        travelType:"Flights",
       
    }
    setFlightData=(data)=>{
          this.props.setFlightData(data);
          let str=JSON.stringify(data);
          localStorage.setItem("searchData",str);
          this.props.history.push("/yatra/airSearch");
    }
    setTravelType=(str)=>{
        let s={...this.state};
        s.travelType=str;
        s.tclassName="Economy";
       s.travellers={Adult:1,Child:0,Infant:0};
        this.setState(s);
    }
  
    render() {
        let {routes,destinations,travelType}=this.state;
        return (
            <React.Fragment>
               <div className="row bgcolor">
                   <div className="col-12">
              
               <div className="row mt-4 ml-1  ">
                   <div className="col-lg-5 ">
                       <div className="px-3 smooth-banner-transition">
                       <div className="row mt-3 mb-3">
                           <div className="col-3">
                               <span className={"fa-stack fa-2x "+(travelType=="Flights"?"iccolor":"icolor")} onClick={()=>this.setTravelType("Flights")} >
                                   <i className="fa fa-circle fa-stack-2x"></i>
                                   <i class="fas fa-plane fa-stack-1x fa-inverse"></i>
                               </span>
                            </div>
                            <div className="col-3">
                                <span className={"fa-stack fa-2x "+(travelType=="Hotels"?"iccolor":"icolor")} onClick={()=>this.setTravelType("Hotels")}>
                                    <i className="fa fa-circle  fa-stack-2x"></i>
                                    <i className="fa fa-bed fa-stack-1x fa-inverse"></i>
                                </span>
                            </div>
                            <div className="col-3">
                                <span className={"fa-stack fa-2x "+(travelType=="Buses"?"iccolor":"icolor")} onClick={()=>this.setTravelType("Buses")}>
                                    <i className="fa fa-circle  fa-stack-2x"></i>
                                    <i className="fa fa-bus fa-stack-1x fa-inverse"></i>
                                </span>
                            </div>
                            <div className="col-3">
                                <span className={"fa-stack fa-2x "+(travelType=="Taxis"?"iccolor":"icolor")} onClick={()=>this.setTravelType("Taxis")}>
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-taxi fa-stack-1x fa-inverse"></i>
                            </span>
                            </div>
                            </div>



                            <div class="row">
                                  <div class="col text-center text-dark"><h5>{travelType}</h5>
                             </div>
                            </div>
                            {travelType=="Flights"?<Flight setFlightData={this.setFlightData}/>:""}


                       </div>
                       


                      
                      
                   </div>
                   <div className="col-lg-7 pl-2 ">
                       <div className="row bg-light mrright " >
                           <div className="col-12">
                           <FlightOffers/>
                           </div>
                       </div>
                       
                   </div>
               </div>
               </div>
               </div>
            </React.Fragment>
        )
    }
}
export default Home;