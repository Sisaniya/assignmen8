import React,{Component} from "react";
class Footer extends Component
{
    state={}
    render() {
        return (
            <React.Fragment>
                 <div className="row icons">
                     <div className="col-4"></div>
                     <div className="col-4">
                         <span className="icon-footer">
                             <i className="fab fa-facebook-f icon-f"></i>
                        </span>
                        <span className="icon-footer">
                            <i className="fab fa-twitter icon-t"></i>
                        </span>
                        <span className="icon-footer">
                            <i className="fab fa-youtube icon-y"></i>
                        </span>
                    </div>
                    <div className="col-4"></div>
                </div>
                 <div className="row">
                     <div className="col text-center copyright">
                         <p>Copyright © 2021 Yatra Online Private Limited, India. All rights reserved</p>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Footer;