import React,{Component} from "react";
class Input extends Component
{
    state={
     
       priceList:["0-5000","5000-10000","10000-15000","15000-20000"],
       timeList:["0-6","6-12","12-18","18-00"],
       airLineList:["Go Air","Indigo","SpiceJet","Air India"],
       airCraftList:["Airbus A320 Neo","AirbusA320","Boeing737","Airbus A320-100"],
        
        }
  
getRadio=(arr,value,name,label)=>{
          
 
return (
  <React.Fragment>
     <div className="row ml-1">
         <span>{label}</span>

     </div>
  
   
{arr.map(ele=>{
    return (
       
        <div className="form-check fsfs" key={ele}>
        <input className="form-check-input"
        type="radio"
         name={name}
         value={ele}
         checked={value == ele}
         onChange={this.handelChange}
         
        />
       <label className="form-check-label">{ele}</label>
       </div>
       
    )
})}
    
    </React.Fragment>
)
}

getDD=(arr,value,name,top)=>{
    
    return (
        <div className="form-group">
            <label className="font-weight-bold d-block bg-light border form-label p-1">Order By</label>
            <select className="form-control"
            name={name}
            value={value}
            onChange={this.handelChange}
            >
                <option value="">{top}</option>
                {arr.map(ele=><option>{ele}</option>)}
            </select>
        </div>
    )
}
handelChange=(e)=>{
   let {currentTarget:input}=e;
   let s={...this.props};
input.type=="checkbox"?s.options[input.name]=this.getCBs(s.options[input.name],input.checked,input.value)
:s.options[input.name]=input.value;


this.props.handelSearch(s.options);


}
getCB=(arr,value,name,label,)=>{
      
  let values=value?value.split(","):[];
return (
<React.Fragment>
<div className="row ml-1">
         <span>{label}</span>

     </div>
{arr.map(ele=>{
  return (
     
      <div className="form-check  fsfs" key={ele}>
      <input className="form-check-input"
      type="checkbox"
       name={name}
       value={ele}
       checked={values.findIndex(b=>b==ele)>=0}
       onChange={this.handelChange}
       
      />
     <label className="form-check-label">{ele}</label>
     </div>
     
  )
})}
  
  </React.Fragment>
)
}
getCBs=(str,checked,value)=>{

  let arr=str?str.split(","):[];
  if(checked)
  {
      arr.push(value);

  }
  else
  {
      let index=arr.findIndex(ele=>ele==value);
      if(index>=0)
         {
             arr.splice(index,1);
         }

  }
  return arr.join(",");
}
    render() {
        let {priceList,airCraftList,airLineList,timeList}=this.state;
        let {price,time,flight,airbus}=this.props.options;
      
        return (
           <div className="row ">
               <div className="col-lg-3 ml-1 col-3">
                   {this.getRadio(priceList,price,"price","Price")}

               </div>
               <div className="col-lg-3 ml-1 col-3">
                   {this.getRadio(timeList,time,"time","Time")}

               </div>
               <div className="col-lg-3 ml-1 col-3">
                   {this.getCB(airLineList,flight,"flight","AirLine")}

               </div>
               <div className="col-lg-2 ml-1 col-3">
                   {this.getCB(airCraftList,airbus,"airbus","AirCraft")}

               </div>
               
              
           
                   
                   
           </div>
        )
    }
}
export default Input;