import React,{Component} from "react";
import {Switch,Route,Link} from "react-router-dom"

class NavBar extends Component
{
    state={}
    render() {
        let {uinfo}=this.props;
       return (
           <React.Fragment>
             <div className="row  bgcolor">
             <div className="col-12 ">
                  <div className="row header-container ml-3 mrright  pt-3 pb-0 pl-0 " >
                      <div className="col-1 pl-0 ">
                               <Link to="/yatra" className="cps ">
                               <img src="https://www.yatra.com/content/fresco/beetle/images/newIcons/yatra_logo.svg" alt="" 
                                 className="yatra-header"/>

                                </Link>
             
                      </div>
                 
                 <div className="col-3">
                     <div className="row">
                     <div className="col-3  icon-header">
                     <div className="row ">
                         <div className="col-12 text-center">
                             <i className="fas fa-plane"></i>
                        </div>
                   
                         <div className="col-12 text-center ">Flights</div>
                     </div>
                     </div>
                     <div className="col-3  icon-header">
                     <div className="row ">
                         <div className="col-12 text-center">
                             <i className="fa fa-bed"></i>
                         </div>
                 
                    <div className="col-12 text-center ">Hotels</div>
                    </div>
                </div>
                <div className="col-3  icon-header">
                    <div className="row ">
                        <div className="col-12 text-center">
                            <i className="fa fa-bus"></i>
                        </div>
                  
                        <div className="col-12 text-center ">Buses</div>
                    </div>
                </div>
                <div className="col-3  icon-header">
                    <div className="row ">
                        <div className="col-12 text-center">
                            <i className="fa fa-taxi"></i>
                        </div>
                   
                        <div className="col-12 text-center ">Cabs</div>
                    </div>
                </div>
                

                     </div>
                 </div>
              <div className="col-2"></div>
              <div className="col-6">
                  <div className="row">
                <div className="mt-1 col-3">
                    {uinfo?(
                        <div className="dropdownu">
                            <div className="dropbtn1"> Hi {uinfo.name}<i className="fa fa-chevron-down sformyacsn" id="onhover" ></i></div>
                            <div className="dropdown-contentu">
                               
                                    <Link to="/yatra/allbookings" className="dropitme">
                                        <i className="fa fa-chevron-left sformyacsn1 mr-1" id="onhover" ></i>
                                         My Bookings</Link>
                              
                                    <Link to="/yatra/eCash" className="dropitme"><i className="fa fa-chevron-left mr-1 sformyacsn1" id="onhover" ></i> 
                                    My eCash</Link>
                               
                                    <Link to="/yatra/profile" className="dropitme"><i className="fa fa-chevron-left mr-1 sformyacsn1" id="onhover" ></i>
                                     My Profile</Link>
                                
                                <Link className="ml-2 dropitme" to="/yatra/logout">Logout</Link>
                                
                            </div>
                        </div>
                    ):(
                         <div className="dropdown">
                         <div className="dropbtn1"> 
                         My Account<i className="fa fa-chevron-down nbddhs" id="onhover" ></i>
                         </div>
                         <div className="dropdown-content">
                             <div className="row mt-2">
                                 <div className="col-3 ml-2">
                                     <i className="fa fa-user-circle nbddhis" ></i>
                                 </div>
                                 <div className="ml-2">
                                     <span className="yt-header-drop-down-text">My Bookings</span>
                                     <div><span className="yt-header-drop-down-text">My eCash</span></div>
                                 </div>
                             </div>
                             <div className="row mt-2 mb-4">
                                 <div className="col-5">
                                     <Link to="/yatra/login">
                                     <button type="button"  className="yt-header-drop-down-text pgLogIn">Login</button>
                                     </Link>
                                    
                                </div>
                                <div className="col-5">
                                <Link to="/yatra/login">
                                    <button type="button" className="yt-header-drop-down-text pgSignUp">Sign Up</button>
                                    </Link>
                                </div>
                             </div>
                         </div>
                     </div>
               

                    )}
                     </div>
                         
                         
                         <div className="mt-1  col-3">
                              <div className="dropdown">
                              <div className="dropbtn1">
                             Support<i className="fa fa-chevron-down nbddhs" id="onhover" ></i>
                               </div>
                               </div>
                         </div>
                         <div className="mt-1  col-3">
                               <div className="dropdown">
                                <div className="dropbtn1"> Recent Search<i className="fa fa-chevron-down nbddhs" id="onhover" ></i></div>
                               <div className="dropdown-content">
                             <div className="col-12 mt-2">Things you view while searching are saved here.</div>
                                </div>
                          </div>
                          </div>
                          <div className="mt-3   col-3">
                              <spna className="ml-3"> Offers</spna>
                              
                           </div>
                        
                        
                        
                        
                           </div>
                     </div>
              
               
               
               
            </div>
            </div></div>
           </React.Fragment>
       )
    }
}
export default NavBar;