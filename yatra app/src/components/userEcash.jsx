import React,{Component} from "react";
import {Link} from "react-router-dom";
import Footer from "./footer";
import UserNavBar from "./userNavbar";
import axios from "axios";
class UserEcash extends Component
{
    state={
        data:[],
    }
    goToLink=(str)=>{
        this.props.history.push(str);
    }
    async componentDidMount() {
        try
        {
            let user=localStorage.getItem("user");
            console.log(user);
            let response=await axios.get("https://evening-brook-89374.herokuapp.com/eCash",{headers: {Authorization: user}});
            let {data}=response;
            console.log(data);
            this.setState({data:data});
        }
        catch(ex)
        {
            
            console.log(ex.response.data);
        }
      
      
         
    }
    showEcash=()=>{
         let {data=[]}=this.state;
        return (
         <React.Fragment>
                <div className="row  bg-light">
                    <div className="col-4 mt-2 mb-2">VIEW ECASH SUMMARY</div>
                    <div className="col-4"></div>
                    <div className="mt-2 mb-2 col-4">CURRENT BALANCE ₹.{data.reduce((acc,curr)=>acc+(+curr.ecash),0)}</div>
                </div>
                <div className="row mt-3 mb-3 border ml-1 mr-1">
                    <div className="col-3 bg-light text-center">
                        <div className="row mt-4 mb-4 ml-1 mr-1 border fecashs">
                            <div className="text-danger mt-2 ml-4">REDEEMABLE</div>
                                <br/>
                            <div className="ml-5"><b>₹0</b>
                            </div>
                            <hr className="col-7"/>
                      </div>
                       <div className="row mt-4 mb-4 ml-1 mr-1 border fecashs" >
                        <div className="text-danger mt-2 ml-4">PENDING</div>
                        <br/>
                        <div className="ml-5"><b>₹0</b></div>
                        <hr className="col-7"/>
                      </div>
                  </div>
                  <div className="col-9">
                      <div className="row mt-3">
                          <div className="col-3">Transferable
                          <i className="fa fa-info-circle"></i>
                          </div>
                          <div className="col-3">
                              <button className="btn btn-danger">Transfer</button>
                          </div>
                          <div className="col-3">Convertible
                          <i className="fa fa-info-circle"></i>
                          </div>
                          <div className="col-3">
                              <button className="btn mr-1">Convert</button>
                         </div>
                     </div>
                     <div className="row">
                         <div className="col-3">₹0</div>
                         <div className="col-3"></div>
                         <div className="col-3">₹0</div>
                    </div><hr/>
                    <div className="row mt-2 mb-2 mr-1">
                        <div className="col-2">Date</div>
                        <div className="col-3">Description</div>
                        <div className="col-4">Reference Number</div>
                        <div className="col-2">Credit</div>
                        <div className="col-1">Debit</div>
                    </div><hr/>
                    {data.map(ele=>{
                        return (
                            <React.Fragment>
                        <div className="row mt-2 mb-2 mr-1 text-secondary">
                            <div className="col-2">{ele.date.split(",").join(" ")}</div>
                            <div className="col-3">{ele.des}</div>
                            <div className="col-4 text-truncate">{ele.ref}</div>
                            <div className="col-2">{ele.ecash}</div>
                            <div className="col-1">₹0</div>
                        </div><hr/><hr/>
                        </React.Fragment>

                        )
                    })}
                   
                </div>
            </div>
            </React.Fragment>
        )
    }
    render() {
        let {fullname,email,mobileNo}=this.props.uinfo;
        return (
            <div className="container-fluid bgcolor">
                <div className="row">
                     <div className="col-lg-9 col-12">
                         <div class="row mt-3 mb-2">
                             <div class="col-9 ml-1">Dashboard /
                             <Link to="/yatra/eCash"> Your eCash</Link> 
                             </div>
                        </div>
                        <div className="row  ml-1" id="payment">
                        <div className="col-lg-3  unavstyle" >
                            <UserNavBar str="/yatra/eCash" goToLink={this.goToLink}/>
                        </div>
                        <div className="col-lg-9 fontstyle">
                        {this.showEcash()}

                        </div>
                      


                        </div>
                     </div>



                     <div className="col-lg-2 col-12">
                         <div className="row ml-1 fontstyle sectionpf">
                             <div className="col p-0 m-0">
                                 <div className="row">
                                     <div className="col-12 text-center uprosps">
                                         <i className="fa fa-user-circle"></i>
                                     </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {fullname}
                                    </div>
                                </div>
                                <div className="row">
                                    <hr className="col-9 text-center"/>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {email}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        Phone: {mobileNo}
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>


                </div>
                <Footer/>
            </div>
        )
    }
}
export default UserEcash;