import React,{Component} from "react";
import {Link} from "react-router-dom";
import FlightDetails from "./bookingcomp/flightDetails";
import InputTDetails from "./bookingcomp/inputTDetails";
import ProtectionComp from "./bookingcomp/protectionComp";
class Booking extends Component 
{
    state={
        cancellation:false,
        insurance:false,
        cDetails:{email:"",mobileNo:"",travellersInfo:{adult:[],Child:[],Infant:[]}},
       error:{},
        codeList:[{name:"NEWPAY",value:400,txt:"Pay with PayPal to save upto Rs.1400 on Domestic flights (Max. discount Rs. 600 + 50% cashback up to Rs. 800)."},
      {name:"YTAMZ19",value:1000,txt:"Save up to Rs.2,000 (Flat 6% (max Rs. 1,000) instant OFF + Flat 5% (max Rs. 1,000) cashback)."},
    {name:"YATAIR",value:900,txt:"Flat 9% OFF. Valid with all cards and wallets."},
      {name:"FREEFLY",value:1500,txt:"Up to 3700 instant off, applicable on all payment modes."},
       {name:"FLYFESTIVE",value:400,txt:"Flat Rs.400 OFF per pax (up to Rs 2,400)."},
        {name:"YTPZ",value:1000,txt:"Flat 10% Instant OFF upto Rs 1000 on using PAYZAPP Wallet."},
        {name:"FLASHUPI",value:515,txt:"Flat Rs.515 OFF per pax. Valid only with UPI Payment modes."}],
        promoCode:"",
        promoPrice:0,
        totalPay:0,
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let error={};
        let s={...this.state};
        error=this.validateForm(s.cDetails);
        if(this.isValid(error))
        {
            let {cancellation,promoPrice,insurance}=this.state;
            let {completeD,uinfo}=this.props;
            let {bflights,searchFlight}=completeD;
        let {travellers}=searchFlight;
        let tp=bflights[0].Price+(searchFlight.rFlight?bflights[1].Price:0);
       let noft=travellers.Adult+travellers.Child+travellers.Infant;
       let totalPay=(tp*noft)+622;
       
       
         totalPay=cancellation? totalPay+747 : totalPay;
         let tprotect=insurance?269*noft:0;
         totalPay=totalPay+tprotect;
         totalPay=totalPay-promoPrice;
            let obj={cancellation:s.cancellation,insurance:s.insurance,cDetails:s.cDetails,promoCode:s.promoCode,totalPay};
            this.props.setBookingDetails(obj);
           this.props.history.push("/payment");
        }
        else
        {
            s.error=error;
            this.setState(s);
        }
    }
    isValid=(error)=>{
        let keys=Object.keys(error);
        let count=keys.reduce((acc,curr)=>error[curr]?acc+1:acc,0);
        return count == 0;
    }
    validateForm=(cDetails)=>{
        let error={};
        let {travellersInfo,email,mobileNo}=cDetails;
        let {Adult,Child,Infant}=travellersInfo;
        Adult.forEach(ele=>{
            if(!ele.lname && !ele.fname)
                error.travellersInfo="Please fill all the travler details";
            
        });
        Child.forEach(ele=>{
            if(!ele.lname && !ele.fname)
                error.travellersInfo="Please fill all the travler details";
            
        });
        Infant.forEach(ele=>{
            if(!ele.lname && !ele.fname)
                error.travellersInfo="Please fill all the travler details";
            
        });
        error.email=!cDetails.email?"Email is required":cDetails.email.indexOf("@")>=0 ?"":"Enter a valid email";
        let mobile=cDetails.mobileNo+"";
        error.mobileNo=!mobile?"Mobile No is required":!(mobile.length == 10) ?"Mobile No must be 10 Digits":"";
        return error;
    }
    handleDChange=(e)=>{
     
        let {currentTarget:input}=e;
      
       let s={...this.state};
       if(input.title=="travellers")
       {
           let travellersInfo=s.cDetails.travellersInfo;
           let type=travellersInfo[input.name];
           let td=type[input.id];
          
           if(input.placeholder=="Last Name" )
             td.lname=input.value;
           if(input.placeholder == "First Name")
             td.fname=input.value;
            type.splice(input.id,1,td);
            travellersInfo[input.name]=type;
            s.cDetails.travellersInfo=travellersInfo;
       }
       else
       {
        s.cDetails[input.name]=input.value;
       }
    
       if(input.name == "email")
       {
        s.error.email=!s.cDetails.email?"Email is required": s.cDetails.email.indexOf("@")>=0 ?"":"Enter a valid email";
       }
       if(input.name == "mobileNo")
       {
           let mobile=s.cDetails.mobileNo+"";
        s.error.mobileNo=!mobile?"Mobile No is required":!(mobile.length == 10) ?"Mobile No must be 10 Digits":"";
       }
      
       this.setState(s);
    }
    createFullArray=(n)=>{
      let arr=[];
        for(let i = 1; i <= n; i++) {
            arr.push({fname:"",lname:""});
          }
          return arr;
    }
    componentDidMount() {
        let {completeD,uinfo}=this.props;
        let s={...this.state};
        let {travellers}=completeD.searchFlight;
       
       let Adult=this.createFullArray(travellers.Adult);
       let Child=this.createFullArray(travellers.Child);
       let Infant=this.createFullArray(travellers.Infant);
        let obj={email:"",mobileNo:"",travellersInfo:{Adult:Adult,Child:Child,Infant:Infant}};
        console.log(obj);
        if(uinfo)
        {
            console.log(uinfo);
           obj.email=uinfo.email;
           obj.mobileNo=uinfo.mobileNo;
        }
       
        s.cDetails=obj;
      this.setState(s);
    }
    handlePChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
       s.promoCode=input.id;
       s.promoPrice=input.value;
        this.setState(s);

    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
       let s={...this.state};
       s[input.name]=input.checked;
       this.setState(s);
    }
   
    render() {
        let {completeD,uinfo}=this.props;
        let {cancellation,insurance,cDetails,codeList,promoCode,error,promoPrice}=this.state;
        let {bflights,searchFlight}=completeD;
        let {travellers}=searchFlight;
        let tp=bflights[0].Price+(searchFlight.rFlight?bflights[1].Price:0);
       let noft=travellers.Adult+travellers.Child+travellers.Infant;
        let totalPay=(tp*noft)+622;
       
         totalPay=cancellation? totalPay+747 : totalPay;
         let tprotect=insurance?269*noft:0;
         totalPay=totalPay+tprotect;
         totalPay=totalPay-promoPrice;
        return (
            <div className="row bgcolor">
                <div className="col-12">
            <div className="container-fluid body-payment">
                <div className="row mt-4 ml-1">
                    <div className="col-lg-9 col-12 body-payment">
                    <div className="row mt-2">
                        <div className="col-1 text-left box-title">
                            <i className="fa fa-search"></i>
                        </div>
                        <div class="col-lg-8 col-10 text-left box-title"> Review your Bookings</div>
                    </div>
                     <FlightDetails completeD={completeD}/>
                     <div className="row bg-white mt-3 " id="box2">
                         <div className="col-12">
                             <div className="row bg-white">
                                 <div className="col-1 text-right"></div>
                             </div>
                             <div className="row bg-white">
                                 <div className="col-lg-1 col-2 text-right pt-2 ml-2">
                                     <input className="form-check-input "
                                      name="cancellation"
                                      checked={cancellation}
                                      onChange={this.handleChange}
                                       type="checkbox" id="cancellation"/>
                                 </div>
                                 <div className="col-1">
                                     <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plane-departure"
                                      className="svg-inline--fa fa-plane-departure fa-w-20 sforcans1" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" >
                                          <path fill="currentColor" d="M624 448H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h608c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM80.55 341.27c6.28 6.84 15.1 10.72 24.33 10.71l130.54-.18a65.62 65.62 0 0 0 29.64-7.12l290.96-147.65c26.74-13.57 50.71-32.94 67.02-58.31 18.31-28.48 20.3-49.09 13.07-63.65-7.21-14.57-24.74-25.27-58.25-27.45-29.85-1.94-59.54 5.92-86.28 19.48l-98.51 49.99-218.7-82.06a17.799 17.799 0 0 0-18-1.11L90.62 67.29c-10.67 5.41-13.25 19.65-5.17 28.53l156.22 98.1-103.21 52.38-72.35-36.47a17.804 17.804 0 0 0-16.07.02L9.91 230.22c-10.44 5.3-13.19 19.12-5.57 28.08l76.21 82.97z">
                                              </path>
                                      </svg>
                                </div>
                             </div>
                             <div className="row text-success cbsfb" id="fs10">
                                 <div className="col sforcans3 mb-0 mx-3 h5">Cancellation Policy</div>
                                 
                            </div>
                            <div className="row ">
                                
                                 <div className="col  mx-3 pt-0 mt-0 " id="fs14">Zero cancellation fee for your tickets when you cancel. Pay additional Rs.747</div>
                            </div>
                            
                            <div className="row bg-white">
                                <div className="col text-center mx-3  sforcans" id="fs14" > 
                                Travel Smart: Get additional refund of Rs.3,051 in case of cancellation. Terms Conditions 
                            </div>
                            </div>
                          </div>
                      </div>



                      <div className="row mt-2">
                          <div className="col-1 text-right box-title">
                              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-edit"
                               className="svg-inline--fa fa-user-edit fa-w-20 sforcans4" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" >
                                   <path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h274.9c-2.4-6.8-3.4-14-2.6-21.3l6.8-60.9 1.2-11.1 7.9-7.9 77.3-77.3c-24.5-27.7-60-45.5-99.9-45.5zm45.3 145.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9-71.7-71.7-137.9 137.8zM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-37.8 37.8-4.1 4.1 71.8 71.7 41.8-41.8c9.3-9.4 9.3-24.5 0-33.9z">
                                    </path>
                              </svg>
                          </div>
                          <div className="col-11 box-title">  Enter Traveller Details |
                           <span className="fs-18">Sign in to book faster and use eCash </span>
                           </div>
                      </div>

                      {<InputTDetails cDetails={cDetails} uinfo={uinfo} error={error}  handleDChange={this.handleDChange}/>}
                      {<ProtectionComp insurance={insurance} handleChange={this.handleChange}/>}
                  

                    </div>






                   <div className="col-lg-3 col-12">
                        <div className="row mt-2 fs-md">
                            <div className="col"> Fare Details </div>
                        </div>


                        <div class="row mt-2 bg-white" id="box3">
                            <div class="col-12">
                                <div class="row bg-white">
                                    <div class="col-8 text-left" id="fs11"> Base Fare(1 Traveller) </div>
                                    <div class="col-4 text-right" id="fs11"> ₹ {tp}</div>
                                </div>
                                <div class="row bg-white">
                                    <div class="col-8 text-left" id="fs11"> Fees & Surcharge </div>
                                    <div class="col-4 text-right" id="fs11"> ₹ 622</div>
                                </div>
                                <div class="row bg-white">
                                    <div class="col-12 text-center"><hr/></div>
                                </div>
                                <div class="row bg-white">
                                    <div class="col-6 text-left" id="fs12"> Total Fare </div>
                                    <div class="col-6 text-right" id="fs12"> ₹ {totalPay}</div>
                                </div>
                                {(insurance || cancellation) ? (
                                     <div class="row bg-white">
                                     <div class="col-6 text-left" id="fs12"> Add Ons({(insurance?1:0)+(cancellation?1:0)}) </div>
                                     <div class="col-6 text-right" id="fs12"></div>
                                 </div>

                                ):""}
                               
                            </div>
                        </div>
                        <div class="row" id="box4">
                            <div class="col-7 text-left" id="fs13">You Pay</div>
                            <div class="col-5 text-right" id="fs13">₹ {totalPay}</div>
                        </div>
                        <div class="row fontstyle"><div class="col">Promo</div></div>


                        <div className="row bg-white ml-1 fontstyle" id="box3">
                            <div className="col-12">
                                <div className="row">
                                    <div className="col-12">
                                        Select a Promo Code
                                    </div>
                                    {promoCode?(
                                        <div className="row">
                                        <div className="col-1">
                                            <i className="fas fa-check-circle promo-applied-icon"></i>
                                        </div>
                                    <div className="col-10">
                                        <span className="fs-sm">Promo applied successfully. You got a discount of Rs. {promoPrice}.</span>
                                    </div>
                                </div>

                                    ):""}
                                    
                                </div>
                                <div className="row">
                                    {codeList.map(ele=>{
                                        return (
                                    <div className="col-12">
                                        <div className="form-check ml-3">
                                            <input type="radio"
                                               className="form-check-input"
                                               value={ele.value}
                                               id={ele.name}
                                               onChange={this.handlePChange}
                                               checked={promoCode == ele.name}
                                               name="promoCode"/>
                                               <label class="form-check-label" id="promobox">
                                                   <span className="bsforpc">
                                                     {ele.name}
                                                    </span>
                                                </label>
                                                <div class="row">
                                                    <div class="col-10" className="bsforpc1">
                                                        {ele.txt}
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                        )
                                    })}
                                    
                                </div>
                            </div>
                               
                        </div>


                   </div>









                </div>
                <div className="row mt-3">
                    <div className="col-12 text-center">
                        <button onClick={this.handleSubmit} className="btn btn-danger text-white ppfpsb "
                         type="submit" >Proceed to Payment
                         </button>
                  </div>
                </div>
            </div>
            </div>
            </div>
        )
    }
}
export default Booking;