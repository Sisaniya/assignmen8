import React,{Component} from "react";
import {Link} from "react-router-dom";
class UserNavBar extends Component
{
    state={}
    render() {
        let {str,goToLink}=this.props;
        return (
             <React.Fragment>
               
                    <div className={str=="/yatra/allbookings"?"row-tab-selected row":"row-tab row"} onClick={()=>goToLink("/yatra/allbookings")}>
                        <div className= "col mt-3 mb-3 ml-3"> 
                        <i className="fa fa-plane unavfont" ></i>
                         <span className="fs-sm">ALL BOOKINGS</span>
                         </div>
                    </div>
               
                <div className="row-tab row">
                    <div className="col mt-3 mb-3 ml-3">
                         <i className="fas fa-edit unavfont" ></i>
                         MODIFY BOOKINGS
                    </div>
                </div>
                <div className="row-tab row">
                    <div className="mt-3 mb-3 ml-3 col"> 
                    <i className="fas fa-ticket-alt unavfont" ></i>
                    TICKETS/VOUCHERS
                    </div>
                </div>
                <div className="row-tab row">
                    <div className="col mt-3 mb-3 ml-3"> 
                    <i className="fa fa-rupee-sign unavfont" ></i>
                     CLAIM REFUND
                     </div>
                </div>
                <div className="row-tab row">
                    <div className="col mt-3 mb-3 ml-3">
                         <i className="fas fa-file-alt unavfont" ></i>
                          FLIGHT REFUND STATUS
                    </div>
                </div>
             
                <div className={str=="/yatra/eCash"?"row-tab-selected row":"row-tab row"} onClick={()=>goToLink("/yatra/eCash")}>
                    <div className="col mt-3 mb-3 ml-3">
                         <i className="fas fa-wallet unavfont" ></i>
                         ECASH<i className={str=="/yatra/eCash"?"fa fa-chevron-down unavfont1":"fa fa-chevron-up unavfont1"}  id="onhover" ></i>
                    </div>
                </div>
              {str=="/yatra/eCash"?(
                  <React.Fragment>
                        <div class="border bg-light cps row ecashitem" >
                    <div class="mt-3 mb-3 ml-3 col " id="abc"> 
                    <i class="fa fa-chevron-left unavfont2" id="onhover" ></i>
                     Summary
                     </div>
                </div>
                <div class="border bg-light cps row ecashitem" >
                    <div class="mt-3 mb-3 ml-3 col " id="abc"> 
                    <i class="fa fa-chevron-left unavfont2" id="onhover" ></i>
                     Transfer
                     </div>
                </div>
                <div class="border bg-light cps row ecashitem" >
                    <div class="mt-3 mb-3 ml-3 col " id="abc"> 
                    <i class="fa fa-chevron-left unavfont2" id="onhover" ></i>
                     Convert To Coupons
                     </div>
                </div>
                <div class="border bg-light cps row ecashitem" >
                    <div class="mt-3 mb-3 ml-3 col " id="abc"> 
                    <i class="fa fa-chevron-left unavfont2" id="onhover" ></i>
                     My Coupons
                     </div>
                </div>



                    </React.Fragment>
              ):""}
                
              
                <div className={str=="/yatra/profile"?"row-tab-selected row":"row-tab row"} onClick={()=>goToLink("/yatra/profile")}>
                    <div className="col mt-3 mb-3 ml-3"> 
                    <i className="fa fa-user unavfont" ></i>
                     YOUR PROFILE
                     </div>
                </div>
                <div className="row-tab row">
                    <div className="col mt-3 mb-3 ml-3"> 
                    <i className="fas fa-pen unavfont" ></i>
                     YOUR COMMUNICATION
                     </div>
                </div>
                </React.Fragment>
        )
    }
}
export default UserNavBar;