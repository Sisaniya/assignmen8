import React,{Component} from "react";
import httpServices from "../services/httpServices";
class Login extends Component
{

    state={
        user:{email:"",password:""},
        show:0,
        error:"",
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s.user[input.name]=input.value;
        s.error="";
        this.setState(s);
    }
    goToPassword=()=>{
        let s={...this.state};
        if(s.user.email)
        {
            s.show=1;
        }
        else
        {
            s.error="Please enter your Email Id / Mobile Number";
        }
        this.setState(s);
    }
    async login(user) {
        try {
            let {data:response,headers}=await httpServices.post("/login",user);
            let token=headers["x-auth-token"];
            localStorage.setItem("user",token);
            let str=JSON.stringify(response);
          localStorage.setItem("uinfo",str);
            window.location="/yatra";

        }
        catch(ex) {
            if(ex.response && ex.response.status==400)
            {
                let error="Invalid Email and Password!";
                this.setState({error:error});
            }
        }
       
    }
    goToLoginAgain=()=>{
        window.location="/yatra/login";
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s={...this.state};
        if(s.user.password)
        {
             this.login(s.user);
        }
        else
        {
            s.error="Please enter your password";
            this.setState(s);
        }
    }
    render() {
        let {user,show,error}=this.state;
        return (
           <div className="row bgcolor">
               <div className="col-12">
               <div className="row mt-4">
                   <div className="col-12 text-center fontstyle">
                       <h2>Welcome to Yatra!</h2>
                       {show?<div>Please enter your password to login</div>:
                       <div>Please Login/Register using your Email/Mobile to continue</div>}
                     
                    </div>
              </div>
              <div className="row mt-3">
                  <div className="col-3"></div>
                  <div className="col-6 fontstyle">
                      <div className="row bg-light">
                          {show?(
                               <div className="col-6 text-center mt-4">
                                  <i className="fa fa-user-circle lpsflog" ></i>
                                   <div className="mt-2">{user.email}</div>
                                   <div className="mt-2">
                                       <input className="form-control"
                                       onChange={this.handleChange}
                                        placeholder="Enter your Password" name="password" 
                                        id="password" type="password" value={user.password}/>
                                   </div>
                                   {error && <div className="text-center text-danger">{error}</div>}
                                   <div className="text-center mt-3 text-primary cps" >OR Login using OTP</div>
                                   <div className="mt-3">
                                       <button className="form-control btn btn-danger" onClick={this.handleSubmit} type="submit">Login</button>
                                   </div>
                                   <div className="text-center mt-3 text-primary cps" onClick={()=>this.goToLoginAgain()}>Login as a different user</div>
                                </div>
                          ):(
                              <div className="col-6 text-center mt-4">
                              <i className="fa fa-user-circle lpsflog" ></i>
                              <div className="mt-2">EMAIL ID / MOBILE NUMBER</div>
                              <div className="mt-2">
                                  <input className="form-control"
                                    onChange={this.handleChange}
                                   placeholder="EMAIL ID / MOBILE NUMBER" 
                                   name="email" id="email" type="text" value={user.email}/>
                              </div>
                              {error && <div className="text-center text-danger">{error}</div>}
                              <div className="mt-4">
                                  <button className="form-control btn btn-danger" onClick={()=>this.goToPassword()}>Continue</button>
                              </div>
                              <div className="text-center mt-3">By proceeding, you agree with our Terms of Service, Privacy Policy Master User Agreement.
                              </div>
                              <hr className="mb-2"/>
                            </div>

                          )}
                          
                          <div className="col-6 text-center mt-4 fontstyle">
                              <div className="lpsflog1">
                                  Logged In/Registered users get MORE!
                              </div>
                              <div className="lpsflog3">
                                  <i className="fa fa-calendar lpsflog2" aria-hidden="true" ></i>
                                   View/ Cancel/ Reschedule bookings
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-ticket lpsflog2" aria-hidden="true"></i>
                                  Check booking history, manage cancellations print eTickets
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-edit lpsflog2" aria-hidden="true" ></i>
                                  Book faster with Pre-Filled Forms, saved Travellers Saved Cards
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-money-bill lpsflog2" aria-hidden="true" ></i>
                                  Use Yatra eCash to get discounts
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-barcode lpsflog2" aria-hidden="true" ></i>
                                  Transfer eCash to your Family/Friends
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-desktop lpsflog2" aria-hidden="true" ></i>
                                 Convert eCash to Shopping Coupons from Amazon, BookMyShow, etc.
                             </div>
                             <div className="lpsflog3">
                                 <i className="fa fa-briefcase lpsflog2" aria-hidden="true" ></i>
                                  Do you have GST number?Additional Benefits of Free Meals, Low Cancellation Fee, Free Rescheduling for SME business customers
                            </div><br/> <br/>
                          </div>
                       </div>
                    </div>
                    <div className="col-3"></div>
                </div>
            </div>
            </div>
        )
    }
}
export default Login;