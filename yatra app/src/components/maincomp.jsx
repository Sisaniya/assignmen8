import React,{Component} from "react";
import {Switch,Link,Route, Redirect} from "react-router-dom";
import Home from "./home";
import Booking from "./booking"
import NavBar from "./navBar";
import SearchFlight from "./searchFlight";
import Login from "./login";
import UserProfile from "./userProfile";
import UserEcash from "./userEcash";
import AllBookings from "./allBookings";
import Logout from "./logout";
import Payment from "./payment";

class MainComp extends Component
{
    state={
       
        bflights:[],
        bDetails:{},
        
      
    }
  setFlightData=(data)=>{
     let str=JSON.stringify(data);
     localStorage.setItem("searchFlight",str);
  let s={...this.state};
    this.setState(s);

  }
  setBookingDetails=(obj)=>{
    let s={...this.state};
    s.bDetails=obj;
    this.setState(s);
  }
  setBookFlight=(arr)=>{
      let s={...this.state};
      let str=localStorage.getItem("searchFlight");
      let searchFlight=str?JSON.parse(str):{};
      let {rFlight}=searchFlight;
      if(rFlight)
      {
          let f1=JSON.stringify(arr[0]);
          let f2=JSON.stringify(arr[1]);
          localStorage.setItem("f1",f1);
          localStorage.setItem("f2",f2);
      }
      else
      {
        let f1=JSON.stringify(arr[0]);
        localStorage.setItem("f1",f1);
        localStorage.removeItem("f2");
      }
     
      this.setState(s);

  }
  
   
  
    render() {
      let {bDetails}=this.state;
        let str=localStorage.getItem("searchFlight");
        let searchFlight=str?JSON.parse(str):{};
        let str1=localStorage.getItem("f1");
        let f1=str1?JSON.parse(str1):{};
        let str2=localStorage.getItem("f2");
        let f2=str2?JSON.parse(str2):{};
      let bflights=[];
      bflights.push(f1);
        bflights.push(f2);
        console.log(bflights);
        
       let str4=localStorage.getItem("uinfo");
       let uinfo=str4?JSON.parse(str4):null;
      
        let completeD={searchFlight:searchFlight,bflights:bflights};
       
        return (
            <React.Fragment>
                <NavBar uinfo={uinfo}/>
               <Switch>
               <Route path="/payment" render={(props)=><Payment bDetails={bDetails} uinfo={uinfo} completeD={completeD}  {...props} />}/>
               <Route path="/yatra/logout" render={(props)=><Logout  {...props} />}/>
               <Route path="/yatra/allbookings" render={(props)=><AllBookings uinfo={uinfo} {...props} />}/>
               <Route path="/yatra/eCash" render={(props)=><UserEcash uinfo={uinfo}  {...props} />}/>
               <Route path="/yatra/profile" render={(props)=><UserProfile  {...props} />}/>
               <Route path="/yatra/login" render={(props)=><Login  {...props} />}/>
               <Route path="/booking" render={(props)=><Booking setBookingDetails={this.setBookingDetails} uinfo={uinfo} completeD={completeD} {...props} />}/>
                   <Route path="/yatra/airSearch" render={(props)=><SearchFlight setBookFlight={this.setBookFlight} {...props} searchdata={searchFlight}/>}/>
                   <Route path="/yatra" render={(props)=><Home setFlightData={this.setFlightData} {...props}/>}/>
                   <Redirect from="/" to="/yatra"/>
               </Switch>
            </React.Fragment>
        )
    }
}
export default MainComp;