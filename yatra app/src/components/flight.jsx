import React,{Component} from "react";
import {Switch,Route,Link} from "react-router-dom"
import i3 from  './slogo/yatra.svg';
import AddTraveller from "./addTraveller";
import Calendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
class Flight extends Component
{
    state={
        weeks:["Monday","Tuesday","Wednesday","Thrusday","Friday","Saturday","Sunday"],
       months:["January","February","March","April","May","June","July","August","September","October","November","December"],
        placeList:["New Delhi (DEL)","Mumbai (BOM)","Banglore (BLR)", "Kolkata (CCU)"],
        selectedPlaceD:"",
        selectedPlaceG:"",
        date:new Date(),
        tclass:"Economy",
        travellers:{Adult:1,Child:0,Infant:0},
        tclassList:["Economy","Premium Economy","Business"],
        show:false,
        rDate:new Date(Date.now()+(3600 * 1000 * 24)),
        rFlight:false,
        showDate:false,
        showRDate:false,
        
    }
    showDetails=()=>{
        let {rDate,date,selectedPlaceD,selectedPlaceG,travellers,months,weeks,rFlight,tclass}=this.state;
        let d=date.getDate()+" "+months[date.getMonth()]+", "+date.getFullYear();
       let  returnDate=rFlight?rDate.getDate()+" "+months[rDate.getMonth()]+", "+rDate.getFullYear():"";
        let obj={date:d,rDate:returnDate,departFrom:selectedPlaceD,goingTo:selectedPlaceG,travellers:travellers,rFlight:rFlight,type:tclass};
        if(selectedPlaceG && selectedPlaceD)
        {
            this.props.setFlightData(obj);

        }
     
        let str="";
        str+="From : "+selectedPlaceD+", To : "+selectedPlaceG;
        str+=", Depart Date : "+date.getDate()+" "+months[date.getMonth()]+", "+date.getFullYear();
        str+=", Return Date : "+rDate.getDate()+" "+months[rDate.getMonth()]+", "+rDate.getFullYear();
    str+=", Number of travellers = "+(travellers.Adult+travellers.Child+travellers.Infant);
    str+="("+travellers.Adult+":Adult,"+travellers.Child+":Child,"+travellers.Infant+":Infant)"
    
}
    setDate=()=>{
        let s={...this.state};
        s.showDate=s.showDate?false:true;
        
        this.setState(s);
    }
    setRDate=()=>{
        let s={...this.state};
        s.showRDate=s.showRDate?false:true;
        this.setState(s);
    }
    onDateChange=(date)=>{
        let s={...this.state};
        s.date=date;
        s.rDate=new Date(date.getFullYear(),date.getMonth(),date.getDate()+1);
        s.showDate=false;
        this.setState(s);
    }
    onRDateChange=(date)=>{
        let s={...this.state};
        if((s.date.getDate() < date.getDate()) || (s.date.getMonth() < date.getMonth()) )
        {
            s.rDate=date;
            s.showRDate=false;
            this.setState(s);
        }
        else 
        {
            window.alert("Select Return Date after Departure Date");
        }
       
    }
    setRFlight=(bool)=>{
        let s={...this.setState};
        s.rFlight=bool;
        this.setState(s);

    }
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        if(input.name == "selectedPlaceG")
        {
            if(s.selectedPlaceD == input.value)
            {
                window.alert("Destination and Arrival have to be different");
                s.selectedPlaceG="";
            }
            else
            {
                s[input.name]=input.value;
            }
        }
       else if(input.name == "selectedPlaceD")
        {
            if(s.selectedPlaceG == input.value)
            {
                window.alert("Destination and Arrival have to be different");
                s.selectedPlaceD="";
            }
            else
            {
                s[input.name]=input.value;
            }
        }
        else {
            s[input.name]=input.value;
        }

       
       
        this.setState(s);
    }
  
    handelTInc=(type)=>{
        let s={...this.state};
       let c=s.travellers[type];
       s.travellers[type]=c+1;
     this.setState(s);
    }
    showForm=()=>{
        let s={...this.state};
       s.show=s.show?false:true;
       console.log(s.show);
      this.setState(s);
      
    }
    handelTDec=(type)=>{
        let s={...this.state};
       let c=s.travellers[type];
       if(c>0)
       {
        s.travellers[type]=c-1;
       }
      
     this.setState(s);
    }
    render() {
        let {showDate,showRDate,placeList,selectedPlaceD,selectedPlaceG,date,travellers,tclass,rFlight,rDate,weeks,months,tclassList}=this.state;
        let {travelType}=this.props;

       return (
           <React.Fragment>
              <div className="row ">
                  <div className="container">
                        <div className="row mb-3 mt-2">

                        
                         <div className="col-12 text-center">
                             <button className={"returnbss "+(!rFlight?"beactive":"benotactive")} onClick={()=>this.setRFlight(false)}>One Way</button>
                             <button className={"returnbss "+(rFlight?"beactive":"benotactive")} onClick={()=>this.setRFlight(true)}>Return</button>
                            
                         </div>
                         </div>
                  
                        <div className="row mt-2">
                            <div className="col-5">
                                <span className="gtyupi">Depart From</span>
                                <div>
                                   
                                            <select className="yt-input-text"
                                             id="selectedFrom"
                                              value={selectedPlaceD}
                                              onChange={this.handelChange}
                                               name="selectedPlaceD">
                                                 <option value="">Select city</option>
                                                 {placeList.map(ele=>{
                                                  return (
                                                         <option>{ele}</option>
                                                        )
                                        
                                                      })}

                                            </select>

                                </div>
                             
                            </div>
                            <div className="col-lg-2">
                                <a data-swap="true" tabindex="-1" className="yt-booking-engine-snipe">
                                    <img title="Swap Origin City and Destination City"
                                     className="beSwapCity" src="https://www.yatra.com/fresco/resources/toucan/dist/images/swipe.svg?17fd684eff42c5149d5fd6cfe4b0b38b"/>
                                </a>
                            </div>
                            <div className="col-5">
                                <span className="gtyupi">Going To</span>
                                <div>
                                   
                                            <select className="yt-input-text"
                                             id="selectedFrom"
                                              value={selectedPlaceG}
                                              onChange={this.handelChange}
                                               name="selectedPlaceG">
                                                 <option value="">Select city</option>
                                                 {placeList.map(ele=>{
                                                  return (
                                                         <option>{ele}</option>
                                                        )
                                        
                                                      })}

                                            </select>

                                </div>
                             
                            </div>
                            
                        </div>
                    <div className="row mt-2 mb-3">
                        <div className="text-danger col-12"></div>
                    </div>
                   <div className="row mt-2 mb-3">
                       <div className="col-6">
                          <span className="gtyupi" >Departure Date</span>
                         <div className="date-text" onClick={()=>this.setDate()}> {date.getDate()} {this.state.months[date.getMonth()]}, {date.getFullYear()}</div> 
                         <div >{this.state.weeks[date.getDay()] }</div>
                          
                         {showDate?  <Calendar
                         showNeighboringMonth={false}
                          minDate={new Date()}
                          onChange={this.onDateChange}
                          value={date}
                          />:""}
                        
                          
                       </div>
                       <div className="col-6">
                           <span className="gtyupi" >Return Date</span>
                           {rFlight?(
                               <React.Fragment>
                         <div className="date-text" onClick={()=>this.setRDate()}> {rDate.getDate()} {this.state.months[rDate.getMonth()]}, {rDate.getFullYear()}</div>
                          <div>{this.state.weeks[rDate.getDay()] }</div>
                          
                          {showRDate?  (<Calendar
                           showNeighboringMonth={false}
                           minDate={new Date()}
                          onChange={this.onRDateChange}
                          value={rDate}
                          />):""}
                        
                               </React.Fragment>
                           ): <p ><a href="#"><small>Book round trip to save extra</small></a></p>}
                          
                      </div>
                   </div>
                   <hr />
                   <div className="row">
                       <div className="col-11">
                           <span className="gtyupi">Travellers,class</span><br/>
                           <div className="traveller-text">
                           {travellers.Adult+travellers.Child+travellers.Infant} Traveller,{tclass}

                           </div>
                          
                       </div>
                       <div className="col-1 py-3">
                           {this.state.show? <i onClick={()=>this.showForm()} className="fas fa-chevron-up"></i>
                           : <i onClick={()=>this.showForm()} className="fas fa-chevron-down"></i>}
                       
                       </div>
                   </div>
                   {this.state.show?(
                       <div>
                    <div className="row mt-2">
                       <div className="col-4">
                           <AddTraveller handelInc={this.handelTInc} handelDec={this.handelTDec} type="Adult" desc="" travellers={travellers}/>
                       </div>
                       <div className="col-4">
                           <AddTraveller handelInc={this.handelTInc} handelDec={this.handelTDec}  type="Child" desc="(2-12 yrs)" travellers={travellers}/>
                       </div>
                       <div className="col-4">
                           <AddTraveller handelInc={this.handelTInc} handelDec={this.handelTDec}  type="Infant" desc="(Below 2 yrs)" travellers={travellers}/>
                       </div>
                       


                   </div>

                   <div className="row mt-2">
                   <div className="ml-2">
                           {this.getRadio(tclassList,tclass,"tclass")}
                    </div>
                   </div>
                   </div>
                   ):""}
                   
                   <hr />
                   <div className="row sbsff">
                       <div className="col-6"></div>
                       <div className="col-6">
                       <button onClick={()=>this.showDetails()} className=" yt-booking-engine-snipe-button" type="submit" disabled="">
                           <span class="ml-1">Search Flights<i className="fa fa-arrow-right" aria-hidden="true"></i>
                           </span>
                        </button>
                     
                       </div>
                   </div>
                   </div>
                   </div>
           </React.Fragment>
       )
    }
    
    getRadio=(arr,value,name)=>{
          
 
        return (
          <React.Fragment>
        
        {arr.map(ele=>{
            return (
               
                <div className="form-check mt-1" key={ele}>
                <input className="form-check-input"
                type="radio"
                 name={name}
                 value={ele}
                 checked={value==ele}
                 onChange={this.handelChange}
                 
                />
               <label className="form-check-label mt-1">{ele}</label>
               </div>
               
            )
        })}
            
            </React.Fragment>
        )
        }
    getDD=(arr,value,name,top)=>{
        return (
            <div className="form-group">
                <select className="form-control"
                name={name}
                value={value}
                onChange={this.handelChange}
                >
                    <option value="">{top}</option>
                    {arr.map(ele=><option>{ele}</option>)}
                </select>
            </div>
        )
    }
}
export default Flight;