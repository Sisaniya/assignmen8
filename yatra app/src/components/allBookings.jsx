import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import UserNavBar from "./userNavbar";
import Footer from "./footer";

class AllBookings extends Component
{
    state={
        data:[],
        upperlinks:["ALL","FLIGHTS","HOTELS","HOMESTAYS","FLIGHTS + HOTELS","BUSES","TRAINS","ACTIVITIES","HOLIDAYS"],
        filteroptions:["Relevance","Upcoming","Completed","Cancelled","Booking Date"],
        index:0,
        showfare:-1,
        months:["January","February","March","April","May","June","July","August","September","October","November","December"],
         days:["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
    }

    goToLink=(str)=>{
        this.props.history.push(str);
    }
    goToMyBooking=()=>{
        let s={...this.state};
           s.showfare=-1;
        this.setState(s);
    }
    handleChangePage=(incre)=>{
        let s={...this.state};
           s.index=s.index + incre;
        this.setState(s);
    }
    goToFareDetails=(index)=>{
        let s={...this.state};
        s.showfare=index;
        this.setState(s);
    }
    fareDetails=()=>{
        let {data,showfare,months,days}=this.state;
        let {departure,arrival,ref,travellers,passangers,type}=data[showfare];
        let {Adult,Child,Infant}=passangers;
        let str=departure.date.split(",").join(" ");
        let arr=str.split(" ");
        let indm=months.findIndex(ele=>ele == arr[1]);

        let d=new Date(+arr[3],indm,+arr[0]);
        console.log(d,arr,indm);
        return (
            <div className="col-9">
                <div className="row mt-2 pt-1 pb-1 ml-1 mr-1 ubpsforsbs" >
                    <div className="col-1 text-center mt-1 d-none d-lg-block">
                        <i className="fa fa-hotel ubpsforsbs1" ></i>
                    </div>
                    <div className="col-7 mt-1">
                        <div className="row"><b>{departure.name}</b></div>
                    </div>
                    <div className="mt-1 ml-3 text-secondary">Booked On:{days[(d.getDay()-1)]}, {departure.date.split(",").join(" ")} | Booking Ref No. {ref}</div>
              </div>
              <div className="row border ml-1 mr-1">
                  <div className="col-4 mt-4 mb-3">
                      <b>Check In</b>
                      <div className="text-secondary ubpsforsbs2" >{departure.desDept}</div>
                  </div>
                  <div className="col-4 mt-4 mb-3">
                      <b>Check Out</b>
                      <div className="text-secondary ubpsforsbs2" >{departure.desArr}</div>
                  </div>
                  <div className="col-3 col-lg-2 mt-4 mb-3">
                      <b>Traveller(s)</b>
                      <div className="text-secondary ubpsforsbs2" >{travellers}</div>
                  </div>
                  <div className="col-2 mt-4 mb-3">
                      <b>Status</b>
                      <div className="text-secondary ubpsforsbs2" >Booking</div>
                  </div>
                  <hr className="col-11"/>
                  <div className="col-2 text-center">
                      <span className="fa-stack ubpsforsbs3">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fas fa-file-invoice fa-stack-1x"></i>
                      </span>
                      <div>Print Invoice</div>
                </div>
                <div className="ubpsforsb4" ></div>
                 <div className="col-3 text-center">
                     <span className="fa-stack ubpsforsbs3" >
                         <i className="far fa-circle fa-stack-2x"></i>
                         <i className="fa fa-location-arrow fa-stack-1x"></i>
                     </span>
                     <div>Get Directions</div>
                 </div>
                 <div className="ubpsforsb4" ></div>
                 <div className="col-2 text-center">
                     <span className="fa-stack ubpsforsbs3" >
                         <i className="far fa-circle fa-stack-2x"></i>
                         <i className="fas fa-sync-alt fa-stack-1x"></i>
                     </span>
                     <div>Re-Book</div>
                 </div>
                 <div className="ubpsforsb4" ></div>
                 <div className="col-2  text-center">
                     <span className="fa-stack ubpsforsbs3" >
                         <i className="far fa-circle fa-stack-2x"></i>
                         <i className="fas fa-file-alt fa-stack-1x"></i>
                     </span>
                     <div>Fare Details</div>
                 </div>
                 <div className="ubpsforsb4" ></div>
                 <div className="col-2  text-center">
                     <span className="fa-stack ubpsforsbs3" >
                         <i className="far fa-circle fa-stack-2x"></i>
                         <i className="fas fa-pen fa-stack-1x"></i>
                     </span>
                     <div>Write To Us</div>
                 </div>
              </div>
              
              <div className="row border ml-1 mr-1 mt-4">
                  <b className="ml-3 mt-2">Your Booking Details</b>
                  <hr className="col-11"/>
                  <div className="col-3 col-lg-4 mt-1 mb-3">
                      <i className="fa fa-hotel ubpsforsbs4" ></i>
                      <b className="ubpsforsbs5">Departure Flight</b>
                      <div className="text-secondary ubpsforsbs5" >{departure.name} {departure.code}</div>
                  </div>
                  <div className="col-2 col-lg-2 mt-1 mb-3">
                      <b>Duration</b>
                      <div className="text-secondary ubpsforsbs2" >{departure.total}</div>
                  </div>
                  <div className="col-4 col-lg-3 mt-1 mb-3">
                      <b>Departure</b>
                      <div class="text-secondary ubpsforsbs2">{departure.timeDept} {departure.T1}</div>
                  </div>
                  <div className="col-4 mt-1 col-lg-3 mb-3">
                      <b>Arrival</b>
                      <div className="text-secondary ubpsforsbs2" >{departure.timeArr} {departure.T2}</div>
                      </div>

                    {arrival.id && (
                        <React.Fragment>
                             <hr className="col-11"/>
                  <div className="col-3 col-lg-4 mt-1 mb-3">
                      <i className="fa fa-hotel ubpsforsbs4" ></i>
                      <b className="ubpsforsbs5">Arrival Flight</b>
                      <div className="text-secondary ubpsforsbs5" >{arrival.name} {arrival.code}</div>
                  </div>
                  <div className="col-2 col-lg-2 mt-1 mb-3">
                      <b>Duration</b>
                      <div className="text-secondary ubpsforsbs2" >{arrival.total}</div>
                  </div>
                  <div className="col-4 col-lg-3 mt-1 mb-3">
                      <b>Departure</b>
                      <div class="text-secondary ubpsforsbs2">{arrival.timeDept} {arrival.T1}</div>
                  </div>
                  <div className="col-4 mt-1 col-lg-3 mb-3">
                      <b>Arrival</b>
                      <div className="text-secondary ubpsforsbs2" >{arrival.timeArr} {arrival.T2}</div>
                      </div>

                        </React.Fragment>
                    )}
                  </div>
                  <div className="row ml-4 col-lg-11 border col-12 mr-2 mt-3 mb-3">
                      <div className="col-3 mt-3 mb-3 ml-2">
                          <b>Inclusions</b>
                       </div>
                       <div className="col-6"></div>
                       <div className="col-9 mb-4 text-secondary">
                           <i className="fa fa-check ubpsforsbs6" ></i>
                            Breakfast, Complimentary WiFi Internet
                        </div>
                    </div>
                    <div className="row ml-1 mt-3  mr-2 col-lg-11 col-12">
                        <div className="col-3 border">
                            <b>Traveller</b>
                        </div>
                        <div className="col-4 border">
                            <b>Type</b>
                        </div>
                        <div className="col-2 border">
                            <b>Baggage</b>
                        </div>
                    </div>
                    {Adult.map(ele=>{
                        return(
                        <div className="row ml-1  mr-2  col-lg-11 col-12">
                            <div className="col-3 border">{ele.fname+" "+ele.lname}</div>
                            <div className="col-4 border">{type}</div>
                            <div className="col-2 border">25 kgs</div>
                        </div> 
                        )})}
                    {Child.map(ele=>{
                        return(
                        <div className="row ml-1  mr-2  col-lg-11 col-12">
                            <div className="col-3 border">{ele.fname+" "+ele.lname}</div>
                            <div className="col-4 border">{type}</div>
                            <div className="col-2 border">25 kgs</div>
                        </div> 
                        )})}
                         {Infant.map(ele=>{
                        return(
                        <div className="row ml-1  mr-2  col-lg-11 col-12">
                            <div className="col-3 border">{ele.fname+" "+ele.lname}</div>
                            <div className="col-4 border">{type}</div>
                            <div className="col-2 border">25 kgs</div>
                        </div> 
                        )})}
                </div>
        )
    }

    showBookings=()=>{
        let {upperlinks,filteroptions,data=[],index}=this.state;
        let obj={};
         let obj1={};
        if(data[index])
        {
          
           obj=data[index].departure;
          obj1=data[index];
        
        }
        let {ref,travellers}=obj1;
        let {name,date="",desDept,desArr}=obj;
        
      
        return (
            <div className="col-9">
                <div className="row bg-light">
                    <div className="text-left mt-2">
                        <i className="fa fa-toggle-left ubpsforsb" id="agd"></i>
                    </div> 
                    {upperlinks.map(ele=><div className="m-2 text-secondary ubpsforsb1" >{ele}</div>)}
                   
                    <div className="text-right mt-2">
                        <i className="fa fa-toggle-right ubpsforsb" ></i>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-9"></div>
                    <div className="col-3 fontstyle1">Filter/ Sort By</div>
                </div>
                <div className="row">
                    <div className="col-9"></div>
                    <div className="col-3">
                        <select className="browser-default custom-select" name="selected">
                            {filteroptions.map(ele=><option>{ele}</option>)}
                           
                        </select>
                    </div>
                </div>
               {data.length? (
                   <React.Fragment>
                          <div className="row mt-2 pt-1 pb-1 ml-1 mr-1 ubpsforsb2">
                    <div className="col-1 text-center mt-1 d-none d-lg-block">
                        <i className="fa fa-fighter-jet ubpsforsb3"></i>
                    </div>
                    <div className="col-4">
                        <div className="row"><b>{name}</b></div>
                        <div className="row" id="abc10">
                            <div className="h6 text-secondary">{travellers} Traveller(s)</div>
                        </div>
                    </div>
                    <div className="mt-1  text-secondary">Booked On:{date.split(",").join(" ")}| Booking Ref No. {ref}</div>
                </div>
                <div className="row border ml-1 mr-1">
                    <div className="col-4">
                        <b>Departure</b>
                        <div className="text-secondary">{desDept}</div>
                    </div>
                    <div className="col-3">
                        <b>Arrival</b>
                        <div className="text-secondary">{desArr}</div>
                    </div>
                    <div className="col-1 ubpsforsb4">
                    </div>
                    <div className="col-4">
                        <button onClick={()=>this.goToFareDetails(index)} className="btn btn-danger btn-sm mt-1">Fare Details</button>
                        <button onClick={()=>this.goToFareDetails(index)} className="btn btn-danger ml-2 btn-sm mt-1">Itinerary</button>
                    </div>
                    <hr className="col-10"/>
                    <div className="col-1"></div>
                    <div className="col-3 text-center ml-1 ">
                        <i className="fas fa-file-invoice icon-sm"></i>
                        <div className="fs-12 font-lightestgrey">Print Invoice</div>
                    </div>
                    <div className=" ubpsforsb4" ></div>
                    <div className="col-3 text-center">
                        <i className="fa fa-location-arrow icon-sm"></i>
                        <div className="fs-12 font-lightestgrey">Get Directions</div>
                    </div>
                    <div className="ubpsforsb4" ></div>
                    <div className="col-2  text-center">
                        <i className="fas fa-sync-alt icon-sm"></i>
                        <div className="fs-12 font-lightestgrey">Re-Book</div>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-6 text-left">Showing {index+1} of {data.length}</div>
                    <div className="col-6 text-right">
                    {index>0?<span onClick={()=>this.handleChangePage(-1)} className="footer-icon-p"><i class="fas fa-chevron-left"></i></span>:""}
                        <span className="ml-1 mr-1">{index+1}</span>
                        {index<data.length-1?<span onClick={()=>this.handleChangePage(+1)} className="footer-icon-p"><i class="fas fa-chevron-right"></i></span>:""}
                        
                   </div>
                </div>
                   </React.Fragment>
               ):<div className="row mt-3 text-center text-muted"><div className="col">No Records Found</div></div>}

                
        </div>
        )
    }
    async componentDidMount() {
        try
        {
            let user=localStorage.getItem("user");
            console.log(user);
            let response=await axios.get("https://evening-brook-89374.herokuapp.com/bookings",{headers: {Authorization: user}});
            let {data}=response;
            console.log(data);
          
            this.setState({data:data});
        }
        catch(ex)
        {
            window.alert(ex.response.status);
            console.log(ex.response.data);
        }
      
      
         
    }
    render() {
        let {fullname,email,mobileNo}=this.props.uinfo;
        let {data,index,showfare}=this.state;
        return (
            <div className="container-fluid bgcolor">
                <div className="row">
                     <div className="col-lg-9 col-12 fontstyle">
                         <div class="row mt-3 mb-2">
                             <div class="col-9 ml-1">Dashboard /
                             <Link onClick={()=>this.goToMyBooking()}> My Bookings</Link>
                             {showfare>=0?<span> / <span> Fare Details</span></span>:""} 
                            
                             </div>
                        </div>
                        <div className="row  ml-1" id="payment">

                        <div className="col-lg-3  unavstyle" >
                            <UserNavBar str="/yatra/allbookings" goToLink={this.goToLink}/>
                        </div>
                       {showfare>=0?this.fareDetails():this.showBookings()
                        
                      }
                         
                       

                        </div>
                     </div>




                     <div className="col-lg-2 col-12">
                         <div className="row ml-1 fontstyle sectionpf">
                             <div className="col p-0 m-0">
                                 <div className="row">
                                     <div className="col-12 text-center uprosps">
                                         <i className="fa fa-user-circle"></i>
                                     </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {fullname}
                                    </div>
                                </div>
                                <div className="row">
                                    <hr className="col-9 text-center"/>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {email}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        Phone: {mobileNo}
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>




                </div>
                <Footer/>
            </div>
        )
    }
}
export default AllBookings;