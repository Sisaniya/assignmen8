import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import SearchDetailsNav from "./searchDetailsNav";
class OneWayFlights extends Component 
{
    state={
        data:this.props.data,
        showItems:this.props.showItems,
       farerules:false,
    }
    setFareRules=(bool)=>{
        let s={...this.state};
        s.farerules=bool;
        this.setState(s);
    }
    setshowItem=(id)=>{
        let s={...this.state};
        let index=s.showItems.findIndex(ele=>ele== id);
        if(index>=0)
        {
            s.showItems.splice(index,1);
        }
        else
        {
            s.showItems.push(id);
        }
        this.setState(s);
    }
   
    setEffect=(e)=>{
        e.currentTarget.className="row mt-1 ml-4 pt-2  onewaybactive";
    }
    removeEffect=(e)=>{
        e.currentTarget.className="row mt-1 ml-4 pt-2  onewaybox";
    }

    render() {
       
        let {data,showItems=[]}=this.props;
      console.log(this.props.showItems);
           let {farerules}=this.state;
        return (
           <div classNameName="container-fluid">
               {data.map(ele=>{
                    let  {id,logo,name,code,checkin,meal,desDept,desArr,Price,airBus,total,timeDept,timeArr,T1,T2} =ele;
    
                   return (
                 
                   <div className="row mt-1 ml-4 pt-2 onewaybox " onMouseLeave={this.removeEffect}  onMouseEnter={this.setEffect} id="box1">
                       <div className="col fontstyle" id="F41 ">
                           <div className="row deal ">
                               <div className="col-1 deal-tag">DEAL</div>
                               <div className="col-8 content ">Book GoAir and get flat Rs 515 OFF per pax (upto Rs. 1,550). Use code: FLASHUPI</div>
                           </div>
                           <div className="row mb-1 flight-det">
                               <div className="col-lg-1 col-2">
                                   <img src={logo} className="sforowfes"/>
                                </div>
                                <div className="col-lg-1 col-3">
                                    <div className="row sforowfes1" >
                                        <div className="col d-none d-lg-block">{name}</div>
                                    </div>
                                    <div className="row text-muted sforowfes2" >{code}</div>
                                </div>
                                
                           
                            <div className="col-lg-1 col-3 text-right sforowfes3" >
                                <div className="row"><span>{timeDept}</span></div>
                                <div className="row sforowfes4" >{desDept}</div>
                           </div>
                           <div className="col-1 d-none d-lg-block">
                               <hr/>
                            </div>
                            <div className="col-lg-1 col-3 text-left sforowfes3" >
                                <div className="row"><span>{timeArr}</span></div>
                                <div className="row sforowfes4" >{desArr}</div>
                            </div>
                            <div className="col-1 pt-1 d-none d-lg-block">
                                <span className="sforowfes5"></span>
                            </div>
                            <div className="col-1 d-none d-lg-block text-left sforowfes6" >
                                <div className="row"><span>{total}</span></div>
                                <div className="row">Non-Stop</div>
                            </div>
                            <div className="col-2 d-none d-lg-block">
                            </div>
                            <div className="col-lg-2 col-5 text-right sforowfes7" >
                                <span>₹ {Price}</span>
                            </div>
                            <div className="col-lg-1 col-3 text-right">
                                <button onClick={()=>this.props.bookFlight(ele)} className="btn btn-sm " id="book">
                                    <span> Book </span>
                                </button>
                            </div>
                            </div>
                        
                        <hr className="sforowfes13"/>
                            <div className="row pb-1">
                                <div className="col-7 d-none d-lg-block">
                                    <a className="yt-row-footer" onClick={()=>this.props.setshowItem(id)}>Flight Details
                                    <i className={showItems.findIndex(ele=>ele== id)>=0?"fa fa-chevron-down":"fa fa-chevron-up"}></i></a>
                                </div>
                                <div className="col-5 text-right d-none d-lg-block">
                                    <span id="rect"> eCash</span><span id="rect1">₹ 250</span>
                                </div>
                            </div>
                      {showItems.findIndex(ele=>ele== id)>=0?(

                     
                        <div className="row flight-details" id="F41">
                            <div className="col-7 ">
                                <div className="row  header"> Flight Details </div>
                                <div className="row contents">
                                    <div className="col-2">
                                        <img src={logo} className="sforowfes8"/>
                                    </div>
                                    <div className="col-7 text-left fs-13">
                                        <div className="row">{name}</div>
                                        <div className="row fs-10 font-lightestgrey">{airBus}</div>
                                    </div>
                                    <div className="col-3 text-right">
                                        <img src="https://i.ibb.co/31BTG9K/icons8-food-100.png" className="sforowfes12"/>
                                    </div>
                               </div>
                               
                               <div className="row contents">
                                   <div className="col-3 ml-1 sforowfes9" >
                                       <div className="row ml-1">{desDept}</div>
                                       <div className="row ml-1 fs-16 bold"><span>{timeDept}</span></div>
                                       <div className="row ml-1 fs-12">{T1}</div>
                                  </div>
                                  <div className="col-5">
                                      <div className="row fs-12">
                                          <div className="col-9 text-center">Time Taken</div>
                                      </div>
                                      <div className="row">
                                          <div className="col-9">
                                              <hr className="hr-row"/>
                                          </div>
                                          <div className="col-2 pt-2 font-lightestgrey sforowfes10" >
                                              <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plane" className="svg-inline--fa fa-plane fa-w-18 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                                  <path fill="currentColor" d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z">
                                                  </path>
                                              </svg>
                                         </div>
                                      </div>
                                 </div>
                                 <div className="col-3 sforowfes9" >
                                     <div className="row">{desArr}</div>
                                     <div className="row fs-16 bold"><span>{timeArr}</span></div>
                                     <div className="row fs-12">{T2}</div>
                                </div>
                                </div>
                                
                                <div className="row amen-details">
                                    <div className="col text-center font-lightestgrey fs-12">Checkin Baggage:
                                        <i className="fa fa-briefcase font-lightestgrey sforowfes10" ></i> 25kg
                                    </div>
                                </div>
                            </div>


                            <div className="col-5 sforowfes11" >
                                <div className="row pl-1 pt-3 pb-3 fs-13 bold">
                                    <div onClick={()=>this.setFareRules(false)} className={farerules?"col-6":"col-6 border-bottom"}>Fare Summary</div>
                                    <div onClick={()=>this.setFareRules(true)} className={farerules?"col-6 border-bottom":"col-6 "}>Fare Rules</div>
                                </div>
                               {farerules?(
                                   <React.Fragment>
                                <div className="row fs-13 bold">
                            <div className="col-6">Duration</div>
                            <div className="col-6 text-right">Per Passenger</div>
                        </div>
                        <div className="row fs-11 greyed">
                            <div className="col-6">{">"}2 hours</div>
                            <div className="col-6 text-right">₹ 2793</div>
                            </div>
                        <div className="row fs-11 greyed">
                            <div className="col-12">We would recommend that you reschedule/cancel your tickets atleast 72 hours prior to the flight departure</div>
                        </div>
                        <hr/>
                        <div className="row fs-13 bold">
                            <div className="col-12">Yatra Service Fee (YSF)**
                            <div className="fs-12">(charged per passenger in addition to airline fee as applicable)</div>
                            </div>
                        </div>
                        <div className="row greyed">
                            <div className="col-6 fs-11">Online Cancellation Service Fee</div>
                            <div className="col-6 text-right">₹ 400</div>
                        </div>
                        <div className="row greyed">
                            <div className="col-6 fs-11">Offline Cancellation Service Fee</div>
                            <div className="col-6 text-right">₹ 400</div>
                            
                        </div>
                        <div className="row greyed">
                            <div className="col-6 fs-11">Online Rescheduling Service Fee</div>
                            <div className="col-6 text-right">₹ 400</div>
                        </div>
                        <div className="row fs-12">
                            <div className="col-12">
                                * Prior to the date/time of departure. **Please note: Yatra service fee is over and above the airline cancellation fee due to which refund type may vary.</div>
                            </div>
                            <div className="row pt-2 pb-2">
                                <div className="col-12 text-center">
                                    <button onClick={()=>this.props.bookFlight(ele)} className="btn btn-danger text-white btn-block" routerlinkactive="router-link-active" tabindex="0">Book</button>
                                </div>
                            </div>
                     
                                   </React.Fragment>
                               ):
                               (<React.Fragment>
                                    <div className="row fs-13 bold">
                                    <div className="col-4">Fare Summary</div>
                                    <div className="col-4">Base and Fare</div>
                                    <div className="col-4">Fees and Taxes</div>
                                </div>
                                <div className="row fs-12 greyed">
                                    <div className="col-4">Adult X 1</div>
                                    <div className="col-4">₹ {Price}</div>
                                    <div className="col-4">₹ 1000</div>
                                </div>
                                <hr/>
                                <div className="row fs-14 bold">
                                    <div className="col-6">You Pay:</div>
                                    <div className="col-6 text-right">₹ {Price}</div>
                                </div>
                                <div className="row fs-12">
                                    <div className="col-12"> Note: Total fare displayed above has been rounded off and may show a slight difference from actual fare. </div>
                                </div>
                                <div className="row pt-2 pb-2">
                                    <div className="col-12 text-center">
                                        <button onClick={()=>this.props.bookFlight(ele)} className="btn btn-danger text-white btn-block" routerlinkactive="router-link-active" tabindex="0">Book</button>
                                    </div>
                                </div>



                               </React.Fragment>)}

                               

                            </div>
                        </div>
                         ):""}



                        </div>
                        <br/>
                    </div>
                     
                     )
               })}
           </div>
        )
    }
}
export default  OneWayFlights;