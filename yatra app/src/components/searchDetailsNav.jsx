import React,{Component} from "react";
class SearchDetailsNav extends Component
{
    state={
        months:["January","February","March","April","May","June","July","August","September","October","November","December"],
    }
    searchAgain=()=>{
        window.location="/yatra";
    }
    render() {
        let {months}=this.state;
        let {date,rDate,departFrom,goingTo,travellers={},type}=this.props.searchdata;
       
        return(
            <React.Fragment>
                 {this.props.searchdata?(
                      <div className="row mt-4 pt-2 pb-2 ppdnbs" >
                      <div className="col-1 text-center mt-1 d-none d-lg-block">
                          <i className="fa fa-fighter-jet ppdnbs1" ></i>
                       </div>
                       <div className="col-lg-2 col-5">
                           <div className="row" id="abc10">
                               <div className="col">From</div>
                           </div>
                           <div className="row" id="abc">
                               <div className="col"><b>{departFrom}</b></div>
                           </div>
                       </div>
                       <div className="col-1 mt-1 ">
                           <i className="fa fa-exchange"></i>
                       </div>
                       <div className="col-lg-2 col-5 ">
                           <div className="row" id="abc10">
                               <div className="col">To</div>
                           </div>
                           <div className="row" id="abc">
                               <div className="col"><b>{goingTo}</b></div>
                           </div>
                       </div>
                       <div className="col-2">
                           <div className="row ppdnbs11" >Date
                           </div>
                           <div className="row ppdnbs12" >
                               <b>{date?date:""}</b>
                           </div>
                       </div>
                       <div className="col-2">
                           <div className="row ppdnbs11" >Traveller(s)</div>
                           <div className="row ppdnbs12" ><b>{travellers.Adult+travellers.Child+travellers.Infant}, {type}</b></div>
                       </div>
                       <div className="col-2 mt-1">
                           <button onClick={()=>this.searchAgain()} className=" btn-submit bold fs-14"  >Search Again</button>
                       </div>
                   </div>


                   ):""}

            </React.Fragment>
           
           
        ) 
    }
}
export default SearchDetailsNav;