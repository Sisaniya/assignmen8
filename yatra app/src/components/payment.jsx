import React,{Component} from "react";
import httpServices from "../services/httpServices";
class Payment extends Component
{
    state={
        paymentList:["UPI","Credit Card","Debit Card","Net Banking","Paypal"],
        ptype:"UPI",
    }
    setPaymentType=(str)=>{
        let s={...this.state};
        s.ptype=str;
        this.setState(s);
    }
    async goToBook(obj) {
        try
        {
             let response=await httpServices.post("/bookings",obj);
             window.alert("Booking Updated Successfully");
             console.log(response.data);
             this.props.history.push("/yatra");
        }
        catch(ex)
        {

        }
    }
    bookFlight=()=>{
        let {uinfo}=this.props;
        let {completeD,bDetails}=this.props;
        let {bflights=[],searchFlight}=completeD;
        let {cDetails={}}=bDetails;
        let {email,mobileNo,travellersInfo={}}=cDetails;
        let {Adult=[],Child=[],Infant=[]}=travellersInfo;
        if(uinfo)
        {
            let obj={userId:uinfo.id}

            obj.departure={...bflights[0],date:searchFlight.date};
            obj.arrival=searchFlight.rFlight?{...bflights[1],date:searchFlight.rDate}:{};
            obj.travellers=Adult.length+Child.length+Infant.length;
            obj.passangers=travellersInfo;
            obj.ecash=(Adult.length+Child.length+Infant.length)*250;
            obj.email=email;
            obj.mobileNo=mobileNo;
            obj.count=bflights.length;
            obj.cancellation=bDetails.cancellation;
            obj.insurance=bDetails.insurance;
            obj.promocode=bDetails.promoCode;
            obj.type=searchFlight.type;

            this.goToBook(obj);
              
        }
       
    }
    render() {
        let {paymentList,ptype}=this.state;
        let {completeD,bDetails}=this.props;
        let {bflights=[],searchFlight}=completeD;
        let {cDetails={}}=bDetails;
        let total=bflights[0].Price+(searchFlight.rFlight?bflights[1].Price:0);

        let {email,mobileNo,travellersInfo={}}=cDetails;
        let {Adult=[],Child=[],Infant=[]}=travellersInfo;
        let eCash=(Adult.length+Child.length+Infant.length)*250;
        console.log(completeD,bDetails);
        return (
            <div className="container-fluid pb-3 bgcolor">
              <div className="row mb-3 backgroundcolor">


                  <div className="col-lg-9 col-12">
                      <div className="row mt-2 mb-2">
                          <div className="col-9 ml-1 pgsforfs" >
                              <span><i className="fa fa-credit-card"></i> Payment Method</span>
                          </div>
                     </div>
                     <div className="row bg-white ml-1" id="payment">
                         <div className="col-lg-2 col-4 text-center" id="fs20">
                             {paymentList.map(ele=>{
                                 return (
                                    <div onClick={()=>this.setPaymentType(ele)} className={ptype==ele?"border bg-white cps":"border bg-light cps"} >
                                        <div className="mt-2 mb-2">{ele}</div>
                                    </div>
                                 )
                             })}
                             
                        
                        </div>
                        <div className="col-8" id="fs20">
                            <br/><br/>
                            <div className="row">
                                <div className="col ml-4 fontstyle1">
                                    <span>Pay with Net {ptype}</span>
                                </div>
                            </div>
                            <br/><br/>
                            <div className="row">
                                <div className="col-lg-4 col-5 ml-4 pgsforfs1 fontstyle" >
                                    ₹ {bDetails.totalPay+350}
                                </div>
                                <div className="col-6">
                                    <button onClick={()=>this.bookFlight()} className="btn btn-danger text-white btn-md">Pay Now</button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col text-secondary text-center pgsforfs2"> 
                                By clicking Pay Now, you are agreeing to terms and Conditions and Privacy Policy 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 

                <div className="col-lg-3 col-12 fontstyle1">
                    <div className="row mt-2 mb-2">
                        <div className="col-11 ml-1 text-left ppsforles" >Payment Details</div>
                    </div>
                    <div className="row bg-white ml-1 mr-1" id="flight">
                        <div className="col">
                            <div className="row">
                                <div className="col-8 text-left ppsforles1" >Total Flight Price</div>
                                <div className="col-4 text-right ppsforles1" > ₹ {bDetails.totalPay} </div>
                            </div>
                            <div className="row">
                                <div className="col-8 text-left ppsforles1" >Convenience Fees</div>
                                <div className="col-4 text-right ppsforles1" > ₹ 350</div>
                            </div>
                            <hr/>
                            <div className="row">
                                <div className="col-6 text-left ppsforles2" ><span>You Pay</span></div>
                                <div className="col-6 text-right ppsforles2" ><span>₹ {bDetails.totalPay+350}</span></div>
                            </div>
                            <hr/>
                            <div className="row">
                                <div className="col-6 text-left">Earn eCash</div>
                                <div className="col-6 text-right ppsforles1" >₹ {eCash}</div>
                            </div>
                        </div>
                    </div>
                    <div _ngcontent-rjw-c11="" className="row mt-1 mb-1">
                        <div _ngcontent-rjw-c11="" className="col ml-1"> Booking summary </div>
                    </div>
                    <div className="row bg-white ml-1 mr-1" id="flight">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-6">
                                    <img src={bflights[0].logo} className="ppsforles3 "
                                   />
                                </div>
                                 <div className="col-5 mr-1 text-right" id="fs20">{bflights[0].code}</div>
                            </div>
                            <div className="row">
                                <div className="col-5 pl-1" id="fs18">{bflights[0].desDept}</div>
                                <div className="col-2 text-left">
                                    <i className="fa fa-fighter-jet ppsforles4"></i>
                                </div>
                                <div className="col-5" id="fs18">{bflights[0].desArr}</div>
                            </div>
                         </div>
                       {searchFlight.rFlight && (
                           <div className="col-12">
                               <hr/>
                           <div className="row">
                               <div className="col-6">
                                   <img src={bflights[1].logo} className="ppsforles3 "
                                  />
                               </div>
                                <div className="col-5 mr-1 text-right" id="fs20">{bflights[1].code}</div>
                           </div>
                           <div className="row">
                               <div className="col-5 pl-1" id="fs18">{bflights[1].desDept}</div>
                               <div className="col-2 text-left">
                                   <i className="fa fa-fighter-jet ppsforles4"></i>
                               </div>
                               <div className="col-5" id="fs18">{bflights[1].desArr}</div>
                           </div>
                        </div>
                       )}


                     </div>
                     <div className="row ml-1 mr-1 mt-1 mb-1">
                         <div className="col"> Contact Details </div>
                     </div>
                     <div className="row bg-white ml-1 pb-3 mr-1" id="detail">
                         <div className="col">
                             <div className="row"> 
                                 <div className="col-12 mt-2" id="fs16">
                                     {Adult.map((ele,index)=><div className="mt-1">{index+1}.  {ele.fname} {ele.lname}</div>)}
                                     {Child.map((ele,index)=><div className="mt-1">{Adult.length+index+1}.  {ele.fname} {ele.lname}</div>)}
                                     {Infant.map((ele,index)=><div className="mt-1">{Adult.length+Child.length+index+1}.  {ele.fname} {ele.lname}</div>)}

                                    
                                     
                                     </div>
                              </div>
                              <hr/>
                              <div className="row" id="fs16">
                                  <div className="col-4 text-secondary">Email</div>
                                  <div className="col-8 text-secondary">{email}</div>
                              </div>
                              <div className="row" id="fs16">
                                  <div className="col-4 text-secondary">Phone</div>
                                  <div className="col-8 text-secondary">{mobileNo}</div>
                              </div>
                         </div>
                      </div>
                    </div>










              </div>
            </div>
        )
    }
}
export default Payment;