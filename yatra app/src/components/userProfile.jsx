import React,{Component} from "react";
import UserNavBar from "./userNavbar";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import axios from "axios";
import Footer from "./footer";

class UserProfile extends Component
{
    state={
        user:{},
        titleList:["Mr.","Ms.","Mrs.","Dr."],
        stateList:["Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chhatisgarh","Goa","Gugarat","Haryana","Himachal Pradesh","Jharkhand","Karnataka","Kerala","Madhya Pradesh","Maharashtra","Manipur","Mizoram","Nagaland","Odisha","Punjab","Rajasthan","Sikkim","Tamil","Telangana","Tripura","Uttar Pradesh","Uttarakhand","West Bengal"],
        edit:false,
    }
    async componentDidMount() {
        try
        {
            let user=localStorage.getItem("user");
            console.log(user);
            let response=await axios.get("https://evening-brook-89374.herokuapp.com/user",{headers: {Authorization: user}});
            let {data}=response;
            console.log(data);
           
            this.setState({user:data});
        }
        catch(ex)
        {
            window.alert(ex.response.status);
            console.log(ex.response.data);
        }
      

    }
    goToLink=(str)=>{
        this.props.history.push(str);
    }
    showEditForm=()=>{
        let s={...this.state};
        s.edit=s.edit?false:true;
        this.setState(s);
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        if(input.name == "mobileNo")
        {
            if(!(((input.value+"").length)>10))
            s.user[input.name]=input.value;
        }
        else
        {
            s.user[input.name]=input.value;
        }
        
        this.setState(s);
    }
    getDD=(arr,value,name,top)=>{
        return (
            <div className="form-group">
                <select
                className="form-control "
                value={value}
                name={name}
                onChange={this.handleChange}
                >
                    <option value="">{top}</option>
                    {arr.map(ele=><option>{ele}</option>)}

                </select>
            </div>
        )
    }
    async putData(user) {
        try 
        {
             let response=await httpServices.put("/user",user);
           
             window.alert("Your profile has been updated successfully.");
            this.setState({edit:false});
        }
        catch(ex) 
        {
             console.log(ex.response.data);
        }
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s={...this.state};
         this.putData(s.user);
    }
    profileForm=()=>{
       let {title,fname,lname,country,email,gender,mobileNo,password,pincode,state,address,city}=this.state.user;
      let {titleList,stateList}=this.state;
      
       return (
           <React.Fragment>
               <div className="row fontstyle mt-1 mb-1">
                   <div class="col">Edit PROFILE</div>
               </div>
               <div className="row border mx-1 mb-3 fontstyle">
                   <div className="col-12">
                   <div className="row  mt-2 ">
                   <div class="col-lg-2  col-12">User Name:<span className="text-danger">*</span></div>
                    <div className="col-3 " >
                        {this.getDD(titleList,title,"title","Title")}
                        
                    </div>
                    <div className="col-3 text-center" >
                       <div className="form-group">
                           <input className="form-control"
                            type="text"
                            value={fname}
                             name="fname"
                             onChange={this.handleChange}
                           />
                       </div>
                    </div>
                    <div className="col-3 text-center" >
                    <div className="form-group">
                           <input className="form-control"
                            type="text"
                            value={lname}
                             name="lname"
                             onChange={this.handleChange}
                           />
                       </div>
                    </div>
                   </div>

                   <div className="row mt-1">
                       <div className="col-lg-2">
                           Email:
                       </div>
                       <div className="col-lg-10 col-12">
                       <input className="form-control"
                            type="text"
                            value={email}
                            readOnly={true}
                             name="email"
                             onChange={this.handleChange}
                           />
                           
                       </div>
                   </div>
                   <div className="row mt-3">
                       <div className="col-lg-2">
                           Password:
                       </div>
                       <div className="col-lg-10 col-12">
                       <input className="form-control"
                            type="text"
                            value={password}
                            readOnly={true}
                             name="password"
                             onChange={this.handleChange}
                           />
                           
                       </div>
                   </div>

                   <div className="row mt-3">
                       <div className="col-lg-2">
                           Phone:<spna className="text-danger">*</spna>
                       </div>
                       <div className="col-lg-10 col-12">
                       <input className="form-control"
                            type="number"
                            value={mobileNo}
                           
                             name="mobileNo"
                             onChange={this.handleChange}
                           />
                           
                       </div>
                   </div>

                   <div className="row mt-3">
                       <div className="col-lg-2">
                           Address:<spna className="text-danger">*</spna>
                       </div>
                       <div className="col-lg-5 col-12">
                       <input className="form-control"
                            type="text"
                            value={country}
                            readOnly={true}
                             name="country"
                             onChange={this.handleChange}
                           />
                           
                       </div>
                       <div className="col-lg-5 col-12">
                      
                           {this.getDD(stateList,state,"state","Select State")}
                       </div>
                   </div>

                   <div className="row mt-2">
                       <div className="col-lg-2">
                          
                       </div>
                       <div className="col-lg-5 col-12">
                       <input className="form-control"
                            type="text"
                            value={city}
                            placeholder="Enter city"
                           
                             name="city"
                             onChange={this.handleChange}
                           />
                           
                       </div>
                       <div className="col-lg-5 col-12">
                       <input className="form-control"
                            type="text"
                            value={pincode}
                            placeholder="Enter pincode"
                           
                             name="pincode"
                             onChange={this.handleChange}
                           />
                       </div>
                   </div>

                   <div className="row mt-3">
                       <div className="col-12">
                           <textarea
                           className="form-control"
                           value={address}
                           rows="3"
                           placeholder="Enter Address...."
                           name="address"
                           onChange={this.handleChange}
                           >

                           </textarea>
                       </div>
                   </div>


                   <div className="row mt-4 mb-5 pb-2">
                       <div className="col-6 text-right">
                         <button onClick={this.handleSubmit}
                         className="btn btn-outline-danger btn-md"
                         >Submit</button>
                       </div>
                       <div className="col-6 text-left">
                         <button onClick={()=>this.showEditForm()}
                         className="btn btn-outline-danger btn-md"
                         >Cancel</button>
                       </div>
                   </div>






                   </div>
                   
               </div>
           </React.Fragment>
       )
    }
    profileInfo=()=>{
        let {title,fname,email,mobileNo,lname}=this.state.user;
        return (
            <React.Fragment>
                <div className="row"><div className="ml-3 mt-1 mb-1 fontstyle">MY PROFILE</div></div>

                <div className="ml-1 mt-1 mr-1 row border mb-4">
                    <div className="col-6 mt-4 ml-3 fontstyle">
                        <div className="row">
                            <div className="col-1">
                            <i className="fa fa-user-circle upsfuib " ></i>
                            </div>
                            <div className="col " >
                                  
                        <strong className="mufns"> {"  "+title+" "+fname+" "+lname}</strong>
                           </div>
                            </div>
                        </div>
                      
                   
                    <div className="col-3 text-right mt-4">
                        <i onClick={()=>this.showEditForm()} className="fa fa-pen-square upsfuib1"></i>
                    </div>
                    <div className="col-12 upsfuib2 fontstyle">
                        <i className="fa fa-envelope"></i><span className="mufns1" >  {email}</span>
                    </div>
                    <div className="col-12"></div> 
                    <div className="col-12 upsfuib2 fontstyle">
                        <i className="fas fa-phone"></i><span className="mufns1">  {mobileNo}</span>
                    </div> 
                    <div className="col-12">
                    </div>
                     <div className="col-12 upsfuib2 fontstyle">
                     <i class="fa fa-map-marker mr-2"></i>
                           India
                     </div> 
                </div>

                <div className="row"><div className="ml-3 mt-2 mb-1 fontstyle">GST DETAILS</div></div>

                <div className="ml-1 mt-1 mr-1 row border mb-4 fontstyle">
                    <div className="mt-4 ml-3 col-1">
                        <i className="fas fa-money-check-alt upsfuib3" ></i>
                    </div>
                    <div className="mt-4  col-5 fontstyle"><b>GST Number</b></div>
                    <div className="col-8 text-right mt-4">
                        <i className="fa fa-plus-square upsfuib4" ></i>
                    </div>
                    <div className="upsfuib5 col-12">Add GST Details</div>
                </div>

                <div className="row"><div className="ml-3 mt-2 mb-1 fontstyle">SAVED TRAVELLERS</div></div>

                <div className="ml-1 mt-1 mr-1 row border mb-4 fontstyle">
                    <div className="mt-4 ml-3 col-1">
                        <i className="fa fa-users upsfuib3" ></i>
                    </div>
                    <div className="mt-4  col-5 fontstyle"><b>No Travellers added</b></div>
                    <div className="col-8 text-right mt-4">
                        <i className="fa fa-plus-square upsfuib4" ></i>
                    </div>
                    <div className="upsfuib5 col-12">Add Traveller for a faster booking experience.</div>
                </div>

            </React.Fragment>
        )
    }
    render() {
        let {edit}=this.state;
        let {title,fname,email,mobileNo,lname}=this.state.user;
        return (
            <div className="container-fluid backgcolor">
                <div className="row">
                     <div className="col-lg-9 col-12">
                         <div className="row mt-3 mb-2">
                             <div className="col-9 ml-1">Dashboard /
                             <Link to="/yatra/profile"> Your Profile</Link> 
                             </div>
                        </div>
                        <div className="row  ml-1" id="payment">
                        <div className="col-lg-3  unavstyle" >
                            <UserNavBar str="/yatra/profile" goToLink={this.goToLink}/>
                        </div>
                         <div className="col-9">
                                <div className="row bg-light">
                                    <div className="col-12 ml-2 mt-2 mb-2 fontstyle">MY ACCOUNT</div>
                                </div>
                                {edit?this.profileForm():this.profileInfo()}
                               
                         </div>

                        </div>
                     </div>
                    


                     <div className="col-lg-2 col-12">
                         <div className="row ml-1 fontstyle sectionpf">
                             <div className="col p-0 m-0">
                                 <div className="row">
                                     <div className="col-12 text-center uprosps">
                                         <i className="fa fa-user-circle"></i>
                                     </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {title+" "+fname+" "+lname}
                                    </div>
                                </div>
                                <div className="row">
                                    <hr className="col-9 text-center"/>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        {email}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 text-center uprosps1" >
                                        Phone: {mobileNo}
                                   </div>
                               </div>
                           </div>
                        </div>
                    </div>








                </div>

                <Footer/>
            </div>
          )
        }
}
export default UserProfile;