import axios from "axios";
let baseurl="https://evening-brook-89374.herokuapp.com";
function get(url){
    return axios.get(baseurl + url);
}
function post(url,obj) {
    return axios.post(baseurl+url,obj);
}
function put(url,obj) {
    return axios.put(baseurl + url,obj);
}
function deletePer(url) {
    return axios.delete(baseurl + url);
}
export default {
    get,
    post,
    put,
    deletePer,
}

