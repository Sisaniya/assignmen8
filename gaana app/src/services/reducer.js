const initialState={
   music:"",
csong:"",
status:0,
duration:"",
search:0,
addsong:null,
playlist:"",
pathname:"/home",
}
const reducer= (state=initialState,action)=>{
 let {music,csong,status}=state;

 if(action.song && action.type=="Player")
 {
  
   let playlist=action.playlist?action.playlist:"";
  if(action.song.url == csong.url)
  {

    if(status)
    {
       state.music.pause();
      return {...state,status:0,duration:music.duration,playlist:playlist}
      
    }
    else
    {
      state.music.play();
      return {...state,status:1,duration:music.duration,playlist:playlist}
    }
    
  }
  else
  {
      if(music)
      {
        if(status)
        {
          state.music.play();
         state.music.pause();
        }
      
      }
      music=new Audio(action.song.url);
      music.play();
       return {...state,music:music,csong:action.song,status:1,duration:music.duration,playlist:playlist}
  }
}
else if(action.type=="PlayMusic")
{
  let {music,csong,status}=state;
 
  if(action.song.url == csong.url)
  {
      
    if(status)
    {
     
      return {...state} 
    }
    else
    {
      state.music.play();
      return {...state,status:1,duration:music.duration} 
    }
  
    
  }
  else
  {
      if(music)
      {
        if(status)
        {
          state.music.play();
         state.music.pause();
        }
      
      }
      music=new Audio(action.song.url);
      music.play();
       return {...state,music:music,csong:action.song,status:1,duration:music.duration}
  }
}
else if(action.type=="Search")
{
  return {...state,search:action.value}
}
else if(action.type=="AddSong")
{
  return {...state,addsong:action.song}
}
else if(action.type=="RemoveSong")
{
  return {...state,addsong:null}
}
else if(action.type=="PathName")
{
  return {...state,pathname:action.pathname}
}
else
   return state;

}

export default reducer;