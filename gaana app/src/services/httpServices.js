import axios from "axios";
let baseurl="http://localhost:2410";
function get(url){
    return axios.get(baseurl + url);
}
function getData(url,obj)
{
    return axios.get(baseurl+url,obj);
}
function post(url,obj) {
    return axios.post(baseurl+url,obj);
}
function put(url,obj) {
    return axios.put(baseurl + url,obj);
}
function deleteP(url) {
    return axios.delete(baseurl + url);
}
function logout(url,obj) {
    return axios.delete(baseurl+url,obj);
}
export default {
    get,
    post,
    put,
    deleteP,
    getData,
    logout,
}

