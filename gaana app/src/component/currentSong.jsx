import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";

import http from "../services/httpServices";

class CurrentSong extends Component
{
   state={
      favoriteSongs:[],
      
   }
   async componentDidMount() {
     let user=localStorage.getItem("user");
     let response=await http.getData("/favoritesongs",{headers:{Authorization:user}});
     let {data}=response;
     this.setState({favoriteSongs:data});
   }
   async postFavorite(obj) {
    try {
        let response=await http.post("/favoritesongs",obj);
         let {data}=response;
         console.log(data);
         this.setState({...this.state,favoriteSongs:data});
    }
    catch(ex)
    {

    }
}
updateFavoriteList=()=>{
  
    let email=localStorage.getItem("email");
    let obj={...this.props.musicInfo.csong};
    let user=localStorage.getItem("user");
    if(user)
    {

        this.postFavorite({song:obj,email:email});

    }
    else
    {
        window.alert("Login to continue");
    }
   
}
   async getNextSong(id)
{
  try
  {
    let response=await http.get(`/nextsong/${id}`);
    let {data}=response;
    console.log(data);
    this.props.setMusic(data);
  }
  catch(ex)
  {
    console.log(ex.response);
  }
  
}
async getPreSong(id)
{
  let response=await http.get(`/presong/${id}`);
  let {data}=response;
 
  this.props.setMusic(data);
}
playNextSong=(id)=>
{
  console.log("getting next song");
  this.getNextSong(id);
}
playPreSong=(id)=>
{
  this.getPreSong(id);
}
    
   render() {
    let {musicInfo}=this.props;
    let {favoriteSongs}=this.state;
       
      
        return (
            <React.Fragment>
                 
            <div className={"row fixed-bottom bg-white mx-2 "+(musicInfo.csong?"":"d-none")}>
                <div className="col-3 bdr m-0 ">
                          <div className="row mt-1">
                                    <div className="col-2 mt-1">
                                        <Link to={`/song/${musicInfo.csong.name}`}>
                                        
                                    <img src={musicInfo.csong.img} width="35" height="35"   className="cps "/>
                                    </Link>   
                                    </div>
                                    <div className="col-6 " style={{overflow:"hidden"}}>
                                    <Link className="bottomboxtextLink" to={`/song/${musicInfo.csong.name}`}>
                                    <div className="  cps" >{musicInfo.csong.name}</div>
                                    </Link>
                                    
                                    {musicInfo.csong.name? musicInfo.csong.name.length < 17 ?
                                    (
                                      <div className="text-muted mt-0 bottomboxtext">{musicInfo.csong.name}</div>
                                    ):"":""}                                  
                                    </div>
                                    <div className="col-4 text-right">
                                      <div className="row">
                                        <div className="col-6 ">
                                       
                                        <i onClick={()=>this.updateFavoriteList()} class={" fa-heart bottombarheart  cps "+(favoriteSongs.findIndex(ele=>ele.id == musicInfo.csong.id)>=0?"heartcolor fas":"heartstyle1 far")}></i>
                                        </div>
                                        <div className="col-6 pt-1">
                                        <svg width="24" height="24" viewBox="0 0 24 24" className="more-player-option player-dots" title="More Options"><g className="bottommenustyle" fill-rule="evenodd"><path d="M7 12a2 2 0 1 1-3.999.001A2 2 0 0 1 7 12M14 12a2 2 0 1 1-3.999.001A2 2 0 0 1 14 12M21 12a2 2 0 1 1-3.999.001A2 2 0 0 1 21 12"></path></g></svg>
                                      
                                        </div>
                                      </div>
                                      
                                     
                                    </div>
                        </div>
                </div>

                <div className="col-4">
                    <div className="row">
                      <div className="col-6">
                        <span>
                        <svg onClick={()=>this.playPreSong(musicInfo.csong.id)} className="bottomplayPrevSvg cps" width="20" height="20" viewBox="0 0 24 24"><path className="bottomplayPrev" fill-rule="evenodd" d="M7.556 19.222V5H4v14.222h3.556zm12.444 0V5L9.333 12.111 20 19.222z"></path></svg>
                        </span>
                         <span>
                         <span className=" bottomplayiconbox" onClick={()=>this.props.setMusic(musicInfo.csong)}>
                           {musicInfo.status?(
                            <svg width="14" height="14" viewBox="0 0 24 24" className="playsvg"><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                           ):(
                            <svg width="14" height="14" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                         
                           )}
                         
                         </span>
                         </span>
                         <span>
                         <svg onClick={()=>this.playNextSong(musicInfo.csong.id)} className="bottomplayNextSvg cps" width="20" height="20" viewBox="0 0 24 24"><path class="bottomplayNext" fill-rule="evenodd" d="M16.444 19.222V5H20v14.222h-3.556zM4 19.222V5l10.667 7.111L4 19.222z"></path></svg>
                         </span>
                      </div>
                      
                    </div>
                </div>
                <div className="col-5">
                  <div className="row pt-2">
                    <div className="col-3 bdr">
                      <div className="row">

                     
                      <div className="volumeiconbox col-4">
                         <i className="fas volumeicon fa-volume-down"></i>
                      </div>
                      <div className=" col-4">
                      <svg className=""  width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><defs></defs><g id="Navigation---v2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="player-states" transform="translate(-1355.000000, -278.000000)" fill="#FFFFFF"><g id="Now-Playing---Track" transform="translate(0.000000, 199.000000)"><g id="component/darkui/player-v2-copy-4" transform="translate(0.000000, 54.000000)"><g id="Player---v2"><g id="More-Controls" transform="translate(1324.000000, 25.000000)"><g id="icon/shuffle" transform="translate(31.000000, 0.000000)"><path class="fill_path" d="M10.5937499,9.40725781 L9.18749973,10.768145 L4,5.747984 L5.40625013,4.38709677 L10.5937499,9.40725781 Z M14.5,4.38709677 L20,4.38709677 L20,9.70967742 L18,7.77419355 L5.40625013,19.8709677 L4,18.5100805 L16.5,6.32258065 L14.5,4.38709677 Z M14.8125003,13.4899195 L18,16.3931453 L20,14.4576614 L20,19.7802421 L14.5,19.7802421 L16.5,17.8447582 L13.4062501,14.8508067 L14.8125003,13.4899195 Z" id="Shape"></path></g></g></g></g></g></g></g></svg>
                      </div>
                      <div className=" col-4">
                      <svg class="repeatallsvg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><defs></defs><g id="Navigation---v2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="player-states" transform="translate(-1391.000000, -278.000000)" fill="#FF3C00"><g id="Now-Playing---Track" transform="translate(0.000000, 199.000000)"><g id="component/darkui/player-v2-copy-4" transform="translate(0.000000, 54.000000)"><g id="Player---v2"><g id="More-Controls" transform="translate(1324.000000, 25.000000)"><g id="icon/repeat" transform="translate(71.000000, 4.000000)"><path className="fill_path" d="M0,8.12903226 C0,6.37499974 0.567708267,4.80745961 1.7031248,3.42641135 C2.83854187,2.0453631 4.27083307,1.13306426 6,0.689516387 L6,2.62500026 C4.83333333,3.06854813 3.875,3.78931458 3.125,4.78729858 C2.375,5.78528206 2,6.89919381 2,8.12903226 C2,9.78225806 2.6041664,11.1330643 3.81250027,12.1814514 L6,10.0645161 L6,15.8709677 L0,15.8709677 L2.40625013,13.5423386 C0.8020832,12.1108872 0,10.3064516 0,8.12903226 L0,8.12903226 Z M16,0.387096774 L13.5937499,2.80645161 C15.1979168,4.25806452 16,6.06250013 16,8.21975794 C16,9.99395148 15.4322917,11.5715726 14.2968752,12.9526209 C13.1614581,14.3336692 11.7291669,15.2459675 10,15.6895164 L10,13.6330643 C11.1666667,13.1895164 12.125,12.4687499 12.875,11.4707659 C13.625,10.4727825 14,9.35887071 14,8.12903226 C14,6.47580645 13.3958336,5.12500026 12.1874997,4.07661316 L10,6.19354839 L10,0.387096774 L16,0.387096774 Z" id="Shape"></path></g></g></g></g></g></g></g></svg>
                      </div>
                      </div>
                    </div>
                    <div className="col-3 bdr ">
                        <div className="qualitybtn">
                          <span className="qualitybtntext">HIGH</span>
                          <span className="arrow-up"></span>
                        </div>
                    </div>
                    <div className="col-4 pl-4" >
                    <span className="qualitybtntext">AUTOPLAY</span>
                    <div className="autoplaybtn">
                          <span className="autoplaytexton">ON</span>
                        
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </React.Fragment>
           
        )
   }
}
const mapStoreToProps=(store)=>{
  console.log(store.duration);
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      duration:store.duration

  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(song)=>dispatch({type:"Player",song:song}),
      getDuration:()=>dispatch({type:"Duration"})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(CurrentSong);