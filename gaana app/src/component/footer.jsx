import React,{Component} from "react";
class Footer extends Component 
{
    state={}
    render() {
        return (
            <div className="row bgcolor">
                <div className="col-12">
                    <hr className="my-4"/>
                    <div className="row mx-4">
                        <div className="col-8">
                        <div className="s_lt"> Gaana is the one-stop solution for all your music needs. Gaana offers you free, unlimited access to over 45 million Hindi Songs, Bollywood Music, English MP3 songs, Regional Music &amp; Mirchi Play. </div>
                        </div>
                        <div className="col-4">
                            <div className="row">
                                <div className="col-2"></div>
                                <div className="col-5 text-right followname">Follow Us</div>
                                <div className="col-2 ">
                                    <div className="facebook">
                                <svg width="13" height="19" viewBox="0 0 10 16"> <path className="footericon" fill-rule="evenodd" d="M2.932 16l-.023-7.273H0V5.818h2.91V4c0-2.7 1.67-4 4.078-4 1.154 0 2.145.086 2.434.124v2.821h-1.67c-1.31 0-1.563.623-1.563 1.536v1.337H10l-1.455 2.91H6.19V16H2.932z"></path> </svg>
                                </div>
                                </div>
                                <div className="col-2">
                                    <div className="twitter">
                                <svg  width="20" height="16" viewBox="0 0 20 16"> <path className="footericon" fill-rule="evenodd" d="M18.67.296a7.744 7.744 0 0 1-2.503.978A3.885 3.885 0 0 0 13.292 0C11.12 0 9.355 1.809 9.355 4.04c0 .316.033.623.101.919C6.182 4.79 3.28 3.184 1.336.737A4.112 4.112 0 0 0 .803 2.77c0 1.4.695 2.637 1.753 3.362A3.867 3.867 0 0 1 .77 5.628v.05c0 1.957 1.358 3.59 3.162 3.96a3.838 3.838 0 0 1-1.78.07c.502 1.605 1.956 2.773 3.68 2.805A7.779 7.779 0 0 1 0 14.187 10.965 10.965 0 0 0 6.038 16c7.247 0 11.208-6.154 11.208-11.492 0-.176-.003-.351-.01-.523a8.08 8.08 0 0 0 1.964-2.09 7.715 7.715 0 0 1-2.262.635A4.031 4.031 0 0 0 18.67.296"></path> </svg>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className="my-4"/>
                    <div className="row s_lt text-center">
                        <div className="col-12 mt-3 mb-2">
                             Advertise on Gaana.com | Terms of Use | Privacy Policy | Feedback | Report an issue | Partners
                        </div>
                        <div className="col-12  mb-2">
                             Sitemap | FAQ
                        </div>

                        <div className="col-12 mb-2">© Gamma Gaana Ltd. 2021, All Rights Reserved</div>
                    </div>
                </div>
                
            </div>
        )
    }
}
export default Footer;