import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
class CSVUpload extends Component
{
   
    state={
        show:0,
        file:null,
    }

    async componentDidMount() {
        
    }
    handelChange=(e)=>{
       
        console.log(e.target.files);
        let s={...this.state};
        s.file=e.target.files[0];
        if(s.error)s.error="";
        this.setState(s);
    }
  
   
    async uploadFile(formData)
    { try
        {
         let response=await httpServices.post("/csvUpload",formData);
         
                window.alert(response.data);
        }
        catch(ex)
        {
            if(ex.response && ex.response.status == 400)
            {
             this.setState({error:ex.response.data});
            }
        }
         
    }
    handelSubmit=(e)=>{
        e.preventDefault();
        e.stopPropagation();
        let s={...this.state};
           var formData=new FormData();
           
         formData.append("myfile",s.file);
         formData.append("name","rohit");
         console.log(formData.getAll("myfile"),formData.getAll("name"),FormData);
      this.uploadFile(formData);

    }
  
    render() {
        let {file}=this.state;
        let {error=""}=this.state;
        return (
            <React.Fragment>
                   <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="csvupload"/>
                    </div>
                    <div className="col-8 bgcoloradmin ">
                    

                    <div className="row mt-3   justify-content-center">
                   
                    <div className="col-12 text-center mt-3 mb-2">
                        <span className="pcsvuphs">Import CSV File!</span>
                    </div>
                    <div className="col-6 text-center">
                       
                        <input
                        className=""
                        type="file"
                        name="file"
                        
                        onChange={this.handelChange}
                         />
                        {error && <span className="text-danger">{error}</span>}
                      
                      
                        <button className="btn  btn-danger mt-2 px-3" disabled={!file} onClick={this.handelSubmit}>
                            Upload now!
                        </button>
                    </div>
                </div>

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default CSVUpload;