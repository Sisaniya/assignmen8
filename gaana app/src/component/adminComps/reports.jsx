import React,{Component} from "react";
import {Link} from "react-router-dom";
import AdminNavBar from "./adminNavbar";

import FileDownload from "js-file-download";
import httpServices from "../../services/httpServices";
import AdminAccount from "./adminAccount";
import AdminNavbar from "./adminNavbar";
class Reports extends Component
{
    state={
       data:"",
       str:"",
     
    }
    componentDidMount() {
         
    }
    componentDidUpdate(prevProps,preState) {
        
    }
    async fetchPlayedSongsReport() {
        let user=localStorage.getItem("user")
        console.log(user);
        let response=await httpServices.getData("/mostplayedsongs",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:"most_played_songs.csv"});
    }
    getMostPlayedSongs=()=>{
        this.fetchPlayedSongsReport();
    }
    async fetchFavoriteSongsReport() {
        let user=localStorage.getItem("user")
        console.log(user);
        let response=await httpServices.getData("/mostfavoritesongs",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:"most_favorite_songs.csv"});
    }
    getMostFavoriteSongs=()=>{
        this.fetchFavoriteSongsReport();
    }
    async fetchSearchedSongsReport() {
        let user=localStorage.getItem("user")
        console.log(user);
        let response=await httpServices.getData("/mostsearchedsongs",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:"most_searched_songs.csv"});
    }
    getMostSearchedSongs=()=>{
        this.fetchSearchedSongsReport();
    }
    async fetchMostPlayedChartsReport() {
        let user=localStorage.getItem("user")
        console.log(user);
        let response=await httpServices.getData("/mostplayedcharts",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:"most_played_charts.csv"});
    }
    getMostPlayedChart=()=>{
        this.fetchMostPlayedChartsReport();
    }
    downloadReportData=()=>{
        let {data,str}=this.state;
        if(str)
        {
            FileDownload(data,str);
        }
        else
        {
            FileDownload(data,'report.csv');
        }
      
      
    }
    render() {
      let {data}=this.state;
      console.log(data);
        return (
            <React.Fragment>
               <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="reports"/>
                    </div>
                    <div className="col-8 bgcoloradmin pt-4 ">
                    
                        {data?(
                        <button onClick={()=>this.downloadReportData()}  className="btn btn-warning btn-sm mt-2 ml-4">Click To Download CSV</button>
                        ):(
                            <div className="row mx-5 pt-3 text-center">
                                <div className="col-6 ml-3 fontStyle rpsfbs" >
                                Most Played Songs
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getMostPlayedSongs( )} style={{fontSize:"10px"}}  className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                                <div className="col-6 ml-3 rpsfbs" >
                                Most Favorite Songs
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getMostFavoriteSongs()} style={{fontSize:"10px"}}  className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                                <div className="col-6 ml-3 rpsfbs" >
                                Most Searched Songs
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getMostSearchedSongs()} style={{fontSize:"10px"}}  className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                                <div className="col-6 ml-3 rpsfbs" >
                                Most Played Charts
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getMostPlayedChart()} style={{fontSize:"10px"}}  className="btn btn-danger btn-md mb-1 pt-2 text10">Download Report</button>
                                </div>
                            </div>
                            
                        )}
                      

                       
                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Reports;

