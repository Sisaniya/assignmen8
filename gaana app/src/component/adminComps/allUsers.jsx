import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
class ManageUsers extends Component
{
    state={
        users:[],
    }
    async componentDidMount() {
        try
        {
            let user=localStorage.getItem("user");
              let response=await httpServices.getData("/users",{headers:{Authorization:user}});
              let {data}=response;
              console.log(data);
              this.setState({users:data});
        }
        catch(ex)
        {
           console.log(ex);
        }
    }
    goToEditUser=(id)=>{
        this.props.history.push(`/edituser/${id}`);
    }
    render() {
       let {users}=this.state;
       
       console.log(users);
        return (
            <React.Fragment>
                <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="allusers"/>
                    </div>
                    <div className="col-8 bgcoloradmin  ">
                    
                      <div className="row mx-3 mt-3">
                            <div className="col-12">
                             <h5>All Users</h5>
                            
                           </div>
                          <div className="col-12">
                              <div className="row border py-2 bg-light ">
                                  <div className="col-2">Name</div>
                                  <div className="col-3">Email</div>
                                  <div className="col-3">Password</div>
                                  <div className="col-2">role</div>
                                  <div className="col-2">
                                      
                                  </div>
                                  
                              </div>
                              {users.map(ele=>{
                                  return (
                                      <div className="row border">
                                          <div className="col-2">{ele.name}</div>
                                          <div className="col-3">{ele.email}</div>
                                          <div className="col-3">{ele.password}</div>
                                          <div className="col-2">{ele.role}</div>
                                          <div className="col-2">
                                          <button onClick={()=>this.goToEditUser(ele.id)} className="btn btn-danger text-light my-1">
                                                  Edit
                                          </button>
                                          </div>

                                      </div>
                                  )
                              })}
                          </div>
                      </div>
                     
                      <hr className="my-2"/>

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default ManageUsers;