import React,{Component } from "react";
import {Link} from "react-router-dom";
import http from "../../services/httpServices";
class AdminNavbar extends Component
{
    state={
        show:"",
    }
    showDropdown=(str)=>{
        let s={...this.state};
        if(str==s.show)
        {
            s.show="";
        }
        else
        {
            s.show=str;
        }
        this.setState(s);
    }
  goToUrl=(str)=>{
      window.location=str;
  }
    render() {
        let {show}=this.state;
        let {active}=this.props;
       
        console.log(active);
        return (
            <React.Fragment>
               <Link className={"admin-nav-link  "} to="/account">
                <div className="row  admin-nbar-style">
                     <div className="col-4 py-3 admin-nbar-img">
                        <img className="d-block mx-auto userimg" width="40" height="40" src="https://a10.gaanacdn.com/images/users/496/crop_110x110_177224496.jpg"/>
                     </div>
                     
                     <div  className="col-8  mt-0  adminnavbarwel">
                          Hello Admin
                     </div>
                    
                 </div>
                 </Link>
               
               
                <div className="row py-3 admin-nav-item anactive">
                     
                     <div className="col-8  ml-4">
                          Upload Songs
                     </div>
                     <div className="col-3 text-right">
                         {show=="upload" ?
                           <i onClick={()=>this.showDropdown("upload")} className="fas fa-chevron-up cps"></i>
                           :<i onClick={()=>this.showDropdown("upload")} className="fas fa-chevron-down cps"></i>
                         }
                       
                     </div>
                 </div>
                 {show=="upload"?(<React.Fragment>
                 <Link className={"admin-nav-link  aactive"} to="formUpload">
                <div className="row ">
                    
                     <div className={"col-11 pl-5   py-3 admin-nav-subitem "+(active=="formupload"?"aactive":"anactive")}>
                          Form Upload
                     </div>
                   
                 </div>
                </Link>

                <Link className={"admin-nav-link  anactive "} to="/csvUpload">
                <div className="row ">
                    
                     <div className={"col-11 pl-5   py-3 admin-nav-subitem "+(active=="csvupload"?"aactive":"anactive")}>
                          CSV Upload
                     </div>
                   
                 </div>
                </Link>
                </React.Fragment>):""}
               

                

                <Link className="admin-nav-link  " to="/managesongs">
                <div className={"row py-3 admin-nav-item "+(active=="downloadsongs"?"aactive":"anactive")}>
                     
                     <div className="col ml-4">
                          Download Songs
                     </div>
                 </div>
                </Link>

               
                <div className="row py-3 admin-nav-item  anactive">
                     
                <div className="col-8  ml-4">
                        Manage Users
                     </div>
                     <div className="col-3 text-right">
                         {show=="users"?
                           <i onClick={()=>this.showDropdown("users")} className="fas fa-chevron-up cps"></i>
                           :<i onClick={()=>this.showDropdown("users")} className="fas fa-chevron-down cps"></i>
                         }
                       
                     </div>
                 </div>

                 {show=="users"?(<React.Fragment>
                 <Link className={"admin-nav-link  "+(active=="newuser"?"aactive":"aactive")} to="/newuser">
                <div className="row ">
                    
                     <div className={"col-11 pl-5   py-3 admin-nav-subitem "+(active=="newuser"?"aactive":"anactive")}>
                          Add New Users
                     </div>
                   
                 </div>
                </Link>

                <Link className="admin-nav-link  anactive" to="/users">
                <div className="row ">
                    
                     <div className={"col-11 pl-5   py-3 admin-nav-subitem "+(active=="allusers"?"aactive":"anactive")}>
                        All Users
                     </div>
                   
                 </div>
                </Link>
                </React.Fragment>):""}




                
               
                <div onClick={()=>this.goToUrl("/reports")} className={"row py-3 admin-nav-item cps  "+(active=="reports"?"aactive":"anactive")}>
                     
                     <div className="col ml-4">
                         Download Reports
                     </div>
                 </div> 
               

                <Link className="admin-nav-link  " to="/export">
                <div className={"row py-3 admin-nav-item "+(active=="exports"?"aactive":"anactive")}>
                     
                     <div className="col ml-4">
                         Export
                     </div>
                 </div> 
                </Link>

                <Link className="admin-nav-link  anactive">
                <div className="row py-3 admin-nav-item" onClick={()=>this.goToReset()}>
                     
                     <div className="col ml-4">
                         Reset
                     </div>
                 </div>
                </Link>

                
                <div onClick={()=>this.goToLogout()} className="row py-4 admin-nav-item  anactive">
                     
                     <div className="col ml-4">
                          Logout
                     </div>
                 </div>
               


              
                
            </React.Fragment>
        )
    }
    goToReset=()=>{
        window.location="/home";
    }
    goToLogout=()=>{
        this.logoutUser();
    }
    async logoutUser() {
        let user=localStorage.getItem("user");
        let role=localStorage.getItem("role");
        let response={};
        if(role=="admin")
        {
        response=await http.logout("/adminlogout",{headers:{Authorization:user}});
        }
        else
        {
        response=await http.logout("/logout",{headers:{Authorization:user}});
        }
        
       
        localStorage.removeItem("user");
        localStorage.removeItem("email");
        localStorage.removeItem("role");
        localStorage.removeItem("id");
        localStorage.removeItem("name");
        window.location="/home";
    }

}
export default AdminNavbar;