import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
class FormUpload extends Component
{
    state={
        song:{name:"",img:"",url:"",artist:""},
        error:{},
        edit:false,
    }
    async fetchData() {
        try
        {
              let {id}=this.props.match.params;
              if(id) {
               
                let response=await httpServices.get(`/getsong/${id}`);
                  let {data}=response;
                  this.setState({song:data,edit:true});
              }
              else
              {
                  let song={name:"",img:"",url:"",artist:""};
                  this.setState({song:song,edit:false});
              }
        }
        catch(ex) {
                 console.log(ex,"reques falid");
        }
    }
    componentDidMount() {
        this.fetchData();
    }
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s.song[input.name]=input.value;
        this.setState(s);

    }
    async postData(obj) {
           try {
               let response=await httpServices.post("/formUpload",obj);
               window.alert("Song uploaded successfully");
               console.log(response.data);
            

           }
           catch(ex)
           {
               if(ex.response && ex.response.status=="400")
               {
                   let s={...this.state};
                   s.error.name="Name already exists";
                   this.setState(s);
               }
           }
    }
    async putData(obj) {
        try {
            let response=await httpServices.put("/formUpload",obj);
            window.alert("Song updated successfully");
            console.log(response.data);
            this.props.history.push("/managesongs");

        }
        catch(ex)
        {
            if(ex.response && ex.response.status=="400")
            {
                let s={...this.state};
                s.error.name="Name already exists";
                this.setState(s);
            }
        }
 }
    handelSubmit=(e)=>{
        e.preventDefault();
        let {song,edit}=this.state;
        let error=this.validateForm(song);
        if(this.isValid(error))
        {
            if(edit)
            {
                this.putData(song);
            }
            else 
            this.postData(song);
        }
        else
        {
            this.setState({...this.state,error:error});
        }
        
    }
    isValid=(error)=>{
        let keys=Object.keys(error);
        let count=keys.reduce((acc,curr)=>error[curr]?acc+1:acc,0);
        return count == 0;
    }
    validateForm=(song)=>{
        let error={};
        error.name=!song.name?"Name is required":"";
        error.artist=!song.artist?"Artist name is required":"";
        error.img=!song.img?"Image url is required":"";
        error.url=!song.url?"Song url is required":"";
        return error;
    }
    isValidForm=()=>{
        let {song}=this.state;
        let error=this.validateForm(song);
        return this.isValid(error);
    }
    render() {
        let {name,img,url,artist}=this.state.song;
        let {error,edit,song}=this.state;
        console.log(song,edit);
        return (
            <React.Fragment>
                   <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="formupload"/>
                    </div>
                    <div className="col-8 bgcoloradmin ">
                    

                     <div className="row mx-4 mt-3">
                         <div className="col-12">

                             <h5>{edit?"Edit Song Details":"Add New Song Details"} </h5>
                            
                         </div>
                         <div className="col-12 mt-3">
                             <div className="form-group row">
                                 <label className="col-2 col-form-label">Name<span className="text-danger">*</span> :</label>
                                 <div className="col-7">
                                     <input className="form-control"
                                     name="name"
                                     value={name}
                                     onChange={this.handelChange}
                                     placeholder="Enter song name "
                                     />
                                     {error.name && <span className="text-danger">{error.name}</span>}
                                 </div>

                             </div>
                         </div>

                         <div className="col-12 mt-3">
                             <div className="form-group row">
                                 <label className="col-2 col-form-label">Artist Name<span className="text-danger">*</span> :</label>
                                 <div className="col-7">
                                     <input className="form-control"
                                     name="artist"
                                     value={artist}
                                     onChange={this.handelChange}
                                     placeholder="Enter artist name "
                                     />
                                      {error.artist && <span className="text-danger">{error.artist}</span>}
                                 </div>

                             </div>
                         </div>

                         <div className="col-12 mt-3">
                             <div className="form-group row">
                                 <label className="col-2 col-form-label">Name<span className="text-danger">*</span> :</label>
                                 <div className="col-7">
                                     <input className="form-control"
                                     name="img"
                                     value={img}
                                     onChange={this.handelChange}
                                     placeholder="Enter image URL... "
                                     />
                                      {error.img && <span className="text-danger">{error.img}</span>}
                                 </div>
                                 {img?<div className="col-3">
                                     <img src={img} className="d-block mx-auto rounded" width="150" height="150"/>
                                 </div>:""}

                             </div>
                         </div>


                         <div className="col-12 mt-3">
                             <div className="form-group row">
                                 <label className="col-2 col-form-label">Song URL<span className="text-danger">*</span> :</label>
                                 <div className="col-7">
                                     <input className="form-control"
                                     name="url"
                                     value={url}
                                     onChange={this.handelChange}
                                     placeholder="Enter Song URL ...  "
                                     />
                                      {error.url && <span className="text-danger">{error.url}</span>}
                                 </div>

                             </div>
                         </div>

                         <div className="col-9 ml-3 mt-4">
                             <div onClick={this.handelSubmit}  className="btn btn-danger btn-md">
                                 {edit?"Update":"Submit"}
                                 
                             </div>
                         </div>

 


                     </div>
                      <hr className="my-4"/>

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default FormUpload;