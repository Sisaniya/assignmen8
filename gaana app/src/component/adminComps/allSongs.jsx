import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
import {connect} from "react-redux";
class AllSongs extends Component
{
    state={
        songs:[],
    }
    async componentDidMount() 
    {   let response=await httpServices.get("/trendings");
         let {data}=response;
        
         this.setState({songs:data});
    }
    goToDownload=(id)=>{
         this.props.history.push(`/editsong/${id}`);
    }
    downloadSong=(obj)=>{
              const link = document.createElement('a');
                link.href =obj.url;
              link.setAttribute('download', obj.url);
                
                 link.click();
    }
    render() {
       let {songs}=this.state;
       let {musicInfo}=this.props;
        return (
            <React.Fragment>
                <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="downloadsongs"/>
                    </div>
                    <div className="col-8 bg-white bgcoloradmin">
                        <div className="row">
                            <div className="col-12 mx-5 mt-3">
                                <h5>All Songs</h5>
                            </div>
                        </div>
                  

                    <div className="row text-muted mx-5 mt-4 ">
                         <div className="col-1"></div>
                         <div className="col-4">TITLE</div>
                         <div className="col-3">ARTIST</div>
                         <div className="col-4"></div>
                     </div>
                    {songs.map(ele=>{
                        return (
                        <div className={"row fontstyle1 songrow  py-2 my-0 mx-5  border-top border-bottom "+((ele.name==musicInfo.csong.name)?"bg-white":"")}>
                            <div className="col-1">
                            <span className=" songlistplaybox" onClick={()=>this.props.setMusic(ele)}>
                              
                               <i className={"fas songlistplaybtn tablesongfontcolor "+((ele.name==musicInfo.csong.name && musicInfo.status)?"fa-pause":"fa-play")}></i>
                             </span>
                            
                            
                            </div>
                           
                            <div className="col-4  tablesongfont ">
                                <div className="row">
                                    <div className="col-2 m-0 p-0 pl-3">
                                    <img src={ele.img} width="33" height="33" style={{display:"inline-block"}} className="rounded m-0 p-0 d-block"/>
                                    </div>
                                    <div className={"col-9 pt-1 "+(ele.name == musicInfo.csong.name?"tablesongtitle":"")}>
                                    {ele.name}
                                    </div>
                                </div>
                              
                               
                                </div>
                            <div className="col-3 pt-1 tablesongfont tablesongfontcolor">{ele.artist.length > 21? ele.artist.substring(0,21)+"...":ele.artist}</div>
                            <div className="col-2 text-center ">
                                <button onClick={()=>this.goToDownload(ele.id)} className="btn btn-danger btn-sm">Edit</button>
                            </div>
                            <div className="col-2 text-center ">
                                <button onClick={()=>this.downloadSong(ele)} className="btn btn-warning btn-sm">Download</button>
                            </div>
                        </div>
 
                        )
                    })}
                    

                       

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(AllSongs);
