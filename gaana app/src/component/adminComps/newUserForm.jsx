import React,{Component} from "react";
import {Formik,Form,Field,ErrorMessage} from "formik";
import * as Yup from "yup";
class NewUserForm extends Component
{
    state={user:this.props.user};
    render() {
       
        let {user}=this.state;
        let {edit}=this.props;
        return (
            <React.Fragment>
                 <h5>{edit?"Edit User Details":"Add New User Details"} </h5>
            <Formik
            enableReinitialize
            initialValues={this.props.user}
            validationSchema={Yup.object().shape({
                name:Yup.string().required("Name is required"),
                role:Yup.string().required("Role of user is required"),
                email:Yup.string().required("Email of user is required"),
                password:Yup.string().min(6,"Password must contains at least 6 characters"),
                cpassword:Yup.string().oneOf([Yup.ref('password'),null],"password must be same").required("Confirm password is required")

            })}
            onSubmit={fields => {
                this.props.handelSubmit(fields);
            }}

            render={({errors,status,touched})=><Form>
                
                 <div className="form-group">
                   <label htmlFor="name">Name<span className="text-danger">*</span> :</label>
                   <Field type="text" className={"form-control "+(errors.name && touched.name ?"is-invalid":"")} name="name" />
                   <ErrorMessage name="name" component="div" className="invalid-feedback"/>
                 </div>
                 <div className="form-group">
                   <label htmlFor="email">Email<span className="text-danger">*</span> :</label>
                   <Field type="text" disabled={this.props.edit} className={"form-control "+(errors.name && touched.name ?"is-invalid":"")} name="email" />
                   <ErrorMessage name="email" component="div" className="invalid-feedback" />
                   </div>
                    
                    <div className="form-group">
                   <label htmlFor="password">Password<span className="text-danger">*</span> :</label>
                   <Field type="password" className={"form-control "+(errors.name && touched.name ?"is-invalid":"")} name="password" />
                   <ErrorMessage name="password" component="div" className="invalid-feedback"/>
                   </div>
                   <div className="form-group">
                   <label htmlFor="cpassword">Comfirm Password<span className="text-danger">*</span> :</label>
                   <Field type="password" className={"form-control "+(errors.name && touched.name ?"is-invalid":"")} name="cpassword" />
                   <ErrorMessage name="cpassword" component="div" className="invalid-feedback"/>
                   </div>
                   <label htmlFor="role">Role<span className="text-danger">*</span> :</label>
                    <div className="form-check">
                  
                   <Field name="role" type="radio" value="user" className="form-check-input" />
                   <label className="form-check-label">User</label>
                   </div>
                   <div className="form-check">
                   <Field  type="radio" name="role" value="admin" className="form-check-input"/>
                   <label className="form-check-label">Admin</label>
                   <ErrorMessage name="role" component="div" className="invalid-feedback"/>
                   </div>
                   <div className="form-group mt-4">
                            <button type="submit" className="btn btn-danger mr-2">{edit?"Update":"Register"}</button>
                            {!edit?<button type="reset" className="btn btn-secondary">Reset</button>:""}
                  </div>
            </Form>}

            />
            </React.Fragment>
        )
    }
}
export default NewUserForm;