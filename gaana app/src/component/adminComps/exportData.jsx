import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
import FileDownload from "js-file-download";
class ExportData extends Component
{
    state={
        csv:"",
    }
    async getData() {
        try {
        let user=localStorage.getItem("user");
        let response=await httpServices.getData("/getAllDataCSV",{headers:{Authorization:user}});
        let {data}=response;
        this.setState({csv:data});
        console.log("data downloaded");
        }
        catch(ex) {
             console.log(ex);
        }
    }
    downloadData=()=>{
        console.log("Enter in downlaod data");
        this.getData();

    }
    goToDownloadData=()=>{
        let {csv}=this.state;
        FileDownload(csv,"alldata.csv");
    }
    render() {
       let {csv}=this.state;
       console.log(csv);

        return (
            <React.Fragment>
                <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="exports"/>
                    </div>
                    <div className="col-8 bgcoloradmin ">
                    

                     <div className="row mt-4 mx-3">
                         {csv?
                         <div className="col-12">
                         <button onClick={()=>this.goToDownloadData()} className="btn btn-warning btn-sm px-4">Click To Download Data</button>
                     </div>
                     :<div className="col-12">
                     <button onClick={()=>this.downloadData()} className="btn btn-danger btn-sm px-4">Export All Data</button>
                         </div>
                        }
                         
                     </div>

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default ExportData;