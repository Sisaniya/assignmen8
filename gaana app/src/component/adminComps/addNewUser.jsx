import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import AdminNavbar from "./adminNavbar";
import NewUserForm from "./newUserForm";
class AddNewUser extends Component
{
    state={
       user: {name:"",role:"",email:"",password:"",cpassword:""},
       edit:false,

    }
    async fetchData() {
        try
        {
              let {id}=this.props.match.params;
              if(id) {
                let user=localStorage.getItem("user");
                let response=await httpServices.getData(`/user/${id}`,{headers:{Authorization:user}});
                  let {data}=response;
                  this.setState({user:data,edit:true});
              }
              else
              {
                  let user={name:"",role:"",email:"",password:"",cpassword:""};
                  this.setState({user:user,edit:false});
              }
        }
        catch(ex) {

        }
    }
    componentDidMount() {
        this.fetchData();
    }
    async componentDidUpdate(prevProps,prevState) {
        if(prevProps != this.props)
        {
            this.fetchData();
        }
    }
    async postData(obj) {
         try {
            let response=await httpServices.post("/user",obj);
            let {data}=response;
            console.log(data);
            window.alert("User added successfully");
            this.props.history.push("/users");
         }
         catch(ex) {

         }
    }
    async putData(obj) {
        try {
            let response=await httpServices.put("/user",obj);
            let {data}=response;
            console.log(data);
            window.alert("User Update successfully");
            this.props.history.push("/users");
         }
         catch(ex) {

         }
    }
    handelSubmit=(user)=>{
      let {edit}=this.state;
      if(edit)
      {
        let obj={id:user.id,name:user.name,email:user.email,role:user.role,password:user.password};
          this.putData(obj);
      }
      else
      {
          let obj={name:user.name,email:user.email,role:user.role,password:user.password};
          this.postData(obj);
      }
     

    }
    render() {
        let {user,edit}=this.state;
        let u=edit?{...user,cpassword:user.password}:user;
        console.log(u);
        return (
            <React.Fragment>
                <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active="newuser"/>
                    </div>
                    <div className="col-8 bgcoloradmin ">
                    
                        <div className="row mx-3 mt-3">
                            <div className="col-12">
                            
                            
                             </div>
                             <div className="col-12">
                                 <NewUserForm user={u} edit={edit} handelSubmit={this.handelSubmit}/>
                             </div>
                        </div>

                      <hr className="my-2"/>

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default AddNewUser;