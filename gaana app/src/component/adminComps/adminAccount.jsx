import React,{Component} from "react";
import AdminNavbar from "./adminNavbar";
class AdminAccount extends Component
{
    state={}
    render() {
        let name=localStorage.getItem('name');
        let email=localStorage.getItem('email');
        console.log(name,email);
        return (
            <React.Fragment>
                <div className="row adminbgcolor">
                    <div className="col-12">
                <div className="row  mt-2 mx-3">
                    <div className="col-3 bg-white admin-navmaincol  mr-2">
                          <AdminNavbar active=""/>
                    </div>
                    <div className="col-8 bg-white ">
                    

                      <div className="row mt-4">
                          <div className="col-12 ">
                          <img className="d-block mx-auto userimg" width="160" height="160" src="https://a10.gaanacdn.com/images/users/496/crop_110x110_177224496.jpg"/>
                          </div>
                          <div className="col-12 namestyle text-center">
                              {name}
                          </div>
                          <div className="col-12 emailstyle text-center">
                              {email}
                          </div>
                      </div>

                      <hr className="my-2"/>

                       

                    </div>
                </div>
                </div>
                </div>
            </React.Fragment>
        )
    }
}
export default AdminAccount;