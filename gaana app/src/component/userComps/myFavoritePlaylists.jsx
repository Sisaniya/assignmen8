import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "../navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";

import httpServices from "../../services/httpServices";

class MyFavoritePlaylists extends Component
{
   state={
     playlists:[],
   }
   async componentDidMount() {
      let user=localStorage.getItem("user");
      if(user)
      {
        let response=await httpServices.getData("/favoriteplaylists",{headers:{Authorization:user}});
        let {data}=response;
        console.log(data);
        this.setState({playlists:data});
      }
   }
   async getPlaylistSongs(obj) {
    try {
        let response={};
        if(obj.createdBy=="gaana")
        {
          response=await httpServices.get(`/playlistsongs/${obj.name}`);
        }
        else
        {
          let user=localStorage.getItem("user");
          response=await httpServices.getData(`/userplaylistsongs/${obj.name}`,{headers:{Authorization:user}});

        }
     
       let {data}=response;
       console.log(data,obj.name);
       if(data.list)
       {
        this.props.setMusic({song:data.list[0],playlist:obj.name});
       }  
     this.setState({...this.state,playlist:obj});
      }
   catch(ex)
      {

      }
   }
   goToPlaySongFromPlaylist=(ele)=>{
        this.getPlaylistSongs(ele);
   }
  goToPlaylistSong=(ele)=>
  {
    if(ele.createdBy=="gaana")
    {
      this.props.history.push(`/playlist/${ele.name}`);
    }
    else
    {
      this.props.history.push(`/music/playlist/${ele.name}`);
    }
    
  }
   render() {
          let {playlists=[]}=this.state;
          console.log(playlists);
          let {musicInfo}=this.props;
         
          let defaultimg="https://a10.gaanacdn.com//media/images-v5/default-album-175x175.jpg";
          return (
            <div className="row">
                <div className="col-9 bgcolor2">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="90" className="mx-auto d-block" decoding="async" src="https://s0.2mdn.net/9427301/03Feb_21-HDFCLife-C2PL-DBM-SkyDiv-Pros-728x90.gif"/>
                          </div>
                      </div>
                     
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/home")}>Gaana</span>
                              <span className="dotstyle">.</span>
                             
                              <span className="songnamelinkstyle">Favorite Playlists</span>
                              <hr className="mt-0"/>
                          </div>
                      </div>

                      <div className="row mx-4">
                        <div className="col-12"><h6>FAVORITE PLAYLISTS</h6></div>
                      </div>

                      

                      <div className="row mx-4 mt-4 mb-4">
                      <div className="col-12 mt-2 trenimgmaincol">
               {playlists.map((ele,index)=>{
                   let {id,img,name}=ele;
                 
                 return (
                   <React.Fragment>
                    
                      <div className={"trenimgcol mr-4  trenimgmain "}>
                      <div className="picimgbody" onClick={()=>this.goToPlaylistSong(ele)}>
                        <img src={img || defaultimg} className="trendingimg picimgstyle"/><p/>
                        <div className="trensongname my-1 fontstyle">
                               {name}
                        </div>
                      </div>
                     
                     
                 <div className=" trenimgiconbody" onClick={()=>this.goToPlaySongFromPlaylist(ele)}>
                                  {ele.name == this.props.playlist && musicInfo.status ?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                                           </div>
                    </div>
                     
                   </React.Fragment>
                 

                 )
               })}
               </div>
                      </div>
                     

               </div>
                <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/9291847/0121_EN_LSS_APAC_StayAhead_LearnMore_300x600.png" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     
                   </div>
                  
                 </div>
           
            </div>
        )
   }
}
const mapStoreToProps=(store)=>{
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      playlist:store.playlist
  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(obj)=>dispatch({type:"Player",song:obj.song,playlist:obj.playlist}),
      addSong:(song)=>dispatch({type:"AddSong",song:song})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(MyFavoritePlaylists);
