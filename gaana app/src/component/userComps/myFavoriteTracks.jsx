import React,{Component} from "react";
import http from "../../services/httpServices";
import {connect} from "react-redux";

class MyFavoriteTracks extends Component
{
    state={
        favoriteSongs:[],
    }
    async componentDidMount() {
        let user=localStorage.getItem("user");
        let favoriteSongs=[];
        if(user)
        {
           let res=await http.getData("/favoritesongs",{headers:{Authorization:user}});
           favoriteSongs=[...res.data];
           this.setState({favoriteSongs:favoriteSongs});
        }

       
       
    }
    async postFavorite(obj) {
        try {
            let response=await http.post("/favoritesongs",obj);
             let {data}=response;
             console.log(data);
             this.setState({...this.state,favoriteSongs:data});
        }
        catch(ex)
        {

        }
    }
    updateFavoriteList=(song)=>{
       
        let email=localStorage.getItem("email");
        let obj={...song};
        let user=localStorage.getItem("user");
        if(user)
        {

            this.postFavorite({song:obj,email:email});

        }
        else
        {
            window.alert("Login to continue");
        }
       
    }
    addToPlayList=(song)=>{
        this.props.addSong(song);
        this.props.createPlaylist(1);
    }
    goToUrl=(name)=>{
        this.props.history.push(`/song/${name}`);
    }
    render() {
        let {favoriteSongs}=this.state;
        console.log(favoriteSongs)
        let {musicInfo}=this.props;
        return (
            <React.Fragment>
              <div className="row">
                  <div className="col-9 bgcolor4">
                       <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="100" className="mx-auto d-block rounded" decoding="async" src="https://tpc.googlesyndication.com/simgad/5953898921057943562?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&amp;rs=AOga4qn2IDHpwZmsIPuJvFHh9isPmRHh4w"/>
                          </div>
                      </div>

                        <div className="row mx-5">
                            <div className="col fsongsp">
                                FAVORITE SONGS
                            </div>
                        </div>

                        <div className="row mx-5 mt-4">
                            <div className="col-12">
                                <div className="row">
                                {favoriteSongs.map(ele=>{
                                    return (
                                        <div className="col-4  my-2">
                                           <div className="row fsmain-songbox">
                                               <div className="col-4 ">
                                                   <div className="fsmain-box" >
                                                   <img src={ele.img} onClick={()=>this.goToUrl(ele.name)} className="cps d-block rounded mx-auto" width="75" height="75" />
                                                      
                                                          <div className="fsmain-content" >
                                                                <span className=" fsbox-playicon" >
                                                                          {musicInfo.status && musicInfo.csong.url == ele.url?(
                                                                       <svg width="18" onClick={()=>this.props.setMusic(ele)} height="18" viewBox="0 0 24 24" className="playsvg"><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                                                         ):(
                                                                       <svg width="18" onClick={()=>this.props.setMusic(ele)} height="18" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                         
                                                                          )}
                         
                                                                 </span>
                                                           </div>
                                                  </div>
                                               </div>

                                               <div className="col-8 ">
                                                   <div className="row ">
                                                       <div className="col-12   fssong-name">
                                                           {ele.name? ele.name.length>15? ele.name.substring(0,16)+" ...":ele.name:""}
                                                       </div>
                                                       <div className="col-12 fsartist-name">
                                                           {ele.artist}
                                                       </div>
                                                       <div className="col-12 fslinks-icons ">
                                                          <div className="row">
                                                              <div className="col-3">
                                                              <i onClick={()=>this.updateFavoriteList(ele)} class={" fa-heart  cps heartcolor fas"}></i>
                                                              </div>
                                                              <div className="col-3">
                                                                     <svg onClick={()=>this.addToPlayList(ele)}  width="24" height="24" viewBox="0 0 24 24" className="addplaylist queue-addplaylisticon cps" data-value="song36368218" data-type="addtoplaylist" title="Add to Playlist"> <g fill="none" fill-rule="evenodd">
                                                                      <path d="M0 0h24v24H0z" data-value="song36368218" data-type="addtoplaylist"></path> 
                                                                       <path className="fill_path" data-value="song36368218" data-type="addtoplaylist" d="M21 22H7a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1zm-1.5-2a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11a.5.5 0 0 0 .5.5h11zm-5-2h-1a.5.5 0 0 1-.5-.5V15h-2.5a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5H13v-2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5V13h2.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H15v2.5a.5.5 0 0 1-.5.5zM4 5v14.778a2 2 0 0 1-2-2V3a1 1 0 0 1 1-1h14.778a2 2 0 0 1 2 2H5a1 1 0 0 0-1 1z"></path> </g>
                                                                      </svg> 
                                                              </div>
                                                              <div className="col-3">
                                                                   <svg width="20" height="20" viewBox="0 0 20 20" className="share option-queue-share" data-value="song36368218" data-type="share" title="Share">
                                                                         <path className="fill_path" data-value="song36368218" data-type="share" fill-rule="evenodd" d="M16 18a2 2 0 1 1 .001-4.001A2 2 0 0 1 16 18M4 12a2 2 0 1 1 .001-4.001A2 2 0 0 1 4 12M16 2a2 2 0 1 1-.001 4.001A2 2 0 0 1 16 2m0 10c-1.2 0-2.266.542-3 1.382l-5.091-2.546c.058-.27.091-.549.091-.836 0-.287-.033-.566-.091-.836L13 6.618C13.734 7.458 14.8 8 16 8c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4c0 .287.033.566.091.836L7 7.382A3.975 3.975 0 0 0 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382l5.091 2.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4"></path>
                                                                   </svg>
                                                              </div>
                                                          </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                        </div>
                                        
                                    )
                                })}
                                </div>
                            </div>
                        </div>

                        <hr className="mx-5 px-1 my-4"/>

 
                  </div>
                  


                  <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 ">
                     <div className="col-12 m-0 p-0 ">
                     <img className="m-0 p-0 " width="315" height="250" src="https://ss3.zedo.com/OzoDB/f/h/2851457/V4/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"/>
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>
            </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song}),
        addSong:(song)=>dispatch({type:"AddSong",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(MyFavoriteTracks);