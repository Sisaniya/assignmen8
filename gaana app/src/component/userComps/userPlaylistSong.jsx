import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../../services/httpServices";
import {connect} from "react-redux";
import {ClickOutsideListener} from "react-click-outside-listener";
import httpServices from "../../services/httpServices";

class UserPlaylistSong extends Component
{
    state={songs:[],
           csong:{},
            playlist:{},
            dropdownshow:0,
    };
    async deletePL(id) {
        try {
             let response=await httpServices.deleteP(`/deleteplaylist/${id}`);
                
               this.props.history.push("/music");
        }
        catch(ex) {

        }
    }
    deletePlaylist=(id)=>{
        this.deletePL(id);
    }
    showOptionsDropdown=()=>{
        let s={...this.state};
     
            s.dropdownshow=s.dropdownshow?0:1;
            this.setState(s);
       
        console.log(s.dropdownshow);
    }
    handelOutsideClick=()=>{
        let s={...this.state};
        if(s.dropdownshow)
        {
            s.dropdownshow=0;
            this.setState(s);
        }
        console.log(s.dropdownshow);
    }
    async componentDidMount() 
    {   let {name}=this.props.match.params;
        let user=localStorage.getItem("user");
         let response=await http.getData(`/userplaylistsongs/${name}`,{headers:{Authorization:user}});
         let {data}=response;
       
       
         this.setState({songs:data.list,csong:data.list[0],playlist:data.playlist});
    }
    async playMusic(song)
    {
       await this.props.setMusic(song);
    }
    playAudioFile=(song)=>{
        let s={...this.state};
        s.csong=song;
       this.playMusic(song);
        this.setState(s);
        }
    getDuration=(url)=>{
        let {musicInfo}=this.props;
             let m=new Audio(url);
            m.load();
            m.play();
            m.pause();
            let duration=m.duration;
        let d=Number.parseInt(duration);
        let minutes=Number.parseInt(d/60);
        let sconds=d-(60*minutes)
        console.log(d,minutes,sconds,m,m.duration,musicInfo.music);
       
        return minutes+":"+sconds;
    }
    goToUrl=(str)=>{
        this.props.history.push(str);
    }
    render() {
        let {songs,csong,playlist,dropdownshow}=this.state;
        let {musicInfo}=this.props;
        let defaultimg="https://a10.gaanacdn.com//media/images-v5/default-album-175x175.jpg";
        return (
             <React.Fragment>
                
              <div className="row ">
                  <div className="col-9 bgsongcolor">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="100" className="mx-auto d-block rounded" decoding="async" src="https://tpc.googlesyndication.com/simgad/5953898921057943562?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&amp;rs=AOga4qn2IDHpwZmsIPuJvFHh9isPmRHh4w"/>
                          </div>
                      </div>
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/home")}>Gaana</span>
                              <span className="dotstyle">.</span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/music")}>My Music</span>
                              <span className="dotstyle">.</span>
                              <span className="songnamelinkstyle">{playlist.name}</span>
                              <hr className="mt-0"/>
                          </div>
                      </div>

                     <div className="row mx-4 mb-2 ">
                         <div className="col-3">
                            <div className="songimgboxstyle">
                                <img src={playlist.img || defaultimg} width="190" height="160" className="d-block mx-auto rounded"/>
                            </div>
                         </div>
                         <div className="col-6">
                                <div className="titlestylesong">{playlist.name}</div>
                                <div className="text-muted titlestylesong1 mt-0">Created by Gaana | {playlist.name}</div>
                               {songs.length?(
                                <div className="playallbutton" onClick={()=>this.playAudioFile(csong)}>
                                    <i className={"fas  playallicon "+(musicInfo.status && csong.url == musicInfo.csong.url?"fa-pause":"fa-play")}></i>
                                    <span className="playalltext">{musicInfo.status && csong.url == musicInfo.csong.url?"PAUSE":"PLAY ALL"}</span>
                                </div>
                                ):""}
                         </div>
                         <div className="col-3">
                             <div className="row">
                                 <div className="col-8">
                                
                                 </div>
                                 <ClickOutsideListener onClickOutside={()=>this.handelOutsideClick()}>
                                 <div className="col-2 uplboxmain">
                                     <div className="uplsbtn" onClick={()=>this.showOptionsDropdown()}>
                                 <svg width="24" height="24" viewBox="0 0 24 24" data-type="song" data-value="36368218" className="moreopt"> <g fill="#FFF" fill-rule="evenodd"> 
                                  <path class="fill_path" data-type="song" data-value="36368218" d="M7 12a2 2 0 1 1-3.999.001A2 2 0 0 1 7 12M14 12a2 2 0 1 1-3.999.001A2 2 0 0 1 14 12M21 12a2 2 0 1 1-3.999.001A2 2 0 0 1 21 12"></path> </g> 
                                  </svg>
                                  </div>
                                 
                                  <div className={" uplbox py-3 "+(dropdownshow?"d-block":"")}>
                                      <div className="row uplbox-item  ">
                                          <div className="col-2 ml-4 text-center uplbox-icon">
                                          <svg className="uplbox-icon" width="15" height="15" viewBox="0 0 20 20" data-type="share" data-value="playlist54593244"> <path className="uplbox-fillpath" data-type="share" data-value="playlist54593244" fill-rule="evenodd" d="M16 18a2 2 0 1 1 .001-4.001A2 2 0 0 1 16 18M4 12a2 2 0 1 1 .001-4.001A2 2 0 0 1 4 12M16 2a2 2 0 1 1-.001 4.001A2 2 0 0 1 16 2m0 10c-1.2 0-2.266.542-3 1.382l-5.091-2.546c.058-.27.091-.549.091-.836 0-.287-.033-.566-.091-.836L13 6.618C13.734 7.458 14.8 8 16 8c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4c0 .287.033.566.091.836L7 7.382A3.975 3.975 0 0 0 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382l5.091 2.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4"></path> </svg>
                                          </div>
                                          <div className="col-6 uplbox-text">
                                              Share
                                          </div>
                                      </div>

                                      <div className="row uplbox-item  " onClick={()=>this.goToUrl(`/editplaylist/${playlist.name}`)}>
                                          <div className="col-2 ml-4 text-center uplbox-icon">
                                         
                                          <svg width="15" height="15" viewBox="0 0 18 18"> <g fill="none" fill-rule="evenodd"> <path d="M-1-1h20v20H-1z"></path> <path className="uplbox-fillpath" d="M16.35 1.65a3.331 3.331 0 0 0-2.372-.983 3.33 3.33 0 0 0-2.371.982l-9.508 9.507s0 .002-.002.003a1.062 1.062 0 0 0-.218.384L.692 16.297a.831.831 0 0 0 .219.79v.001l.002.001c.206.207.506.291.79.22l4.754-1.189c.136-.035.316-.15.384-.218l.003-.002 9.508-9.506a3.33 3.33 0 0 0 .981-2.372c0-.896-.348-1.739-.981-2.373h-.001zm-5.34 2.957l2.385 2.385-7.14 7.14-2.386-2.385 7.14-7.14zM2.645 15.355l.496-1.978 1.483 1.484-1.98.494zM15.174 2.828c.637.638.637 1.75 0 2.387l-.599.6-2.388-2.388.6-.599c.638-.636 1.75-.637 2.387 0z"></path> </g> </svg>
                                          </div>
                                          <div className="col-6 uplbox-text">
                                              Edit playlist
                                          </div>
                                      </div>

                                      <div className="row uplbox-item  " onClick={()=>this.deletePlaylist(playlist.id)}>
                                          <div className="col-2 ml-4 text-center uplbox-icon">
                                         
                                          <svg width="15" height="16" viewBox="0 0 16 17"> <g fill="none" fill-rule="evenodd"> <path d="M-2-2h20v20H-2z"></path> <g className="uplbox-fillpath"> <path d="M2.167 14.667c0 .919.747 1.666 1.666 1.666h8.334c.919 0 1.666-.747 1.666-1.666v-10H2.167v10zm1.666-8.334h8.334v8.334H3.834V6.333zM10.5 2.167V.5h-5v1.667h-5v1.666h15V2.167z"></path> <path d="M5.5 8h1.667v5H5.5zM8.833 8H10.5v5H8.833z"></path> </g> </g> </svg>
                                          
                                          </div>
                                          <div className="col-8 uplbox-text" >
                                              Delete playlist
                                          </div>
                                      </div>

                                      <div className="row uplbox-item  ">
                                          <div className="col-2 ml-4 text-center uplbox-icon">
                                         
                                          <svg width="15" height="18" viewBox="0 0 16 20" data-value="" data-type="download"> <g fill="none" fill-rule="evenodd"> <path d="M-4-1.995h24v24H-4z"></path> <path className="uplbox-fillpath" data-value="" data-type="download" d="M14 16.001v2H2v-2H0v2c0 1.102.896 2 2 2h12c1.104 0 2-.898 2-2v-2h-2zM7.293 13.708a.997.997 0 0 0 1.414 0l5-5A1 1 0 0 0 13 7.001h-2v-6a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1v6H3a1 1 0 0 0-.707 1.707l5 5zM6 9.001a1 1 0 0 0 1-1v-6h2v6a1 1 0 0 0 1 1h.586L8 11.587 5.414 9.001H6z"></path> </g> </svg>
                                          </div>
                                          <div className="col-6 uplbox-text">
                                              Download
                                          </div>
                                      </div>

                                      <div className="row uplbox-item  ">
                                          <div className="col-2 ml-4 text-center uplbox-icon">
                                          
                                          <svg width="16" height="16" viewBox="0 0 26 26" data-id="54593244" data-value="guest-dgwdw-rogh-vka8npznbd" data-type="getplaylistinfo"> <path className="uplbox-fillpath" data-id="54593244" data-value="guest-dgwdw-rogh-vka8npznbd" data-type="getplaylistinfo" fill-rule="evenodd" d="M1 13C1 6.372 6.372 1 13 1s12 5.372 12 12-5.372 12-12 12S1 19.628 1 13zm2 0c0 5.524 4.477 10 10 10 5.524 0 10-4.477 10-10 0-5.524-4.477-10-10-10C7.476 3 3 7.477 3 13zm10.946 4.604h-1.778v-6.762h1.778v6.762zm-.896-7.49A1.06 1.06 0 0 1 12 9.05c0-.588.476-1.05 1.05-1.05.588 0 1.064.462 1.064 1.05 0 .588-.476 1.064-1.064 1.064z"></path> </svg>
                                          </div>
                                          <div className="col-8 uplbox-text">
                                              Get Playlist Info
                                          </div>
                                      </div>


                                  </div>
                                

                                 </div>
                                 </ClickOutsideListener>
                                  
                            </div>
                         </div>
                     </div>
                       {songs.length && (
                        <div className="row text-muted mx-5 mt-4 ">
                             <div className="col-2">#</div>
                             <div className="col-4">TITLE</div>
                             <div className="col-3">ARTIST</div>
                             <div className="col-3"></div>
                         </div>

                       )}
                   
                    {songs.length ? songs.map(ele=>{
                        return (
                        <div className={"row fontstyle1 songrow  py-2 my-0 mx-5  border-top border-bottom "+((ele.url==csong.url)?"bg-white":"")}>
                            <div className="col-1">
                            <span className=" songlistplaybox " onClick={()=>this.playAudioFile(ele)}>
                              
                               <i className={"fas songlistplaybtn tablesongfontcolor "+((ele.url==musicInfo.csong.url && musicInfo.status)?"fa-pause":"fa-play")}></i>
                             </span>
                            
                            
                            </div>
                            <div className="col-1 pt-1 ">
                             <i  class="far fa-heart heartstylelist tablesongfontcolor"></i>
                             </div>
                            <div className="col-4  tablesongfont ">
                                <div className="row">
                                    <div className="col-2 m-0 p-0 pl-3">
                                    <img src={ele.img} width="33" height="33" style={{display:"inline-block"}} className="rounded m-0 p-0 d-block"/>
                                    </div>
                                    <div className={"col-9 pt-1 "+(ele.url == csong.url?"tablesongtitle":"")}>
                                    {ele.name}
                                    </div>
                                </div>
                              
                               
                                </div>
                            <div className="col-3 pt-1 tablesongfont tablesongfontcolor">{ele.artist.length > 21? ele.artist.substring(0,21)+"...":ele.artist}</div>
                            <div className="col-3 tablesongfont tablesongfontcolor"></div>
                        </div>
 
                        )
                    }):<div className="row"><div className="col-12 text-center">
                        There is no playlist songs</div></div>}
                    

                  </div>
                  <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/8660235/300x600_DBM_TH-M1.jpg" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>
             </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song}),
        playMusic:(song)=>dispatch({type:"PlayMusic",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(UserPlaylistSong);