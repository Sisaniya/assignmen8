import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "../navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";

import httpServices from "../../services/httpServices";
import MyFavoritePlaylists from "./myFavoritePlaylists";

class UserPlaylist extends Component
{
   state={
     playlists:[],
     favoriteSongs:[],
     favoritePlaylists:[]
   }
   async componentDidMount() {
      let user=localStorage.getItem("user");
      if(user)
      {
        let response=await httpServices.getData("/userplaylists",{headers:{Authorization:user}});
        let response1=await httpServices.getData("/favoriteplaylists",{headers:{Authorization:user}});
        let response2=await httpServices.getData("/favoritesongs",{headers:{Authorization:user}});
        let {data}=response;
        console.log(data);
        this.setState({playlists:data,favoritePlaylists:response1.data,favoriteSongs:response2.data});
      }
   }
   async getPlaylistSongs(obj) {
    try {
        let response={};
        if(obj.createdBy=="gaana")
        {
          response=await httpServices.get(`/playlistsongs/${obj.name}`);
        }
        else
        {
          let user=localStorage.getItem("user");
          response=await httpServices.getData(`/userplaylistsongs/${obj.name}`,{headers:{Authorization:user}});

        }
     
       let {data}=response;
       console.log(data,obj.name);
       if(data.list)
       {
        this.props.setMusic({song:data.list[0],playlist:obj.name});
       }  
     this.setState({...this.state,playlist:obj});
      }
   catch(ex)
      {

      }
   }
   goToPlaySongFromPlaylist=(ele)=>{
        this.getPlaylistSongs(ele);
   }
   async postFavorite(obj) {
    try {
        let response=await httpServices.post("/favoritesongs",obj);
         let {data}=response;
         console.log(data);
         this.setState({...this.state,favoriteSongs:data});
    }
    catch(ex)
    {

    }
}
updateFavoriteList=(song)=>{
   
    let email=localStorage.getItem("email");
    let obj={...song};
    let user=localStorage.getItem("user");
    if(user)
    {

        this.postFavorite({song:obj,email:email});

    }
    else
    {
        window.alert("Login to continue");
    }
   
}
  goToPlaylistSong=(name)=>
  {
    this.props.history.push(`/music/playlist/${name}`);
  }
  goToUrl=(name)=>{
    this.props.history.push(`/song/${name}`);
           }
           goToPlaylistSong=(ele)=>
           {
             if(ele.createdBy=="gaana")
             {
               this.props.history.push(`/playlist/${ele.name}`);
             }
             else
             {
               this.props.history.push(`/music/playlist/${ele.name}`);
             }
             
           }
      addToPlayList=(song)=>{
            this.props.addSong(song);
            this.props.createPlaylist(1);
        }
   render() {
          let {playlists=[],favoritePlaylists,favoriteSongs}=this.state;
          console.log(this.props.playlist);
          let {musicInfo}=this.props;
         
          let defaultimg="https://a10.gaanacdn.com//media/images-v5/default-album-175x175.jpg";
          return (
            <div className="row">
                <div className="col-9 bgcolor2">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="90" className="mx-auto d-block" decoding="async" src="https://s0.2mdn.net/9427301/03Feb_21-HDFCLife-C2PL-DBM-SkyDiv-Pros-728x90.gif"/>
                          </div>
                      </div>
                     
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/home")}>Gaana</span>
                              <span className="dotstyle">.</span>
                             
                              <span className="songnamelinkstyle cps">My Music</span>
                              <hr className="mt-0"/>
                          </div>
                      </div>

                      <div className="row mx-5">
                        <div className="col-12"><h6>PLAYLISTS</h6></div>
                      </div>

                      <div className="row mx-5">
                        <div className="col-12">
                          <div className="createplaylistbtn cps" onClick={()=>this.props.createPlaylist(1)}>
                          <svg width="18" height="18" className="createplaylisticon" viewBox="0 0 22 22" data-type="openCreatePlaylist"> <g fill="#FFF" fill-rule="evenodd"> <path class="fill_path" d="M22 3h-3V0h-2v3h-3v2h3v3h2V5h3zM11.018 10.481l-5 1.631a.749.749 0 0 0-.518.713V16A1.5 1.5 0 1 0 7 17.5v-4.13l3.5-1.142V15a1.5 1.5 0 1 0 1.5 1.5v-5.306a.752.752 0 0 0-.982-.713"></path> <path class="fill_path" d="M2 20V9h12l.002 11H2zM14 7H2C.897 7 0 7.897 0 9v11c0 1.103.897 2 2 2h12c1.103 0 2-.897 2-2V9c0-1.103-.897-2-2-2z"></path> </g> </svg>
                             
                             <span className="createplaylisttext">Create Playlist</span>
                          </div>
                        </div>
                      </div>

                      <div className="row mx-5 mt-4 mb-4">
                      <div className="col-12 mt-2 trenimgmaincol">
               {playlists.map((ele,index)=>{
                   let {id,img,name}=ele;
                 
                 return (
                   <React.Fragment>
                    
                      <div className={"trenimgcol mr-4  trenimgmain "}>
                      <div className="picimgbody" onClick={()=>this.goToPlaylistSong(ele)}>
                        <img src={img || defaultimg} className="trendingimg picimgstyle"/><p/>
                        <div className="trensongname my-1 fontstyle">
                               {name}
                        </div>
                      </div>
                     
                          <div className=" trenimgiconbody" onClick={()=>this.goToPlaySongFromPlaylist(ele)}>
                                  {ele.name == this.props.playlist && musicInfo.status ?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                                           </div>
                    </div>
                     
                   </React.Fragment>
                 

                 )
               })}
               </div>
                </div>


                      {favoriteSongs.length?(
                        <React.Fragment>
                          <div className="row mx-5">
                                 <div className="col-12">
                                   <hr/>
                                 </div>
                          </div>
                          <div className="row mx-5">
                            <div className="col-6"><h6>MY FAVORITE TRACKS</h6></div>
                               <div className="col-6 text-right text-danger cps viewAllLinkStyleBox">
                               <Link  to="/myfavoritetracks" className=" viewAllLinkStyle">
                                   View All   <i class="fas fa-chevron-right "></i>
                                  </Link>
                               </div>
                          </div>
                          
                              <div className="row mx-5 mb-4">
                                <div className="col-12">
                                  <div className="row">
                           {favoriteSongs.map((ele,index)=>{
                             if(index<3)
                             {
                             return (
                              <div className="col-4  my-2">
                              <div className="row fsmain-songbox">
                                  <div className="col-4 ">
                                      <div className="fsmain-box" >
                                      <img src={ele.img} onClick={()=>this.goToUrl(ele.name)} className="cps d-block rounded mx-auto" width="75" height="75" />
                                         
                                             <div className="fsmain-content" >
                                                   <span className=" fsbox-playicon" >
                                                             {musicInfo.status && musicInfo.csong.url == ele.url?(
                                                          <svg width="18" onClick={()=>this.props.setMusic({song:ele,playlist:""})} height="18" viewBox="0 0 24 24" className="playsvg"><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                                            ):(
                                                          <svg width="18" onClick={()=>this.props.setMusic({song:ele,playlist:""})} height="18" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
            
                                                             )}
            
                                                    </span>
                                              </div>
                                     </div>
                                  </div>

                                  <div className="col-8 ">
                                      <div className="row ">
                                          <div className="col-12   fssong-name">
                                              {ele.name? ele.name.length>15? ele.name.substring(0,16)+" ...":ele.name:""}
                                          </div>
                                          <div className="col-12 fsartist-name">
                                          {ele.artist? ele.artist.length>15? ele.artist.substring(0,16)+" ...":ele.artist:""}
                                          </div>
                                          <div className="col-12 fslinks-icons ">
                                             <div className="row">
                                                 <div className="col-3">
                                                 <i onClick={()=>this.updateFavoriteList(ele)} class={" fa-heart  cps heartcolor fas"}></i>
                                                 </div>
                                                 <div className="col-3">
                                                        <svg onClick={()=>this.addToPlayList(ele)}  width="24" height="24" viewBox="0 0 24 24" className="addplaylist queue-addplaylisticon cps" data-value="song36368218" data-type="addtoplaylist" title="Add to Playlist"> <g fill="none" fill-rule="evenodd">
                                                         <path d="M0 0h24v24H0z" data-value="song36368218" data-type="addtoplaylist"></path> 
                                                          <path className="fill_path" data-value="song36368218" data-type="addtoplaylist" d="M21 22H7a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1zm-1.5-2a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11a.5.5 0 0 0 .5.5h11zm-5-2h-1a.5.5 0 0 1-.5-.5V15h-2.5a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5H13v-2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5V13h2.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H15v2.5a.5.5 0 0 1-.5.5zM4 5v14.778a2 2 0 0 1-2-2V3a1 1 0 0 1 1-1h14.778a2 2 0 0 1 2 2H5a1 1 0 0 0-1 1z"></path> </g>
                                                         </svg> 
                                                 </div>
                                                 <div className="col-3">
                                                      <svg width="20" height="20" viewBox="0 0 20 20" className="share option-queue-share" data-value="song36368218" data-type="share" title="Share">
                                                            <path className="fill_path" data-value="song36368218" data-type="share" fill-rule="evenodd" d="M16 18a2 2 0 1 1 .001-4.001A2 2 0 0 1 16 18M4 12a2 2 0 1 1 .001-4.001A2 2 0 0 1 4 12M16 2a2 2 0 1 1-.001 4.001A2 2 0 0 1 16 2m0 10c-1.2 0-2.266.542-3 1.382l-5.091-2.546c.058-.27.091-.549.091-.836 0-.287-.033-.566-.091-.836L13 6.618C13.734 7.458 14.8 8 16 8c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4c0 .287.033.566.091.836L7 7.382A3.975 3.975 0 0 0 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382l5.091 2.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4"></path>
                                                      </svg>
                                                 </div>
                                             </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                           </div>
                           
                                )
                                }
                                else
                                return "";
                                
                           })}    </div>
                               </div>
                              </div>
                        </React.Fragment>
                      ):""}



                       

                       {favoritePlaylists.length?(
                         <React.Fragment>
                            <div className="row mx-5 ">
                                 <div className="col-12">
                                   <hr/>
                                 </div>
                          </div>
                          <div className="row mx-5">
                            <div className="col-6"><h6>FAVORITE PLALISTS</h6></div>
                               <div className="col-6 text-right text-danger cps viewAllLinkStyleBox">
                               <Link  to="/myfavoriteplaylists" className=" viewAllLinkStyle">
                                   View All   <i class="fas fa-chevron-right "></i>
                                  </Link>
                               </div>
                          </div>

                          <div className="row mx-5 mt-2 mb-4">
                             <div className="col-12 mb-4">
                             {favoritePlaylists.map((ele,index)=>{
                                  let {id,img,name}=ele;
                                            if(index<4)
                                            {
                                        return (
                                         <React.Fragment>
                    
                                       <div className={"trenimgcol mr-4  trenimgmain "}>
                                      <div className="picimgbody" onClick={()=>this.goToPlaylistSong(ele)}>
                                       <img src={img || defaultimg} className="trendingimg picimgstyle"/><p/>
                                      <div className="trensongname my-1 fontstyle">
                                         {name}
                                     </div>
                                  </div>
                     
                                   
                                       <div className=" trenimgiconbody" onClick={()=>this.goToPlaySongFromPlaylist(ele)}>
                                  {ele.name == this.props.playlist && musicInfo.status ?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                                           </div>
                                        </div>
                     
                                         </React.Fragment>
                 

                                    )
                                     }
                                     else
                                     return "";
                                   })}
                             </div>
                          </div>

                         </React.Fragment>
                       ):""}

               </div>
                <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/9291847/0121_EN_LSS_APAC_StayAhead_LearnMore_300x600.png" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     
                   </div>
                  
                 </div>
           
            </div>
        )
   }
}
const mapStoreToProps=(store)=>{
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      playlist:store.playlist
  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(obj)=>dispatch({type:"Player",song:obj.song,playlist:obj.playlist}),
      addSong:(song)=>dispatch({type:"AddSong",song:song})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(UserPlaylist);
