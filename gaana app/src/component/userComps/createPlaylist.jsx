import React,{Component} from "react";
import httpServices from "../../services/httpServices";
import {connect} from "react-redux";
import {createBrowserHistory} from "history";
class CreatePlaylist extends Component 
{
    state={
        name:"",
        playlists:[],
    }
    async componentDidMount() {
        let addsong=this.props.addsong;
        let user=localStorage.getItem("user");
        if(addsong && user)
        {
          let response=await httpServices.getData("/userplaylists",{headers:{Authorization:user}});
          let {data}=response;
          console.log(data);
          this.setState({playlists:data});
        }
    }
   
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s[input.name]=input.value;
        this.setState(s);
    }
    addToPlaylist=(obj)=>{
        let {addsong}=this.props;
        let obj1={pl:obj,song:addsong,npl:false};
                 this.addSongToPlaylist(obj1);
    }
    async addSongToPlaylist(obj) {
        try {
            let response=await httpServices.post("/addsongtouserplaylist",obj);
            let {data}=response;
            console.log(data);
           this.goToBack();
            let history=createBrowserHistory();
            if(history.location.pathname == "/music")
            {
                window.location="/music";
            }
            
        }
        catch(ex)
        {

        }

    }
    async postData(obj) {
       
        try {
            let response=await httpServices.post("/createplaylist",obj);
            let {data}=response;
            console.log(data);
            this.props.createPlaylist(0);
            window.location="/music";
        }
        catch(ex)
        {

        }
    }
    handelEnter=(e)=>{
        if(e.key=="Enter")
        {
            e.preventDefault();
            let {name}=this.state;
            let {addsong}=this.props
            let id=localStorage.getItem("id");
            let obj={name:name,img:"",userId:id,createdBy:"user"};
            if(name)
            {
                 if(addsong)
                 {
                     let obj1={pl:obj,song:addsong,npl:true};
                     this.addSongToPlaylist(obj1);
                 }
                 else
                 {
                    this.postData(obj);
                 }
            }
        }
    }
    handelSubmit=(e)=>{
        e.preventDefault();
        let {name}=this.state;
        let {addsong}=this.props
        let id=localStorage.getItem("id");
        let obj={name:name,img:"",userId:id,createdBy:"user"};
        if(name)
        {
             if(addsong)
             {
                 let obj1={pl:obj,song:addsong,npl:true};
                 this.addSongToPlaylist(obj1);
             }
             else
             {
                this.postData(obj);
             }
        }
       
    }
    goToBack=()=>{
        this.props.removeSong();
        this.props.createPlaylist(0);
        
    }
    render() {
        let {name,playlists}=this.state;
        let {addsong}=this.props;
        console.log(addsong);
        return (
            <React.Fragment>
            <div className="box">
                    <div className="container-box" onClick={()=>this.goToBack()}>
                
                    </div>
             </div>
             <div className="box-body-create-playlist">

             <div className="row py-2 my-2 mx-2">
                                <div className="col-8 mt-1 addtoptext">
                                      ADD TO PLAYLIST
                                   </div>
                                  
                                   <div className="col-4 mt-1 text-right closeicon">
                                   <svg className="mr-2 " onClick={()=>this.goToBack()} width="17" height="17" viewBox="0 0 17 17"> <path className="fill_path" fill-rule="evenodd" d="M16.293 1.592l-1.3-1.3-6.7 6.701-6.7-6.7-1.3 1.299 6.7 6.7-6.7 6.701 1.3 1.3 6.7-6.7 6.7 6.7 1.3-1.3-6.7-6.7z"></path> </svg>
                                   </div>
                               </div>

                               <div className="row">
                                   <div className="col-12">
                                       <hr className={"mt-0  pt-0 "+(!playlists.length?"mb-4 pb-2":"mb-0 pb-0")}/>
                                   </div>
                               </div>


                             {!playlists.length? (
                               <div className="row mt-4">
                                   <div className="col-12 mt-4 text-center createplaylisticon2" >
                                   <svg width="100" height="100"  viewBox="0 0 22 22" data-type="openCreatePlaylist"> <g fill="" fill-rule="evenodd"> <path class="fill_path" d="M22 3h-3V0h-2v3h-3v2h3v3h2V5h3zM11.018 10.481l-5 1.631a.749.749 0 0 0-.518.713V16A1.5 1.5 0 1 0 7 17.5v-4.13l3.5-1.142V15a1.5 1.5 0 1 0 1.5 1.5v-5.306a.752.752 0 0 0-.982-.713"></path> <path class="fill_path" d="M2 20V9h12l.002 11H2zM14 7H2C.897 7 0 7.897 0 9v11c0 1.103.897 2 2 2h12c1.103 0 2-.897 2-2V9c0-1.103-.897-2-2-2z"></path> </g> </svg>
                             
                                   </div>
                                   <div className="col-12 createpltext">
                                       Create a playlist to continue
                                   </div>
                               </div>
                                ):""}

                                {playlists.length?(
                                    <div className="playlistsbox ">
                                        {playlists.map(ele=>{
                                            return (
                                                <div className="row px-3 cps py-2 border-bottom " onClick={()=>this.addToPlaylist(ele)}>
                                                    <div className="col-1 ml-4">
                                                    <svg className="addtoplaylist_svg" width="18" height="18" viewBox="0 0 14 14"><g fill="none" fill-rule="evenodd"><path d="M-1-1h16v16H-1z"></path><path className="createplis" fill-opacity=".7" d="M13.35 5.1a.669.669 0 0 0-.648-.03l-4 2a.666.666 0 0 0-.369.597v2.7A1.667 1.667 0 1 0 9.666 12V8.08l2.667-1.334v2.288a1.667 1.667 0 1 0 1.333 1.633v-5a.666.666 0 0 0-.315-.567zM.333.333h11.333v1.333H.333zM.333 3h11.333v1.333H.333zM.333 5.667h6V7h-6zM.333 8.333h6v1.333h-6z"></path></g></svg>
                                                    </div>
                                                    <div className="col-8 createpllisttext">
                                                        {ele.name}
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                ):""}
                                   



                               <div className={"row   mx-4 "+(!playlists.length?"mt-4":" mt-2")}>
                                   <div className={"col-12 pr-0 inputbox "+(!playlists.length?"inputboxheight":"")}>
                                       
                                           <input type="text" onKeyDown={this.handelEnter}
                                            placeholder="create a new playlist"
                                            name="name"
                                            value={name}
                                            className="inputfilepl"
                                            onChange={this.handelChange}
                                            />
                                             <span>
                                                 <i onClick={this.handelSubmit} className="arrowpl fas fa-arrow-right"></i>
                                             </span>
                                   </div>
                               </div>
                     
             </div>
             </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        addsong:store.addsong,
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        removeSong:()=>dispatch({type:"RemoveSong"})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(CreatePlaylist);