import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "../navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";

import httpServices from "../../services/httpServices";

class UserProfile extends Component
{
   state={
      user:[],
     
   }
   async componentDidMount() {
       let user=localStorage.getItem("user");
       let response=await httpServices.getData("/user",{headers:{Authorization:user}});
       let {data}=response;
       console.log(data,response);
        this.setState({user:data});
   }
  
   render() {
      let {user}=this.state;
       console.log(user);
        return (
            <div className="row">
                <div className="col-9 bgsongcolor">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="90" className="mx-auto d-block" decoding="async" src="https://s0.2mdn.net/9291847/0121_EN_LSS_APAC_StayAhead_LearnMore_728x90.png"/>
                          </div>
                      </div>

                      <div className="row">
                          <div className="col-12 mt-2">
                          <img className="d-block mx-auto userimg" width="160" height="160" src="https://a10.gaanacdn.com/images/users/496/crop_110x110_177224496.jpg"/>
                          </div>
                          <div className="col-12 namestyle text-center">
                              {user.name}
                          </div>
                          <div className="col-12 emailstyle text-center">
                              {user.email}
                          </div>
                      </div>

                      <div className="row">
                          <div className="col-2"></div>
                          <div className="col-8">

                              <div className="row">
                                  <div className="col-6 text-left">
                                      <div className="subscription">SUBSCRIPTION</div>
                                  </div>
                                  <div className="col-6 text-right text-danger">
                                      VIEW TRANSACTIONS HISTORY
                                  </div>
                              </div>

                              <div className="row bg-white mt-3">
                                  <div className="col-12">
                                      <div className="row mx-4 ">
                                          <div className="col-12 mt-4">
                                              <div className="currentplan">Current Plan</div>
                                               <div className="adsupported">Ad Supported Free Streaming</div>
                                               <div className="addsupportedtext">
                                                   You are currently active on ad supported, low, medium, and high quality streaming services
                                               </div>
                                               <hr/>
                                          </div>

                                          <div className="col-12 text-center mt-4">
                                              <div className="getmorestyle">Get more with Gaana Plus</div>
                                          </div>

                                          <div className="col-12 mt-2">
                                                  <div className="rc_plan">
                                                       <div className="_blk">
                                                        <small>PREMIUM</small> 
                                                        <svg width="50" height="50" viewBox="0 0 50 50">
                                                             <defs> <linearGradient id="uDownload" x1="50%" x2="50%" y1="0%" y2="100%"> <stop offset="0%" stop-color="#B4EC51"></stop> <stop offset="100%" stop-color="#429321"></stop> </linearGradient> </defs> <g fill="none" fill-rule="evenodd"> <path d="M0 0h50v50H0z"></path> <path fill="url(#uDownload)" d="M25.556 4.444c11.659 0 21.11 9.452 21.11 21.112 0 11.659-9.451 21.11-21.11 21.11-11.66 0-21.112-9.451-21.112-21.11 0-11.66 9.452-21.112 21.112-21.112zm7.11 30H18.608a.83.83 0 0 0-.83.83v.563c0 .458.371.83.83.83h14.058a.83.83 0 0 0 .83-.83v-.563a.83.83 0 0 0-.83-.83zm-4.072-18.888H22.79a.415.415 0 0 0-.415.415v7.523c0 .229-.185.415-.415.415h-3.563a.415.415 0 0 0-.295.707l7.005 7.053a.831.831 0 0 0 1.178 0l6.998-7.053a.415.415 0 0 0-.294-.707h-3.565a.415.415 0 0 1-.415-.415V15.97a.415.415 0 0 0-.415-.415z"> </path> </g> </svg>
                                                              <span className="premimutext">Unlimited Downloads</span> 
                                                       </div>
                                                              <div className="_blk"> 
                                                              <small>PREMIUM</small> 
                                                              <svg width="50" height="50" viewBox="0 0 50 50"> <defs> <linearGradient id="b_noadd" x1="50%" x2="50%" y1="0%" y2="100%"> <stop offset="0%" stop-color="#B4EC51"></stop> <stop offset="100%" stop-color="#429321"></stop> </linearGradient> <circle id="a_noadd" cx="21.111" cy="21.111" r="21.111"></circle> <circle id="d_noadd" cx="25.556" cy="25.556" r="21.111"></circle> </defs> <g fill="none" fill-rule="evenodd"> <path d="M0 0h50v50H0z"></path> <g transform="translate(4.444 4.444)"> <mask id="c_noadd" fill="#fff"> <use href="#a_noadd"></use> </mask> <use fill="url(#b_noadd)" href="#a_noadd"></use> <text fill="#4A4A4A" font-family="Lato" font-size="13" font-weight="600" mask="url(#c)"> <tspan x="9.25" y="25.917">ADS</tspan> </text> <path fill="#3C3A3D" d="M32.222 3.889h2.222v8.889h-2.222z" mask="url(#c_noadd)" transform="rotate(45 33.333 8.333)"></path> <path fill="#3C3A3D" d="M8.889 30.556h2.222v8.888H8.89z" mask="url(#c_noadd)" transform="rotate(45 10 35)"></path> </g> <use stroke="url(#b_noadd)" stroke-width=".83" href="#d_noadd"></use> </g> </svg> 
                                                              <span className="premimutext">Get rid of Ads</span> </div>
                                                               <div className="_blk">
                                                                    <small>PREMIUM</small>
                                                                     <svg width="50" height="50" viewBox="0 0 50 50"> <defs> <linearGradient id="a_hd" x1="50%" x2="50%" y1="0%" y2="100%"> <stop offset="0%" stop-color="#B4EC51"></stop> <stop offset="100%" stop-color="#429321"></stop> </linearGradient> </defs> <path fill="url(#a_hd)" fill-rule="nonzero" d="M48.677 21.163c-.967-1.276-2.542-1.98-4.435-1.98h-4.689a18.61 18.61 0 0 0-17.61-12.516c-10.278 0-18.61 8.29-18.61 18.518 0 10.228 8.332 18.519 18.61 18.519a18.61 18.61 0 0 0 17.61-12.517h2.55a7.664 7.664 0 0 0 4.542-1.465 7.326 7.326 0 0 0 2.729-3.878c.505-1.76.257-3.422-.697-4.68zm-26.734 11.12c-3.94 0-7.134-3.177-7.134-7.098 0-3.92 3.194-7.099 7.134-7.099 3.94 0 7.134 3.178 7.134 7.1 0 3.92-3.194 7.098-7.134 7.098zm13.832-3.497l.803-2.798H33.58l-.804 2.798h-1.84l2.067-7.201h1.841l-.85 2.962h2.998l.85-2.962h1.84l-2.067 7.201h-1.84zm8.467-7.202c2.274 0 3.426 1.462 2.812 3.601-.611 2.13-2.626 3.601-4.952 3.601h-3.04l1.367-4.758h1.84l-.947 3.297h1.313c1.127 0 2.181-.864 2.544-2.13.367-1.275-.23-2.15-1.398-2.15h-3.07l.419-1.46h3.112zm-22.39.129a3.472 3.472 0 1 1 0 6.944 3.472 3.472 0 0 1 0-6.944z"> </path> </svg>
                                                                      <span className="premimutext">Listen in HD Quality</span> 
                                                                     </div>
                                                   </div>
                                          </div>

                                          <div className="col-12  mt-3 mb-4 justify-content-center  text-center">
                                              <div className="row">
                                                  <div className="col-3"></div>
                                                  <div className="col-6">
                                                  <div className="upgradebutton">
                                                  UPGRADE NOW
                                                      </div>
                                                  </div>
                                                  <div className="col-3"></div>
                                              </div>
                                              
                                          </div>


                                         
                                      </div>
                                  </div>

                              </div>

                            <hr/>
                          </div>
                          <div className="col-2"></div>
                      </div>
                      
               </div>
                <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/9291847/0121_EN_LSS_APAC_StayAhead_LearnMore_300x600.png" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     
                   </div>
                  
                 </div>
           
            </div>
        )
   }
}
export default UserProfile;