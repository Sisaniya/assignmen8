import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../../services/httpServices";
import {connect} from "react-redux";
import {ClickOutsideListener} from "react-click-outside-listener";

class EditPlaylist extends Component
{
    state={songs:[],
           selectedsongs:[],
            playlist:{},
           
    };
    async saveEdit(obj) {
        try {
             let response=await http.put("/editplaylist",obj);
             let {data}=response;
             console.log(data);
            this.props.history.push("/music");
        }
        catch(ex)
        {

        }
    }
    goToSaveEdit=()=>{
        let {songs,playlist}=this.state;
         let obj={songlist:songs,playlist:playlist};
         if(playlist.name)
         {
            this.saveEdit(obj);
         }
         else
         {
             window.alert("Enter playlist name");
         }
        

    }
    deleteSelecteItems=()=>{
        let s={...this.state};
        s.songs=s.songs.filter(ele=> s.selectedsongs.findIndex(el=>el.id == ele.id)>=0?false:true);
       s.selectedsongs=[];
        this.setState(s);

    }
    selectAllItems=()=>{
        let s={...this.state};
        if(s.selectedsongs.length == s.songs.length)
        {
            s.selectedsongs=[];
        }
        else
        {
            s.selectedsongs=[...s.songs];
        }
      

        this.setState(s);
    }
    addToSelectedList=(item)=>{
        let s={...this.state};
            let index=s.selectedsongs.findIndex(ele=>ele.id == item.id);
            if(index>=0)
            {
                s.selectedsongs.splice(index,1);
            }
            else
            {
                s.selectedsongs.push(item);
            }
            console.log(s);
            this.setState(s);
    }
    deleteSong=(id)=>{
        let s={...this.state};
        let index=s.songs.findIndex(ele=>ele.id == id);
        s.songs.splice(index,1);
        let ind=s.selectedsongs.findIndex(ele=>ele.id == id);
        if(ind>=0){
            s.selectedsongs.splice(ind,1)
        }
        this.setState(s);
    }
   handelChange=(e)=>{
       let s={...this.state};
       let {currentTarget:input}=e;
       s.playlist[input.name]=input.value;
       this.setState(s);
   }
    async componentDidMount() 
    {   let {name}=this.props.match.params;
        let user=localStorage.getItem("user");
         let response=await http.getData(`/userplaylistsongs/${name}`,{headers:{Authorization:user}});
         let {data}=response;
       
       
         this.setState({songs:data.list,csong:data.list[0],playlist:data.playlist});
    }
   
    goToUrl=(str)=>{
        this.props.history.push(str);
    }
    render() {
        let {songs,playlist,selectedsongs}=this.state;
        let {musicInfo}=this.props;
      
        return (
             <React.Fragment>
                
              <div className="row ">
                  <div className="col-9 bgcoloruser">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="100" className="mx-auto d-block rounded" decoding="async" src="https://tpc.googlesyndication.com/simgad/5953898921057943562?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&amp;rs=AOga4qn2IDHpwZmsIPuJvFHh9isPmRHh4w"/>
                          </div>
                      </div>
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/home")}>Gaana</span>
                              <span className="dotstyle">.</span>
                              <span className="songlinkstyle cps" onClick={()=>this.goToUrl("/music")}>playlists</span>
                             
                              <hr className="mt-0"/>
                          </div>
                      </div>

                     <div className="row mx-4 mb-2 ">
                         <div className="col-3">
                            <div className="songimgboxstyle">
                                <img src={playlist.img} width="170" height="160" className="d-block mx-auto rounded"/>
                            </div>
                         </div>
                         <div className="col-6">
                                <div className="editplayliststyle">
                                    <input className="editinputfield"
                                     name="name"
                                     value={playlist.name}
                                     placeholder="Enter playlist name"
                                     onChange={this.handelChange}
                                    />
                              </div>
                               
                         </div>
                        
                     </div>
                     <div className="row mt-4 mb-2 mx-5">
                         <div className="col-5">
                             {songs.length ?(
                             <div className="row">
                                 <div className="col-12  p-0">
                                      <div onClick={()=>this.selectAllItems()} className="selectallitems row">
                                       <div className="col-1">
                                         {(selectedsongs.length && selectedsongs.length == songs.length)?(
                                          <svg class="checkedsvg" width="18" height="18" viewBox="0 0 16 16"> <g fill="" fill-rule="evenodd"> <path fill="rgb(88, 86, 86)" class="" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path> <path class="" fill="#fff" d="M7.201 12.928L3.158 8.885l1.767-1.768 1.957 1.956 4.142-5.799 2.035 1.453z"></path> </g> </svg>
                               
                                          ):(
                                          <i className="far fa-circle uncheckedsvg"></i>
                                           )}
                                       </div>
                                         <div className="selectalltext p-0 col-5"> SELECT ALL</div>
                                       </div>
                                 </div>
                             </div>
                             ):""}
                            
                         </div>
                         <div className="col-7">
                             <div className="row">
                                 <div className="col-6">
                                    
                                     {selectedsongs.length? <div onClick={()=>this.deleteSelecteItems()} className="editdelete row">
                                        <div className="selecteddeleteicon col-2">
                                        <svg  className="cps " width="17" height="19" viewBox="0 0 16 17"> <g fill="none" fill-rule="evenodd"> <path d="M-2-2h20v20H-2z"></path> <g className="editdeleteicon"> <path d="M2.167 14.667c0 .919.747 1.666 1.666 1.666h8.334c.919 0 1.666-.747 1.666-1.666v-10H2.167v10zm1.666-8.334h8.334v8.334H3.834V6.333zM10.5 2.167V.5h-5v1.667h-5v1.666h15V2.167z"></path> <path d="M5.5 8h1.667v5H5.5zM8.833 8H10.5v5H8.833z"></path> </g> </g> </svg>
                                    
                                        </div>
                                         <div className="selecteddeletetext col-8"> DELETE SELECTED</div>
                                        
                                         </div>:""}
                                    
                                 </div>
                                 <div className="col-3">
                                        <div className="editsave" onClick={()=>this.goToSaveEdit()}>
                                            SAVE
                                        </div>
                                 </div>
                                 <div className="col-3">
                                     <div className="editcancel" onClick={()=>this.goToUrl("/music")}>
                                         CANCEL
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                      
                   
                    {songs.length ? songs.map((ele,index)=>{
                        return (
                        <div className={"row fontstyle1 songrow  py-2 my-0 mx-5 px-3 "+(index == 0?" editsongsrow":" editsongsrow1")}>
                            <div className="col-1">
                                <span className="cps" onClick={()=>this.addToSelectedList(ele)}>
                                    {selectedsongs.find(e=>e.id == ele.id)?(
                                        <svg class="checkedsvg" width="18" height="18" viewBox="0 0 16 16"> <g fill="" fill-rule="evenodd"> <path fill="rgb(88, 86, 86)" class="" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path> <path class="" fill="#fff" d="M7.201 12.928L3.158 8.885l1.767-1.768 1.957 1.956 4.142-5.799 2.035 1.453z"></path> </g> </svg>
                               
                                    ):(
                                          <i className="far fa-circle uncheckedsvg"></i>
                                    )}
                               
                               
                                </span>
                              
                               
                            </div>
                            
                            <div className="col-4  tablesongfont ">
                                <div className="row">
                                    <div className="col-2 m-0 p-0 pl-3">
                                    <img src={ele.img} width="33" height="33" style={{display:"inline-block"}} className="rounded m-0 p-0 d-block"/>
                                    </div>
                                    <div className={"col-9 tablesongfontcolor pt-1 "}>
                                    {ele.name}
                                    </div>
                                </div>
                              
                               
                                </div>
                            <div className="col-5 pt-1 tablesongfont tablesongfontcolor">{ele.artist.length > 21? ele.artist.substring(0,21)+"...":ele.artist}</div>
                            <div className="col-1 pl-3">
                            <svg onClick={()=>this.deleteSong(ele.id)} className="cps" width="17" height="19" viewBox="0 0 16 17"> <g fill="none" fill-rule="evenodd"> <path d="M-2-2h20v20H-2z"></path> <g className="editdeleteicon"> <path d="M2.167 14.667c0 .919.747 1.666 1.666 1.666h8.334c.919 0 1.666-.747 1.666-1.666v-10H2.167v10zm1.666-8.334h8.334v8.334H3.834V6.333zM10.5 2.167V.5h-5v1.667h-5v1.666h15V2.167z"></path> <path d="M5.5 8h1.667v5H5.5zM8.833 8H10.5v5H8.833z"></path> </g> </g> </svg>
                                          
                            </div>
                        </div>
 
                        )
                    }):<div className="row"><div className="col-12 text-center">
                        There is no playlist songs</div></div>}
                    

                  </div>
                  <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/8660235/300x600_DBM_TH-M1.jpg" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>
             </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(EditPlaylist);