import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";
import Home from "./home";
import SongPlayer from "./songPlayer";
import PlaylistSong from "./playlistSong";
import TrendingAll from "./trendingAll";
import PlaylistAll from "./playlistAll";
import http from "../services/httpServices";
import CurrentSong from "./currentSong";
import Login from "./login";
import UserProfile from "./userComps/userProfile";
import UserPlaylist from "./userComps/userPlaylist";
import CreatePlaylist from "./userComps/createPlaylist";
import UserPlaylistSong from "./userComps/userPlaylistSong";
import EditPlaylist from "./userComps/editPlaylist";
import MyFavoriteTracks from "./userComps/myFavoriteTracks";
import MyFavoritePlaylists from "./userComps/myFavoritePlaylists";
import AdminAccount from "./adminComps/adminAccount";
import FormUpload from "./adminComps/formUpload";
import CSVUpload from "./adminComps/csvUpload";
import AddNewUser from "./adminComps/addNewUser";
import ManageUsers from "./adminComps/allUsers";
import ExportData from "./adminComps/exportData";
import AllSongs from "./adminComps/allSongs";
import Reports from "./adminComps/reports";
import Parent from "./parent";
import SearchPage from "./searchPage";
class MyComp extends Component
{
   state={
     showlogin:0,
     createpl:0,
   }
   createPlaylist=(m)=>{
    let s={...this.state};
    s.createpl=m;
    this.setState(s);
   }
   openLogin=(m)=>
   {
     let s={...this.state};
     s.showlogin=m;
     this.setState(s);
   }
    
   render() {
    let {musicInfo}=this.props;
       let {showlogin,createpl}=this.state;
      
        return (
            <React.Fragment>
              <div className={showlogin || createpl?"cop":""}>
                <Navbar openLogin={this.openLogin}/>
            <Switch>
            <Route path="/search/:search" render={(props)=><SearchPage   {...props}/>}/>
            <Route path="/reports" render={(props)=><Reports   {...props}/>}/>
            <Route path="/managesongs" render={(props)=><AllSongs   {...props}/>}/>
            <Route path="/export" render={(props)=><ExportData   {...props}/>}/>
            <Route path="/edituser/:id" render={(props)=><AddNewUser   {...props}/>}/>
            <Route path="/users" render={(props)=><ManageUsers   {...props}/>}/>
            <Route path="/newuser" render={(props)=><AddNewUser   {...props}/>}/>
            <Route path="/csvUpload" render={(props)=><CSVUpload   {...props}/>}/>
            <Route path="/editsong/:id" render={(props)=><FormUpload   {...props}/>}/>
            <Route path="/formUpload" render={(props)=><FormUpload   {...props}/>}/>
            <Route path="/account" render={(props)=><AdminAccount   {...props}/>}/>
            <Route path="/myfavoriteplaylists" render={(props)=><MyFavoritePlaylists createPlaylist={this.createPlaylist}   {...props}/>}/>
            <Route path="/myfavoritetracks" render={(props)=><MyFavoriteTracks createPlaylist={this.createPlaylist}   {...props}/>}/>
            <Route path="/editplaylist/:name" render={(props)=><EditPlaylist   {...props}/>}/>
            <Route path="/music/playlist/:name" render={(props)=><UserPlaylistSong   {...props}/>}/>
            <Route path="/music" render={(props)=><UserPlaylist createPlaylist={this.createPlaylist}  {...props}/>}/>
            <Route path="/profile" render={(props)=><UserProfile  {...props}/>}/>
            <Route path="/playlists" render={(props)=><PlaylistAll  {...props}/>}/>
            <Route path="/songs" render={(props)=><TrendingAll  {...props}/>}/>
            <Route path="/playlist/:name" render={(props)=><PlaylistSong   {...props}/>}/>
            <Route path="/song/:name" render={(props)=><SongPlayer createPlaylist={this.createPlaylist}  {...props}/>}/>
             <Route path="/home" render={(props)=><Home {...props}/>}/>
             <Route path="/assignment" component={Parent}/>
              <Redirect from="/" to="/home"/>
            </Switch>
           <CurrentSong/>
              </div>
              {showlogin?<Login openLogin={this.openLogin}/>:""}
              {createpl?<CreatePlaylist createPlaylist={this.createPlaylist}/>:""}
            </React.Fragment>
           
        )
   }
}
const mapStoreToProps=(store)=>{
  console.log(store.duration);
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      duration:store.duration

  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(song)=>dispatch({type:"Player",song:song}),
      getDuration:()=>dispatch({type:"Duration"})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(MyComp);