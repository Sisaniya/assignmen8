import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";
import http from "../services/httpServices"
import Footer from "./footer";

class PlaylistAll extends Component
{
   state={
      data:[],
     playlist:{},
   }
   async componentDidMount() {
       let response=await http.get("/topcharts");
       let {data}=response;
       console.log(data,response);
        this.setState({data:data});
   }
   async getPlaylistSongs(obj) {
    try {
      let response=await http.get(`/playlistsongs/${obj.name}`);
       let {data}=response;
       console.log(data);
       if(data.list)
       {
        await this.props.setMusic({song:data.list[0],playlist:obj.name});
       }  
     this.setState({...this.state,playlist:obj});
      }
   catch(ex)
      {

      }
   }
   goToPlaySongFromPlaylist=(ele)=>{
        this.getPlaylistSongs(ele);
   }
   goToPlaylistSong=(name)=>{
       this.props.history.push(`/playlist/${name}`);
   }
   render() {
      let {data,}=this.state;
        let {musicInfo}=this.props;
       console.log(data);
        return (
            <div className="row">
                <div className="col-9 bgsongcolor">
                <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="90" className="mx-auto d-block" decoding="async" src="https://tpc.googlesyndication.com/simgad/12118969256046587501?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&amp;rs=AOga4ql0eBmFmQrE_3ak0APnDCbBKGHd8w"/>
                          </div>
                      </div>
               
            <div className="row ">
             <div className="col-12 piclistmain ">
            
               <div className="row  mx-4">
                 <div className="col-12 my-2">
                
                 <h6>ALL TOP CHARTS</h6>
                 </div>
              <div className="col-12 trenimgmaincol">
               {data.map((ele,index)=>{
                   let {id,img,name}=ele;
                 
                 return (
                   <React.Fragment>
                    
                      <div className={"trenimgcol mr-4  trenimgmain "}>
                      <div className="picimgbody" onClick={()=>this.goToPlaylistSong(name)}>
                        <img src={img} className="trendingimg picimgstyle"/><p/>
                        <div className="trensongname my-1 fontstyle">
                               {name}
                        </div>
                      </div>
                     
                      <div className=" trenimgiconbody" onClick={()=>this.goToPlaySongFromPlaylist(ele)}>
                                  {ele.name == this.props.playlist && musicInfo.status ?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                         </div>
                    </div>
                     
                   </React.Fragment>
                 

                 )
               })}
               </div>
               </div>
              
                
               </div>
            </div>
               <Footer/>
            </div>
            <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/8660235/300x600_DBM_TH-M1.jpg" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
           
            </div>
        )
   }
}
const mapStoreToProps=(store)=>{
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      playlist:store.playlist
  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(obj)=>dispatch({type:"Player",song:obj.song,playlist:obj.playlist}),
      addSong:(song)=>dispatch({type:"AddSong",song:song})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(PlaylistAll);

