import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";
import TopChart from "./topChart";
import Trendings from "./trending";
import TopPicks from "./topPicks";
import http from "../services/httpServices";
import Footer from "./footer";

class Home extends Component
{
   state={
      data:[],
      strendings:0,
      searchvalue:"",
   }
   async componentDidMount() {
    let response=await http.get("/trendingSong");
    let {data}=response;
   
     this.setState({data:data});
}
handelChange=(e)=>{
  let {currentTarget:input}=e;
   let s={...this.state};
   s.searchvalue=input.value;
   this.setState(s);
}
handelSubmit=(e)=>{
  let {searchvalue}=this.state;
  this.props.history.push(`/search/${searchvalue}`);
  this.props.showSearch(0);
}
handelEnter=(e)=>{
  if(e.key=="Enter")
  {
    let {searchvalue}=this.state;
    this.props.history.push(`/search/${searchvalue}`);
    this.props.showSearch(0);
  }
 
}
goToPlaySong=(name)=>{
  this.props.history.push(`/song/${name}`);
}
goToPlaylistSong=(name)=>{
  this.props.history.push(`/playlist/${name}`);
}
goToSongList=(data)=>{
  
  this.props.history.push(`/${data.category}/${data.name}`);
}

showTrendings=(e)=>{

 
  let s={...this.state};
  if(s.strendings)
  {
    e.currentTarget.className="trending-search-btn-notactive";
    s.strendings=0;
  }
  else
  {
    e.currentTarget.className="trending-search-btn-active";
    s.strendings=1;
  }
  
  this.setState(s);
}
   removeBorder=(e)=>{
    console.log(e.currentTarget.className);
     e.currentTarget.className="searchinput1";
     console.log(e.currentTarget.className);
   }
   render() {
      let {data,strendings,searchvalue}=this.state;
      let {musicInfo}=this.props;
     
        return (
            <React.Fragment>
              
              <div className="row ">
                 <div className="col-9 ">
                   {this.props.search?(
                  <div className="row my-2">
                    <div className="col-lg-8">
                          <div className="search-wrap clearfix"> 
                          <div className="search_song">
                             <div className="search_container">
                                  <input onChange={this.handelChange}
                                   name="search"
                                   onKeyDown={this.handelEnter}
                                   value={searchvalue}
                                  onFocus={this.removeBorder}  type="search" placeholder="Search for Songs, Artists, Playlists and More"
                                   className="searchinput1" autocomplete="off"/>
                                   <a className="search_btn" onClick={this.handelSubmit}> 
                                    <svg width="25" height="25" viewBox="0 0 25 25"> 
                                    <path className="fill_path" fill="#FFF" fill-rule="evenodd" d="M69.5 34a6.5 6.5 0 0 1 6.5 6.5c0 1.61-.59 3.09-1.56 4.23l.27.27h.79l5 5-1.5 1.5-5-5v-.79l-.27-.27A6.516 6.516 0 0 1 69.5 47a6.5 6.5 0 1 1 0-13zm0 2C67 36 65 38 65 40.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5z" transform="translate(-59 -32)">
                                    </path> </svg> Search 
                                    </a> 
                              </div> 
                              </div>
                           </div>

                               
                    </div>
                    <div className="col-lg-4 my-2 d-none d-lg-block " style={{position:"relative"}}>
                               <div className="trending-search-btn-box" >
                                  <div onClick={this.showTrendings} className="trending-search-btn-notactive">
                                    trending {data.length}
                                  </div>
                                  <div className={"trending-show-box bg-white "+(strendings?"d-block":"")}>
                                   {data.map(ele=>
                                           {
                                   return (
                                     <div className="search-trending-name my-1" onClick={()=>this.goToPlaySong(ele.name)}>
                                         <span   className="  cps" >{ele.name+" - "+ele.artist}</span>
                                     </div>
                                   )
                                  }
                                   )}
                                  </div>
                               </div>
                               <div className="downloadapp mr-3">
                                 Download App
                               </div>
                               <div className="platfromicons ">
                               <svg width="27" className="mr-1 " height="31" viewBox="0 0 27 31" id="iphonesvg"><path className="fill_path_adroidicon"   d="M3.955 8.528a6.121 6.121 0 0 1 2.087-2.132 5.241 5.241 0 0 1 2.813-.816H8.9c.575 0 1.376.196 2.404.59.635.272 1.074.408 1.316.408.211 0 .65-.136 1.315-.409 1.15-.453 2.057-.68 2.722-.68.121 0 .242.015.363.045 1.875.06 3.311.847 4.31 2.36.03.03.045.067.045.113 0 .045-.03.083-.09.113v.045a4.698 4.698 0 0 0-.84.613c-.287.257-.62.718-.998 1.383a4.01 4.01 0 0 0-.522 2.178c0 .726.129 1.383.386 1.973.257.59.59 1.058.998 1.406.408.348.726.59.952.726l.567.34h.046c.09.03.12.106.09.227a13.315 13.315 0 0 1-1.587 3.312 26.296 26.296 0 0 1-1.02 1.383c-.258.318-.636.643-1.135.975-.499.333-1.005.507-1.52.522-.514.015-1.134-.128-1.86-.43-.695-.333-1.33-.5-1.905-.5-.605 0-1.285.167-2.041.5-.665.271-1.225.423-1.678.453h-.091c-.696 0-1.316-.257-1.86-.771s-1.164-1.255-1.86-2.223c-.726-1.059-1.315-2.268-1.77-3.63a12.498 12.498 0 0 1-.634-4.24c.03-1.467.348-2.745.952-3.834zM17.02 0c.12 0 .181.06.181.181.152 1.331-.257 2.586-1.224 3.766-.938 1.149-2.042 1.723-3.312 1.723a.746.746 0 0 1-.227-.045c-.09 0-.15-.045-.181-.136-.151-1.3.272-2.525 1.27-3.674.423-.515.96-.938 1.61-1.27C15.787.211 16.415.03 17.02 0z"  transform="translate(1 2)"></path></svg>
                               <svg width="28" height="32" viewBox="0 0 28 32" id="androidsvg"><path className="fill_path_adroidicon"  d="M14.502 23.953c-.8 0-1.5-.698-1.5-1.499v-3.502H11v3.502c0 .8-.698 1.5-1.499 1.5-.8 0-1.5-.7-1.5-1.5v-3.502h-1c-.601 0-1.001-.4-1.001-1V7.95h12.004v10.001c0 .6-.4 1.001-1.002 1.001h-1v3.502c0 .8-.7 1.5-1.5 1.5zm6.002-6.002c-.801 0-1.5-.698-1.5-1.499V9.449c0-.797.699-1.499 1.5-1.499.8 0 1.5.702 1.5 1.499v7.003c0 .801-.7 1.5-1.5 1.5zm-17.005 0c-.8 0-1.499-.698-1.499-1.499V9.449c0-.797.698-1.499 1.5-1.499.8 0 1.498.702 1.498 1.499v7.003c0 .801-.698 1.5-1.499 1.5zM16.802.151c.2.2.2.498 0 .698l-1.299 1.299c.419.28.798.621 1.128 1.012a5.876 5.876 0 0 1 1.363 3.476l.003.041c.004.09.007.18.007.271H6c0-.09.002-.18.006-.27l.002-.042c.07-1.32.574-2.54 1.364-3.476.33-.39.71-.733 1.128-1.012L7.2.848a.48.48 0 0 1 0-.697.479.479 0 0 1 .698 0L9.242 1.49l.062.062c.796-.399 1.689-.598 2.683-.6H12.016c.994.002 1.887.201 2.683.6l.062-.062L16.104.151a.479.479 0 0 1 .698 0zM9.5 5.201a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5zm5.005 0a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5z"  transform="translate(2 2)"></path></svg>
                               </div>
                    </div>
                  </div>
                  ):""}
                   <TopPicks goToSongList={this.goToSongList}/>
                  
                   <Trendings goToPlaySong={this.goToPlaySong}/>
                   <div className="row bgcolor px-4">
                     <div className="col-12">
                       <hr className="mb-0"/>
                     <img width="728" className="mx-auto d-block rounded"  height="90" src="https://ss3.zedo.com/OzoDB/k/g/2895490/V2/norton.jpg" border="0" alt="Click Here!" title="Click Here!"/>
                     </div>
                   </div>
                   <TopChart goToPlaylistSong={this.goToPlaylistSong}/>
                   <Footer/>
                 </div>
                 <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-fixed">
                     <div className="col-12 m-0 p-0 ">
                     <img className="m-0 p-0 " width="315" height="250" src="https://ss3.zedo.com/OzoDB/f/h/2851457/V4/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"/>
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>
             
            </React.Fragment>
           
        )
   }
}
const mapStoreToProps=(store)=>{
  console.log(store.duration);
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      duration:store.duration,
      search:store.search,

  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(song)=>dispatch({type:"Player",song:song}),
      getDuration:()=>dispatch({type:"Duration"}),
      showSearch:(value)=>dispatch({type:"Search",value:value})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(Home);