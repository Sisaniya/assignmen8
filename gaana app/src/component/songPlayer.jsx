import React,{Component} from "react";
import http from "../services/httpServices";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import Footer from "./footer";
class SongPlayer extends Component
{
    state={
        song:{},
        favoriteSongs:[],
    }
    addSongToPlaylist=()=>{
        let {song}=this.state;
        console.log("adding song to playlist");
       this.props.addSong(song);
        this.props.createPlaylist(1);
    }
    
    async fetchData() {
        let {name}=this.props.match.params;

        let response=await http.get(`/song/${name}`);
        let user=localStorage.getItem("user");
        let favoriteSongs=[];
        let role=localStorage.getItem("role");
        if(user && role=="user")
        {
           let res=await http.getData("/favoritesongs",{headers:{Authorization:user}});
           favoriteSongs=[...res.data];
        }
       
       
        let {data}=response;
       
        this.props.playMusic(data);
        this.setState({song:data,favoriteSongs:favoriteSongs});
    }
    componentDidMount() {
        this.fetchData();
    }
   
    async postFavorite(obj) {
        try {
            let response=await http.post("/favoritesongs",obj);
             let {data}=response;
             console.log(data);
             this.setState({...this.state,favoriteSongs:data});
        }
        catch(ex)
        {

        }
    }
    updateFavoriteList=()=>{
        let {song}=this.state;
        let email=localStorage.getItem("email");
        let obj={...song};
        let user=localStorage.getItem("user");
        if(user)
        {

            this.postFavorite({song:obj,email:email});

        }
        else
        {
            window.alert("Login to continue");
        }
       
    }
     getDuration=(url)=>{
        
       let m=new Audio(url);
        let d=Number.parseInt(m.duration);
        let minutes=Number.parseInt(d/60);
        let sconds=d-(60*minutes)
        console.log(d,minutes,sconds,m,m.duration,url);
       
       
    }
    goToUrl=(str)=>{
        this.props.history.push(str);
    }
    render()
    {
        let {song,favoriteSongs}=this.state;
        let {musicInfo,setMusic}=this.props;
        let {csong={}}=musicInfo;
        let {artist="",url=""}=csong;
        console.log(song,musicInfo);
        return (
            <React.Fragment>
              <div className="row">
                  <div className="col-9 bgsongcolor">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="100" className="mx-auto d-block rounded" decoding="async" src="https://tpc.googlesyndication.com/simgad/5953898921057943562?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&amp;rs=AOga4qn2IDHpwZmsIPuJvFHh9isPmRHh4w"/>
                          </div>
                      </div>
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle" onClick={()=>this.goToUrl("/home")}>Gaana</span>
                              <span className="dotstyle">.</span>
                              <span className="songlinkstyle" onClick={()=>this.goToUrl("/songs")}>Songs</span>
                              <span className="dotstyle">.</span>
                              <span className="songnamelinkstyle">{song.name} song</span>
                              <hr className="mt-0"/>
                          </div>
                      </div>

                     <div className="row mx-4 mb-2 ">
                         <div className="col-3">
                            <div className="songimgboxstyle">
                                <img src={song.img} width="190" height="160" className="d-block mx-auto rounded"/>
                            </div>
                         </div>
                         <div className="col-6">
                                <div className="titlestylesong">{song.name}</div>
                                <div className="text-muted titlestylesong1 mt-0">{song.name}</div>
                                <div className="">
                                    <span>Composed by </span>
                                    <span className="text-muted titlestylesong3 ">{song.artist}</span>
                                </div>
                                <div className="playallbutton" onClick={()=>setMusic(song)}>
                                    <i className={"fas  playallicon "+(musicInfo.status && song.url == csong.url?"fa-pause":"fa-play")}></i>
                                    <span className="playalltext">{musicInfo.status && song.url == csong.url?"PAUSE":"PLAY ALL"}</span>
                                </div>
                         </div>
                         <div className="col-3">
                             <div className="row">
                                 <div className="col-2">
                                 <i onClick={()=>this.updateFavoriteList()} class={" fa-heart  cps "+(favoriteSongs.findIndex(ele=>ele.id == song.id)>=0?"heartcolor fas":"heartstyle1 far")}></i>
                                
                                 </div>
                                 <div className="col-2">
                                 <svg width="20" height="20" viewBox="0 0 20 20" className="share option-queue-share" data-value="song36368218" data-type="share" title="Share">
                                             <path className="fill_path" data-value="song36368218" data-type="share" fill-rule="evenodd" d="M16 18a2 2 0 1 1 .001-4.001A2 2 0 0 1 16 18M4 12a2 2 0 1 1 .001-4.001A2 2 0 0 1 4 12M16 2a2 2 0 1 1-.001 4.001A2 2 0 0 1 16 2m0 10c-1.2 0-2.266.542-3 1.382l-5.091-2.546c.058-.27.091-.549.091-.836 0-.287-.033-.566-.091-.836L13 6.618C13.734 7.458 14.8 8 16 8c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4c0 .287.033.566.091.836L7 7.382A3.975 3.975 0 0 0 4 6c-2.206 0-4 1.794-4 4s1.794 4 4 4c1.2 0 2.266-.542 3-1.382l5.091 2.546c-.058.27-.091.549-.091.836 0 2.206 1.794 4 4 4s4-1.794 4-4-1.794-4-4-4"></path>
                                 </svg> 
 
                                 </div>
                                 
                                 <div className="col-2 cps  " onClick={()=>this.addSongToPlaylist()} >
                                 <svg  width="24" height="24" viewBox="0 0 24 24" className="addplaylist queue-addplaylisticon cps" data-value="song36368218" data-type="addtoplaylist" title="Add to Playlist"> <g fill="none" fill-rule="evenodd">
                                      <path d="M0 0h24v24H0z" data-value="song36368218" data-type="addtoplaylist"></path> 
                                      <path className="fill_path" data-value="song36368218" data-type="addtoplaylist" d="M21 22H7a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1zm-1.5-2a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0-.5.5v11a.5.5 0 0 0 .5.5h11zm-5-2h-1a.5.5 0 0 1-.5-.5V15h-2.5a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5H13v-2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5V13h2.5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H15v2.5a.5.5 0 0 1-.5.5zM4 5v14.778a2 2 0 0 1-2-2V3a1 1 0 0 1 1-1h14.778a2 2 0 0 1 2 2H5a1 1 0 0 0-1 1z"></path> </g>
                                 </svg> 

                                 </div>
                                <div className="col-2">
                                <svg width="16" height="20" viewBox="0 0 16 20" className="download" id="song_queue36368218" data-value="song36368218" data-type="download" title="Download Album ''"> <g fill="none" fill-rule="evenodd">
                                      <path d="M-4-1.995h24v24H-4z"></path>
                                       <path className="fill_path" data-value="song36368218" data-type="download" d="M14 16.001v2H2v-2H0v2c0 1.102.896 2 2 2h12c1.104 0 2-.898 2-2v-2h-2zM7.293 13.708a.997.997 0 0 0 1.414 0l5-5A1 1 0 0 0 13 7.001h-2v-6a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1v6H3a1 1 0 0 0-.707 1.707l5 5zM6 9.001a1 1 0 0 0 1-1v-6h2v6a1 1 0 0 0 1 1h.586L8 11.587 5.414 9.001H6z"></path> </g>
                                 </svg>

                                </div>
                                 <div className="col-2">
                                 <svg width="24" height="24" viewBox="0 0 24 24" data-type="song" data-value="36368218" className="moreopt"> <g fill="#FFF" fill-rule="evenodd"> 
                                  <path class="fill_path" data-type="song" data-value="36368218" d="M7 12a2 2 0 1 1-3.999.001A2 2 0 0 1 7 12M14 12a2 2 0 1 1-3.999.001A2 2 0 0 1 14 12M21 12a2 2 0 1 1-3.999.001A2 2 0 0 1 21 12"></path> </g> 
                                  </svg>

                                 </div>
                                  
                            </div>
                         </div>
                     </div>

                     <div className="row text-muted mx-5 mt-4 ">
                         <div className="col-2">#</div>
                         <div className="col-4">TITLE</div>
                         <div className="col-3">ARTIST</div>
                         <div className="col-3"></div>
                     </div>

                     <div className="row bg-white fontstyle1 songrow  py-2 my-0 mx-5 mt-2 mb-3 border-top border-bottom">
                         <div className="col-1">
                         <span className=" songlistplaybox" onClick={()=>setMusic(song)}>
                           
                            <i className={"fas songlistplaybtn tablesongfontcolor "+((musicInfo.status && song.url == musicInfo.csong.url)?"fa-pause":"fa-play")}></i>
                          </span>
                         
                         
                         </div>
                         <div className="col-1 pt-1 ">
                        
                          <i onClick={()=>this.updateFavoriteList()} class={" fa-heart  cps heartstylelist  "+(favoriteSongs.findIndex(ele=>ele.id == song.id)>=0?"heartcolor fas":"heartstyle1 far")}></i>
                                
                          </div>
                         <div className="col-4  tablesongfont tablesongtitle">
                         <div className="row">
                                    <div className="col-2 m-0 p-0 pl-3">
                                    <img src={song.img} width="33" height="33" style={{display:"inline-block"}} className="rounded m-0 p-0 d-block"/>
                                    </div>
                                    <div className="col-9 pt-1">
                                    {song.name}
                                    </div>
                                </div>
                             </div>
                         <div className="col-3 pt-1 tablesongfont tablesongfontcolor">{song.artist?song.artist.length > 21? song.artist.substring(0,21)+"...":song.artist:""}</div>
                         <div className="col-3 tablesongfont tablesongfontcolor">{this.getDuration(url)}</div>
                     </div>
                        <Footer/>
                  </div>
                  <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-fixed">
                     <div className="col-12 m-0 p-0 ">
                     <img className="m-0 p-0 " width="315" height="250" src="https://ss3.zedo.com/OzoDB/f/h/2851457/V4/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"/>
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>


              
            </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song}),
        addSong:(song)=>dispatch({type:"AddSong",song:song}),
        playMusic:(song)=>dispatch({type:"PlayMusic",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(SongPlayer);