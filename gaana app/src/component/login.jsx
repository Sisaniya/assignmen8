import React,{Component} from "react";
import http from "../services/httpServices";
class Login extends Component
{
    state={
        user:{email:"",password:""},
        page:"",
        error:"",
    }
    async postData(obj)
    {
        try
        {

        
        let {data:response,headers}=await http.post("/login",obj);
        let token=headers["x-auth-token"];
        localStorage.setItem("user",token);
        localStorage.setItem("email",response.email);
        localStorage.setItem("role",response.role);
        localStorage.setItem("id",response.id);
        localStorage.setItem("name",response.name);
      
        console.log(response,token);
        this.props.openLogin(0);
        window.location="/home";
        }
        catch(ex){
            let s={...this.state};
            s.error="Invalid email and password!";
            this.setState(s);
        }
    }
    handelLogin=()=>{
        let s={...this.state};
        if(s.user.password)
        {
             this.postData(s.user);
        }
        else
        {
            s.error="Please,enter your password";
        }
       
        this.setState(s);
    }
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s.user[input.name]=input.value;
        s.error="";
        this.setState(s);
    }
    goToBack=()=>{
       let s={...this.state};
        s.page=s.page=="password"?"email":"";
        this.setState(s);
    }
    setEmailPage=()=>{
        let s={...this.state};
        s.page="email";
        this.setState(s);
    }
    setPasswordPage=()=>{
        let s={...this.state};
        if(s.user.email)
        {
            s.page="password";
            s.user.password="";
        }
        else
        {
            s.error="Please,enter your email address";
        }
       
        this.setState(s);
    }
    showPasswordPage=()=>{
        let {user,error}=this.state;
        let {email,password}=user;
       
        return (
            <React.Fragment>
                <div className="row my-4">
                      <div className="col-12 text-center">
                          <svg width="130" height="50" viewBox="0 0 83 33"><g class="fill_path orange" fill-rule="evenodd" transform="translate(-84 -28)"><path d="M142.715 47.83c.182-1.009.346-1.955.518-2.901.572-3.215 1.144-6.429 1.698-9.643.163-.929-.418-1.599-1.38-1.643-.618-.027-1.244-.018-1.862-.018-1.289.009-2.142.759-2.36 2.009-.69 3.946-1.399 7.893-2.107 11.83-.027.134-.19.349-.29.349-1.771-.027-3.532-.045-5.294-.161-1.144-.072-2.061-.643-2.706-1.59-.036-.044-.082-.08-.145-.151-.245.259-.472.509-.717.74-.808.795-1.744 1.269-2.915 1.18-.826-.063-1.67 0-2.479-.144-1.444-.258-2.624-.964-3.332-2.366-.11.483-.227.956-.336 1.438-.245 1.152-.236 1.143-1.435 1.09-.853-.036-1.707-.153-2.442-.59-.563-.34-1.044-.821-1.607-1.268-.164.17-.39.42-.627.652-.808.786-1.743 1.232-2.915 1.187-1.116-.044-2.233.027-3.314-.366-1.925-.705-2.969-2.357-2.633-4.357.436-2.634.817-5.286 1.417-7.884.608-2.607 2.197-4.428 5.003-4.99.4-.081.808-.126 1.216-.126 2.316-.009 4.631-.009 6.946-.009.118 0 .227.018.382.036-.227 1.268-.454 2.509-.672 3.75-.518 2.866-1.044 5.723-1.544 8.59-.045.267.064.597.2.839.345.616 1.417 1.044 2.125.928.263-3.045.871-6.027 1.516-8.991.572-2.625 2.197-4.464 5.021-5.027.4-.08.808-.125 1.217-.125 2.315-.009 4.63-.009 6.946-.009.1 0 .209.018.363.027-.136.777-.263 1.536-.4 2.286-.617 3.402-1.243 6.803-1.86 10.205-.091.482.081.884.48 1.179.155.116.328.232.51.277.371.098.79.267 1.143.196.49-.098.309-.634.372-.973.709-3.84 1.39-7.679 2.07-11.527.1-.536.2-1.08.31-1.643.145-.009.272-.027.408-.027 2.252 0 4.513-.009 6.765 0a5.97 5.97 0 0 1 2.542.572c1.353.66 2.143 1.74 2.08 3.232-.046 1.25-.273 2.5-.482 3.74-.545 3.144-1.117 6.287-1.68 9.42-.027.18-.045.358-.1.527-.036.09-.154.206-.236.206-1.126.053-2.224.044-3.378.044zm-27.902-14.17c-.136-.026-.182-.035-.218-.035-1.144 0-2.297-.009-3.441 0-1.144.009-2.007.696-2.216 1.795a382.321 382.321 0 0 0-1.27 7.044c-.2 1.188.399 1.857 1.652 1.911.499.018.998.009 1.498 0 1.37-.009 2.188-.705 2.433-2.036.372-2.071.763-4.143 1.144-6.205a93.62 93.62 0 0 1 .418-2.473zm15.172.01c-.127-.027-.172-.045-.208-.045-1.154 0-2.298-.018-3.45 0-1.163.018-2.026.705-2.234 1.821a637.194 637.194 0 0 0-1.272 7.045c-.181 1.063.291 1.697 1.38 1.83.6.072 1.208.045 1.807.045 1.317-.009 2.152-.705 2.38-1.982l1.062-5.732c.181-.973.354-1.964.535-2.982zM98.605 47.84c-.317 0-.572.008-.826 0-1.253-.027-2.515.026-3.759-.108-1.88-.214-3.187-1.375-3.56-2.973-.208-.875-.09-1.741.064-2.607.418-2.304.817-4.616 1.262-6.92.536-2.786 2.724-4.786 5.62-5.009 1.699-.134 3.415-.08 5.122-.098.908-.009 1.807 0 2.787 0-.118.705-.227 1.375-.345 2.036-.653 3.651-1.316 7.303-1.98 10.955-.417 2.295-.798 4.59-1.27 6.875-.636 3.116-3.051 4.554-5.53 4.902-.6.08-1.208.09-1.807.09-2.088.008-4.177 0-6.265 0-.027 0-.054-.01-.118-.028.01-.053 0-.107.027-.151.581-1.027 1.153-2.054 1.762-3.063.09-.143.363-.241.554-.241 1.834-.018 3.659-.009 5.493-.009 1.398 0 2.116-.562 2.415-1.91.145-.563.227-1.099.354-1.742zm-1.38-3.519v.054c.472 0 .944-.036 1.408.009.463.045.626-.143.699-.571.481-2.804.98-5.608 1.48-8.411.19-1.054-.354-1.741-1.453-1.777-.608-.018-1.208-.009-1.816-.009-1.217.018-2.043.705-2.26 1.893-.237 1.286-.482 2.562-.71 3.848-.199 1.152-.426 2.304-.562 3.464-.1.83.363 1.375 1.226 1.474.653.08 1.325.026 1.988.026zM162.918 30.134c-.318 1.777-.635 3.491-.944 5.214a980.315 980.315 0 0 0-1.29 7.134c-.172.956.237 1.554 1.172 1.857.118.036.254.26.236.375-.163 1.027-.354 2.045-.545 3.125-1.607-.098-3.005-.58-4.004-1.937-.263.277-.49.535-.735.777-.8.767-1.717 1.232-2.879 1.17-1.008-.054-2.016.044-3.005-.26-2.225-.678-3.342-2.402-2.951-4.652.445-2.589.826-5.187 1.416-7.75.563-2.41 2.016-4.17 4.558-4.839a6.972 6.972 0 0 1 1.607-.223c2.361-.027 4.722-.009 7.074-.009.072 0 .154.009.29.018zm-4.176 3.527c-.182-.018-.291-.036-.41-.036-1.089 0-2.178-.009-3.268 0-1.126.018-1.988.679-2.188 1.759-.454 2.42-.88 4.84-1.299 7.268-.154.91.318 1.536 1.263 1.652.608.08 1.234.062 1.852.062 1.416 0 2.234-.714 2.479-2.09.4-2.25.826-4.49 1.235-6.731.118-.616.217-1.232.336-1.884z"></path></g></svg>
                      </div>
                  </div>
                  <div className="row mt-2 mb-4">
                      <div className="col-12 text-center">
                        <span>Welcome {email}</span>
                         
                      </div>
                  </div>

                  <div className="row mt-2 mb-4">
                      <div className="col-6 text-right">
                          Enter Password:
                      </div>
                      <div className="col-4 text-center">
                       
                           <input className="emailfield"
                            type="text"
                            name="password"
                           value={password}
                            placeholder="Enter your password"
                            onChange={this.handelChange}
                           />
                       <hr className="mt-0"/>
                      </div>
                      <div className="col-2"></div>
                      <div className="col-12 text-center text-danger">
                          {error && <span>{error}</span>}
                      </div>
                  </div>

                  <div className="row mt-4 justify-content-center">
                                <div className="col-2"></div>
                                <div className="col-5 mx-3 mt-4 emailpagebutton cps" onClick={()=>this.handelLogin()}>
                                  
                                      Login
                                </div>
                                <div className="col-2"></div>
                   </div>

                   <div className="row mt-4">
                       <div className="col-3"></div>
                       <div className="col-6 mt-2 text-center">
                           <span className="termsstylemain">By proceeding you agree to the <span className="termsstyle">Terms of Use</span></span>
                       </div>
                       <div className="col-3"></div>
                   </div>
            </React.Fragment>
        )
    }
    showWelcomePage=()=>{
        return (
            <React.Fragment>
                     
                               <div className="row mt-2">
                                   <div className="col-12 text-center">
                                       <span className="loginheading">INDIA'S NO. 1 MUSIC APP</span><br/>
                                       <small className="text-muted">Over 45 million songs to suit every mood and occasion</small>
                                   </div>
                               </div>
                               <div className="row text-center mt-4">
                                   <div className="col-4">
                                   <div class="plList"><svg width="30" height="30" viewBox="0 0 30 30"> <g fill="none" fill-rule="evenodd"> <circle cx="15" cy="15" r="14" stroke="#E72C30" stroke-width="2"></circle> <path fill="#E72C30" d="M20.132 13.263A.602.602 0 0 1 21 13.8v4.5a1.5 1.5 0 1 1-1.2-1.47v-2.059l-2.4 1.2v3.53a1.5 1.5 0 1 1-1.2-1.47V15.6a.6.6 0 0 1 .332-.538zM14.4 16.2v1.2H9v-1.2h5.4zm0-2.4V15H9v-1.2h5.4zm4.8-2.4v1.2H9v-1.2h10.2zm0-2.4v1.2H9V9h10.2z"></path> </g> </svg>
                                  <br/> <small className="text-muted">Create your<br/>own playlists</small>
                                   </div>
                                   </div>
                                   <div className="col-4">
                                   <div class="share"><svg width="30" height="30" viewBox="0 0 30 30"> <g fill="none" fill-rule="evenodd"> <circle cx="15" cy="15" r="14" stroke="#E72C30" stroke-width="2"></circle> <path fill="#E72C30" d="M18.6 19.8a1.2 1.2 0 1 1 0-2.4 1.2 1.2 0 0 1 0 2.4m-7.2-3.6a1.2 1.2 0 1 1 0-2.4 1.2 1.2 0 0 1 0 2.4m7.2-6a1.2 1.2 0 1 1 0 2.4 1.2 1.2 0 0 1 0-2.4m0 6c-.72 0-1.36.325-1.8.83l-3.055-1.528a2.384 2.384 0 0 0 0-1.004l3.055-1.527c.44.504 1.08.829 1.8.829 1.324 0 2.4-1.076 2.4-2.4 0-1.324-1.076-2.4-2.4-2.4a2.402 2.402 0 0 0-2.345 2.902L13.2 13.429a2.385 2.385 0 0 0-1.8-.829A2.402 2.402 0 0 0 9 15c0 1.324 1.076 2.4 2.4 2.4.72 0 1.36-.325 1.8-.83l3.055 1.528A2.402 2.402 0 0 0 18.6 21c1.324 0 2.4-1.076 2.4-2.4 0-1.324-1.076-2.4-2.4-2.4"></path> </g> </svg>
                                   <br/> <small className="text-muted">Share music with<br/>family and friends</small>
                                   </div>
                                   </div>
                                   <div className="col-4">
                                   <div class="like"><svg width="30" height="30" viewBox="0 0 30 30"> <g fill="none" fill-rule="evenodd"> <circle cx="15" cy="15" r="14" stroke="#E72C30" stroke-width="2"></circle> <path fill="#E72C30" d="M12.611 10.5c-1.194 0-2.167 1.01-2.167 2.25 0 2.522 3.354 5.405 5.056 6.605 1.702-1.2 5.056-4.083 5.056-6.605 0-1.24-.973-2.25-2.167-2.25-1.22 0-2.167 1.21-2.167 2.25 0 .414-.323.75-.722.75-.399 0-.722-.336-.722-.75 0-1.04-.947-2.25-2.167-2.25M15.5 21a.704.704 0 0 1-.383-.114C14.867 20.724 9 16.868 9 12.75 9 10.682 10.62 9 12.611 9c1.151 0 2.218.628 2.889 1.56.67-.932 1.738-1.56 2.889-1.56C20.38 9 22 10.682 22 12.75c0 4.118-5.867 7.974-6.117 8.136A.704.704 0 0 1 15.5 21"></path> </g> </svg>
                                   <br/><small className="text-muted">Save your<br/>favourites</small>
                                   </div>
                                   </div>
                               </div>
                            
                            <hr className="mx-4 "/>

                            <div className="row mt-4">
                                <div className="col-4"></div>
                                <div className="col-4 mt-2 loginButton cps" onClick={()=>this.setEmailPage()}>
                                    Login with Gmail
                                </div>
                                <div className="col-4"></div>
                            </div>

                            <div className="row mt-4 text-center">
                                <div className="col-3"></div>
                                <div className="col-6">
                                   <div className="row">
                                       <div className="col-4  m-0 p-0"><hr/></div>
                                       <div className="col-4 m-0 p-0">
                                           <span className="continuewith">or continue with</span>
                                       </div>
                                       <div className="col-4  m-0 p-0"><hr/></div>
                                   </div>
                                </div>
                                <div className="col-3"></div>
                            </div>

                            <div className="row mt-4 text-center">
                                <div className="col-3"></div>
                                <div className="col-6">
                                   <div className="row">
                                       <div className="col-4  ">
                                       <svg width="50" viewBox="0 0 50 50"> <g fill="none" fill-rule="evenodd"> <circle fill="#4267B2" cx="25" cy="25" r="25"></circle> <path d="M27.294 24.977V37h-4.943V24.977H20V20.75h2.351v-2.734c0-1.955.923-5.017 4.987-5.017l3.662.015v4.102h-2.657c-.436 0-1.048.219-1.048 1.152v2.486h3.694l-.432 4.222h-3.263z" fill="#FFF"></path> </g> </svg>
                                       <small>Facebook</small>
                                       </div>
                                       <div className="col-4">
                                       <svg  width="50" viewBox="0 0 52 52"> <g transform="translate(1 1)" fill="none" fill-rule="evenodd"> <circle stroke-opacity=".3" stroke="#000" fill="#FFF" cx="25" cy="25" r="25"></circle> <path d="M36.52 25.273c0-.851-.076-1.67-.218-2.455H25v4.642h6.458a5.52 5.52 0 01-2.394 3.622v3.01h3.878c2.269-2.088 3.578-5.165 3.578-8.82z" fill="#4285F4"></path> <path d="M25 37c3.24 0 5.956-1.075 7.942-2.907l-3.878-3.011c-1.075.72-2.45 1.145-4.064 1.145-3.125 0-5.77-2.11-6.715-4.947h-4.009v3.11A11.995 11.995 0 0025 37z" fill="#34A853"></path> <path d="M18.285 27.28A7.213 7.213 0 0117.91 25c0-.79.136-1.56.376-2.28v-3.11h-4.009A11.995 11.995 0 0013 25c0 1.936.464 3.77 1.276 5.39l4.01-3.11z" fill="#FBBC05"></path> <path d="M25 17.773c1.762 0 3.344.605 4.587 1.794l3.442-3.442C30.951 14.19 28.235 13 25 13c-4.69 0-8.75 2.69-10.724 6.61l4.01 3.11c.943-2.836 3.589-4.947 6.714-4.947z" fill="#EA4335"></path> </g> </svg>
                                          <small>Google</small>
                                       </div>
                                       <div className="col-4 ">
                                       <svg width="50" viewBox="0 0 52 52"> <g transform="translate(1 1)" fill="none" fill-rule="evenodd"> <circle stroke-opacity=".3" stroke="#000" fill="#FFF" cx="25" cy="25" r="25"></circle> <g fill="#000" fill-opacity=".4" fill-rule="nonzero"> <path d="M33.16 18H16.84c-.54 0-.84.37-.84.906v.453c0 .103.04.185.12.247l8.68 5.703c.14.082.32.082.44 0l8.64-5.806a.296.296 0 00.12-.247v-.35c0-.535-.3-.906-.84-.906z"></path> <path d="M33.78 21.397l-8.38 5.6c-.14.082-.22.124-.38.124a.588.588 0 01-.36-.124l-8.44-5.518c-.1-.061-.22 0-.22.124v9.409c0 .556.3.906.84.906h16.32c.54 0 .84-.37.84-.906V21.52c0-.124-.12-.206-.22-.124z"></path> </g> </g> </svg>
                                         <br/>  <small>Email</small>
                                       </div>
                                   </div>
                                </div>
                                <div className="col-3"></div>
                            </div>

            </React.Fragment>
        )
    }
    showEmailPage=()=>{
        let {user,error}=this.state;
        let {email,password}=user;
        return (
            <React.Fragment>
                  <div className="row my-4">
                      <div className="col-12 text-center">
                          <svg width="130" height="50" viewBox="0 0 83 33"><g class="fill_path orange" fill-rule="evenodd" transform="translate(-84 -28)"><path d="M142.715 47.83c.182-1.009.346-1.955.518-2.901.572-3.215 1.144-6.429 1.698-9.643.163-.929-.418-1.599-1.38-1.643-.618-.027-1.244-.018-1.862-.018-1.289.009-2.142.759-2.36 2.009-.69 3.946-1.399 7.893-2.107 11.83-.027.134-.19.349-.29.349-1.771-.027-3.532-.045-5.294-.161-1.144-.072-2.061-.643-2.706-1.59-.036-.044-.082-.08-.145-.151-.245.259-.472.509-.717.74-.808.795-1.744 1.269-2.915 1.18-.826-.063-1.67 0-2.479-.144-1.444-.258-2.624-.964-3.332-2.366-.11.483-.227.956-.336 1.438-.245 1.152-.236 1.143-1.435 1.09-.853-.036-1.707-.153-2.442-.59-.563-.34-1.044-.821-1.607-1.268-.164.17-.39.42-.627.652-.808.786-1.743 1.232-2.915 1.187-1.116-.044-2.233.027-3.314-.366-1.925-.705-2.969-2.357-2.633-4.357.436-2.634.817-5.286 1.417-7.884.608-2.607 2.197-4.428 5.003-4.99.4-.081.808-.126 1.216-.126 2.316-.009 4.631-.009 6.946-.009.118 0 .227.018.382.036-.227 1.268-.454 2.509-.672 3.75-.518 2.866-1.044 5.723-1.544 8.59-.045.267.064.597.2.839.345.616 1.417 1.044 2.125.928.263-3.045.871-6.027 1.516-8.991.572-2.625 2.197-4.464 5.021-5.027.4-.08.808-.125 1.217-.125 2.315-.009 4.63-.009 6.946-.009.1 0 .209.018.363.027-.136.777-.263 1.536-.4 2.286-.617 3.402-1.243 6.803-1.86 10.205-.091.482.081.884.48 1.179.155.116.328.232.51.277.371.098.79.267 1.143.196.49-.098.309-.634.372-.973.709-3.84 1.39-7.679 2.07-11.527.1-.536.2-1.08.31-1.643.145-.009.272-.027.408-.027 2.252 0 4.513-.009 6.765 0a5.97 5.97 0 0 1 2.542.572c1.353.66 2.143 1.74 2.08 3.232-.046 1.25-.273 2.5-.482 3.74-.545 3.144-1.117 6.287-1.68 9.42-.027.18-.045.358-.1.527-.036.09-.154.206-.236.206-1.126.053-2.224.044-3.378.044zm-27.902-14.17c-.136-.026-.182-.035-.218-.035-1.144 0-2.297-.009-3.441 0-1.144.009-2.007.696-2.216 1.795a382.321 382.321 0 0 0-1.27 7.044c-.2 1.188.399 1.857 1.652 1.911.499.018.998.009 1.498 0 1.37-.009 2.188-.705 2.433-2.036.372-2.071.763-4.143 1.144-6.205a93.62 93.62 0 0 1 .418-2.473zm15.172.01c-.127-.027-.172-.045-.208-.045-1.154 0-2.298-.018-3.45 0-1.163.018-2.026.705-2.234 1.821a637.194 637.194 0 0 0-1.272 7.045c-.181 1.063.291 1.697 1.38 1.83.6.072 1.208.045 1.807.045 1.317-.009 2.152-.705 2.38-1.982l1.062-5.732c.181-.973.354-1.964.535-2.982zM98.605 47.84c-.317 0-.572.008-.826 0-1.253-.027-2.515.026-3.759-.108-1.88-.214-3.187-1.375-3.56-2.973-.208-.875-.09-1.741.064-2.607.418-2.304.817-4.616 1.262-6.92.536-2.786 2.724-4.786 5.62-5.009 1.699-.134 3.415-.08 5.122-.098.908-.009 1.807 0 2.787 0-.118.705-.227 1.375-.345 2.036-.653 3.651-1.316 7.303-1.98 10.955-.417 2.295-.798 4.59-1.27 6.875-.636 3.116-3.051 4.554-5.53 4.902-.6.08-1.208.09-1.807.09-2.088.008-4.177 0-6.265 0-.027 0-.054-.01-.118-.028.01-.053 0-.107.027-.151.581-1.027 1.153-2.054 1.762-3.063.09-.143.363-.241.554-.241 1.834-.018 3.659-.009 5.493-.009 1.398 0 2.116-.562 2.415-1.91.145-.563.227-1.099.354-1.742zm-1.38-3.519v.054c.472 0 .944-.036 1.408.009.463.045.626-.143.699-.571.481-2.804.98-5.608 1.48-8.411.19-1.054-.354-1.741-1.453-1.777-.608-.018-1.208-.009-1.816-.009-1.217.018-2.043.705-2.26 1.893-.237 1.286-.482 2.562-.71 3.848-.199 1.152-.426 2.304-.562 3.464-.1.83.363 1.375 1.226 1.474.653.08 1.325.026 1.988.026zM162.918 30.134c-.318 1.777-.635 3.491-.944 5.214a980.315 980.315 0 0 0-1.29 7.134c-.172.956.237 1.554 1.172 1.857.118.036.254.26.236.375-.163 1.027-.354 2.045-.545 3.125-1.607-.098-3.005-.58-4.004-1.937-.263.277-.49.535-.735.777-.8.767-1.717 1.232-2.879 1.17-1.008-.054-2.016.044-3.005-.26-2.225-.678-3.342-2.402-2.951-4.652.445-2.589.826-5.187 1.416-7.75.563-2.41 2.016-4.17 4.558-4.839a6.972 6.972 0 0 1 1.607-.223c2.361-.027 4.722-.009 7.074-.009.072 0 .154.009.29.018zm-4.176 3.527c-.182-.018-.291-.036-.41-.036-1.089 0-2.178-.009-3.268 0-1.126.018-1.988.679-2.188 1.759-.454 2.42-.88 4.84-1.299 7.268-.154.91.318 1.536 1.263 1.652.608.08 1.234.062 1.852.062 1.416 0 2.234-.714 2.479-2.09.4-2.25.826-4.49 1.235-6.731.118-.616.217-1.232.336-1.884z"></path></g></svg>
                      </div>
                  </div>
                  <div className="row mt-2 mb-4">
                      <div className="col-12 text-center">
                        <span>Hello, Enter your email address</span>
                         
                      </div>
                  </div>

                  <div className="row mt-2 mb-4">
                      <div className="col-3"></div>
                      <div className="col-6 text-center">
                       
                           <input className="emailfield"
                            type="text"
                            name="email"
                            value={email}
                            placeholder="example@gmail.com"
                            onChange={this.handelChange}
                           />
                       <hr className="mt-0"/>
                      </div>
                      <div className="col-3"></div>
                      <div className="col-12 text-center text-danger">
                          {error && <span>{error}</span>}
                      </div>
                  </div>

                  <div className="row mt-4 justify-content-center">
                                <div className="col-2"></div>
                                <div className="col-5 mx-3 mt-4 cps emailpagebutton" onClick={()=>this.setPasswordPage()}>
                                   <span className="mr-2">Continue</span>   <i className="fas fa-arrow-right"></i>
                                </div>
                                <div className="col-2"></div>
                   </div>

                   <div className="row mt-4">
                       <div className="col-3"></div>
                       <div className="col-6 mt-2 text-center">
                           <span className="termsstylemain">By proceeding you agree to the <span className="termsstyle">Terms of Use</span></span>
                       </div>
                       <div className="col-3"></div>
                   </div>
            </React.Fragment>
        )
    }
    render() {
        let {page,email,password}=this.state;
        return (
            <React.Fragment>
                  <div className="box">
                       <div className="container-box" onClick={()=>this.props.openLogin(0)}>
                           
                       </div>
                  </div>
                  <div className="box-body">
                              <div className="row mt-2 mx-2">
                                   <div className="col-4 mt-1  text-left">
                                       {page?(
                                          <i onClick={()=>this.goToBack()} className="fas fa-arrow-left arrowicon"></i>
                                       ):""}
                                   </div>
                                   <div className="col-4"></div>
                                   <div className="col-4 text-right">
                                   <svg className="mr-2 " onClick={()=>this.props.openLogin(0)} width="17" height="17" viewBox="0 0 17 17"> <path className="fill_path" fill-rule="evenodd" d="M16.293 1.592l-1.3-1.3-6.7 6.701-6.7-6.7-1.3 1.299 6.7 6.7-6.7 6.701 1.3 1.3 6.7-6.7 6.7 6.7 1.3-1.3-6.7-6.7z"></path> </svg>
                                   </div>
                               </div>
                      {page=="email"?this.showEmailPage():page=="password"?this.showPasswordPage():this.showWelcomePage()}       
                  </div>
            </React.Fragment>
        )
    }
}
export default Login;