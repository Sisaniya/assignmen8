import React,{Component} from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {createBrowserHistory} from "history";
import http from "../services/httpServices";
import {ClickOutsideListener} from "react-click-outside-listener";
class Navbar extends Component
{
    state={
        showDropdown:0,
    }
    handelDropdown=()=>{
        let s={...this.state};
        s.showDropdown=s.showDropdown?0:1;
        this.setState(s);
    }
    handelDropdownOutside=()=>{
        let s={...this.state};
        if(s.showDropdown)
        {
            s.showDropdown=0;
            this.setState(s);
        }
       
    }
    render() {
        let {user}=this.props;
        let {showDropdown}=this.state;
        let email=localStorage.getItem("email");
        let role=localStorage.getItem("role");
        let history=createBrowserHistory();
        let {pathname}=this.props;
        console.log(pathname,history.location);
        return (
           
                <div className="row header p-0 m-0 sticky-top">
                      <div className="col-1 ">
                      <a className="navbar-brand logo">
                              <svg width="83" height="33" viewBox="0 0 83 33">
                                   <g className="fill_path orange" fill-rule="evenodd" transform="translate(-84 -28)"> 
                                   <path d="M142.715 47.83c.182-1.009.346-1.955.518-2.901.572-3.215 1.144-6.429 1.698-9.643.163-.929-.418-1.599-1.38-1.643-.618-.027-1.244-.018-1.862-.018-1.289.009-2.142.759-2.36 2.009-.69 3.946-1.399 7.893-2.107 11.83-.027.134-.19.349-.29.349-1.771-.027-3.532-.045-5.294-.161-1.144-.072-2.061-.643-2.706-1.59-.036-.044-.082-.08-.145-.151-.245.259-.472.509-.717.74-.808.795-1.744 1.269-2.915 1.18-.826-.063-1.67 0-2.479-.144-1.444-.258-2.624-.964-3.332-2.366-.11.483-.227.956-.336 1.438-.245 1.152-.236 1.143-1.435 1.09-.853-.036-1.707-.153-2.442-.59-.563-.34-1.044-.821-1.607-1.268-.164.17-.39.42-.627.652-.808.786-1.743 1.232-2.915 1.187-1.116-.044-2.233.027-3.314-.366-1.925-.705-2.969-2.357-2.633-4.357.436-2.634.817-5.286 1.417-7.884.608-2.607 2.197-4.428 5.003-4.99.4-.081.808-.126 1.216-.126 2.316-.009 4.631-.009 6.946-.009.118 0 .227.018.382.036-.227 1.268-.454 2.509-.672 3.75-.518 2.866-1.044 5.723-1.544 8.59-.045.267.064.597.2.839.345.616 1.417 1.044 2.125.928.263-3.045.871-6.027 1.516-8.991.572-2.625 2.197-4.464 5.021-5.027.4-.08.808-.125 1.217-.125 2.315-.009 4.63-.009 6.946-.009.1 0 .209.018.363.027-.136.777-.263 1.536-.4 2.286-.617 3.402-1.243 6.803-1.86 10.205-.091.482.081.884.48 1.179.155.116.328.232.51.277.371.098.79.267 1.143.196.49-.098.309-.634.372-.973.709-3.84 1.39-7.679 2.07-11.527.1-.536.2-1.08.31-1.643.145-.009.272-.027.408-.027 2.252 0 4.513-.009 6.765 0a5.97 5.97 0 0 1 2.542.572c1.353.66 2.143 1.74 2.08 3.232-.046 1.25-.273 2.5-.482 3.74-.545 3.144-1.117 6.287-1.68 9.42-.027.18-.045.358-.1.527-.036.09-.154.206-.236.206-1.126.053-2.224.044-3.378.044zm-27.902-14.17c-.136-.026-.182-.035-.218-.035-1.144 0-2.297-.009-3.441 0-1.144.009-2.007.696-2.216 1.795a382.321 382.321 0 0 0-1.27 7.044c-.2 1.188.399 1.857 1.652 1.911.499.018.998.009 1.498 0 1.37-.009 2.188-.705 2.433-2.036.372-2.071.763-4.143 1.144-6.205a93.62 93.62 0 0 1 .418-2.473zm15.172.01c-.127-.027-.172-.045-.208-.045-1.154 0-2.298-.018-3.45 0-1.163.018-2.026.705-2.234 1.821a637.194 637.194 0 0 0-1.272 7.045c-.181 1.063.291 1.697 1.38 1.83.6.072 1.208.045 1.807.045 1.317-.009 2.152-.705 2.38-1.982l1.062-5.732c.181-.973.354-1.964.535-2.982zM98.605 47.84c-.317 0-.572.008-.826 0-1.253-.027-2.515.026-3.759-.108-1.88-.214-3.187-1.375-3.56-2.973-.208-.875-.09-1.741.064-2.607.418-2.304.817-4.616 1.262-6.92.536-2.786 2.724-4.786 5.62-5.009 1.699-.134 3.415-.08 5.122-.098.908-.009 1.807 0 2.787 0-.118.705-.227 1.375-.345 2.036-.653 3.651-1.316 7.303-1.98 10.955-.417 2.295-.798 4.59-1.27 6.875-.636 3.116-3.051 4.554-5.53 4.902-.6.08-1.208.09-1.807.09-2.088.008-4.177 0-6.265 0-.027 0-.054-.01-.118-.028.01-.053 0-.107.027-.151.581-1.027 1.153-2.054 1.762-3.063.09-.143.363-.241.554-.241 1.834-.018 3.659-.009 5.493-.009 1.398 0 2.116-.562 2.415-1.91.145-.563.227-1.099.354-1.742zm-1.38-3.519v.054c.472 0 .944-.036 1.408.009.463.045.626-.143.699-.571.481-2.804.98-5.608 1.48-8.411.19-1.054-.354-1.741-1.453-1.777-.608-.018-1.208-.009-1.816-.009-1.217.018-2.043.705-2.26 1.893-.237 1.286-.482 2.562-.71 3.848-.199 1.152-.426 2.304-.562 3.464-.1.83.363 1.375 1.226 1.474.653.08 1.325.026 1.988.026zM162.918 30.134c-.318 1.777-.635 3.491-.944 5.214a980.315 980.315 0 0 0-1.29 7.134c-.172.956.237 1.554 1.172 1.857.118.036.254.26.236.375-.163 1.027-.354 2.045-.545 3.125-1.607-.098-3.005-.58-4.004-1.937-.263.277-.49.535-.735.777-.8.767-1.717 1.232-2.879 1.17-1.008-.054-2.016.044-3.005-.26-2.225-.678-3.342-2.402-2.951-4.652.445-2.589.826-5.187 1.416-7.75.563-2.41 2.016-4.17 4.558-4.839a6.972 6.972 0 0 1 1.607-.223c2.361-.027 4.722-.009 7.074-.009.072 0 .154.009.29.018zm-4.176 3.527c-.182-.018-.291-.036-.41-.036-1.089 0-2.178-.009-3.268 0-1.126.018-1.988.679-2.188 1.759-.454 2.42-.88 4.84-1.299 7.268-.154.91.318 1.536 1.263 1.652.608.08 1.234.062 1.852.062 1.416 0 2.234-.714 2.479-2.09.4-2.25.826-4.49 1.235-6.731.118-.616.217-1.232.336-1.884z"></path> </g> </svg>
                        </a>
                      </div>
                    <div className="col-6 navitem  p-0">
                       
                       <nav className="navbar navbar-expand-sm " >
                      
                           <button className="navbar-toggler" data-target="#mynavbar" data-toggle="collapse">
                             <span className="navbar-toggler-icon"></span>
                           </button>
                           <div className="collapse navbar-collapse " id="mynavbar">
                               <ul className="navbar-nav">
                                 <li className="nav-item" onClick={()=>this.props.setPathName("/home")}>
                                     <Link to="/home" className={"nav-link  tlcolor "+(pathname=="/home"?"active":"")}>HOME</Link>
                                 </li>
                                 <li className="nav-item">
                                     <Link className="nav-link  tlcolor">BROWSE</Link>
                                 </li>
                                 <li className="nav-item">
                                     <Link className="nav-link tlcolor">DISCOVER</Link>
                                 </li>
                                 <li className="nav-item">
                                     <Link className="nav-link tlcolor">RADIO</Link>
                                 </li>
                                 {email?(
                                 <li className="nav-item" onClick={()=>this.props.setPathName("/music")}>
                                     <Link className={"nav-link  tlcolor "+(pathname=="/music"?"active":"")} to="/music">MY MUSIC</Link>
                                 </li>
                                 ):""}
                                 <li className="nav-item">
                                     <Link className="nav-link tlcolor">PODCASTS</Link>
                                 </li>
                                 <li className="nav-item">
                                     <Link className="nav-link tlcolor">INDIA'S MUSIC</Link>
                                 </li>
                                 {!this.props.search?(
                                <li className="nav-item">
                                      <span className="searchbtnbox" onClick={()=>this.props.showSearch(1)}>
                                      <svg width="25" height="25" viewBox="0 0 25 25"><path className="searchbtnboxcolor"  fill="#FFF" fill-rule="evenodd" d="M69.5 34a6.5 6.5 0 0 1 6.5 6.5c0 1.61-.59 3.09-1.56 4.23l.27.27h.79l5 5-1.5 1.5-5-5v-.79l-.27-.27A6.516 6.516 0 0 1 69.5 47a6.5 6.5 0 1 1 0-13zm0 2C67 36 65 38 65 40.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5z" transform="translate(-59 -32)"></path></svg>
                                 
                                      </span>
                                 
                                  </li>

                                 ):""}
                                
                               </ul>
                           </div>
                       </nav>
                    </div>
                    {email  && role=="user" && <div className="col-1"></div>}
                    <div className="col-3 pl-4 border-right  m-0">
                      
                           
                                <div className="gtaddf ml-2 mrfgtaddf" >
                                GO ADD FREE
                                </div>
                               
                           
                                <div className="gtaddf">  GET GAANA PLUS</div>
                             
                          
                    </div>

                    {!email && 
                    <div className="col-2">
                        <div className="row">
                        <div className="iconsfn col-3">
                        <div className="iconstyle">
                              <svg width="24" height="24" viewBox="0 0 24 24">
                                     <g fill="none" fill-rule="evenodd">
                                         <path d="M0 0h24v24H0z">
                                </path>
                                <path className="fill_path" d="M6.6 3.582A9.981 9.981 0 0 0 6 7c0 5.523 4.477 10 10 10 1.99 0 3.843-.58 5.4-1.582A10.004 10.004 0 0 1 12 22C6.477 22 2 17.523 2 12a9.992 9.992 0 0 1 4.6-8.418z"></path>
                             </g></svg>
                           </div>
                              
                       
                       <div className="themehover outsidelogin " >
                         
                           <div className="themehoverinner m-0 ">
                               <span className="cois">Check out Gaana in White Theme</span>
                               <p className="cois1">Click on the Moon icon to continue</p>
                            </div>
                        </div> 
                        </div>

                        <div className= "iconsfn1 col-3 ">
                             <div className="iconstyle1 ">
                                     <svg width="18" height="18" viewBox="0 0 18 18" id="languageicon" >
                                         <path className="fill_path" fill="#000" fill-rule="evenodd" d="M.002.992C.002.443.447 0 .995 0H18L0 18.001.002.992zm10.499.996H8.934v7.01l-.858-.627v-2.92a1.46 1.46 0 0 1-.214.067A2.23 2.23 0 0 1 7.3 5.6c-.11 0-.192-.005-.247-.016a2.492 2.492 0 0 1-.858-.297 1.845 1.845 0 0 1-.33-.231c.044.11.071.198.082.264.055.22.083.434.083.643 0 .22-.02.402-.058.545a1.464 1.464 0 0 1-.255.478c-.396.56-.902.841-1.518.841-.297 0-.594-.071-.89-.214a3.265 3.265 0 0 1-.842-.594c-.22-.21-.473-.5-.759-.874a7.408 7.408 0 0 1-.478-.76c-.055-.11-.132-.258-.231-.444l.363-.198c.12.209.214.373.28.494.231.385.424.666.578.842.23.264.48.48.75.651.27.17.553.256.85.256.198 0 .379-.039.544-.116.319-.143.566-.36.742-.651.176-.292.264-.564.264-.817 0-.374-.126-.66-.38-.857a1.3 1.3 0 0 0-.395-.198l-1.468.676-.479-.775 1.469-.66c.197-.088.34-.165.428-.231.286-.198.43-.423.43-.676a2.72 2.72 0 0 0 .016-.231c0-.264-.072-.457-.215-.578-.143-.12-.34-.181-.593-.181a1.99 1.99 0 0 0-.479.066 7.642 7.642 0 0 0-.56.165l-.726.38-.512-.776c.165-.077.292-.137.38-.181.297-.143.536-.237.717-.28A2.48 2.48 0 0 1 3.59.997c.594 0 1.105.176 1.534.528.429.352.643.78.643 1.287 0 .11-.005.192-.016.247-.044.253-.16.5-.347.742-.055.077-.148.17-.28.28.209.188.385.325.528.413.461.286.945.429 1.451.429.231 0 .462-.099.693-.297.077-.066.17-.17.28-.313V1.988H6.51l-.56-.858H9.94l.561.858zm7.497 15.02a.994.994 0 0 1-.993.993H0L18-.001l-.002 17.01zm-2.673-.76a.99.99 0 0 0 .994-.999l-.19-12.56L2.912 16.261l12.413-.015zm-1.748-1.434l-.307-.808h-2.618l-.308.826c-.12.322-.222.54-.307.652-.085.113-.224.17-.417.17a.612.612 0 0 1-.435-.18.551.551 0 0 1-.19-.41c0-.087.016-.178.045-.272.03-.093.077-.224.145-.39l1.647-4.182.17-.433c.065-.168.135-.308.21-.42a.857.857 0 0 1 .294-.27.902.902 0 0 1 .45-.103c.182 0 .334.035.455.103.122.07.22.158.294.266.075.108.138.225.19.35.05.124.116.29.195.498l1.682 4.155c.132.316.198.546.198.69a.57.57 0 0 1-.187.41.614.614 0 0 1-.45.187.54.54 0 0 1-.448-.204 1.374 1.374 0 0 1-.16-.29c-.058-.13-.107-.245-.148-.345zm-2.583-1.788h1.924l-.97-2.657-.954 2.657z" opacity=".7"></path></svg>
                            </div>
                        </div>

                        <div className={"col-6  "+(email?"":"iconsfn1 ")+(showDropdown?"dropdownbg":"")}>
                           
                           <div className="iconstyle2 " onClick={()=>this.props.openLogin(1)}>
                            SIGN IN
                            </div>
                       
                        </div>

                        </div>
                       
                        
                    </div>}


                    {email && role=="admin" && 
                    <div className="col-2">
                        <div className="row">
                        <div className="iconsfn col-3">
                        <div className="iconstyle">
                              <svg width="24" height="24" viewBox="0 0 24 24">
                                     <g fill="none" fill-rule="evenodd">
                                         <path d="M0 0h24v24H0z">
                                </path>
                                <path className="fill_path" d="M6.6 3.582A9.981 9.981 0 0 0 6 7c0 5.523 4.477 10 10 10 1.99 0 3.843-.58 5.4-1.582A10.004 10.004 0 0 1 12 22C6.477 22 2 17.523 2 12a9.992 9.992 0 0 1 4.6-8.418z"></path>
                             </g></svg>
                           </div>
                              
                       
                       <div className="themehover outsidelogin " >
                         
                           <div className="themehoverinner m-0 ">
                               <span className="cois">Check out Gaana in White Theme</span>
                               <p className="cois1">Click on the Moon icon to continue</p>
                            </div>
                        </div> 
                        </div>

                        <div className= "iconsfn1 col-3 ">
                             <div className="iconstyle1 ">
                                     <svg width="18" height="18" viewBox="0 0 18 18" id="languageicon" >
                                         <path className="fill_path" fill="#000" fill-rule="evenodd" d="M.002.992C.002.443.447 0 .995 0H18L0 18.001.002.992zm10.499.996H8.934v7.01l-.858-.627v-2.92a1.46 1.46 0 0 1-.214.067A2.23 2.23 0 0 1 7.3 5.6c-.11 0-.192-.005-.247-.016a2.492 2.492 0 0 1-.858-.297 1.845 1.845 0 0 1-.33-.231c.044.11.071.198.082.264.055.22.083.434.083.643 0 .22-.02.402-.058.545a1.464 1.464 0 0 1-.255.478c-.396.56-.902.841-1.518.841-.297 0-.594-.071-.89-.214a3.265 3.265 0 0 1-.842-.594c-.22-.21-.473-.5-.759-.874a7.408 7.408 0 0 1-.478-.76c-.055-.11-.132-.258-.231-.444l.363-.198c.12.209.214.373.28.494.231.385.424.666.578.842.23.264.48.48.75.651.27.17.553.256.85.256.198 0 .379-.039.544-.116.319-.143.566-.36.742-.651.176-.292.264-.564.264-.817 0-.374-.126-.66-.38-.857a1.3 1.3 0 0 0-.395-.198l-1.468.676-.479-.775 1.469-.66c.197-.088.34-.165.428-.231.286-.198.43-.423.43-.676a2.72 2.72 0 0 0 .016-.231c0-.264-.072-.457-.215-.578-.143-.12-.34-.181-.593-.181a1.99 1.99 0 0 0-.479.066 7.642 7.642 0 0 0-.56.165l-.726.38-.512-.776c.165-.077.292-.137.38-.181.297-.143.536-.237.717-.28A2.48 2.48 0 0 1 3.59.997c.594 0 1.105.176 1.534.528.429.352.643.78.643 1.287 0 .11-.005.192-.016.247-.044.253-.16.5-.347.742-.055.077-.148.17-.28.28.209.188.385.325.528.413.461.286.945.429 1.451.429.231 0 .462-.099.693-.297.077-.066.17-.17.28-.313V1.988H6.51l-.56-.858H9.94l.561.858zm7.497 15.02a.994.994 0 0 1-.993.993H0L18-.001l-.002 17.01zm-2.673-.76a.99.99 0 0 0 .994-.999l-.19-12.56L2.912 16.261l12.413-.015zm-1.748-1.434l-.307-.808h-2.618l-.308.826c-.12.322-.222.54-.307.652-.085.113-.224.17-.417.17a.612.612 0 0 1-.435-.18.551.551 0 0 1-.19-.41c0-.087.016-.178.045-.272.03-.093.077-.224.145-.39l1.647-4.182.17-.433c.065-.168.135-.308.21-.42a.857.857 0 0 1 .294-.27.902.902 0 0 1 .45-.103c.182 0 .334.035.455.103.122.07.22.158.294.266.075.108.138.225.19.35.05.124.116.29.195.498l1.682 4.155c.132.316.198.546.198.69a.57.57 0 0 1-.187.41.614.614 0 0 1-.45.187.54.54 0 0 1-.448-.204 1.374 1.374 0 0 1-.16-.29c-.058-.13-.107-.245-.148-.345zm-2.583-1.788h1.924l-.97-2.657-.954 2.657z" opacity=".7"></path></svg>
                            </div>
                        </div>

                        <ClickOutsideListener onClickOutside={()=>this.handelDropdownOutside()}>
                            <div onClick={()=>this.handelDropdown()} className={"col-6 dropdown  cps pl-2 "+(showDropdown?"dropdownbg":"")}>
                           <div className={""} >
                                <div className={"dropdown-btn "+(showDropdown?"dropdownbg":"")}>
                                <img className=" userimg  mt-2 mx-auto d-block" width="40" height="40" src="https://a10.gaanacdn.com/images/users/496/crop_110x110_177224496.jpg"/>
                                </div>
                           
                               <div className={"dropdown-content  "+(showDropdown?"userdropdownactive":"userdropdownnotactive")}>
                               <Link className="redcolor" to="/account">
                                   <div className=" ">
                                      <div className=" dropdown-item pl-4 py-2">
                                          Account
                                         
                                      </div>
                                   </div>
                                   </Link>
                                   <Link className="redcolor" to="/formUpload">
                                   <div className="">
                                      <div className="redcolor dropdown-item pl-4 py-2">
                                          Upload Song
                                         
                                      </div>
                                   </div>
                                   </Link>
                                   <Link className="redcolor" to="/csvUpload">
                                   <div className="">
                                      <div className=" dropdown-item pl-4 py-2">
                                     Upload CSV
                                          
                                      </div>
                                   </div>
                                   </Link>

                                   <Link className="redcolor" to="/managesongs">
                                   <div className="">
                                      <div className=" dropdown-item pl-4 py-2">
                                     Download Songs
                                          
                                      </div>
                                   </div>
                                   </Link>
                                   <div onClick={()=>this.goToLogout()} className="border-top">
                                      <div  className=" dropdown-item pl-4 py-2">
                                          Logout
                                      </div>
                                   </div>
                               </div>
                               
                                </div>
 
                            </div>
                            </ClickOutsideListener>

                        </div>
                       
                        
                    </div>}


                   
                    {email && role=="user" && <div className="col-1">
                        <div className="row ">
                        <div className= "ml-0 pl-0 text-center iconsfn1 col-5 ">
                             <div className="iconstyle2  text-center">
                             <svg width="26" height="26" >
                                 <g fill="none" fill-rule="evenodd" ><ellipse cx="16.222" cy="1.778" rx="1.778" ry="1.778"></ellipse><g fill="#FFF" fill-opacity=".7"><path class="fill_path" d="M5.555 18c.693 1.19 1.969 2 3.445 2 1.477 0 2.752-.81 3.446-2H5.555zM3.035 15h11.93A5.376 5.376 0 0 1 13 10.838V8c0-2.206-1.794-4-4-4-2.205 0-4 1.794-4 4v2.838c0 1.648-.745 3.16-1.965 4.162zM18 17H0v-2.721l.684-.227A3.385 3.385 0 0 0 3 10.838V8c0-3.309 2.692-6 6-6 3.309 0 6 2.691 6 6v2.838c0 1.46.932 2.751 2.317 3.214l.683.227V17z"></path></g></g></svg>
                            </div>
                        </div>
                             <ClickOutsideListener onClickOutside={()=>this.handelDropdownOutside()}>
                            <div onClick={()=>this.handelDropdown()} className={"col-6 dropdown  cps pl-2 "+(showDropdown?"dropdownbg":"")}>
                           <div className={""} >
                                <div className={"dropdown-btn "+(showDropdown?"dropdownbg":"")}>
                                <img className=" userimg  mt-2 " width="40" height="40" src="https://a10.gaanacdn.com/images/users/496/crop_110x110_177224496.jpg"/>
                                </div>
                           
                               <div className={"dropdown-content  "+(showDropdown?"userdropdownactive":"userdropdownnotactive")}>
                               <Link className="redcolor" to="/profile">
                                   <div className=" ">
                                      <div className=" dropdown-item pl-4 py-2">
                                         
                                         
                                      </div>
                                   </div>
                                   </Link>

                                   <Link className="redcolor" to="/music">
                                   <div className="">
                                      <div className="redcolor dropdown-item pl-4 py-2">
                                         Playlist
                                         
                                      </div>
                                   </div>
                                   </Link>

                                   <Link className="redcolor" to="/myfavoritetracks">
                                   <div className="">
                                      <div className=" dropdown-item pl-4 py-2">
                                      Favorite Songs
                                          
                                      </div>
                                   </div>
                                   </Link>

                                   <Link className="redcolor" to="/myfavoriteplaylists">
                                   <div className="">
                                      <div className=" dropdown-item pl-4 py-2">
                                      Favorite Playlist
                                          
                                      </div>
                                   </div>
                                   </Link>
                                   <div onClick={()=>this.goToLogout()} className="border-top">
                                      <div  className=" dropdown-item pl-4 py-2">
                                          Logout
                                      </div>
                                   </div>
                               </div>
                               
                                </div>
 
                            </div>
                            </ClickOutsideListener>



                        </div>


                    </div>}


                </div>
           
        )
    }
    goToLogout=()=>{
        this.logoutUser();
    }
    async logoutUser() {
        let user=localStorage.getItem("user");
        let role=localStorage.getItem("role");
        let response={};
        if(role=="admin")
        {
        response=await http.logout("/adminlogout",{headers:{Authorization:user}});
        }
        else
        {
        response=await http.logout("/logout",{headers:{Authorization:user}});
        }
        
       
        localStorage.removeItem("user");
        localStorage.removeItem("email");
        localStorage.removeItem("role");
        localStorage.removeItem("id");
        localStorage.removeItem("name");
        window.location="/home";
    }
}
const mapStoreToProps=(store)=>{
    return {search:store.search,pathname:store.pathname}
}
const mapDisptchToProps=(dispatch)=>{
    return {
        showSearch:(value)=>dispatch({type:"Search",value:value}),
        setPathName:(str)=>dispatch({type:"PathName",pathname:str})
    }
}
export default connect(mapStoreToProps,mapDisptchToProps)(Navbar);