import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../services/httpServices";
import {connect} from "react-redux";

class TrendingAll extends Component
{
    state={songs:[],
           csong:{},
         favoriteSongs:[],
    };
    async componentDidMount() 
    {   let response=await http.get("/trendings");
         let {data}=response;
         let user=localStorage.getItem("user");
         let role=localStorage.getItem("role");
         let favoriteSongs=[];
         if(user && role=="user")
         {
            let res=await http.getData("/favoritesongs",{headers:{Authorization:user}});
            favoriteSongs=[...res.data];
         }
        await this.props.setMusic(data[0]);
         this.setState({songs:data,csong:data[0],favoriteSongs:favoriteSongs});
    }
    async postFavorite(obj) {
        try {
            let response=await http.post("/favoritesongs",obj);
             let {data}=response;
             console.log(data);
             this.setState({...this.state,favoriteSongs:data});
        }
        catch(ex)
        {
    
        }
    }
    updateFavoriteList=(item)=>{
        let {csong}=this.state;
        let email=localStorage.getItem("email");
        let obj={...item};
        let user=localStorage.getItem("user");
        if(user)
        {
    
            this.postFavorite({song:obj,email:email});
    
        }
        else
        {
            window.alert("Login to continue");
        }
       
    }
    async playMusic(song)
    {
       await this.props.setMusic(song);
    }
    playAudioFile=(song)=>{
        let s={...this.state};
        s.csong=song;
       this.playMusic(song);
        this.setState(s);
        }
    getDuration=(url)=>{
        let {musicInfo}=this.props;
             let m=new Audio(url);
            m.load();
            m.play();
            m.pause();
            let duration=m.duration;
        let d=Number.parseInt(duration);
        let minutes=Number.parseInt(d/60);
        let sconds=d-(60*minutes)
        console.log(d,minutes,sconds,m,m.duration,musicInfo.music);
       
        return minutes+":"+sconds;
    }
    render() {
        let {songs,favoriteSongs}=this.state;
        let {musicInfo}=this.props;
         let {csong}=musicInfo;
        return (
             <React.Fragment>
                
              <div className="row">
                  <div className="col-9 bgsongcolor">
                      <div className="row mb-2">
                          <div className="col-12">
                          <img alt="" width="800" height="90" className="mx-auto d-block" decoding="async" src="https://tpc.googlesyndication.com/simgad/12118969256046587501?sqp=4sqPyQQ7QjkqNxABHQAAtEIgASgBMAk4A0DwkwlYAWBfcAKAAQGIAQGdAQAAgD-oAQGwAYCt4gS4AV_FAS2ynT4&amp;rs=AOga4ql0eBmFmQrE_3ak0APnDCbBKGHd8w"/>
                          </div>
                      </div>
                      <div className="row mx-4">
                          <div className="col-12 fontstyle">
                              <span><svg itemprop="name" width="24" height="24" viewBox="0 0 24 24"> <g className="gannaiconfill" fill="gannaiconfill" fill-rule="evenodd"> <rect width="24" height="24" class="fill_path" rx="2"></rect> <path class="fill_path blackbg" fill="#FFF" d="M12.872 14.976c-.176 0-.317.005-.458 0-.694-.015-1.392.015-2.081-.06-1.04-.12-1.765-.774-1.97-1.674-.116-.493-.05-.98.035-1.468.23-1.297.452-2.6.698-3.896.297-1.569 1.508-2.695 3.112-2.82.94-.076 1.89-.046 2.835-.056.503-.005 1 0 1.544 0-.066.397-.126.774-.191 1.146-.362 2.057-.73 4.113-1.096 6.169-.231 1.292-.443 2.584-.704 3.87-.352 1.755-1.69 2.564-3.062 2.76a7.41 7.41 0 0 1-1 .05c-1.156.006-2.312 0-3.469 0-.015 0-.03-.004-.065-.014.005-.03 0-.06.015-.086.322-.578.639-1.156.975-1.724.05-.08.201-.136.307-.136 1.015-.01 2.026-.005 3.041-.005.775 0 1.172-.317 1.338-1.076.08-.316.125-.618.196-.98zm-.764-1.98v.03c.261 0 .522-.02.779.005.256.025.347-.08.387-.322.266-1.579.543-3.157.82-4.736.105-.593-.197-.98-.805-1-.337-.01-.669-.005-1.005-.005-.674.01-1.132.397-1.252 1.066-.13.724-.267 1.442-.392 2.166-.111.649-.237 1.297-.312 1.95-.055.468.201.775.679.83.362.046.734.015 1.1.015z"></path> </g> </svg></span>
                              <span className="songlinkstyle">Gaana</span>
                              <span className="dotstyle">.</span>
                              <span className="songnamelinkstyle">{csong.name} song</span>
                              <hr className="mt-0"/>
                          </div>
                      </div>

                     <div className="row mx-4 mb-2 ">
                        
                         <div className="col-12 text-center">
                                <div className="titlestylesong">Trending Songs</div>
                                <div className="text-muted titlestylesong1 mt-0">
                                    Top Trending Hits.Refreshed Daily
                                </div>
                                <div className="text-muted titlestylesong1 mt-0">
                                    {songs.length} songs
                                </div>
                                
                                <div className="playallbutton" onClick={()=>this.playAudioFile(csong)}>
                                    <i className={"fas  playallicon "+(musicInfo.status?"fa-pause":"fa-play")}></i>
                                    <span className="playalltext">{musicInfo.status?"PAUSE":"PLAY ALL"}</span>
                                </div>
                        
                      
                         </div>
                     </div>
                     
                     <div className="row text-muted mx-5 mt-4">
                         <div className="col-2">#</div>
                         <div className="col-4">TITLE</div>
                         <div className="col-3">ARTIST</div>
                         <div className="col-3"></div>
                     </div>
                    {songs.map(ele=>{
                        return (
                        <div className={"row fontstyle1 songrow  py-2 my-0 mx-5  border-top border-bottom "+((ele.url==csong.url)?"bg-white":"")}>
                            <div className="col-1">
                            <span className=" songlistplaybox" onClick={()=>this.playAudioFile(ele)}>
                              
                               <i className={"fas songlistplaybtn tablesongfontcolor "+((ele.url==csong.url && musicInfo.status)?"fa-pause":"fa-play")}></i>
                             </span>
                            
                            
                            </div>
                            <div className="col-1 pt-1 ">
                            <i onClick={()=>this.updateFavoriteList(ele)} class={" fa-heart  cps heartstylelist  "+(favoriteSongs.findIndex(el=>el.id == ele.id)>=0?"heartcolor fas":"heartstyle1 far")}></i>
                             
                             </div>
                            <div className="col-4  tablesongfont ">
                                <div className="row">
                                    <div className="col-2 m-0 p-0 pl-3">
                                    <img src={ele.img} width="33" height="33" style={{display:"inline-block"}} className="rounded m-0 p-0 d-block"/>
                                    </div>
                                    <div className={"col-9 pt-1 "+(ele.url == csong.url?"tablesongtitle":"")}>
                                    {ele.name}
                                    </div>
                                </div>
                              
                               
                                </div>
                            <div className="col-3 pt-1 tablesongfont tablesongfontcolor">{ele.artist.length > 21? ele.artist.substring(0,21)+"...":ele.artist}</div>
                            <div className="col-3 tablesongfont tablesongfontcolor"></div>
                        </div>
 
                        )
                    })}
                    

                  </div>
                  <div className="col-3 bgaddcolor p-0 m-0   addboxstyle">
                   <div className="row bgaddcolor addmargin ml-2 mt-2 position-sticky">
                     <div className="col-12 m-0 p-0 ">
                     <img src="https://s0.2mdn.net/8660235/300x600_DBM_TH-M1.jpg" alt="Advertisement" border="0" width="315" height="600" />
                     </div>
                     <div className="col-12 m-0 mt-2 p-0 bgaddcolor">
                     <img width="315" className="m-0 p-0 " height="250" src="https://ss3.zedo.com/OzoDB/h/g/2895487/V2/300x250.jpg" border="0" alt="Click Here!" title="Click Here!"></img>
                     </div>
                    
                   </div>

                 </div>
              </div>
             </React.Fragment>
        )
    }
}
const mapStoreToProps=(store)=>{
    return {
        musicInfo:{
        csong:store.csong,
        music:store.music,
        status:store.status
        }
    }
}
const mapDispatchToProps=(dispatch)=>{
    return {
        setMusic:(song)=>dispatch({type:"Player",song:song})
    }
}
export default connect(mapStoreToProps,mapDispatchToProps)(TrendingAll);