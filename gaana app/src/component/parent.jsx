import React,{Component} from "react";

class Parent extends Component
{
    state={
        arr:["","","",""],
        col1:"col-4",
        col2:"col-6",
        rev:false,
      }

      changeStr=(st)=>{
          let s={...this.state};
          if(st=="row")
          {
            s.col1="col-8";
            s.col2="col-3";
            s.rev=false;
          }
         else if(st=="colin")
          {
            s.col1="col-2";
            s.col2="col-12";
            s.rev=false;
          }
          else if(st=="colde")
          {
            s.col1="col-2";
            s.col2="col-12";
            s.rev=true;
           
          }
          else
          {
            s.col1="col-4";
            s.col2="col-6"; 
            s.rev=false;
          }
          this.setState(s);
      }
      handelChange=(e)=>{
          let {currentTarget:input}=e;
          let s={...this.state};
          s.arr.splice(input.id,1,input.value);
          this.setState(s);
      }
    render() {
        let {arr,col1,col2,rev}=this.state;
        return (
            <React.Fragment>

                   <div className="row justify-content-center mt-4">
                       <div className={col1}>
                           <div className="row">
                         
                       {arr.map((ele,index)=>{
                              
                           return (
                               <div className={(rev?"flowof":"")+ " p-0 "+col2 }>
                                   <input className=""
                                   id={rev?(arr.length-1)-index:index}
                                   className="inputstyle"
                                   type="text"
                                   value={rev?(arr[(arr.length-1)-index]):ele}
                                   onChange={this.handelChange}
                                   />
                               </div>
                           )
                       })}
                          </div>
                          
                       </div>
                      
                   </div>
                   <div className="row justify-content-center mt-4">
                       <div className="col-3"></div>
                   <div className="col-6 p-0">
                                  <button onClick={()=>this.changeStr("")} className="mr-1">Button 1</button>
                                  <button onClick={()=>this.changeStr("row")} className="mr-1" >Button 2</button>
                                  <button onClick={()=>this.changeStr("colin")} className="mr-1" >Button 3</button>
                                  <button onClick={()=>this.changeStr("colde")} className="mr-1" >Button 4</button>
                 </div>

                   </div>
               
            </React.Fragment>
        )
    }
}
export default Parent;
