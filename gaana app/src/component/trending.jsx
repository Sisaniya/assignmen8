import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";
import http from "../services/httpServices"

class Trendings extends Component
{
   state={
      data:[],
      first:0,
      last:5,
   }
   async componentDidMount() {
       let response=await http.get("/trendings");
       let {data}=response;
       console.log(data,response);
        this.setState({data:data});
   }
   changeImgNext=()=>{
    let s={...this.state};
    if(s.last < s.data.length-1)
    {
      let l=s.last+4;
      if(l<s.data.length)
      {
        s.last=l;
        s.first+=4;
      }
      else
      {
        s.last=s.data.length-1;
        s.first=s.last-5;
      }
      
       
     
      this.setState(s);
    }
   }
   changeImgPre=()=>{
     let s={...this.state};
     if(s.first>0)
     {
      
      
        s.last-=4;
        let f=s.first-4;
            s.first=f>=0?f:0;
      
      if(s.last - s.first != 5)
      {
        s.first=0;
        s.last=5;
      }
       
       this.setState(s);
     }
   }
   goToSetMusic=(ele)=>{
     this.props.setMusic(ele);
     this.setState({...this.state});
   }
   render() {
      let {data,first,last}=this.state;
      let list=data.filter((ele,index)=>index>=first && index<=last);
      console.log(last,first,list);
      let {musicInfo}=this.props;
       console.log(data);
        return (
            <div className="row bgcolor">
             <div className="col-12 piclistmain ">
            
               <div className="row  mx-4">
                 <div className="col-12 my-2">
                 <hr className="my-3"/>
                 <div className="row">
                   <div className="col-3">  <h6>TRENDING SONGS</h6></div>
                   <div className="col-6"></div>
                   <div className="col-3 text-right text-danger cps viewAllLinkStyleBox">
                     <Link to="/songs" className=" viewAllLinkStyle">
                     View All   <i class="fas fa-chevron-right "></i>
                     </Link>
                    
                      </div>
                 </div>
               
                 </div>
              <div className="col-12 trenimgmaincol">
               {list.map((ele,index)=>{
                   let {id,img,name,url,like,artist}=ele;
                   console.log(last,index,first);
                 return (
                   <React.Fragment>
                     {(index == list.length-1 && (last != data.length-1)) || (last == data.length-1 && index ==0)?(
                             <div className="trenimgcollast  trenimgmain" >
                             <div className={"picimgbody "}>
                               <img src={img} onClick={()=>this.props.goToPlaySong(name)} className="trendingimg picimgstyle "/><br/>
                               <div className="trensongname mb-1 mt-3 fontstyle">
                               {name}
                                </div>
                             </div>
                            
                             <div className=" trenimgiconbodylast cps" onClick={()=>this.goToSetMusic(ele)} >
                                    {musicInfo.csong.url == ele.url && musicInfo.status && !this.props.playlist ?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                        </div>
                           </div>
                     ):(
                      <div className={"trenimgcol  trenimgmain "+((last == data.length-1 && index ==1)?"ml-1":"")}>
                      <div className="picimgbody">
                        <img src={img} className="trendingimg picimgstyle" onClick={()=>this.props.goToPlaySong(name)}/><p/>
                        <div className="trensongname my-1 fontstyle">
                               {name}
                        </div>
                      </div>
                     
                      <div className=" trenimgiconbody cps"  onClick={()=>this.goToSetMusic(ele)}>
                       
                     {musicInfo.csong.url == ele.url && musicInfo.status ?
                        <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                        :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                    }
                        
                 </div>
                    </div>
                     )}
                   </React.Fragment>
                 

                 )
               })}
               </div>
               </div>
              
                 <div className=" trenlistbodyleft" onClick={()=>this.changeImgPre()}>
                 <i class="fas fa-chevron-left treniconstyle"></i>
                 </div>
                
                 <div className="trenlistbodyright " onClick={()=>this.changeImgNext()}>
                 <i class="fas fa-chevron-right treniconstyle"></i>
                 </div>
                
               </div>
            </div>
        )
   }
}
const mapStoreToProps=(store)=>{
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      playlist:store.playlist

  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(song)=>dispatch({type:"Player",song:song}),
      addSong:(song)=>dispatch({type:"AddSong",song:song})
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(Trendings);

