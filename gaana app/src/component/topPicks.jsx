import React,{Component} from "react";
import {connect} from "react-redux";
import Navbar from "./navbar";
import {Link,Switch,Route,Redirect} from "react-router-dom";
import http from "../services/httpServices"
import httpServices from "../services/httpServices";

class TopPicks extends Component
{
   state={
      data:[],
      first:0,
      last:2,
   }
   async componentDidMount() {
       let response=await http.get("/toppicks");
       let {data}=response;
       console.log(data,response);
        this.setState({data:data});
   }
   async getPlaylistSongs(obj) {
     console.log(obj);
    try {
        let response={};
      
          response=await httpServices.get(`/toppickname/${obj.id}`);
          let {data}=response;
          if(data.category=="playlist")
          {
              let res=await http.get(`/playlistsongs/${data.name}`);
              
              let {list,playlist}=res.data;
              this.props.setMusic({song:list[0],playlist:playlist.id});
              this.setState({...this.state,playlist:playlist});
          }
          else
          {
            let res=await http.get(`/song/${data.name}`);
           
            this.props.setMusic({song:res.data,playlist:""});
            this.setState({...this.state,playlist:""});
          }
       
     
      }
   catch(ex)
      {

      }
   }
   goToPlaySongFromPlaylist=(ele)=>{
        this.getPlaylistSongs(ele);
   }
   changeImgNext=()=>{
    let s={...this.state};
    if(s.last < s.data.length-1)
    {
      s.last+=1;
      s.first+=1;
      this.setState(s);
    }
   }
   changeImgPre=()=>{
     let s={...this.state};
     if(s.first>0)
     {
       s.first--;
       s.last--;
       this.setState(s);
     }
   }
   async getListInfo(id) {
     let response=await httpServices.get(`/toppickname/${id}`);
     let {data}=response;
       this.props.goToSongList(data);
   }
   goToSongList=(id)=>{
    this.getListInfo(id);
      

   }
   render() {
      let {data,first,last}=this.state;
      let list=data.filter((ele,index)=>index>=first && index<=last);
    
     
       let {musicInfo,playlist}=this.props;
       console.log(musicInfo.csong,playlist)
        return (
            <div className="row bgcolor">
             <div className="col-12 piclistmain ">
            
               <div className="row  mx-4">
                 <div className="col-12 my-2">
                 <h6>TOP PICKS </h6>
                 </div>
              
               {list.map((ele,index)=>{
                   let {id,img,playListId,songId}=ele;
                 
                   return (
                   <React.Fragment>
                     {(index == list.length-1 && last != data.length-1) || (first == data.length-3 && index == 0)?(
                             <div className="col-2">
                             <div className="picimgbodylast cps">
                               <img src={img} className={"picimg picimgstylelast "+((first == data.length-3 && index == 0)?"fright":"")}/>
                             </div>
                           </div>
                     ):(
                      <div className="col-5 picimgmain">
                      <div className="picimgbody cps"  onClick={()=>this.goToSongList(id)}>
                        <img src={img} className="picimg picimgstyle"/>
                      </div>
                     
                      <div className="  picimgiconbody cps" onClick={()=>this.goToPlaySongFromPlaylist(ele)}>
                                  {((playListId && playListId == this.props.playlist && musicInfo.status)  || (songId && songId == musicInfo.csong.id && musicInfo.status ) )?
                                    <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg "><g fill="none" fill-rule="nonzero"><path className="bottomplayicon" d="M4.3 20.8h5.029V3.2H4.3v17.6zM14.357 3.2v17.6h5.029V3.2h-5.029z"></path><path className="fill_path orange" fill-opacity=".01" d="M1 1h22v22H1z"></path></g></svg>
                                      :  <svg width="22"  height="22" viewBox="0 0 24 24" className="playsvg"><path className="bottomplayicon" fill-rule="evenodd" d="M4.321 1v22.5L22 12.25z"></path></svg>
                                        }
                      </div>
                        
                    </div>
                     )}
                   </React.Fragment>
                 

                 )
               })}
               </div>
              
                 <div className=" piclistbodyleft" onClick={()=>this.changeImgPre()}>
                   <span className="fa-stack fa-2x piciconstyle">
                     <i className={"fas fa-circle fa-stack-2x  "+(first==0?"listiconnotactive":"listiconactive")}></i>
                     <i className="fas piciconchevron fa-chevron-left fa-stack-1x fa-inverse"></i>
                   </span>
                 </div>
                
                 <div className="piclistbodyright " onClick={()=>this.changeImgNext()}>
                 <span className="fa-stack fa-2x piciconstyle">
                     <i className={"fas fa-circle fa-stack-2x  "+(last < (data.length-1)?"listiconactive":"listiconnotactive")}></i>
                     <i className="fas piciconchevron fa-chevron-right fa-stack-1x fa-inverse"></i>
                   </span>
                 </div>
                
               </div>
            </div>
        )
   }
}
const mapStoreToProps=(store)=>{
  return {
      musicInfo:{
      csong:store.csong,
      music:store.music,
      status:store.status
      },
      playlist:store.playlist
  }
}
const mapDispatchToProps=(dispatch)=>{
  return {
      setMusic:(obj)=>dispatch({type:"Player",song:obj.song,playlist:obj.playlist}),
      addSong:(song)=>dispatch({type:"AddSong",song:song}),
      
  }
}
export default connect(mapStoreToProps,mapDispatchToProps)(TopPicks);
