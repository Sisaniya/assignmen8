import React,{Component} from 'react';
import NewPerson from './newPerson';



class PersonComp extends Component
{
    state={
        persons: [{name:"Mark",email:"mark33@emil.com",age:32},
        {name:"Steve",email:"steve5685@emil.com",age:20},
        {name:"John",email:"john833@emil.com",age:52},
        {name:"Mary",email:"mary58666@emil.com",age:33},
        {name:"Johnsom",email:"johnsom@emil.com",age:45},],
        show:0,
      editindex:-1, 
    }
    addPerson=(person)=>{
        let s={...this.state};
        s.editindex>=0?s.persons.splice(s.editindex,1,person):s.persons.push(person);
        s.editindex=-1;
        s.show=0;

         this.setState(s);
    }
    showForm=()=>{
        let s={...this.state};
        s.show=1;
        this.setState(s);
    }
    editForm=(index)=>{
        let s={...this.state};
        s.show=1;
        s.editindex=index;
        this.setState(s); 
    }
  
    getTable=(arr)=>{
     
        return (
            <React.Fragment>
                <h3>Person Details</h3>
              <div className="row text-center bg-dark text-white">
                    <div className="col-3 border">Name</div>
                    <div className="col-3 border">Age</div>
                    <div className="col-3 border">Email</div>
                   
                    <div className="col-3 border">
                       
                       
                    </div>
                </div>
                {arr.map((co,index)=>{
                    return (
                    <div className="row text-center">
                    <div className="col-3 border">{co.name}</div>
                    <div className="col-3 border">{co.age}</div>
                    <div className="col-3 border">{co.email}</div>
                   
                    <div className="col-3 border">
                        <button className="btn btn-primary btn-sm m-1" onClick={()=>this.editForm(index)}>Edit</button>
                       
                    </div>
                </div>);
                })}
               <button className="btn btn-primary btn-sm m-2" onClick={()=>this.showForm()}>Add New Person</button> 
            </React.Fragment>
        );
        
           
    }
    
    render() {
        let {persons,show,editindex}=this.state;
        let person={name:'',age:'',email:''}
        return (
            <React.Fragment>
                <div className="container">
               
                    {show==1?(<NewPerson handleSubmit={this.addPerson} editindex={editindex} person={editindex>=0?persons[editindex]:person} />)
                    :this.getTable(persons)}
                  
               
                </div>
            </React.Fragment>
        );
    }
}
export default PersonComp;