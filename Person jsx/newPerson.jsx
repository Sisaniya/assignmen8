import React,{Component} from 'react';
class NewPerson extends Component
{
    state={
        person:this.props.person,
    }
    handleChange=(e)=>{
        console.log(e.currentTarget);
        let s={...this.state};
        s.person[e.currentTarget.name]=e.currentTarget.value;
        s.person[e.currentTarget.age]=e.currentTarget.age;
        this.setState(s);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        console.log(this.state.person,'enterinsf');
        this.props.handleSubmit(this.state.person);
    }
    render() {
        let {person,editindex}=this.props;
        let {name,age,email}=person;
        return (
            <div className="container">
                <h2>{editindex>=0?"Edit person details":"Add New Person"}</h2>
                <div className="form-group">
                    <label>Name</label>
                    <input 
                    className="form-control"
                    type="text"
                    placeholder="Enter name"
                    id="name"
                    name="name"
                    value={name}
                    onChange={this.handleChange}
                    />
                    <label>Age</label>
                    <input 
                    className="form-control"
                    type="number"
                    placeholder="Enter age"
                    id="age"
                    name="age"
                    value={age}
                    onChange={this.handleChange}
                    />
                     <label>Email Id</label>
                    <input 
                    className="form-control"
                    type="text"
                    placeholder="Enter email id"
                    id="email"
                    name="email"
                    value={email}
                    onChange={this.handleChange}
                    />
                    <button 
                    className="btn btn-primary btn-sm m-2"
                    onClick={this.handleSubmit}
                    >{editindex>=0?"Update":"Submit"}</button>
                </div>
            </div>
        );

    }
}
export default NewPerson;