import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import AdminNavBar from "./adminNavbar";
import FormUpload from "./formUpload";
import QNA from "./qna";

class AdminProducts extends Component
{
    state={data:[],edit:""}

    async fetchData() {
        let user=localStorage.getItem("usertoken")
        localStorage.setItem("open","");
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/allproducts",{headers: {Authorization: user}});

        let {data}=response;
        this.setState({data:data,edit:""});
    }
    componentDidMount() {
        this.fetchData();
    }
    componentDidUpdate(prevProps,preState) {
        let open=localStorage.getItem("open");
           if(open)
           {
               this.fetchData();
           }
    }
    goToEdit=(id)=>{
        let s={...this.state};
        s.edit=id;
        this.setState(s);
    }
    backToView=()=>{
       this.fetchData();
    }
    render() {
        let {data,edit}=this.state;
        console.log(data);
        return (
            <React.Fragment>
                <div className="row bgcolor mx-2 mb-2">
                    <div className="col-lg-3 my-2 ml-3">
                        <AdminNavBar />
                    </div>
                   
                    <div className="col-lg-8 normalbgcolor  mt-2 ml-4">
                        
                        {edit?<FormUpload id={edit} backToView={this.backToView}/>:(

                            <React.Fragment>
                                <div className="row ml-2 pt-3">
                                    <div className="col-12">
                                    <span  className="d-block aphs">All Products</span>
                                    </div>
                                </div>
                                
                                     
                        {data.map(ele=>{
                            let {name,price,img,id}=ele;
                            return (
                                <React.Fragment>
                                <div className="row ml-2  py-2  ">
                                   <div className="col-lg-4 apns" >
                                       {name}
                                    </div>
                                    <div className="col-lg-2">
                                        <img className="d-block rounded mx-auto" src={img} width="60" height="120" />
                                    </div>
                                    <div className="col-3 text-center  apns" >
                                    ₹{price}
                                    </div>
                                    <div className="col-3 text-muted text-center">
                                        <i onClick={()=>this.goToEdit(id)} className="fas fa-edit"></i>
                                    </div>
                                </div>
                               
                                </React.Fragment>
                            )
                        })}
                            </React.Fragment>
                        )}
                        
                        <QNA/>
                       
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default AdminProducts;

