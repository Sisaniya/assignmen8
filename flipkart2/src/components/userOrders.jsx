import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../services/httpServices";
import axios from "axios";
import ExtraInfo from "./extraInfo";
class UserOrders extends Component
{
    state={
        orderList:[],
    }
    async componentDidMount() {
        let user=localStorage.getItem("usertoken")
            console.log(user);
            let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/getUserOrders",{headers: {Authorization: user}});
                let {data}=response;
                console.log(data);
                this.setState({orderList:data.orders});
        }
    render() {
        let {orderList=[]}=this.state;
        console.log(orderList);
        return (
            <React.Fragment>
            <div className="row bg-white">
                <div className="col-12 bg-white">
                <div className="row mx-2 upsbgcolor" >
                  <div className="container ">


              <div className="row">
                  <div className="col-12 my-2  fontstyle uporlssts"  >
                  <Link to="/home"   className="userOrderLink">
                          Home                
                  </Link>
                  <svg width="16" height="27" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg" className="_39X-Og">
                      <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#fff" className="DpXnhQ">
                  </path></svg>
                 
                  
                  <Link to="/dashboard/account"  className="userOrderLink">
                          My Account                
                  </Link>
                  <svg width="16" height="27" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg" className="_39X-Og">
                      <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#fff" className="DpXnhQ">
                  </path></svg>


                  <Link to="/dashboard/account/orders"  className="userOrderLink">
                   My Orders              
                  </Link> 
                  <svg width="16" height="27" viewBox="0 0 16 27" xmlns="http://www.w3.org/2000/svg" className="_39X-Og">
                      <path d="M16 23.207L6.11 13.161 16 3.093 12.955 0 0 13.161l12.955 13.161z" fill="#fff" className="DpXnhQ">
                  </path></svg>
                  

             </div>
          </div>



              {orderList.map((ele,index)=>{
                  let {name,price,discount,img,date}=ele;
                  return (
                    <div className="row normalbgcolor my-1 border py-2 mb-2" style={{borderRadius:"5px"}}>
                        {index==0 && (
                            <div className="col-12 my-2">
                               
                                <div class="_3jifPN fontstyle">
                                    <span>Flipkart Customer shared this order with you.</span>
                                </div>

                            </div>
                        )}
                    <div className="col-1 py-1 mr-3">
                        <img className="d-block rounded float-left" src={img} width="38" height="76"/>
                     </div>
                     <div className="col-3 py-2 fontstyle text14 text500" >
                         {name}
                         </div>
                     <div className="col-3 text-center py-2 fontstyle text14 text500" >
                     <span className=""> ₹{price}</span>
                     </div>
                     <div className="col-1"></div>
                    <div className="col-3 py-2" >
                        <i class="fas fa-circle greenDot text-success mr-2"></i>
                        <span className="fontstyle text12 text600" >
                            Delivered on {date}</span><br/>
                        <span className="fontstyle upords" >
                            Your item has been delivered</span>
                    </div>
               </div>
                  )
                 
              })}
          
          </div>
            </div>
           
                </div>
            </div>
            <ExtraInfo/>
            </React.Fragment>
           
           
        )
    }
}
export default UserOrders;