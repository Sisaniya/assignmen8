import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../services/httpServices";
class ViewList extends Component
{
    state={}
    buyMobile=(brand,id)=>{
        this.props.history.push(`/home/Mobile/${brand}/${id}`);
    }
    async putToList(obj) {
        try
         {
            let response=await http.put("/wishlist",obj);
             window.alert("Wish list changed");
        }
        catch(ex)
         {
               console.log(ex.response);
        }
    }
    addToWishList=(item)=>{
        let id=localStorage.getItem("id");
        if(id)
        {
             let obj={userId:id,item:item};
             this.putToList(obj);
        }
        else
        {
            window.alert("Login to continue");
        }
    }
    nameEffect=(e)=>{
        e.currentTarget.className="reviewNameStyle";
    }
    removeEffect=(e)=>{
        e.currentTarget.className="removeEffectMove";
    }
    render() {
        let {items}=this.props;
        return (
            <React.Fragment>
                <div className="row bg-white text-center">
                    <div className="col-12 bg-white mt-4 my-2">
                        <span 
                         className=" mt-2 mb-2 fontStyle vpheas ">Recently Viewed </span><br/>
                        <sapn className="vpheas1 ">{items.length} items</sapn>
                       <hr className="bg-light"/>
                    </div>
                </div>
                <div className="row bg-white p-1 mx-2">
                {items.map(ele=>{
                     let {id,name,brand,img,rating,ratingDesc,details=[],
                        price,assured,discount,emi,exchange,ram,offers=[],popularity,qty}=ele;
                        return (
                            <div className="col-3 bg-white">
                                 <div className="row">
                                     <div className="col-10 mb-3">
                                         <img  src={img} className="d-block rounded mx-auto" width="40" height="80"/>
                                     </div>
                                     <div className="col-lg-1 mb-2">
                                   <Link className="text-right "><i  className="fas fa-heart vpheas2"></i></Link>
                               </div>
                                     <div className="col-12">
                                        <Link onClick={()=>this.buyMobile(ele.brand,ele.id)} className="notActivePPath"
                                        style={{fontSize:"14px"}} onMouseOut={this.removeEffect} onMouseMove={this.nameEffect}>
                                            {name}</Link> <br/>
                                         <strong className=" ratingStyleViewList">{rating}<i className="fas fa-star iconSizeRating vpheas3" ></i></strong>
                                         <span style={{display:"inline-block"}} className="text-muted mx-2">({popularity})</span>
                                         {assured == true?<img height="22"  src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png" />:""}<br/>
                                         <h5 className="ml-3"> ₹{price}</h5>
                                     </div>
                                 </div>
                            </div>
                        )
                })}
                </div>
               
            </React.Fragment>
        )
    }
}
export default ViewList;