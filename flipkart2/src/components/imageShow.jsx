import React,{Component} from "react";
import {Switch,Route,Link} from "react-router-dom";
import auth from "../services/storageServices.js";
import 'bootstrap/dist/css/bootstrap.css';

class ImageShow extends Component
{
    state={}
    async componentDidMount() {
       
    }
    render() {
       
       return (
           <React.Fragment>
          
                
               <div className="row px-2 ">

              
                <div className="col-lg-12   my-2 ">
     <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
  <ol className="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
   
  </ol>
  <div className="carousel-inner">
  <div className="carousel-item active" data-interval="1000">
      <img src="https://i.ibb.co/qxHzVsp/30d7dffe1a1eae09.jpg" className="d-block w-100" alt="..."/>
    </div>
    <div className="carousel-item " data-interval="1000">
      <img src="https://i.ibb.co/tq9j6V7/4dfdf0c59f26c4a1.jpg" className="d-block w-100" alt="..."/>
    </div>
    <div className="carousel-item" data-interval="1000">
      <img src="https://i.ibb.co/vQZhcvT/68af1ae7331acd1c.jpg" className="d-block w-100" alt="..."/>
    </div>
    
  </div>
  <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="sr-only">Previous</span>
  </a>
  <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="sr-only">Next</span>
  </a>
</div></div>


</div>
        </React.Fragment>
       )
    }
}
export default ImageShow;