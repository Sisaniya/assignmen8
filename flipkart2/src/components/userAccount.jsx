import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "../services/httpServices";
import Input from "./input";
import ExtraInfo from "./extraInfo";
import {createBrowserHistory} from "history";
import axios from "axios";

class UserAccount extends Component
{
    state={
     
    }
    
    render() {
        let history=createBrowserHistory();
        let pathname=history.location.pathname+history.location.search;
      
        return (
            <React.Fragment>
              
                <div className="row bg-white pt-5 mx-2 justify-content-center">
                    <div className="col-12 bg-white">
                    <div className="row bgcolor">
                 
                       <div className="col-2"></div>
            
             
                   <div className="col-lg-9 ml-1 bg-white border">
                       <div className="row ml-1 mt-2 mb-4 " >
                           <div className="col-12">
                           <span >
                               <Link  
                                
                               to="/home" className=" notActive fontstyle ualsfl text500">Home  </Link>
                               </span>
                               <span
                               className="ualsfl text500"
                               >{"    /    "}</span>
                       <span  className=""> 
                       <Link 
                       className=" notActive fontstyle ualsfl text500"
                       
                    
                        to={`/home/account`}>  Account  </Link></span>
                         <span
                              className="ualsfl text500"
                               >{"    /    "}</span>

                        </div>
                      </div>
                      
                
                       <span  className="mt-2 normalColor d-block fontstyle text14 text500">Account</span>
                     
                      
                        <div className="row my-2">
                        <div className="col-12 my-5">
                            <img width="250" height="200" className="d-block mx-auto rounded"
                            src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90"/>
                        </div>
                        <div className="col-12 my-2 text-center">
                            <span className="commingSoonStyle">Coming Soon</span>
                        </div>
                    </div>
                     
                      
                   </div>
                   
               </div>
              
                    </div>
                </div>
            
               <ExtraInfo/>
            </React.Fragment>
        )
    }
    }
export default UserAccount;