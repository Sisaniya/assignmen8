import React,{Component} from "react";
import {Link} from "react-router-dom";
import AdminNavBar from "./adminNavbar";
import QNA from "./qna";
class AdminProfile extends Component
{
    state={}
    render() {
        return (
            <React.Fragment>
                <div className="row backgcolor mx-2 mb-2">
                    <div className="col-lg-3 ml-3  my-2 ">
                        <AdminNavBar />
                    </div>
                    <div className="col-lg-8 ml-4 pt-4 normalbgcolor mt-2">
                        <QNA/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default AdminProfile;

