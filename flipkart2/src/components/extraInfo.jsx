import React,{Component} from "react";
class ExtraInfo extends Component
{
    state={}
    render() {
        return (
            <React.Fragment>
                 
              <div  className=" row mx-2 fontStyle extraInfoStyle justify-content-center text13">
                <div className="col-lg-10  m-3 ">
                      <div className="row mb-2">
                      <div className="col-lg-2">
                        <span className="text-muted ">ABOUT</span><p/>
                        <span className="tcolor footerStyle">Contact Us</span><br/>
                        <span className="tcolor footerStyle">About Us</span><br/>
                        <span className="tcolor footerStyle">Careers</span><br/>
                        <span className="tcolor footerStyle">Flipkart Stories</span><br/>
                        <span className="tcolor footerStyle">Press</span><br/>
                        <span className="tcolor footerStyle">Flipkart Wholesale</span><br/>
                     </div>
                      
                      <div className="col-lg-2">
                        <span className="text-muted ">HELP</span><p/>
                        <span className="tcolor footerStyle">Payments</span><br/>
                        <span className="tcolor footerStyle">Shipping</span><br/>
                        <span className="tcolor footerStyle">Cancellation</span><br/>
                        <span className="tcolor footerStyle">Returns</span><br/>
                        <span className="tcolor footerStyle">FAQ</span><br/>
                        <span className="tcolor footerStyle">Report Infringment</span><br/>
                     </div>
                     <div className="col-lg-2">
                        <span className="text-muted ">POLICY</span><p/>
                        <span className="tcolor footerStyle">Return Policy</span><br/>
                        <span className="tcolor footerStyle">Terms Of Use</span><br/>
                        <span className="tcolor footerStyle">Security</span><br/>
                        <span className="tcolor footerStyle">Privacy</span><br/>
                        <span className="tcolor footerStyle">Sitemap</span><br/>
                        <span className="tcolor footerStyle">EPR Compliance</span><br/>
                     </div>
                     <div className="col-lg-1 ">
                        <span className="text-muted ">SOCIAL</span><p/>
                        <span className="tcolor footerStyle">Facebook</span><br/>
                        <span className="tcolor footerStyle">Twitter</span><br/>
                        <span className="tcolor footerStyle">YouTube</span><br/>
                       
                     </div>
                     <div className="col-lg-3 ">
                         <span className="socialCell d-block">
                        <span className="text-muted ">Mail Us:</span><p/>
                        <span className="tcolor footerStyle">Flipkart Internet Private Limited,</span><br/>
                        <span className="tcolor footerStyle">Buildings Alyssa, Begonia &</span><br/>
                        <span className="tcolor footerStyle">Clove Embassy Tech Village,</span><br/>
                        <span className="tcolor footerStyle">Outer Ring Road, Devarabeesanaha</span><br/>
                        <span className="tcolor footerStyle">Bengaluru, 560103,</span><br/>
                        <span className="tcolor footerStyle">Karnataka, India</span><br/>
                        </span>
                     </div>

                     <div className="col-lg-2">
                        <span className="text-muted ">Registered Office Address:</span><p/>
                        <span className="tcolor footerStyle">Flipkart Internet Private Limited</span><br/>
                        <span className="tcolor footerStyle">Buildings Alyssa, Begonia &</span><br/>
                        <span className="tcolor footerStyle">Clove Embassy Tech Village,</span><br/>
                        <span className="tcolor footerStyle">Outer Ring Road, Devarabeesanaha</span><br/>
                        <span className="tcolor footerStyle">Bengaluru, 560103,</span><br/>
                        <span className="tcolor footerStyle">Karnataka, India</span><br/>
                        <span className="tcolor footerStyle">CIN : U51109KA2012PTC066107</span><br/>
                        <span className="tcolor footerStyle">Telephone: 1800 202 9898</span><br/>
                     </div>
                    </div>
                  <hr className="bg-secondary"/>
                </div>
                
                </div>
              
                <div className="row extraInfoStyle  mx-2 pl-5 pb-4 justify-content-center">
               
                
                                                                                                    <div className="col-2 ">
                                                                                                        <span className="mr-1 "><img 
                                                                                                        src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNiIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE2IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0yLTJoMjB2MjBILTJ6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTUuOTMgNS42MTRoLTIuOTQ4VjQuMTRjMC0uODE4LS42NTUtMS40NzMtMS40NzMtMS40NzNIOC41NmMtLjgxNyAwLTEuNDczLjY1NS0xLjQ3MyAxLjQ3M3YxLjQ3NEg0LjE0Yy0uODE4IDAtMS40NjYuNjU2LTEuNDY2IDEuNDc0bC0uMDA3IDguMTA1YzAgLjgxOC42NTUgMS40NzQgMS40NzMgMS40NzRoMTEuNzljLjgxOCAwIDEuNDc0LS42NTYgMS40NzQtMS40NzRWNy4wODhjMC0uODE4LS42NTYtMS40NzQtMS40NzQtMS40NzR6bS00LjQyMSAwSDguNTZWNC4xNGgyLjk0OHYxLjQ3NHoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0yIC0yKSIvPgogICAgPC9nPgo8L3N2Zz4K"/>
                                                                                                        <span className="tcolor ml-2">Sell On Flipkart</span></span></div>
                                                                                                        <div className="col-2">
                                                                                                            <span className="mr-1 "><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE1IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0zLTNoMjB2MjBILTN6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTAuNDkyIDNDNi4zNTMgMyAzIDYuMzYgMyAxMC41YzAgNC4xNCAzLjM1MyA3LjUgNy40OTIgNy41QzE0LjY0IDE4IDE4IDE0LjY0IDE4IDEwLjUgMTggNi4zNiAxNC42NCAzIDEwLjQ5MiAzem0zLjE4IDEyTDEwLjUgMTMuMDg4IDcuMzI3IDE1bC44NC0zLjYwN0w1LjM3IDguOTdsMy42OS0uMzE1TDEwLjUgNS4yNWwxLjQ0IDMuMzk4IDMuNjkuMzE1LTIuNzk4IDIuNDIyLjg0IDMuNjE1eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTMgLTMpIi8+CiAgICA8L2c+Cjwvc3ZnPgo="/>
                                                                                                            <span className="tcolor ml-2">Advertise</span></span></div>
                                                                                                            <div className="col-2"><span className="mr-1">
                                                                                                                <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxOCIgaGVpZ2h0PSIxNyIgdmlld0JveD0iMCAwIDE4IDE3Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0xLTFoMjB2MjBILTF6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMTYuNjY3IDVIMTQuODVjLjA5Mi0uMjU4LjE1LS41NDIuMTUtLjgzM2EyLjQ5NyAyLjQ5NyAwIDAgMC00LjU4My0xLjM3NUwxMCAzLjM1bC0uNDE3LS41NjdBMi41MSAyLjUxIDAgMCAwIDcuNSAxLjY2N2EyLjQ5NyAyLjQ5NyAwIDAgMC0yLjUgMi41YzAgLjI5MS4wNTguNTc1LjE1LjgzM0gzLjMzM2MtLjkyNSAwLTEuNjU4Ljc0Mi0xLjY1OCAxLjY2N2wtLjAwOCA5LjE2NkExLjY2IDEuNjYgMCAwIDAgMy4zMzMgMTcuNWgxMy4zMzRhMS42NiAxLjY2IDAgMCAwIDEuNjY2LTEuNjY3VjYuNjY3QTEuNjYgMS42NiAwIDAgMCAxNi42NjcgNXptMCA2LjY2N0gzLjMzM3YtNWg0LjIzNEw1LjgzMyA5LjAyNWwxLjM1Ljk3NSAxLjk4NC0yLjdMMTAgNi4xNjdsLjgzMyAxLjEzMyAxLjk4NCAyLjcgMS4zNS0uOTc1LTEuNzM0LTIuMzU4aDQuMjM0djV6IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMSAtMSkiLz4KICAgIDwvZz4KPC9zdmc+Cg=="/>
                                                                                                        <span className="tcolor ml-2">Gift Cards</span>
                                                                                                        </span></div>
                                                                                                        <div className="col-2">
                                                                                                            <span className="mr-1"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNSIgaGVpZ2h0PSIxNSIgdmlld0JveD0iMCAwIDE1IDE1Ij4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCBpZD0iYSIgeDE9IjAlIiB4Mj0iODYuODc2JSIgeTE9IjAlIiB5Mj0iODAuMjAyJSI+CiAgICAgICAgICAgIDxzdG9wIG9mZnNldD0iMCUiIHN0b3AtY29sb3I9IiNGRkQ4MDAiLz4KICAgICAgICAgICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjRkZBRjAwIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0yLTNoMjB2MjBILTJ6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0idXJsKCNhKSIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNOS41IDNDNS4zNiAzIDIgNi4zNiAyIDEwLjUgMiAxNC42NCA1LjM2IDE4IDkuNSAxOGM0LjE0IDAgNy41LTMuMzYgNy41LTcuNUMxNyA2LjM2IDEzLjY0IDMgOS41IDN6bS43NSAxMi43NWgtMS41di0xLjVoMS41djEuNXptMS41NTMtNS44MTNsLS42NzYuNjljLS41NC41NDgtLjg3Ny45OTgtLjg3NyAyLjEyM2gtMS41di0uMzc1YzAtLjgyNS4zMzgtMS41NzUuODc3LTIuMTIzbC45My0uOTQ1Yy4yNzgtLjI3LjQ0My0uNjQ1LjQ0My0xLjA1NyAwLS44MjUtLjY3NS0xLjUtMS41LTEuNVM4IDcuNDI1IDggOC4yNUg2LjVhMyAzIDAgMSAxIDYgMGMwIC42Ni0uMjcgMS4yNi0uNjk3IDEuNjg4eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTIgLTMpIi8+CiAgICA8L2c+Cjwvc3ZnPgo="/>
                                                                                                            <span className="tcolor ml-2">Help Center</span></span></div>
                                                                                                            <div className="col-4">
                                                                                                                <img src="https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/payment-method_7934bc.svg"/>
                                                                                                                    </div>
                </div>
               
               
            </React.Fragment>
        )
    }
}
export default ExtraInfo;