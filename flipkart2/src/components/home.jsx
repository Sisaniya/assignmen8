import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import ExtraInfo from "./extraInfo";
import ExtraInfo2 from "./extraInfo2";
import ImageShow from "./imageShow";
import NavBar2 from "./navBar2";
class Home extends Component
{
    state={
            products:[],
            page:1,
    }
    handelPage=(inc)=>{
        let s={...this.state};
        s.page= s.page+inc;
        this.setState(s);
    }
    async componentDidMount() {
        let response=await httpServices.get("/getDealsForTheDay");
        let {data}=response;
        this.setState({products:data});
    }
    buyMobile=(brand,id)=>{
        let {products=[]}=this.state;
        let find=products.find(ele=>ele.id == id);
        this.props.addToViewList(find);
        this.props.history.push(`/home/Mobile/${brand}/${id}`);
    }
    render() {
        let {products,page}=this.state;
        let size=5;
        let firstIndex=(page-1)*size;
        let lastIndex=firstIndex+(size-1);
        lastIndex=products.length>lastIndex?lastIndex:products.length-1;
        let pagePro=products.filter((ele,index)=>index>=firstIndex && index<=lastIndex);
        
        return (
            <React.Fragment>
                   
                   <div className="row bg-white mx-2">
                       <div className="col-12">

                     
                    <div className="row  my-1 bgcolor">
                        
                                <ImageShow/>
                        
                              
                    <div className="col-lg-9  bg-white">
                      <div className="row">
                          <div className="col-lg-11 ">
                              <span  className="fontStyle pdotds">Deals Of the Day</span>
                          </div>
                          <div className="col-lg-12 ">
                              <hr className="bg-seconday"/>
                            <div className="row mt-2">
                                
                                     {page>1 && 
                                      <div className="col-1   justify-content-center  pl-5" >
                                          <i onClick={()=>this.handelPage(-1)}
                                           className="d-block fas fa-chevron-left dealsMobileIcons cps"></i> </div>}
                                    
                                 {pagePro.map(ele=>{
                                     return (
                                         <div className="col-2 text-center">
                                            <img 
                                            className="d-block rounded mx-auto cps" onClick={()=>this.buyMobile(ele.brand,ele.id)} src={ele.img}  height="150"/><br/>
                                            <small className="dark text12">{ele.name.substring(0,21)}...</small><br/>
                                            <small className="text-success d-block mt-2 pdotdds">{ele.discount}%</small>

                                         </div>
                                     )
                                 })}
                               
                                     {(lastIndex+1)<products.length &&   <div className="col-1 text-center pr-5"> 
                                      <i  onClick={()=>this.handelPage(1)} className="fas fa-chevron-right dealsMobileIcons cps"></i>  </div>}
                                  
                            </div>
                          </div>
                      </div>
                    </div>
                    <div className="col-lg-2  bg-white">
                        <img src="https://i.ibb.co/1GBrRnn/fa04c5362949d9f1.jpg" className="dealsImgStyle"/>
                    </div>
                
                    <div className="col-lg-4 ">
                        <img className="dealsBImgStyle " src="https://i.ibb.co/dPVHZGW/d5db30a716f82657.jpg"/>
                    </div>
                    <div className="col-lg-4">
                        <img className="dealsBImgStyle " src="https://i.ibb.co/Lzz36zB/31efaad41a3e4208.jpg"/>
                    </div>
                    <div className="col-lg-4">
                        <img className="dealsBImgStyle " src="https://i.ibb.co/fGX7sFh/4e219998fadcbc70.jpg"/>
                    </div>
                </div>
                <div className="row text-muted bg-white justify-content-center py-2 ">
                    <div className="col-lg-12 ">
                    <span className="text-muted headingStyle">Flipkart: The One-stop Shopping Destination</span>
                    <p>
                    E-commerce is revolutionizing the way we all shop in India. Why do you want to hop from one store to another in search of the latest phone when you can find it on the Internet in a single click? Not only mobiles. Flipkart houses everything you can possibly imagine, from trending electronics like laptops, tablets, smartphones, and mobile accessories to in-vogue fashion staples like shoes, clothing and lifestyle accessories; from modern furniture like sofa sets, dining tables, and wardrobes to appliances that make your life easy like washing machines, TVs, ACs, mixer grinder juicers and other time-saving kitchen and small appliances; from home furnishings like cushion covers, mattresses and bedsheets to toys and musical instruments, we got them all covered. You name it, and you can stay assured about finding them all here. For those of you with erratic working hours, Flipkart is your best bet. Shop in your PJs, at night or in the wee hours of the morning. This e-commerce never shuts down.
                    </p>
                    <span className="text-muted headingStyle">Flipkart Plus</span>
                    <p>
                    A world of limitless possibilities awaits you - Flipkart Plus was kickstarted as a loyalty reward programme for all its regular customers at zero subscription fee. All you need is 500 supercoins to be a part of this service. For every 100 rupees spent on Flipkart order, Plus members earns 4 supercoins & non-plus members earn 2 supercoins. Free delivery, early access during sales and shopping festivals, exchange offers and priority customer service are the top benefits to a Flipkart Plus member. In short, earn more when you shop more! What's more, you can even use the Flipkart supercoins for a number of exciting services, like: An annual Zomato Gold membership An annual Hotstar Premium membership 6 months Gaana plus subscription Rupees 550 instant discount on flights on ixigo Check out https://www.flipkart.com/plus/all-offers for the entire list. Terms and conditions apply.
                    </p>
                    <span className="text-muted headingStyle">No Cost EMI</span>
                    <p>
                    In an attempt to make high-end products accessible to all, our No Cost EMI plan enables you to shop with us under EMI, without shelling out any processing fee. Applicable on select mobiles, laptops, large and small appliances, furniture, electronics and watches, you can now shop without burning a hole in your pocket. If you've been eyeing a product for a long time, chances are it may be up for a no cost EMI. Take a look ASAP! Terms and conditions apply.
                    </p>
                    </div>
                    
                </div>
               
                </div>
                   </div>                                                                       
              

                <ExtraInfo/>
            </React.Fragment>
        )
    }
}
export default Home;