import React,{Component} from "react";
import {Link} from "react-router-dom";
class CompareList extends Component 
{
    state={}
    addToCart=(id)=>{
        let {cartItems,compareList}=this.props;
        let product=compareList.find(ele=>ele.id == id);
        let index=cartItems.findIndex(ele=>ele.id == id);
    
         this.props.handelCart(product);
         
         this.props.history.push("/cart");
   
    
    }
    buyProduct=(id)=>{
       
        let {compareList=[]}=this.props;
      let find=compareList.find(ele=>ele.id == id);

        this.props.addToViewList(find);
      this.props.history.push(`/home/Mobile/${find.brand}/${id}`);
     
    
  }
  goToHome=()=>{
    this.props.history.push("/home");
  }
    render() {
        let {compareList=[]}=this.props;
        let FP=compareList[0];
        return (
            <React.Fragment>
                {compareList.length?(
                    <React.Fragment>
                <div className="row mx-1 bg-white border-bottom border-top pt-2">
                    <div className="col-3  bg-white">
                        <div className="row bg-white">
                            <div className="col-10">
                            <span className="fontstyle cplhs" >{compareList.length && compareList[0].name} vs others</span><br/>
                        <span className="normalcolor"> {compareList.length} items</span>
                   
                            </div>
                        </div>
                       
                    </div>
                    
                    {compareList.map(ele1=>{
                        let {img,name,price,assured,discount,id,brand}=ele1;
                        return (
                            <div className="col-lg-2  py-1 ">
                                <div className="row bg-white">
                                                    <div className="col-6 pb-4 mb-4 mt-3">
                                                    <img src={img} className="d-block rounded mx-auto" width="80" height="165"/>
                                                  
                                                    </div>
                                                    <div className="col-4"></div>
                                                    <div  className="col-1 text-right">
                                                        {compareList.length>1?(
                                                          
                                                           <div onClick={()=>this.props.removeFromCompareList(ele1)} className="_1vuGld" title="Remove Product">✕</div>
                                                        ):""}
                                                       
                                                    </div>
                                                    <div className="col-12 pb-2">
                                                    <Link onClick={()=>this.buyProduct(id)}  className="cplpns fontstyle notActivePPath"> {name}</Link>
                                                    </div>
                                                    <div className="col-10 ml-2">
                                                    <span className="font-weight-bold cplpps" > ₹{price}</span>
                                                    </div>
                                                   
                                               </div>
                                            
                                
                               {assured == true?<img  height="21" src="https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/fa_62673a.png"/>:""}<br/>

                            </div>
                        )
                    })}
                </div>
                <div className="row bg-white  mx-1 ">
                         <div style={{fontSize:"16px"}} className="col-lg-3 border-right ">
                               <strong>Rating and reviews</strong>
                          </div>
                          {compareList.map(ele=>{
                              let {rating,ratingDesc}=ele;
                              return (
                                  <div className="col-2 py-1 pb-5  border-right">
                                      <div className="row pt-3 pb-2">
                                          <div className="col-12 pb-2">
                                          <span className="compareRatingStyle">{rating} <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                                               className="compareRatingStar"/></span>  
                                          </div>
                                          
                                           <span className="text-muted ucpdns" >{ratingDesc}</span>
                                          
                                          
                                      </div>
                                     
                                  </div>
                                
                              )
                          })}
                </div>
                <div className="row bg-white mx-1  ">
                         <div className="col-lg-3 border-right ucpps">
                         <strong>Highlights</strong>
                          </div>
                          {compareList.map(ele=>{
                              let {details,id}=ele;
                              return (
                                  <div  className="fontstyle col-2 py-1 border-right cplpdss">
                                    {details.map(ele=><React.Fragment><span>{ele}</span>.<br/></React.Fragment>)}
                                 
                                            
                                  </div>
                                
                              )
                          })}
                </div>
                
                <div className="row mx-1 bg-white ">
                         <div className="col-lg-3 bg-white border-right">
                        
                          </div>
                          {compareList.map(ele=>{
                              let {details,id}=ele;
                              return (
                                  <div className="col-2 pb-1 bg-white pt-5 border-right">
                                   
                                     <div className="row" >
                                        <div className="col">
                                            <button 
                                             onClick={()=>this.addToCart(id)}  className="comparecartbuttonstyle fontstyle cpslbpbs" type="button">
                                                <i className="fa fa-bolt"></i> BUY NOW</button>
                                                </div>
                                                </div>
                                            
                                  </div>
                                
                              )
                          })}
                </div>

                <div className="row mx-1 py-2 bg-light">
                         <div className="col-lg-3 ucpps" >
                         <strong>PLATFORM AND PERFORMANCE</strong>
                          </div>
                          {compareList.map(ele=>{
                              let {details,id}=ele;
                              return (
                                  <div className="col-2">
                                     
                                  </div>
                                
                              )
                          })}
                </div>
                <div className="row bg-white mb-2 mx-1 pb-3 border-bottom">
                         <div className="col-lg-3  py-3 border-right ucpps" >
                         <strong>RAM</strong>
                          </div>
                          {compareList.map(ele=>{
                              let {details,id,ram}=ele;
                              return (
                                  <div className="cplpdss col-2 py-3 fontStyle border-right">
                                        {ram} GB
                                  </div>
                                
                              )
                          })}
                </div>
                </React.Fragment>
                ):this.goToHome()}
               
            </React.Fragment>
        )
    }
}
export default CompareList;