import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import http from "../services/httpServices";
import ExtraInfo from "./extraInfo";
import {createBrowserHistory} from "history";
import QNA from "./qna";
import httpServices from "../services/httpServices";
import UserNavbar from "./userNavbar";
class UserWishlist extends Component 
{
    state={
        wishList:[],
    }
   
        async fetchData() {
            let user=localStorage.getItem("usertoken")
            console.log(user);
            let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/wishlist",{headers: {Authorization: user}});
                let {data}=response;
                console.log(data);
                this.setState({wishList:data});
        }
        async componentDidMount() {
                this.fetchData();
        }
       
        buyMobile=(brand,id)=>{
            let {wishList=[]}=this.state;
            if(wishList.length)
            {
               
              
            let find=wishList.find(ele=>ele.id == id);
    
              this.props.addToViewList(find);
            this.props.history.push(`/home/Mobile/${brand}/${id}`);
            }
             
            
        }
        nameEffect=(e)=>{
            e.currentTarget.className="nameEffectMove";
        }
        removeEffect=(e)=>{
            e.currentTarget.className="removeEffectMove";
        }
        async putToList(obj) {
            try {
                let response=await httpServices.put("/wishlist",obj);
                 window.alert("Wishlist changed");
                 this.fetchData();
            }
            catch(ex) {

            }
           
        }
        addToWishList=(item)=>{
            let id=localStorage.getItem("id");
            if(id)
            {
                 let obj={userId:id,item:item};
                 this.putToList(obj);
            }
        }
        showProfile=()=>{
            this.props.history.push("/dashboard/account");
        }
    render() {
        let {wishList}=this.state;
        const history = createBrowserHistory();
        let  pathname=history.location.pathname;
        return (
            <React.Fragment>
                <div className="row backgcolor ">
                    <div className="col-12">
                 <div className="row backgcolor mx-2">
                 <div className="col-lg-3 mx-2 mt-1">
                   <UserNavbar/>

                </div>
                <div className="col-lg-8 normalbgcolor mt-2 mx-2">
                    {wishList.length ?(
                          <div className="row my-1 py-1 px-2 pl-4 border-bottom">
                          <div className="col-lg-12  pt-3 pb-4">
                              <span className="uwlihs" >
                                  My WishList({wishList.length})</span>
                           </div>
                       </div>


                    ):""}
              
                      
                        {wishList.map((ele,index)=>{
                            let {img,name,price,discount,rating,ratingDesc}=ele;
                            return (
                                <React.Fragment>
                                <div className= {index==wishList.length-1?"row py-4 wishlistbottom":"row py-4 border-bottom"}>
                                      <div className="col-3">
                                      <Link onClick={()=>this.buyMobile(ele.brand,ele.id)} className="notActivePPath"
                                       >
                                          <img className="d-block rounded mx-auto" src={img} width="40" height="80"/>
                                       </Link>
                                          
                                      </div>
                                      <div className="col-4">
                                          <div className="row uwlihs1" >
                                              <div className="col-12 fontstyle">
                                                  {name}

                                              </div>
                                              <div className="col-12">
                                              <span className="compareRatingStyle">{rating} <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                                               className="compareRatingStar"/></span> 
                                                      <span className="ml-1 text-muted fontstyle text500" >(14)</span>
                                              </div>
                                              <div className="col-12 fontstyle uwlihs2" >
                                              ₹ {price}
                                              
                                              </div>
                                          </div>
                                      
                                      </div>
                                      <div className="col-3 text-right">
                                      <span className="" onClick={()=>this.addToWishList(ele)}><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMiIgaGVpZ2h0PSIxNiIgdmlld0JveD0iMCAwIDEyIDE2Ij4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPHBhdGggZD0iTS0xLjUuNWgxNXYxNWgtMTV6Ii8+CiAgICAgICAgPHBhdGggZmlsbD0iI0MyQzJDMiIgZmlsbC1ydWxlPSJub256ZXJvIiBkPSJNMSAxMy44MzNjMCAuOTE3Ljc1IDEuNjY3IDEuNjY3IDEuNjY3aDYuNjY2Yy45MTcgMCAxLjY2Ny0uNzUgMS42NjctMS42Njd2LTEwSDF2MTB6bTEwLjgzMy0xMi41SDguOTE3TDguMDgzLjVIMy45MTdsLS44MzQuODMzSC4xNjdWM2gxMS42NjZWMS4zMzN6Ii8+CiAgICA8L2c+Cjwvc3ZnPgo="
                                       className="_2Nq6Qc"/></span>
                                          
                                          
                                      </div>
                                </div>
                                  
                                </React.Fragment>
                            )
                        })}    
          
                <QNA/>
                
                </div>
            </div>
            </div></div>
            <ExtraInfo/>
               
               
            </React.Fragment>
        )
    }
}
export default UserWishlist;