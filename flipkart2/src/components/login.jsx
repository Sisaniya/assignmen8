import React,{Component} from "react";
import Modal from "react-bootstrap/Modal";
import http from "../services/httpServices";

import "bootstrap/dist/css/bootstrap.min.css";
class Login extends Component
{
    state={
     
      user:{email:"",password:""},
      errors:"",
    }
    handelShow=()=>{
      let s={...this.state};
      s.show=s.show?false:true;
      this.setState(s);
    }
    handelChange=(e)=>{
      let {currentTarget:input}=e;
     
      let s={...this.state};
      s.user[input.name]=input.value;
      s.errors="";
      this.setState(s);
  }
  async postData(url,obj) {
      try
      {
          let {data:res,headers}=await http.post(url,obj);
          let usertoken=headers["x-auth-token"];
       
          localStorage.setItem("usertoken",usertoken);
          console.log(res,headers);
          localStorage.setItem("role",res.role);
          localStorage.setItem("id",res.id);
         
         let str=res.role=="admin"?"/admin":"/home";
         window.location=str;
      }
      catch(ex) {
          if(ex.response && ex.response.status == 400)
          {
              let errors="Invalid Email and password!";
             
              this.setState({errors:errors});

          }
          else {
              let errors="";
              errors="Sever side errors";
             
              this.setState({errors:errors});
          }
      }
     
  }
  removeInputFocus=(e)=>{
    e.currentTarget.className="pincodeFieldStyle";
    console.log(e.currentTarget.className);
  }
  handelInputFocus=(e)=>{
    e.currentTarget.className="removeBorders";
    console.log(e.currentTarget.className);
  }
  handelSubmit=(e)=>{
      e.preventDefault();
      console.log("login submit");
      let {user}=this.state;
      this.postData("/login",user);
  }
    render() {
     
      let {user,errors,show}=this.state;
      let {email,password}=user;
        return (
            <React.Fragment>
        
    
        <Modal.Dialog>
         <div className="row modalwidth">
           <div className="col-6 px-4 py-3 bg-primary">
             <div className="row">
               <div className="col-12">
                <h3 className="text-light">Login</h3>
               </div>
               <div className="col-12 my-2 text-light" >
                <span>Get access to your Orders,<br/></span>
                <sapn>Wishlist and Recommendations</sapn>
               </div>
               <div className="col-12"></div>
             </div>
           </div>
           <div className="col-6 py-2 px-1">
           <sapn className="closeLogin" onClick={()=>this.props.showLogin()}>x</sapn>
             <div className="row justify-content-center">
                 
                  
                
                     <div className="col-12 my-2">
                     <small className="my-2">Email/Mobile number</small><br/>
                               <input
                               className="pincodeFieldStyle "
                                name="email"
                                onBlur={this.removeInputFocus}
                                onFocus={this.handelInputFocus}
                                value={email}
                                placeholder="Enter email/mobile"
                                onChange={this.handelChange}
                                 />
                                 <hr className="bg-light my-1"/>
                     </div>
                    
                    
                    
                     <div className="col-12 ">
                     <span className=" my-1">Password</span><br/>
                     </div>
                     <div className="col-8 mt-2">
                    
                               <input type="text"
                               className="pincodeFieldStyle"
                                name="password"
                                onBlur={this.removeInputFocus}
                                onFocus={this.handelInputFocus}
                                value={password}
                                placeholder="Enter Password"
                                onChange={this.handelChange}
                                 />
                        
                     </div>
                     <div className="col-4">
                       <span className="text-primary mt-2 d-block">{password?"Forget?":""}</span>
                     </div>
                     <div className="col-12">
                     <hr className="bg-light"/>
                     </div>
                    
                     {errors && (
                  
                   <div className="col-12 text-center text-danger">
                     {errors}
                   
                   </div>
                       )}
                     <div className="col-12 my-2 text-center text-muted">
                       <small>
                       By continuing, you agree to Flipkart'sTerms of Use and Privacy Policy
                       </small>
                   
                     </div>
                     <div className="col-1"></div>
                     <div onClick={this.handelSubmit} className="col-10 text-center my-2 text-light font-weight-bold  py-2 buyNowStyle">
                              Login
                     </div>
                     <div className="col-12 text-center mb-2 text-muted">OR</div>
                     <div className="col-12 text-center my-2 text-primary">
                       <span className="d-block ml-3">
                       Request OTP
                       </span>
                      
                       </div>
             </div>
             <hr className="bg-light"/>
              <div className="row">
                <div className="col-12 my-2 px-1 text-center text-primary">
                  <span></span>
                  New to Flipkart? Create an account
                </div>
              </div>
             
           </div>
         </div>
        </Modal.Dialog>
     
     
  </React.Fragment>
        )
    }
}
export default Login;