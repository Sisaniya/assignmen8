import React,{Component} from "react";
import {Link,Switch,Route, Redirect} from "react-router-dom";
import NavBar from "./navBar";
import Home from "./home";
import Products from "./products";
import Product from "./product";
import CartPage from "./cartPage";
import ViewList from "./viewList";
import CompareList from "./compareList";
import Input from "./input";
import Login from "./login";
import Logout from "./logout";
import UserProfile from "./userProfile";
import UserOrders from "./userOrders";
import UserWishlist from "./userWishlist";
import AdminProfile from "./adminProfile";
import ProductForm from "./productForm";
import AdminProducts from "./adminProducts";
import FormUpload from "./formUpload";
import DownloadProducts from "./downloadProducts";
import ReportLogs from "./reportLog";
import {createBrowserHistory} from "history";
import queryString from "query-string";
import UserAccount from "./userAccount";
class MainComp extends Component
{
    state={
      search:"",
      cartItems:[],
      viewList:[],
      compareList:[],
      spincode:"",
    }
    setPincode=(pin)=>{
      let s={...this.state};
      s.spincode=pin;
      this.setState(s);
    }
    addToCompareList=(find)=>{
      let s={...this.state};
      let size=s.compareList.length;
      if(size<4)
      {
      
        s.compareList.push(find);
      }
      else
      {
        s.compareList.shift();
        s.compareList.push(find);
      }
      this.setState(s);
      
    }

    removeFromCompareList=(find)=>{
      let s={...this.state};
      let index=s.compareList.findIndex(ele=>ele.id == find.id);
      s.compareList.splice(index,1);
      this.setState(s);
    }
    manageCompareList=(find)=>{
      let s={...this.state};
      let index=s.compareList.findIndex(ele=>ele.id == find.id);
      if(index>=0)
      {
        s.compareList.splice(index,1);
      }
      else
      {
        let size=s.compareList.length;
        if(size<4)
        {
        
          s.compareList.push(find);
        }
        else
        {
          s.compareList.shift();
          s.compareList.push(find);
        }
      }
      this.setState(s);

    }
    addToViewList=(pro)=>{
      let s={...this.state};
      let index=s.viewList.findIndex(ele=>ele.id == pro.id);
      if(!index>=0)s.viewList.push(pro);
      this.setState(s);
    }
    handelIncrement=(id)=>{
      let s={...this.state};
        let index=s.cartItems.findIndex(ele=>ele.id == id);
        let item=s.cartItems[index];
        let q=item.qty;
        item.qty=q+1;
      
        this.setState(s);
         
    }
    handelDecrement=(id)=>{
      let s={...this.state};
      let index=s.cartItems.findIndex(ele=>ele.id == id);
      let item=s.cartItems[index];
      if(item.qty == 1)
      {
        s.cartItems.splice(index,1);
      }
      else
      {
        let q=item.qty;
        item.qty=q-1;
      
      }
     
      this.setState(s);
    }
    handelCart=(product)=>{
      let s={...this.state};
      let index=s.cartItems.findIndex(ele=>ele.id == product.id);
      if(index>=0)
      {
        let item=s.cartItems[index];
        let q=item.qty;
        item.qty=q+1;
      }
      else
      {
        s.cartItems.push({...product,qty:1});
      }
      this.setState(s);
    }
    handelSearch=(search)=>{
      let s={...this.state};
      s.search=search;
      this.setState(s);

    }
    emptyCartList=()=>{
      let s={...this.state};
      s.cartItems=[];
      this.setState(s);
    }
   
    render() {
     let {search,cartItems,viewList,compareList,spincode}=this.state;
     let history=createBrowserHistory();
     let {q}=queryString.parse(history.location.search);
    
        return(
          <React.Fragment>

          <NavBar viewList={viewList} compareList={compareList} cartItems={cartItems.reduce((acc,curr)=>acc+curr.qty,0)} search={q} handelSearch={this.handelSearch}/>
        
           
            <Switch>
            <Route path="/home/account" render={(props)=><UserAccount {...props}/>}/>
            <Route path="/admin/report/logs" render={(props)=><ReportLogs  open={"open"} {...props}/>}/>
            <Route path="/admin/products/download" render={(props)=><DownloadProducts {...props}/>}/>
            <Route path="/admin/products/view" render={(props)=><AdminProducts {...props}/>}/>
            <Route path="/admin/products/upload" render={(props)=><ProductForm {...props}/>}/>
            <Route path="/admin" render={(props)=><AdminProfile {...props}/>}/>
            <Route path="/dashboard/wishlist" render={(props)=><UserWishlist addToViewList={this.addToViewList} {...props}/>}/>
            <Route path="/dashboard/account/orders" render={(props)=><UserOrders/>}/>
            <Route path="/dashboard/account" render={(props)=><UserProfile {...props}/>}/>
            <Route path="/mobiles/compare" render={(props)=><CompareList addToViewList={this.addToViewList}  removeFromCompareList={this.removeFromCompareList} handelCart={this.handelCart} compareList={compareList} {...props} cartItems={cartItems}/>}/>
            <Route path="/recentlyviewed" render={(props)=><ViewList {...props} items={viewList}/>}/>
              <Route path="/cart" render={(props)=><CartPage emptyCartList={this.emptyCartList} handelDecrement={this.handelDecrement} handelIncrement={this.handelIncrement} {...props} cartItems={cartItems}/>}/>
            <Route path="/home/:category/:brand/:id" render={(props)=><Product
            setPincode={this.setPincode}
            spincode={spincode}
            cartItems={cartItems} handelCart={this.handelCart}  {...props}/>}/>
            <Route path="/home/:category/:brand" render={(props)=><Products
            manageCompareList={this.manageCompareList}
             addToCompareList={this.addToCompareList}
             removeFromCompareList={this.removeFromCompareList}
             compareList={compareList} addToViewList={this.addToViewList}  {...props}/>}/>
              <Route path="/home/:category" render={(props)=><Products
               manageCompareList={this.manageCompareList}
             addToCompareList={this.addToCompareList}
             removeFromCompareList={this.removeFromCompareList}
             compareList={compareList} addToViewList={this.addToViewList}  {...props}/>}/>
              <Route path="/logout" render={(props)=><Logout/>}/>
           <Route path="/home" render={(props)=><Home addToViewList={this.addToViewList} {...props}/>}/>
              <Redirect from="/" to="/home"/>
            </Switch>
          </React.Fragment>
        )
    }
}
export default MainComp;