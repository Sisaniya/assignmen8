import React,{Component} from "react";
import {Link} from "react-router-dom";
import AdminNavBar from "./adminNavbar";
import FormUpload from "./formUpload";
import QNA from "./qna";
import queryString from "query-string";
import CsvUpload from "./csvUpload";
class ProductForm extends Component
{
    state={
       
    }
   backToAdmin=()=>{
       this.props.history.push("/admin");
   }
    render() {
        let {q}=queryString.parse(this.props.location.search);
        return (
            <React.Fragment>
                <div className="row backgcolor mb-2 mx-2">
                    <div className="col-lg-3 ml-3 my-2">
                        <AdminNavBar/>
                    </div>
                    <div className="col-lg-8  normalbgcolor ml-4 mt-2">
                     {q=="form"?<FormUpload backToAdmin={this.backToAdmin}/>:<CsvUpload/>}

                        <QNA/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default ProductForm;

