import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import ExtraInfo from "./extraInfo";
import {createBrowserHistory} from "history";
import QNA from "./qna";
class UserNavbar extends Component
{
    state={
        user:{},
        errors:{},
    }
   
    render() {
        let {fname,lname,gender,email}=this.state.user;
        let {errors}=this.state;
        const history = createBrowserHistory();
        let  pathname=history.location.pathname;
        console.log(pathname);
        console.log(pathname);
        return (
            <React.Fragment>
          
                    <div className="row my-1 p-3 normalbgcolor">
                       <div className="col-lg-12">
                          <img className="mr-1" width="50" height="50"
                          src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/profile-pic-female_3c17ab.svg"/> 
                          <span style={{fontSize:"14px",fontWeight:"bold",display:"inline-block",marginRight:"10px"}}>

                              Hello
                          </span>
                       
                       </div>
                      
                    </div>
                    <div className="row my-2 normalbgcolor">
                    <div className="col-12">
                        <div className="row px-2 pt-3 mb-4 pb-2 border-bottom">
                            <div className="col-2  text-center">
                            <img className="_3GARO3 pl-1 " src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNCIgaGVpZ2h0PSIxOCIgdmlld0JveD0iMCAwIDI0IDE4Ij48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC04LjY5NCAtMTEpIj48ZWxsaXBzZSBjeD0iMjAuNTU3IiBjeT0iMjAiIHJ4PSIyMC41NTciIHJ5PSIyMCIvPjxwYXRoIGZpbGw9IiMyODc0RjEiIGQ9Ik05IDExdjE3LjEwOGMwIC40OTMuNDEuODkyLjkxOC44OTJoNC45M3YtNS4yNTdoLTMuMDMzbDQuOTEyLTQuNzcgNC45NzIgNC44M2gtMy4wMzVWMjloMTIuNDE3Yy41MDcgMCAuOTE4LS40LjkxOC0uODkyVjExSDl6Ii8+PC9nPjwvc3ZnPg=="/>
                            </div>
                            <div className="col-10">
                            <Link to="/dashboard/account/orders" className="userTopLink">

                                <span 
                                 className="userTopLink  fontstyle">MY ORDERS</span>
                             </Link>
                            </div>
                        </div>
                      
                        <div className="row px-2 pt-1 pb-2">
                            <div className="col-2 text-center">
                            <img className="_3GARO3" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMSIgdmlld0JveD0iMCAwIDIyIDIxIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC05LjY5NCAtMTApIj48cGF0aCBmaWxsPSIjMjg3NEYwIiBkPSJNMTQuMjc1IDIyLjcwNGMyLjI3Mi0uNDEyIDQuMzQ3LS42MTggNi4yMjUtLjYxOCAxLjg3OCAwIDMuOTUzLjIwNiA2LjIyNS42MThhNS4xNSA1LjE1IDAgMCAxIDQuMjMgNS4wNjhWMzFoLTIwLjkxdi0zLjIyOGE1LjE1IDUuMTUgMCAwIDEgNC4yMy01LjA2OHptMS4yNzQtNy43MjRjMC0yLjU4IDIuMTYzLTQuNjczIDQuODMyLTQuNjczIDIuNjY3IDAgNC44MyAyLjA5MiA0LjgzIDQuNjczIDAgMi41OC0yLjE2MyA0LjY3My00LjgzIDQuNjczLTIuNjcgMC00LjgzMy0yLjA5Mi00LjgzMy00LjY3M3oiLz48ZWxsaXBzZSBjeD0iMjAuNTU3IiBjeT0iMjAiIHJ4PSIyMC41NTciIHJ5PSIyMCIvPjwvZz48L3N2Zz4="/>
                            </div>
                            <div className="col-10">
                                <span 
                                 className="userTopLink  fontstyle">ACCOUNT SETTINGS</span>
                            </div>
                        </div>
                        <div
                         className={pathname=="/dashboard/account"?"row px-2  fontstyle activeUserLink":"userLink row px-2"}
                       >
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <Link  
                                 className={pathname=="/dashboard/account"?"fontstyle text14" :"fontstyle notActive text14"}
                                 to="/dashboard/account">Profile Information</Link>
                              
                            </div>
                        </div>
                        <div className="row px-2 userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span   className="fontStyle text14"
                                className=" d-block">Mangage Addresses</span>
                            </div>
                        </div>
                        <div className="row px-2 mb-4  border-bottom userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span   className="fontStyle text14"
                                 className=" d-block">PAN Card Information</span>
                            </div>
                        </div>



                       
                        <div className="row px-2 pt-1 pb-2">
                            <div className="col-2 text-center">
                            <img className="_3GARO3" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIyMiIgdmlld0JveD0iMCAwIDIzIDIyIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC05LjY5NCAtOSkiPjxlbGxpcHNlIGN4PSIyMC41NTciIGN5PSIyMCIgcng9IjIwLjU1NyIgcnk9IjIwIi8+PHBhdGggZD0iTTcgNmgyOHYyOEg3eiIvPjxwYXRoIGZpbGw9IiMyODc0RjAiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTMxLjUgMjd2MS4xNjdhMi4zNCAyLjM0IDAgMCAxLTIuMzMzIDIuMzMzSDEyLjgzM2EyLjMzMyAyLjMzMyAwIDAgMS0yLjMzMy0yLjMzM1YxMS44MzNBMi4zMzMgMi4zMzMgMCAwIDEgMTIuODMzIDkuNWgxNi4zMzRhMi4zNCAyLjM0IDAgMCAxIDIuMzMzIDIuMzMzVjEzSDIxYTIuMzMzIDIuMzMzIDAgMCAwLTIuMzMzIDIuMzMzdjkuMzM0QTIuMzMzIDIuMzMzIDAgMCAwIDIxIDI3aDEwLjV6TTIxIDI0LjY2N2gxMS42Njd2LTkuMzM0SDIxdjkuMzM0em00LjY2Ny0yLjkxN2MtLjk3IDAtMS43NS0uNzgyLTEuNzUtMS43NXMuNzgtMS43NSAxLjc1LTEuNzVjLjk2OCAwIDEuNzUuNzgyIDEuNzUgMS43NXMtLjc4MiAxLjc1LTEuNzUgMS43NXoiLz48L2c+PC9zdmc+"/>
                            </div>
                            <div className="col-10">
                                <span 
                                className="userTopLink fontstyle">PAYMENTS</span>
                            </div>
                        </div>
                        <div className="row px-2 userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span   className="fontStyle text14"
                                className=" d-block">Gift cards</span>
                            </div>
                        </div>
                        <div className="row px-2 userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span   className="fontStyle text14"
                                className=" d-block">Saved UPIs</span>
                            </div>
                        </div>
                        <div className="row px-2 mb-4 border-bottom userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span  className="fontStyle text14"
                                className=" d-block">Saved Addresses</span>
                            </div>
                        </div>




                        <div className="row px-2 pt-1 pb-2">
                            <div className="col-2 text-center">
                            <img class="_3GARO3" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIxOSIgdmlld0JveD0iMCAwIDIzIDE5Ij48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGZpbGw9IiMyODc0RjAiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTIwLjUgMi43NWgtOUw5LjI1LjVIMi41QTIuMjQ3IDIuMjQ3IDAgMCAwIC4yNiAyLjc1bC0uMDEgMTMuNUEyLjI1NyAyLjI1NyAwIDAgMCAyLjUgMTguNWgxOGEyLjI1NyAyLjI1NyAwIDAgMCAyLjI1LTIuMjVWNWEyLjI1NyAyLjI1NyAwIDAgMC0yLjI1LTIuMjV6bS01LjYyNSAzLjM3NWEyLjI1NyAyLjI1NyAwIDAgMSAyLjI1IDIuMjUgMi4yNTcgMi4yNTcgMCAwIDEtMi4yNSAyLjI1IDIuMjU3IDIuMjU3IDAgMCAxLTIuMjUtMi4yNSAyLjI1NyAyLjI1NyAwIDAgMSAyLjI1LTIuMjV6bTQuNSA5aC05VjE0YzAtMS40OTYgMy4wMDQtMi4yNSA0LjUtMi4yNXM0LjUuNzU0IDQuNSAyLjI1djEuMTI1eiIvPjxwYXRoIGQ9Ik0tMi00aDI3djI3SC0yeiIvPjwvZz48L3N2Zz4="/>
                            
                            </div>
                            <div className="col-10">
                                <span 
                                className="userTopLink fontstyle ">MY STUFF</span>
                            </div>
                        </div>
                        <div className="row px-2 userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span
                              className="fontStyle text14"
                               >My Coupons</span>
                            </div>
                        </div>
                        <div className="row px-2 userLink">
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <span 
                                 className="fontStyle text14"
                                className=" d-block">My Reviews and rating</span>
                            </div>
                        </div>


                       
                        <div
                        className={pathname=="/dashboard/wishlist"?"row px-2 mb-3 border-bottom fontstyle activeUserLink":"userLink row mb-3 border-bottom px-2"}>
                           
                            <div className="col-2 text-center">
                              
                            </div>
                            <div className="col-10">
                                <Link  
                                 className={pathname=="/dashboard/wishlist"?"fontstyle text14" :"fontstyle notActive text14"}
                                   to="/dashboard/wishlist">My Wishlist</Link>
                               
                            </div>
                        </div>
                        
                   
                        <div className="row px-2 pt-3  mb-2 pb-2">
                        <div className="col-2 text-center">
                            <i class="fa fa-power-off logoutIconStyle text-primary" ></i>
                            </div>
                            <div className="col-10">
                            <Link to="/logout"className="userTopLink">

                                <span 
                                 className="userTopLink  fontstyle">Logout</span>
                             </Link>
                            </div>
                        </div>
                       
                    </div>
                    </div>

         
            </React.Fragment>
        )
    }
}
export default UserNavbar;