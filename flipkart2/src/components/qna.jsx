import React,{Component} from "react";
class QNA extends Component 
{
    state={}
    render() {
        return (
            <React.Fragment>
                
                 <div className="row mx-2 mt-2">
                     <div className="col-12 my-1 mt-4">
                         <p  className="bigText fontStyle text19 text500">FAQs</p>
                     </div>
                     <div className="col-12 my-1">
                    
                     <div className="fontstyle mb-2 text600 text12" >
                         <span>What happens when I update my email address (or mobile number)?</span>
                     </div>
                     <div className="fontstyle qnaanss" >
                        <p>Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</p>
                     </div>
                     <div className="fontstyle mb-2 qnaquess" >
                         <p>When will my Flipkart account be updated with the new email address (or mobile number)?</p>
                     </div>
                     <div className="fontstyle qnaanss" >
                       <p>It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</p>
                     </div>
                     <div className="fontstyle mb-2 qnaquess" >
                         <p>What happens to my existing Flipkart account when I update my email address (or mobile number)?</p>
                     </div>
                     <div className="fontstyle qnaanss" >
                     <p>Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</p>
                     </div>
                     <div className="fontstyle mb-2 qnaquess" >
                         <p>Does my Seller account get affected when I update my email address?</p>
                     </div>
                     <div className="fontstyle qnaanss" >
                       <p>Flipkart has a 'single sign-on' policy. Any changes will reflect in your Seller account also.</p>
                     </div>
                     </div>
                 </div>
                 <div className="row mx-2">
                     <div className="col-12">
                         <img className="d-block w-100" src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/myProfileFooter_0cedbe.png"/>
                     </div>
                 </div>
            </React.Fragment>
        )
    }
}
export default QNA;