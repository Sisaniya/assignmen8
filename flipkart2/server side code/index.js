var port = process.env.PORT || 2410;
const { convertArrayToCSV } = require('convert-array-to-csv');
const csv=require('csvtojson');
var multer = require('multer');
var upload = multer();
const converter=require('json-2-csv');
var express = require("express");
const fs=require("fs");
const BodyParser=require("body-parser");
var jwt=require("jsonwebtoken");
var passport=require("passport");
var passportJWT=require("passport-jwt");
const Strategy=passportJWT.Strategy;
const ExtractJwt=passportJWT.ExtractJwt;
let secretKey="my_passport_token_flipkart";
let app=express();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true })); 
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers,Authorization,Access-Control-Request,Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "PUT,HEAD,POST, GET, DELETE, OPTIONS");
  res.setHeader("Access-Control-Expose-Headers","*");
  next();
});
var params={
  secretOrKey: secretKey,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

passport.initialize();
var strategy1=new Strategy(params,function(payload,done){
  let user=users.find(ele=>ele.id == payload.id);
  user=user?user:null;
  if(user)
  {
    if(user.role == "admin")
    {
     console.log("admin varified",user);
      done(null,{id:user.id})
    }
    else
    {
     console.log("admin not varified",user);
     done(new Error("Invalid User"),null);
    }
  }
  else
  {
   done(new Error("Invalid Request!"),null);
  }
});
var strategy2=new Strategy(params,function(payload,done){
     let user=users.find(ele=>ele.id == payload.id);
     console.log(payload);
     console.log(user,"in user authentication");
     if(user)
     {
      if(user.role == "user")
      {
       console.log("user varified",user);
        done(null,{id:user.id})
      }
      else
      {
       console.log("user not varified",user);
       done(new Error("Invalid User"),null);
      }
     }
     else
     {
       done(new Error("Invalid Request!"),null);
     }
});
var strategy3=new Strategy(params,function(payload,done){
  let user=users.find(ele=>ele.id == payload.id);
  console.log(payload);
  console.log(user,"in user authentication");
  if(user)
  {
   if(user.role == "user" || user.role == "admin")
   {
    console.log("user varified",user);
     done(null,{id:user.id})
   }
   else
   {
    console.log("user not varified",user);
    done(new Error("Invalid User"),null);
   }
  }
  else
  {
    done(new Error("Invalid Request!"),null);
  }
});
passport.use("local.one",strategy1);
passport.use("local.two",strategy2);
passport.use("local.three",strategy3);
let mobiles=require("./mobiles.js").mobiles;
let pincodes=require("./pincodes.js").pincodes;
let reviews=require("./reviews.js").reviews;
let usersData=require("./usersData");
let users=usersData.users;
let usersOrders=usersData.usersOrders;
let wishList=usersData.wishList;
let searchHistory=usersData.searchHistory;

let mostAdded=usersData.mostAdded;
let mostFavorite=usersData.mostFavorite;
let mostSearch=usersData.mostSearch;
let logsReport=usersData.logsReport;
app.listen(port,()=>console.log("Listening at the post =>",port));
app.get("/",function(req,res){
  res.send("welcome to flipkart server");
});
let jwtExpiresSeconds=300000;
app.post("/login",function(req,res,payload){

  let body={...req.body};
  if(body.email && body.password)
  {
        let user=users.find(ele=>ele.email == body.email && ele.password == body.password);
         if(user)
         {
          let logslist=[];
          let d=new Date();
          let str=d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+"T"+d.getHours()+":"+d.getMinutes()+":"+d.getUTCSeconds()+"Z";
       
         
           logslist.push(`"user":"${user.email}"`);
           logslist.push(`loginTime:"${str}"`);
           logsReport.push(logslist);
          var payload={id:user.id};
          console.log(user,payload);
          let token=jwt.sign(payload,secretKey,{algorithm:"HS256",expiresIn:jwtExpiresSeconds});
           res.setHeader("X-Auth-Token","bearer "+token);
           let obj={name:user.name,role:user.role,id:user.id};
           res.json(obj);
         }
       else
       {
        res.sendStatus(400);
       }
    
       
     
  }
     else {
         res.sendStatus(400);
     }

});
app.get("/downloadLogsReport",passport.authenticate("local.one",{session:false}),function(req,res){
 

  
  const logs=convertArrayToCSV(logsReport);
  console.log(logs);
  res.send(logs);
  
})

app.get("/downloadSearchReport",passport.authenticate("local.one",{session:false}),function(req,res){
  
  converter.json2csv(mostSearch,function(err,csv){
    if(err) res.status.send(err);
    else {
      console.log(csv);
     res.send(csv);
     
    } 

  })
  
})

app.get("/downloadFavoriteReport",passport.authenticate("local.one",{session:false}),function(req,res){
  
  console.log(mostFavorite);
  converter.json2csv(mostFavorite,function(err,csv){
    if(err) res.status.send(err);
    else {
      console.log(csv);
     res.send(csv);
     
    } 

  })
  
})

app.get("/downloadProductsReport",passport.authenticate("local.one",{session:false}),function(req,res){
 
  console.log(mostAdded);
  converter.json2csv(mostAdded,function(err,csv){
    if(err) res.status(400).send(err);
    else {
      console.log(csv);
     res.send(csv);
     
    } 

  })
  
})
app.post("/csvUpload",upload.single("myfile"),(req,res,next)=>{
  console.log(req.body.name,req.file,"got files");
  if(req.file.mimetype != 'application/vnd.ms-excel')
  res.status(400).send("Invalid File type");
  else
  {
   fs.writeFile(req.file.originalname,req.file.buffer,function(err){if(err) res.status(400).send(err)});

    let name=req.file.originalname;
    csv().fromFile(__dirname+"/"+name)
    .then(result=>{
      console.log(result);
        mobiles.concat(result);
       
        let obj={uploads:{Mobiles:mobiles.length,Laptops:0,Cameras:0},
              Edits:{Mobiles:4,Laptops:0,Cameras:0}}
              res.send(obj);

    })
  
  }
  
})
app.get("/downloadAllProducts",passport.authenticate("local.one",{session:false}),function(req,res){
     converter.json2csv(mobiles,function(err,csv){
       if(err) res.status.send(err);
       else {
         console.log(csv);
        res.send(csv);
        
       } 

     })
     
})
app.put("/product/:id",function(req,res){
  let id=req.params.id;
  let product={...req.body};
  let index=mobiles.findIndex(ele=>ele.id == id);
  mobiles.splice(index,1,product);
  res.send(product);
})
app.get("/allproducts",passport.authenticate("local.one",{session:false}),function(req,res){
  
   res.send(mobiles);
})
app.get("/product/:id",passport.authenticate("local.one",{session:false}),function(req,res){
  let id=req.params.id;
   let product=mobiles.find(ele=>ele.id == id);
   res.send(product);
})
app.post("/product",function(req,res){
   let product={...req.body};
   let len=mobiles.length;
   let newProduct={...product,id:product.brand+len};
   mobiles.push(newProduct);
   res.send(newProduct);
})
app.get("/user",passport.authenticate("local.two",{session:false}),function(req,res){
  let user=users.find(ele=>ele.id == req.user.id);
  let logslist=[];
  logslist.push(`"url":"/user"`);
  logslist.push(`queryparams:{}`);
  logslist.push(`status:200`);
  logsReport.push(logslist);
  res.send(user);
});
app.put("/user",function(req,res){
  let user={...req.body};
  
  let index=users.findIndex(ele=>ele.id == user.id);
  users.splice(index,1,user);
  res.send("Updated");
})
var logout=require("express-passport-logout");


app.delete("/logout",passport.authenticate("local.two",{session:false}),logout());

app.get("/getDealsForTheDay",function(req,res){
  let products=mobiles.filter((ele,index)=>index<6);
  let pro1=mobiles.filter((ele,index)=>index>30 && index<=36);
  res.send(products.concat(pro1));
})
app.get("/wishlist",passport.authenticate("local.three",{session:false}),function(req,res){
    let find=wishList.find(ele=>ele.userId == req.user.id );

     res.send(find.items);
})

app.put("/wishlist",function(req,res){
      let body={...req.body};
      let {userId,item}=body;
      let find=wishList.find(ele=>ele.userId == userId);
      if(find)
      {
        let items=find.items;
         let index=items.findIndex(ele=>ele.id == item.id);
         if(index>=0)
         {
           items.splice(index,1);

         }
         else
         {
            items.unshift(item);
            let findex=mostFavorite.findIndex(ele=>ele.prodId == item.id);
            if(findex>=0)
            {
              let pro={...mostFavorite[findex]};
       pro.count=pro.count+1;
       mostFavorite.splice(findex,1,pro);

            }
            else
            {
              let obj={id:mostFavorite.length+1,prodId:item.id,count:1};
              mostFavorite.push(obj);
            }
         }
         find.items=items;
      }
      else
      {
        let obj={userId:userId,items:[item]};
        wishList.push(obj);
        let findex=mostFavorite.findIndex(ele=>ele.prodId == item.id);
        if(findex>=0)
        {
          let pro={...mostFavorite[findex]};
       pro.count=pro.count+1;
       mostFavorite.splice(findex,1,pro);

        }
        else
        {
          let obj={id:mostFavorite.length+1,prodId:item.id,count:1};
          mostFavorite.push(obj);
        }
      }
     
   
      res.send("added successfully")

})
app.put("/addOrder",function(req,res){
  let body=req.body;
  let find=usersOrders.find(ele=>ele.userId == body.userId);
  if(find) {
      let orders=find.orders;
      let neworders=body.orders.concat(orders);
      find.orders=neworders;
  }
  else
  {
    usersOrders.unshift(body);
  }
   body.orders.forEach(ele1=>{
    let findex=mostAdded.findIndex(ele=>ele.prodId == ele1.id);
    if(findex>=0)
    {
      let pro={...mostAdded[findex]};
       pro.count=pro.count+1;
       mostAdded.splice(findex,1,pro);

    }
    else
    {
      let obj={id:mostAdded.length+1,prodId:ele1.id,count:1};
      mostAdded.push(obj);
    }
   })
   res.send("order placed");
})
app.get("/getUserOrders",passport.authenticate("local.two",{session:false}),function(req,res){
  let data=usersOrders.find(ele=>ele.userId == req.user.id);
  let logslist=[];
  logslist.push(`"url":"/orders"`);
  logslist.push(`queryparams:{}`);
  logslist.push(`status:200`);
  logsReport.push(logslist);
  res.send(data);
})
app.get("/products/:category/:brand",(req,res)=>{
  let {category,brand}=req.params;
  let {ram,rating,price,sort,page=1,assured,q}=req.query;
 
  if(q)
  {
    searchHistory.push({q:q,date:new Date().toLocaleDateString()});
  }
  let products=[];
  if(category=="Mobiles")
  {
    products=[...mobiles];
  }
  else
  {
    products=[];
  }
   products=brand?products.filter(ele=>ele.brand==brand):products;
  products=assured?products.filter(ele=>ele.assured):products;
    
   products=ram?products.filter(ele=>ram.split(",").find(r=>r.indexOf(ele.ram+"")>=0?true:false)):products;
   products=rating?products.filter(ele=>rating.split(",").find(r=>r<ele.rating)):products;
   if(price)
   {
     let pricearr=price.split(",");
   
     products=products.filter(ele=>{
       
         let find=pricearr.find(element => {
            let arr=element.split("-");
            let lowprice= +arr[0];
            let highprice=+arr[1];
           
            if(ele.price>=lowprice && ele.price<=highprice) return true;
            else return false;
          });
         
          return find;
     })
   
   }

   if(sort=="popularity")
   {
     products.sort((p1,p2)=>p1.popularity-p2.popularity);

   }
   if(sort=="lowtohigh")
      products.sort((p1,p2)=>(+p1.price)-(+p2.price));
      if(sort=="hightolow")
      products.sort((p1,p2)=>(+p2.price)-(+p1.price));


  let p=+page;
  let size=5;
  let firstIndex=(p-1)*size;
  let lastIndex=firstIndex+(size-1);
  lastIndex=products.length>lastIndex?lastIndex:products.length-1;
  let pageProducts=products.filter((ele,index)=>index>=firstIndex && index<=lastIndex);
  let totalItems=products.length;
  let obj={
    firstIndex:totalItems?firstIndex+1:0,
    lastIndex:totalItems?lastIndex+1:0,
    page:p,
    totalItems:totalItems,
    items:pageProducts,
    totalPageItems:size,

  }
  res.send(obj);

});
app.get("/products/:category",(req,res)=>{
  let {category}=req.params;
  let {ram,rating,price,sort,page=1,assured,q}=req.query;
  if(q)
  {
    searchHistory.push({q:q,date:new Date().toLocaleDateString()});
  }
  let products=[];
  if(category=="Mobiles")
  {
    products=[...mobiles];
  }
  else
  {
    products=[];
  }
  
   products=q?products.filter(ele=>ele.brand==q):products;
  products=assured?products.filter(ele=>ele.assured):products;
    
   products=ram?products.filter(ele=>ram.split(",").find(r=>r.indexOf(ele.ram+"")>=0?true:false)):products;
   products=rating?products.filter(ele=>rating.split(",").find(r=>r<ele.rating)):products;
   if(price)
   {
     let pricearr=price.split(",");
   
     products=products.filter(ele=>{
       
         let find=pricearr.find(element => {
            let arr=element.split("-");
            let lowprice= +arr[0];
            let highprice=+arr[1];
           
            if(ele.price>=lowprice && ele.price<=highprice) return true;
            else return false;
          });
         
          return find;
     })
   
   }

   if(sort=="popularity")
   {
     products.sort((p1,p2)=>p1.popularity-p2.popularity);

   }
   if(sort=="lowtohigh")
      products.sort((p1,p2)=>(+p1.price)-(+p2.price));
      if(sort=="hightolow")
      products.sort((p1,p2)=>(+p2.price)-(+p1.price));


  let p=+page;
  let size=5;
  let firstIndex=(p-1)*size;
  let lastIndex=firstIndex+(size-1);
  lastIndex=products.length>lastIndex?lastIndex:products.length-1;
  let pageProducts=products.filter((ele,index)=>index>=firstIndex && index<=lastIndex);
  let totalItems=products.length;
  let obj={
    firstIndex:totalItems?firstIndex+1:0,
    lastIndex:totalItems?lastIndex+1:0,
    page:p,
    totalItems:totalItems,
    items:pageProducts,
    totalPageItems:size,

  }
  res.send(obj);

});
app.get("/productForUser/:id",(req,res)=>{
  let id=req.params.id;
  let product=mobiles.find(ele=>ele.id == id);
  res.send(product);
});
app.get("/pincode/:pincode/:productId",function(req,res){
  let {pincode,productId}=req.params;
     let pincodeMobiles=pincodes.find(ele=>ele.pincode == pincode);
     if(pincodeMobiles){
       let mobile=pincodeMobiles.mobileList.find(ele=>ele.id == productId);
       if(mobile)
       {
          res.send(mobile);
       }
       else
       {
         res.status(400).send("This product delivery not available in this area");
       }
     }
     else
     {
      res.status(400).send("Cannot be delivered to this region");
     }
});
app.get("/reviews/:productId",function(req,res){
 
  let productId=req.params.productId;
   let review=reviews.find(ele=>ele.mobileId == productId);
   if(review)
   {
    let logslist=[];
    logslist.push(`"url":"/reviews/${productId}"`);
    logslist.push(`queryparams:{}`);
    logslist.push(`status:200`);
    logsReport.push(logslist);
     res.send(review.ratings);
   }
   else
   {
     res.status(400).send("There is no review");
   }
});

