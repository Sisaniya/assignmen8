let users=[{id:1,fname:"Rohit",lname:"Kumar",gender:"Male",email:"admin@test.com",password:"admin123",role:"admin"},
{id:2,fname:"Vishal",lname:"Kumar",gender:"Male",email:"user@test.com",password:"user123",role:"user"}];
let usersOrders=[{
    "userId": "2",
    "orders": [
        {
            "id": "M2",
            "category": "Mobiles",
            "brand": "Mi",
            "name": "Redmi 8 (Ruby Red, 64 GB)",
            "img": "https://i.ibb.co/4W96nDZ/mi-redmi-8-mzb8251in-original-imafhyacmxaefxgw.jpg",
            "rating": 4.5,
            "ratingDesc": "95348 Rating & 9727 Reviews",
            "details": [
                "4 GB RAM | 64 GB ROM | Expandable Upto 512 GB",
                "15.8 cm (6.22 inch) HD+ Display",
                "12MP + 2MP | 8MP Front Camera",
                "5000 mAh Battery",
                "Qualcomm Snapdragon 439 Processor",
                "Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories"
            ],
            "price": 7999,
            "assured": true,
            "prevPrice": 10999,
            "discount": 27,
            "EMI": "No Cost EMI",
            "exchange": "Upto 7700 on Exchange",
            "ram": 4,
            "popularity": 76070,
            "offers": [
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "10% Cashback* on HDFC Bank Debit Cards"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "5% Unlimited Cashback on Flipkart Axis Bank Credit Card"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "5% Instant Discount on EMI with ICICI Bank Credit and Debit Cards"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "Extra 5% off* with Axis Bank Buzz Credit Card"
                },
                {
                    "img": "https://i.ibb.co/PgZY1JF/f3bae257-60c1-4ef5-960a-1d8170ea7a42.png",
                    "type": "Bank Offer",
                    "detail": "Get upto ₹10800 off on exchange"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "Special Price"
                },
                {
                    "img": "https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",
                    "type": "Bank Offer",
                    "detail": "No Cost EMI"
                }
            ],
            "qty": 1,
            "date": "Tue Apr 20 2021"
        },
        {
            "id": "M25",
            "offers": [
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "10% Cashback* on HDFC Bank Debit Cards"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "5% Unlimited Cashback on Flipkart Axis Bank Credit Card"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "5% Instant Discount on EMI with ICICI Bank Credit and Debit Cards"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "Extra 5% off* with Axis Bank Buzz Credit Card"
                },
                {
                    "img": "https://i.ibb.co/PgZY1JF/f3bae257-60c1-4ef5-960a-1d8170ea7a42.png",
                    "type": "Bank Offer",
                    "detail": "Get upto ₹10800 off on exchange"
                },
                {
                    "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                    "type": "Bank Offer",
                    "detail": "Special Price"
                },
                {
                    "img": "https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",
                    "type": "Bank Offer",
                    "detail": "No Cost EMI"
                }
            ],
            "category": "Mobiles",
            "brand": "Samsung",
            "name": "Samsung Galaxy M10S (Metallic Blue, 32 GB)",
            "img": "https://i.ibb.co/bLShFsJ/samsung-galaxy-m10s-sm-m107fmbdins-original-imafh8wsfsghgwav.jpg",
            "rating": "4.4",
            "ratingDesc": "83,156 Ratings & 4,440 Reviews",
            "details": [
                "4 GB RAM | 32 GB ROM | Expandable Upto 512 GB",
                "15.8 cm (6.22 inch) HD+ Display",
                "Qualcomm Snapdragon 439 Processor",
                "12MP Rear Camera | 8MP Front Camera",
                "5000 mAh Battery",
                "Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories"
            ],
            "price": 10999,
            "prevPrice": 14999,
            "discount": 26,
            "emi": "No Cost EMI",
            "assured": true,
            "exchange": "Upto ₹8,350 Off on Exchange",
            "ram": 4,
            "popularity": 50597,
            "qty": 3,
            "date": "Tue Apr 20 2021"
        }
    ]
}];
let wishList=[{userId:2,
    items:[
    {
        "id": "M38",
        "offers": [
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "10% Cashback* on HDFC Bank Debit Cards"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "5% Unlimited Cashback on Flipkart Axis Bank Credit Card"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "5% Instant Discount on EMI with ICICI Bank Credit and Debit Cards"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "Extra 5% off* with Axis Bank Buzz Credit Card"
            },
            {
                "img": "https://i.ibb.co/PgZY1JF/f3bae257-60c1-4ef5-960a-1d8170ea7a42.png",
                "type": "Bank Offer",
                "detail": "Get upto ₹10800 off on exchange"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "Special Price"
            },
            {
                "img": "https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",
                "type": "Bank Offer",
                "detail": "No Cost EMI"
            }
        ],
        "category": "Mobiles",
        "brand": "OPPO",
        "name": "OPPO F11 (Fluorite Purple, 128 GB)",
        "img": "https://i.ibb.co/nmKVgN3/oppo-f11-cph1911-original-imafght4bbcxzmgc.jpg",
        "rating": "4.4",
        "ratingDesc": "50,597 Ratings & 5,185 Reviews",
        "details": [
            "4 GB RAM | 128 GB ROM | Expandable Upto 256 GB",
            "16.59 cm (6.53 inch) Display",
            "48MP + 5MP | 16MP Front Camera",
            "4020 mAh Battery",
            "MTK MT6771V (P70) Processor",
            "Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories"
        ],
        "price": 18999,
        "prevPrice": 20999,
        "discount": 9,
        "emi": "",
        "assured": false,
        "exchange": "",
        "ram": 4,
        "popularity": 50597
    },
    {
        "id": "M37",
        "offers": [
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "10% Cashback* on HDFC Bank Debit Cards"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "5% Unlimited Cashback on Flipkart Axis Bank Credit Card"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "5% Instant Discount on EMI with ICICI Bank Credit and Debit Cards"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "Extra 5% off* with Axis Bank Buzz Credit Card"
            },
            {
                "img": "https://i.ibb.co/PgZY1JF/f3bae257-60c1-4ef5-960a-1d8170ea7a42.png",
                "type": "Bank Offer",
                "detail": "Get upto ₹10800 off on exchange"
            },
            {
                "img": "https://i.ibb.co/zZCY6nY/c22c9fc4-0555-4460-8401-bf5c28d7ba29.png",
                "type": "Bank Offer",
                "detail": "Special Price"
            },
            {
                "img": "https://i.ibb.co/zVRyyTn/49f16fff-0a9d-48bf-a6e6-5980c9852f11.png",
                "type": "Bank Offer",
                "detail": "No Cost EMI"
            }
        ],
        "category": "Mobiles",
        "brand": "OPPO",
        "name": "OPPO F11 (Marble Green, 128 GB)",
        "img": "https://i.ibb.co/6s69BZy/oppo-f11-cph1911-original-imafght4kvkhnzzg.jpg",
        "rating": "4.4",
        "ratingDesc": "50,597 Ratings & 5,185 Reviews",
        "details": [
            "4 GB RAM | 128 GB ROM | Expandable Upto 256 GB",
            "16.59 cm (6.53 inch) Display",
            "48MP + 5MP | 16MP Front Camera",
            "4020 mAh Battery",
            "MTK MT6771V (P70) Processor",
            "Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories"
        ],
        "price": 21900,
        "prevPrice": 22999,
        "discount": 5,
        "emi": "No Cost EMI",
        "assured": false,
        "exchange": "",
        "ram": 4,
        "popularity": 50597
    }
]}];
let searchHistory=[{q:"Mi",date:"25/5/2021"}];
let logsReport=[];
let mostAdded=[ { id: 1, prodId: 'M1', count: 2 },
    { id: 2, prodId: 'M2', count: 1 },
    { id: 4, prodId: 'M37', count: 2 },
  { id: 5, prodId: 'M45', count: 2 }
  ];
let mostFavorite=[{ id: 1, prodId: 'M1', count: 1 },
{ id: 2, prodId: 'M33', count: 1 },
{ id: 3, prodId: 'M46', count: 1 }];
let mostSearch=[
    { id: '1', prodId: 'M1', count: '1' },
    { id: '2', prodId: 'M2', count: '1' },
    { id: '3', prodId: 'M3', count: '1' },
    { id: '4', prodId: 'M4', count: '1' },
    
  ];
module.exports={users:users,
    mostAdded:mostAdded,
    mostFavorite:mostFavorite,
    mostSearch:mostSearch,
    logsReport:logsReport,
    usersOrders:usersOrders,wishList:wishList,searchHistory:searchHistory};