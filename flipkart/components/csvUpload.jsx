import axios from "axios";
import React,{Component} from "react";
import FileDownload from "js-file-download";
import http from "../services/httpServices";
class CsvUpload extends Component 
{
    state={
        show:0,
        file:null,
    }

    async componentDidMount() {
        
    }
    handelChange=(e)=>{
       
        console.log(e.target.files);
        let s={...this.state};
        s.file=e.target.files[0];
        if(s.error)s.error="";
        this.setState(s);
    }
  
   
    async uploadFile(formData)
    { try
        {
         let response=await axios.post("https://aqueous-dawn-47322.herokuapp.com/csvUpload",formData);
         
                window.alert(JSON.stringify(response.data));
        }
        catch(ex)
        {
            if(ex.response && ex.response.status == 400)
            {
             this.setState({error:ex.response.data});
            }
        }
         
    }
    handelSubmit=(e)=>{
        e.preventDefault();
        e.stopPropagation();
        let s={...this.state};
           var formData=new FormData();
           
         formData.append("myfile",s.file);
         formData.append("name","rohit");
         console.log(formData.getAll("myfile"),formData.getAll("name"),FormData);
      this.uploadFile(formData);

    }
    
    render() {
        let {show,msg="",file}=this.state;
        return(
            <React.Fragment>
                <div className="row mt-3   justify-content-center">
                    <div className="col-12 text-center mb-2">
                        <span style={{fontSize:"26px",fontWeight:"500"}}>Import CSV File!</span>
                    </div>
                    <div className="col-6 text-center">
                       
                        <input
                        className=""
                        type="file"
                        name="file"
                        
                        onChange={this.handelChange}
                         />
                        
                      
                      
                        <button className="btn  btn-primary mt-2 px-3" disabled={!file} onClick={this.handelSubmit}>
                            Upload now!
                        </button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default CsvUpload;