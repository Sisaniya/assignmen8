import React,{Component} from "react";
class UserQNA extends Component 
{
    state={}
    render() {
        return (
            <React.Fragment>
                
                 <div className="row ">
                     <div className="col-12  mt-4">
                         <p style={{fontSize:"18px",fontWeight:"500"}} 
                         className=" fontstyle">FAQs</p>
                     </div>
                     <div className="col-12">
                     <div className="fontstyle mb-2" style={{fontSize:"12px",fontWeight:"600"}}>
                         <span>What happens when I update my email address (or mobile number)?</span>
                     </div>
                     <div className="fontstyle" style={{fontSize:"12px",marginTop:"15px"}}>
                        <p>Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</p>
                     </div>
                     <div className="fontstyle mb-2" style={{fontSize:"12px",fontWeight:"600",marginTop:"15px"}}>
                         <p>When will my Flipkart account be updated with the new email address (or mobile number)?</p>
                     </div>
                     <div className="fontstyle" style={{fontSize:"12px",marginTop:"15px"}}>
                       <p>It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</p>
                     </div>
                     <div className="fontstyle mb-2" style={{fontSize:"12px",fontWeight:"600",marginTop:"15px"}}>
                         <p>What happens to my existing Flipkart account when I update my email address (or mobile number)?</p>
                     </div>
                     <div className="fontstyle" style={{fontSize:"12px",marginTop:"15px"}}>
                     <p>Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</p>
                     </div>
                     <div className="fontstyle mb-2" style={{fontSize:"12px",fontWeight:"600",marginTop:"15px"}}>
                         <p>Does my Seller account get affected when I update my email address?</p>
                     </div>
                     <div className="fontstyle" style={{fontSize:"12px",marginTop:"15px"}}>
                       <p>Flipkart has a 'single sign-on' policy. Any changes will reflect in your Seller account also.</p>
                     </div>
                     </div>
                 </div>
                 <div className="row ">
                     <div className="col-12">
                         <img className="d-block w-100" src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/myProfileFooter_0cedbe.png"/>
                     </div>
                 </div>
            </React.Fragment>
        )
    }
}
export default UserQNA;