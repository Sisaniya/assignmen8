import React,{Component} from "react";
import {Link} from "react-router-dom";
import AdminNavBar from "./adminNavbar";
import FormUpload from "./formUpload";
import QNA from "./qna";
import queryString from "query-string";
import axios from "axios";
import FileDownload from "js-file-download";
class ReportLogs extends Component
{
    state={
       data:"",
       str:"",
     
    }
    componentDidMount() {
          let open=localStorage.getItem("open");
          localStorage.setItem("open","");
    }
    componentDidUpdate(prevProps,preState) {
        let open=localStorage.getItem("open");
           if(open)
           {
               let s={...this.state};
               s.data="";
               this.setState(s);
               localStorage.setItem("open","");
           }
    }
    async fetchProductReport() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/downloadProductsReport",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:""});
    }
    getProductReport=()=>{
        this.fetchProductReport();
    }
    async fetchSearchReport() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/downloadSearchReport",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:""});
    }
    getSearchReport=()=>{
        this.fetchSearchReport();
    }
    async fetchFavoriteReport() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/downloadFavoriteReport",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:""});
    }
    getFavoriteReport=()=>{
        this.fetchFavoriteReport();
    }
    async fetchLogsReport() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/downloadLogsReport",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data,str:"logs.csv"});
    }
    getLogsReport=()=>{
        this.fetchLogsReport();
    }
    downloadReportData=()=>{
        let {data,str}=this.state;
        if(str)
        {
            FileDownload(data,str);
        }
        else
        {
            FileDownload(data,'report.csv');
        }
      
      
    }
    render() {
      let {data}=this.state;
      console.log(data);
        return (
            <React.Fragment>
                <div className="row backgcolor mx-2">
                    <div className="col-lg-3 ml-3 my-2">
                        <AdminNavBar/>
                    </div>
                    <div className="col-lg-8 normalbgcolor pt-3 ml-4 mt-2">
                        {data?(
                        <button onClick={()=>this.downloadReportData()}  className="btn btn-primary btn-sm mt-2 ml-4">Click To Download CSV</button>
                        ):(
                            <div className="row mx-5 pt-3 text-center">
                                <div className="col-6 ml-3 fontStyle" style={{fontSize:"13px",color:"#212121",fontWeight:"500"}}>
                                Most Added Product Report
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getProductReport()} style={{fontSize:"10px"}} className="btn btn-danger btn-md mb-1 pt-2 ">Download Report</button>
                                </div>
                                <div className="col-6 ml-3" style={{fontSize:"13px",color:"#212121",fontWeight:"500"}}>
                                Most Searched Product Report
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getSearchReport()} style={{fontSize:"10px"}} className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                                <div className="col-6 ml-3" style={{fontSize:"13px",color:"#212121",fontWeight:"500"}}>
                                Most Favorite Product Report
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getFavoriteReport()} style={{fontSize:"10px"}} className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                                <div className="col-6 ml-3" style={{fontSize:"13px",color:"#212121",fontWeight:"500"}}>
                                User Logs Report
                                </div>
                                <div className="col-4">
                                <button onClick={()=>this.getLogsReport()} style={{fontSize:"10px"}} className="btn btn-danger btn-md mb-1 pt-2">Download Report</button>
                                </div>
                            </div>
                            
                        )}
                      

                        <QNA/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default ReportLogs;

