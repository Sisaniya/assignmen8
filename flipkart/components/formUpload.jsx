import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import AdminNavBar from "./adminNavbar";
import QNA from "./qna";

class FormUpload extends Component
{
    state={
        product:{name:"",brand:"",img:"",rating:"",ratingDesc:"",details:[],
            price:"",category:"",assured:null,discount:"",EMI:"",exchange:"",ram:"",offers:"",popularity:"",prevPrice:""},
       showDF:false,
       categoryList:["Mobiles","Laptops","Camaras"],
       brandList:["Mi","RealMe","Samsung","OPPO","Apple"],
        }
        async componentDidMount() {
            let user=localStorage.getItem("usertoken")
            let {id}=this.props;
            if(id)
            {
                console.log(user);
            let response=await axios.get(`https://aqueous-dawn-47322.herokuapp.com/product/${id}`,{headers: {Authorization: user}});
                let {data}=response;
                this.setState({product:data});
            }
           
        }
        addDetailField=()=>{
            let s={...this.state};
          s.product.details.push("");
            this.setState(s);
        }
        removeDetailField=(index)=>{
            let s={...this.state};
          s.product.details.splice(index,1);
          s.showDF=s.product.details.length?s.showDF:false;
            this.setState(s);
        }
        showDetailsField=()=>{
            let s={...this.state};
            s.showDF=true;
            s.product.details.push("");
            this.setState(s);
        }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
         input.name=="details"?s.product[input.name].splice(input.id,1,input.value)
        
         :s.product[input.name]=input.value;
         if(input.name=="assured")
         {
            s.product[input.name]=input.value=="Yes"?true:false;
         }
         this.setState(s);

    }
    async postData(obj) {
      try
      {
        let response=await httpServices.post("/product",obj);
           window.alert("Product added");
           console.log(response.data);
           
      }
      catch(ex)
      {
          console.log(ex.response);
      }
    }
    async putData(obj) {
        try
        {
            let {id}=this.props;
          let response=await httpServices.put(`/product/${id}`,obj);
             window.alert("Product modified");
             console.log(response.data);
             
             
        }
        catch(ex)
        {
            console.log(ex.response);
        }
      }
    handleSubmit=(e)=>{
        e.preventDefault();
           let {product}=this.state;
          console.log(product);
          let {id}=this.props;
          if(id)
          {
              this.putData(product);
          }
          else
          {
            this.postData(product);
          }
         
    }
    render() {
        let {prevPrice,name,brand,img,rating,ratingDesc,price,details,assured,discount,ram,offers,popularity,category}=this.state.product;
        let {showDF,categoryList,brandList}=this.state;
        let {id}=this.props;
        return (
            <React.Fragment>
                       <div className="container">
                          <div className="row mx-2 mt-3">
                              <div className="col-12 mt-4">
                          
                     
                      <span className="d-block mb-2" style={{fontSize:"16px",fontWeight:"bold"}}>Product Page</span>
                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Name</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="text"
                              name="name"
                              value={name}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Price</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="text"
                              name="price"
                              value={price}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">prevPrice</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="text"
                              name="prevPrice"
                              value={prevPrice}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>


                      

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Rating</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="number"
                              name="rating"
                              value={rating}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Rating Desc</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="text"
                              name="ratingDesc"
                              value={ratingDesc}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">RAM</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="text"
                              name="ram"
                              value={ram}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Discount</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="number"
                              name="discount"
                              value={discount}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Popularity</label>
                          <div className="col-9">
                              <input className="form-control"
                              type="number"
                              name="popularity"
                              value={popularity}
                              onChange={this.handleChange}
                              />
                          </div>
                       </div>

                      


                       <div className="form-group mb-3 row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Details</label>
                         {details.length || showDF?<React.Fragment> {details.map((ele,index)=>{
                             return (
                                 <React.Fragment>
                                     {index == 0?"": <div className="col-3"></div>}
                                   
                                     <div className="col-7 mb-1">
                                         <input className="form-control"
                                     type="text"
                                     id={index}
                                    
                                          name="details"
                                        value={ele}
                                        onChange={this.handleChange}
                                        />
                                    </div>
                                    <div className="col-2">
                                        <button  onClick={()=>this.removeDetailField(index)} className="btn btn-primary btn-md mr-2">-</button>
                                        <button onClick={()=>this.addDetailField()}  className="btn btn-primary btn-md">+</button>
                                    </div>
                                 </React.Fragment>
                             )
                         })}</React.Fragment>:(
                             <div className="col-7">
                                   <span class="_3wj6q3">
                                <div><button type="button" onClick={()=>this.showDetailsField()}>Add Detail</button>
                                </div></span>
                             </div>
                           
                            
                         )}   
                       </div>

                       <div className="form-group row mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-3 col-form-label">Image</label>
                          <div className="col-7">
                              <input className="form-control"
                              type="text"
                              name="img"
                              value={img}
                              onChange={this.handleChange}
                              />
                          </div>
                          <div className="col-2">
                              {img?( <img src={img} width="60" height="120"/>):""}               
                          </div>
                       </div>
                            
                       <div className="row my-2  mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-12 ">Assured</label>
                           <div className="col-3">
                          <div className="form-check-inline">
                              <input className="form-check-input"
                              type="radio"
                              name="assured"
                            value="Yes"
                              checked={assured === true}
                              onChange={this.handleChange}
                              />
                              <label className="form-check-label mr-2">Yes</label>
                          </div>
                          </div>
                          <div style={{color:"rgb(93, 95, 97)"}} className="col-3  mb-3">
                          <div className="form-check-inline">
                              <input className="form-check-input"
                              type="radio"
                              name="assured"
                              value="No"
                              checked={assured === false}
                              onChange={this.handleChange}
                              />
                              <label className="form-check-label">No</label>
                          </div>
                          </div>
                       </div>
                      
                       <div className="row my-2  mb-3">
                           <label style={{color:"rgb(93, 95, 97)"}} className="col-12">Category:</label>
                           
                           {categoryList.map(ele=>{
                               return (
                                <div className="col-3">
                                <div className="form-check-inline">
                                <input className="form-check-input"
                                type="radio"
                                name="category"
                                value={ele}
                                checked={category == ele}
                                onChange={this.handleChange}
                                />
                                <label className="form-check-label mr-2">{ele}</label>
                                </div>
                            </div>
                               )
                           })}
                           
                       </div>
                    
                       <button onClick={this.handleSubmit} className="productSubmit">SAVE</button>


                               
                       </div>
                 </div>
                       </div>
            </React.Fragment>
        )
    }
}
export default FormUpload;

