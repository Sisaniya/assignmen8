import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "../services/httpServices";
import Input from "./input";
import ExtraInfo from "./extraInfo";
import {createBrowserHistory} from "history";
import axios from "axios";

class Products extends Component
{
    state={
      data:{},
      compareId:"",
      wishList:[],
      
    }
    async fetchData() {
        let {brand,category}=this.props.match.params;       
        let {q}=queryString.parse(this.props.location.search);
      let  mobilesList=["Mi","RealMe","Samsung","OPPO","Apple"];
      let str=this.props.location.search;
      if(q) {
        let regex=new RegExp(`^${q}$`,"i");
        let find=mobilesList.find(ele=>regex.test(ele));
        let options=queryString.parse(this.props.location.search);
        options.q=find;
        let searchstr=this.makeSearchString(options);
        str=`?${searchstr}`;
      }
       
     
      
        let response={};
        if(brand)
        {
            response=await http.get(`/products/${category}/${brand}${str}`);
        }
        else
        {
            response=await http.get(`/products/${category}${str}`);
        }
        let {data}=response;
        this.setState({data:data});
    }
     addToCompareList=(id)=>{
        let {items=[]}=this.state.data;
        let find=items.find(ele=>ele.id == id);
        this.props.manageCompareList(find);
     }
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let {items=[]}=this.state.data;
        let find=items.find(ele=>ele.id == input.value);
        if(input.checked)
        {
         
          this.props.addToCompareList(find);
        }
        else
        {
            this.props.removeFromCompareList(find);
        }
    }
     componentDidMount() {
        this.fetchData();
    }

    handelSearch=(options)=>{
       options.page=1;
       this.callURL(options);
    }
    getSortProducts=(s)=>{
        let options=queryString.parse(this.props.location.search);
        options.sort=s;
        options.page=1;
        this.callURL(options);
    }
    callURL=(options)=>{
        let {category,brand}=this.props.match.params;
       let searchString=this.makeSearchString(options);
       let url="";
       if(brand) url=`home/${category}/${brand}`;
       else url=`home/${category}`;
        this.props.history.push({
            pathName:url,
            search:searchString,
        })
         console.log("enter in call url",searchString);
    }
    makeSearchString=(options)=>{
        let {assured,ram,rating,price,page,sort,q}=options;
        let str="";
        str=this.addToString(str,"page",page);
        str=this.addToString(str,"assured",assured);
        str=this.addToString(str,"ram",ram);
        str=this.addToString(str,"rating",rating);
        str=this.addToString(str,"price",price);
        str=this.addToString(str,"sort",sort);
        str=this.addToString(str,"q",q);
        return str;
    }
    addToString=(str,name,value)=>{
        let s=value?str?`${str}&${name}=${value}`:`${name}=${value}`:str;
        return s;
    }
    buyProduct=(id)=>{
        let {items=[]}=this.state.data;
        if(items.length)
        {
            let {brand}=this.props.match.params;
          let {items=[]}=this.state.data;
        let find=items.find(ele=>ele.id == id);

          this.props.addToViewList(find);
        this.props.history.push(`/home/Mobile/${brand}/${id}`);
        }
         
       
      
    }
    componentDidUpdate(prevProps,prevState) {
        if(prevProps != this.props)
        this.fetchData();
    }
    async putToList(obj) {
        try
         {
            let response=await http.put("/wishlist",obj);
           
            window.alert("Wishlist changed");
            this.getWishlist();
         
        }
        catch(ex)
         {
               console.log(ex.response);
        }
    }
    async fetchWishlistData() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/wishlist",{headers: {Authorization: user}});
            let {data}=response;
            console.log(data);
            
            this.setState({wishList:data});
    }
    getWishlist=()=>{
        this.fetchWishlistData();
    }
    addToWishList=(item,e)=>{
        console.log(e);
       
        console.log(e);
        let history=createBrowserHistory();
        let pathname=history.location.pathname+history.location.search;
        let options=queryString.parse(this.props.location.search);
        let id=localStorage.getItem("id");
        if(id)
        {
             let obj={userId:id,item:item};
             this.putToList(obj);
             
        }
        else
        {
           window.alert("Login to continue");
           this.callURL(options);
        }
      
    }
    
    render() {
        let {category,brand,}=this.props.match.params;
     
        let {data,wishList=[]}=this.state;
        console.log(wishList);
        let {compareList}=this.props;
        let {items=[],page=1,lastIndex,firstIndex,totalItems,totalPageItems}=data;
        console.log(data);
        let options=queryString.parse(this.props.location.search);
        let p=+page;
        let pages=Math.ceil(totalItems/totalPageItems);
        let {q,sort}=queryString.parse(this.props.location.search);
        let history=createBrowserHistory();
        let pathname=history.location.pathname;
       let completepath=pathname+history.location.search;

        let arr=[];
        for(let i=1;i<=pages;i++)
             arr.push(i);
        console.log(arr);
        return (
            <React.Fragment>
                {localStorage.getItem("id") ? wishList.length?"":this.getWishlist():""}
                <div className="row bg-white pt-4 mx-2 justify-content-center">
                    <div className="col-12 bg-white">
                    <div className="row bgcolor">
                   {category=="Mobiles"?(
                           <div className="col-lg-2">
                     
                           <Input options={options} handelSearch={this.handelSearch}/>
                  
                      </div>
                   ):(
                       <div className="col-2"></div>
                   )}
             
                   <div className="col-lg-9 ml-1 bg-white border">
                       <div className="row ml-1 mt-2 mb-4 " style={{fontSize:"12px",fontWeight:"500"}}>
                           <div className="col-12 fontstyle">
                           <span style={{margin:"0px 10px"}}><Link className="notActivePPath" to="/home">Home</Link></span>/
                       <span style={{margin:"0px 10px"}}> <Link className={(pathname==`/home/${category}` && !q )?"activeButNotChange":"notActivePPath"}  to={`/home/${category}`}>{category}</Link></span>/
                       <span style={{margin:"0px 10px"}}>{brand && brand}</span><br/>
                           </div>
                       </div>
                      
                
                       <span style={{fontSize:"14px",fontWeight:"500"}} className="mt-2 normalColor d-block">{brand} {category}</span>
                     
                       {category == "Mobiles"?(
                           <React.Fragment>
                              <div className="row border-bottom mb-2 ">
                              <div style={{fontSize:"14px"}} className="col-lg-2"> <strong>Sort By</strong>  </div>
                               <div style={{fontSize:"14px"}} className="col-lg-2" onClick={()=>this.getSortProducts("popularity")}>
                                   <Link className={sort=="popularity"?"activePPath":"normalTextColor"}>Popularity</Link>
                                   </div>  
                               <div style={{fontSize:"14px"}} className="col-lg-2 normalColor" onClick={()=>this.getSortProducts("hightolow")}>
                               <Link className={sort=="hightolow"?"activePPath":"normalTextColor"}> Price High to Low</Link>
                                  </div>
                               <div style={{fontSize:"14px"}} className="col-lg-2 normalColor" onClick={()=>this.getSortProducts("lowtohigh")}>
                               <Link className={sort=="lowtohigh"?"activePPath":"normalTextColor"}>Price Low to High</Link>
                                  </div>       
                          </div>
                       
                          {items.map((ele,index)=>{
                              let {id,name,brand,img,rating,ratingDesc,details,
                               price,assured,discount,EMI,exchange,ram,offers,popularity,prevPrice}=ele;
                              return (
                                  <React.Fragment>
                               <div className="row  mb-4">
                               <div className="col-lg-3" >
                                  <div className="row ml-2">
                                      <div className="col-6">
                                          <Link onClick={()=>this.buyProduct(id)}>
                                          <img  src={img} width="100" height="200"/>
                                          </Link>
                                     
                                      </div>
                                      <div className="col-lg-6 text-center">
                                       <Link to={completepath} className="text-right text-muted  notActivePPath">
                                           <i onClick={()=>this.addToWishList(ele)} style={{fontSize:"15px"}}
                                           className={wishList.findIndex(ele=>ele.id==id)>=0?"fas fa-heart text-danger":"fas fa-heart text-muted"}>
                                               </i></Link>
                                     </div>
                                      <div className="col-12">
                                      
                                      </div>
                                  </div>
                                  <div className="form-check" style={{display:"block"}}>
                                      <input
                                      
                                      className="form-check-input" checked={compareList.findIndex(ele=>ele.id==id)>=0?true:false} 
                                      onChange={this.handelChange} value={id} name="compareId" type="checkbox"/>
                                      <span style={{fontSize:"14px",fontWeight:"490",color:"black",cursor:"pointer"}} onClick={()=>this.addToCompareList(id)} className="form-check-label ">
                                          Add to Compare
                                          </span>
                                  </div>
                                 
                               </div>
                              
                               <div className="col-lg-5">
                                   <span className="fontStyle" style={{fontSize:"16px"}}>{name}</span><br/>
                                   <span className="_3LWZlK">{rating} <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                                               className="_1wB99o"/></span>  
                                   <span className="text-muted">{ratingDesc}</span><br/>
                                   <ul className="productsDescription">
                                        {details.map(element=><li>{element}</li>)}
                                   </ul>
                               </div>
                               <div className="col-lg-3">
                                     <span className="priceStyle" style={{fontSize:"28px"}}> {price}</span>
                                  
                                   {assured == true?<img width="70"  src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png" />:""}<br/>
                                   <span className="text-muted discountStyle ml-2 mr-1">{prevPrice} </span><span>{discount}%</span><br/>
                                      {exchange && <span className="ml-2 mr-1">{exchange}</span>}
                               </div>
                           </div>
                           <hr className="bg-light mb-1"/> 
                           </React.Fragment>
                              )
                          })}
                           <div className="row ml-2 my-2 fontStyle">
                      <div  style={{fontSize:"16px",fontWeight:"495"}} className="col-2 p-1">
                          Page {p} of {arr.length}
                      </div>
                      <div style={{fontSize:"16px",fontWeight:"495"}} className="col-8 p-1 text-center">
                     
                       {arr.map(ele=>{
                           if(ele==p)
                           {
                               return <span onClick={()=>this.handelPage(ele)} style={{display:"inline-block",cursor:"pointer"}} className="bg-primary text-light m-1 pageIndexStyle">{ele}</span>
                           }
                           else {
                               return <span onClick={()=>this.handelPage(ele)} style={{display:"inline-block",cursor:"pointer"}} className="mr-2 m-1">{ele}</span>
                           }
                       })}
                       {p<arr.length && <span style={{display:"inline-block",cursor:"pointer"}}
                        onClick={()=>this.handelPageInc(1)} className="text-primary notActivePPath">Next</span>}
                      </div>
                  </div>
                  </React.Fragment>
                       ):(
                        <div className="row my-2">
                        <div className="col-12 my-5">
                            <img width="250" height="200" className="d-block mx-auto rounded"
                            src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90"/>
                        </div>
                        <div className="col-12 my-2 text-center">
                            <span className="commingSoonStyle">Coming Soon</span>
                        </div>
                    </div>
                       )}
                      
                   </div>
                   
               </div>
              
                    </div>
                </div>
            
               <ExtraInfo/>
            </React.Fragment>
        )
    }
    handelPageInc=(inc)=>{
        let options=queryString.parse(this.props.location.search);
        let page=options.page;
        options.page= +page +inc;
        this.callURL(options);
    }
    handelPage=(p)=>{
        let options=queryString.parse(this.props.location.search);
       
        options.page= p;
        this.callURL(options);
    }
}
export default Products;