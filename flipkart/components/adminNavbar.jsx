import React,{Component} from "react";
import {Link} from "react-router-dom";
import { createBrowserHistory } from "history";


class AdminNavBar extends Component
{
    state={
        active:"",
    }
   goToLink=(str)=>{
       window.location=str;
   }
   goToReport=()=>{
       localStorage.setItem("open","open");
   }
    render() {
    
        const history = createBrowserHistory();
      let  pathname=history.location.pathname+history.location.search;
      console.log(pathname);
        return (
            <React.Fragment>
                 <div className="row  p-2 py-3 normalbgcolor    navbar">
                       <div className="col-lg-12">
                          <img className="mr-2" width="50" height="50"
                          src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/profile-pic-female_3c17ab.svg"/> 
                         <strong style={{fontSize:"14px"}}  >Hello Admin</strong>
                       </div>
                      
                    </div>
                    <div className="row my-1 normalbgcolor  my-3">
                    <div className="col-12">
                        <div className="row p-2 mt-3 mb-1">
                            <div className="col-2 text-center">
                            <img className="_3GARO3" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIyMiIgdmlld0JveD0iMCAwIDIzIDIyIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC05LjY5NCAtOSkiPjxlbGxpcHNlIGN4PSIyMC41NTciIGN5PSIyMCIgcng9IjIwLjU1NyIgcnk9IjIwIi8+PHBhdGggZD0iTTcgNmgyOHYyOEg3eiIvPjxwYXRoIGZpbGw9IiMyODc0RjAiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTMxLjUgMjd2MS4xNjdhMi4zNCAyLjM0IDAgMCAxLTIuMzMzIDIuMzMzSDEyLjgzM2EyLjMzMyAyLjMzMyAwIDAgMS0yLjMzMy0yLjMzM1YxMS44MzNBMi4zMzMgMi4zMzMgMCAwIDEgMTIuODMzIDkuNWgxNi4zMzRhMi4zNCAyLjM0IDAgMCAxIDIuMzMzIDIuMzMzVjEzSDIxYTIuMzMzIDIuMzMzIDAgMCAwLTIuMzMzIDIuMzMzdjkuMzM0QTIuMzMzIDIuMzMzIDAgMCAwIDIxIDI3aDEwLjV6TTIxIDI0LjY2N2gxMS42Njd2LTkuMzM0SDIxdjkuMzM0em00LjY2Ny0yLjkxN2MtLjk3IDAtMS43NS0uNzgyLTEuNzUtMS43NXMuNzgtMS43NSAxLjc1LTEuNzVjLjk2OCAwIDEuNzUuNzgyIDEuNzUgMS43NXMtLjc4MiAxLjc1LTEuNzUgMS43NXoiLz48L2c+PC9zdmc+"/>
                            </div>
                            <div className="col-10">
                          

                                <Link style={{fontSize:"18px"}} className="topLink d-2 d-block">PRODUCTS</Link>
                            
                            </div>
                        </div>
                       <Link className={pathname=="/admin/products/upload?q=form"?"text-primary active ":"notActive"}  to="/admin/products/upload?q=form">
                       <div className={pathname=="/admin/products/upload?q=form"?"text-primary row px-3 py-2 activeElement ":"row px-3 py-2 "}>
                           
                           <div className="col-2 text-center">
                             
                           </div>
                           <div style={{fontSize:"14px"}}  className="col-10 fontStyle">
                           <Link  className={pathname=="/admin/products/upload?q=form"?"text-primary active ":"notActive"}  to="/admin/products/upload?q=form"  >Form Upload</Link>
                              
                             
                           </div>
                       </div>
                       </Link>
                       <Link className={pathname=="/admin/products/upload?q=csv"?"text-primary active ":"notActive"}  to="/admin/products/upload?q=csv">
                       <div className={pathname=="/admin/products/upload?q=csv"?"text-primary row px-3 py-2 activeElement ":"row px-3 py-2 "}>
                            <div className="col-2 text-center">
                              
                            </div>
                            <div style={{fontSize:"14px"}} className="col-10 fontStyle">
                            <Link className={pathname=="/admin/products/upload?q=csv"?"text-primary active ":"notActive"}  to="/admin/products/upload?q=csv">CSV Upload</Link>
                          
                            </div>
                        </div>
                       </Link>
                       <Link className={pathname=="/admin/products/download"?"text-primary active":"notActive"} to="/admin/products/download">
                       <div className={pathname=="/admin/products/download"?"text-primary row px-3 py-2 activeElement ":"row px-3 py-2 "}>
                            <div className="col-2 text-center">
                              
                            </div>
                            <div style={{fontSize:"14px"}} className="col-10 fontStyle">
                            <Link className={pathname=="/admin/products/download"?"text-primary active":"notActive"} to="/admin/products/download" >Download</Link>
                           
                            </div>
                        </div>
                      </Link >
                       <Link className={pathname=="/admin/products/view"?"text-primary active":"notActive"}  onClick={()=>this.goToReport()}   to="/admin/products/view">
                       <div className={pathname=="/admin/products/view"?"text-primary row px-3 py-2 activeElement ":"row px-3 py-2"}>
                            <div className="col-2 text-center">
                              
                              </div>
                               <div style={{fontSize:"14px"}} className="col-10 fontStyle">
                                <Link className={pathname=="/admin/products/view"?"text-primary active":"notActive"}
                                to="/admin/products/view"
                                onClick={()=>this.goToReport()}  >Edit</Link>
                           
                                </div>
                                 </div>
                                 </Link>
                       
                        <hr className="bg-light my-1"/>
                   
                        <div className="row px-2 pt-3 pb-2">
                            <div className="col-2 text-center">
                            <img className="_3GARO3" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMyIgaGVpZ2h0PSIxOSIgdmlld0JveD0iMCAwIDIzIDE5Ij48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGZpbGw9IiMyODc0RjAiIGZpbGwtcnVsZT0ibm9uemVybyIgZD0iTTIwLjUgMi43NWgtOUw5LjI1LjVIMi41QTIuMjQ3IDIuMjQ3IDAgMCAwIC4yNiAyLjc1bC0uMDEgMTMuNUEyLjI1NyAyLjI1NyAwIDAgMCAyLjUgMTguNWgxOGEyLjI1NyAyLjI1NyAwIDAgMCAyLjI1LTIuMjVWNWEyLjI1NyAyLjI1NyAwIDAgMC0yLjI1LTIuMjV6bS01LjYyNSAzLjM3NWEyLjI1NyAyLjI1NyAwIDAgMSAyLjI1IDIuMjUgMi4yNTcgMi4yNTcgMCAwIDEtMi4yNSAyLjI1IDIuMjU3IDIuMjU3IDAgMCAxLTIuMjUtMi4yNSAyLjI1NyAyLjI1NyAwIDAgMSAyLjI1LTIuMjV6bTQuNSA5aC05VjE0YzAtMS40OTYgMy4wMDQtMi4yNSA0LjUtMi4yNXM0LjUuNzU0IDQuNSAyLjI1djEuMTI1eiIvPjxwYXRoIGQ9Ik0tMi00aDI3djI3SC0yeiIvPjwvZz48L3N2Zz4="/>
                            </div>
                            <div className="col-10">
                            <Link className={pathname=="/admin/report/logs"?"text-primary active":"topLink"} 
                            onClick={()=>this.goToReport()} to="/admin/report/logs" >
                            <span style={{fontSize:"18px"}} className=" d-2 d-block ">REPORTS</span>
                            </Link>
                               
                            </div>
                        </div>
                        
                        <hr className="bg-light mt-1 mb-3"/>
                   
                        <div className="row px-2 pt-3 pb-2">
                            <div className="col-2 pt-1 text-center">
                            <i class="fa fa-power-off logoutIconStyle text-primary" ></i>
                            </div>
                            <div className="col-10 pb-2">
                                <Link to="/logout" style={{fontSize:"18px"}} className="topLink">Logout</Link>
                                
                            </div>
                        </div>
                    </div>
                    </div>

            </React.Fragment>
        )
    }
}
export default AdminNavBar;