import axios from "axios";
import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
import ExtraInfo from "./extraInfo";
import {createBrowserHistory} from "history";
import QNA from "./qna";
import UserNavbar from "./userNavbar";
import UserQNA from "./userqna";
class UserProfile extends Component
{
    state={
        user:{},
        errors:{},
    }
    async componentDidMount() {
        try {
            let user=localStorage.getItem("usertoken")
            console.log(user);
            let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/user",{headers: {Authorization: user}});
            console.log(response.data);
            this.setState({user:response.data});
        }
        catch(ex) {
            console.log(ex.response);
        }
       
    }
    handleChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s.user[input.name]=input.value;
        s.errors={};
        this.setState(s);
    }
    async putData(obj) {
        try
        {
            let response=await httpServices.put(`/user`,obj);
            window.alert("Details updated successfully!");
        
        }
        catch(ex)
        {
            console.log(ex.response);
        }
      
          
    }
    handleSubmit=(e)=>{
        e.preventDefault(e);
        let {user}=this.state;
        let errors=this.formValidation(user);
        console.log(user);
        if(this.isValid(errors))
        { 
             this.putData(user);
        }
        else {
            let s={...this.state};
            s.errors=errors;
            this.setState(s);
        }
        
    }
    isValid=(errors)=>{
        let keys=Object.keys(errors);
        let count=keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return count == 0;

}
    formValidation=(user)=>{
        let errors={};
        errors.fname=!user.fname?"First Name is Required!":"";
        errors.email=!user.email?"Email is required!":"";
        errors.gender=!user.gender?"Select gander!":"";
        return errors;
    }

    render() {
        let {fname,lname,gender,email}=this.state.user;
        let {errors}=this.state;
        const history = createBrowserHistory();
        let  pathname=history.location.pathname;
        console.log(pathname);
        console.log(pathname);
        return (
            <React.Fragment>
                <div className="row backgcolor ">
                    <div className="col-12">

                  
            <div className="row backgcolor   mx-2">
                <div className="col-lg-3 mx-2 mt-1">
                   <UserNavbar/>

                </div>
               
                <div className="col-lg-8 normalbgcolor mx-2 mt-2 pt-3 " >
                   
                <div className="row pl-4 my-2" style={{borderBottom:"2px solid #e0e0e0"}}>
                    <div className="col-12">
                    <span style={{fontSize:"18px",fontWeight:"500"}}
                     className="fontstyle mr-3">Personal Information</span>
                    <span style={{fontSize:"14px",fontWeight:"400"}} className="text-primary fontstyle"> Edit</span>
                    </div>

                    <div className="col-12">
                    <div className="form-group row  my-1">
                        <label style={{fontSize:"14px",fontWeight:"500"}} className="col-3 col-form-label fontstyle">First Name</label>
                        <div className="col-8">
                           <input
                           className="form-control"
                           name="fname"
                           value={fname}
                           onChange={this.handleChange}

                           />
                           <span className="text-danger">{errors.fname && errors.fname}</span>
                        </div>
                    </div>
                    </div>

                    <div className="col-12 mt-2">
                    <div className="form-group row my-1">
                        <label style={{fontSize:"14px",fontWeight:"500"}} className="col-3 col-form-label fontstyle">Last Name</label>
                        <div className="col-8">
                           <input
                           className="form-control"
                           name="lname"
                           value={lname}
                           onChange={this.handleChange}
                           
                           />
                        </div>
                    </div>
                    </div>

                    <div className="col-12 mt-2">
                    <div className="form-group row my-3">
                        <label className="col-12 col-form-label fontstyle "
                        style={{fontSize:"18px",fontWeight:"500"}}
                        >Email</label>
                        <div className="col-9">
                           <input
                           type="text"
                           className="form-control"
                           name="email"
                           value={email}
                           onChange={this.handleChange}
                           
                           />
                           <span className="text-danger">{errors.email && errors.email}</span>
                        </div>
                    </div>
                    </div>

                    <div className="col-12 mt-4">
                    <div className="row my-3" >
                        <div className="col-12">
                           <span style={{fontSize:"14px"}} className="fontstyle">Gander?</span>
                        </div>
                        <div className="col-2  mt-2 mr-4">
                        <div className="form-check-inline">
                          <input className="form-check-input"
                          type="radio"
                          onChange={this.handleChange}
                          name="gender"
                          value="Male"
                          checked={gender == "Male"}
                          />
                          <label style={{color:"#878787",cursor:"not-allowed",fontSize:"16px"}} className="form-check-label">Male</label>
                         </div>
                        </div>
                        <div className="col-2 mt-2 mr-2">
                        <div className="form-check-inline">
                          <input className="form-check-input"
                          type="radio"
                          name="gender"
                          onChange={this.handleChange}
                          value="Female"
                          checked={gender == "Female"}
                          />
                           <label style={{color:"#878787",cursor:"not-allowed",fontSize:"16px"}} className="form-check-label">Female</label>
                         </div>

                        </div>
                       
                    </div>
                    </div>

                    <div className="col-12 my-4 pb-4" >
                    <div className="text-center my-2 usereditbutton" onClick={this.handleSubmit}>Edit</div>
                    </div>
                </div>
                   
                   
                   
           
              
                  <UserQNA/>
                
                </div>
            </div>
            </div>
                </div>
            <ExtraInfo/>
            </React.Fragment>
        )
    }
}
export default UserProfile;