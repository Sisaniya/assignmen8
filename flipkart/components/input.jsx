import React,{Component} from "react";
class Input extends Component
{
    state={
     
      ramList:[{value:">=6",name:"6 GB AND MORE"},{value:"<=4",name:"4 GB"},{value:"<=3 GB",name:"3 GB"},{value:"<=2",name:"2 GB"}],
      ratingList:["4","3","2","1"],
       
        priceList:[{name:"0-5,000",value:"0-5000"},
                 {name:"5,000-10,000",value:"5000-10000"},
                  {name:"10,000-20,000",value:"10000-20000"},
                {name:"MORE THAN 20,000",value:"20000-999999"}],
      
        }
  
getRadio=(arr,value,name,label)=>{
          
 
return (
  <React.Fragment>
     
   <label className="font-weight-bold  p-1 d-block bg-light border form-label">{label}</label>
   
{arr.map(ele=>{
    return (
       
        <div className="form-check border py-1" key={ele}>
        <input className="form-check-input"
        type="radio"
         name={name}
         value={ele}
         checked={value == ele}
         onChange={this.handelChange}
         
        />
       <label  className="form-check-label">{ele}</label>
       </div>
       
    )
})}
    
    </React.Fragment>
)
}

getDD=(arr,value,name,top)=>{
    
    return (
        <div className="form-group">
            <label className="font-weight-bold d-block bg-light border form-label p-1">Order By</label>
            <select className="form-control"
            name={name}
            value={value}
            onChange={this.handelChange}
            >
                <option value="">{top}</option>
                {arr.map(ele=><option>{ele}</option>)}
            </select>
        </div>
    )
}
handelChange=(e)=>{
   let {currentTarget:input}=e;
   let s={...this.props};
input.name=="assured"?s.options[input.name]=input.checked
:s.options[input.name]=this.getCBs(s.options[input.name],input.checked,input.value)

console.log(input.value,s.options);

this.props.handelSearch(s.options);


}
getCB=(arr,value,name,label,)=>{
      
  let values=value?value.split(","):[];
return (
<React.Fragment>
<label className="">{label}</label>
{arr.map(ele=>{
  return (
     
      <div className="form-check  py-1"className="form-check pb-1 " style={{fontSize:"18px"}} key={ele}>
      <input className="form-check-input"
      type="checkbox"
       name={name}
       value={ele.value}
       checked={values.findIndex(b=>b==ele.value)>=0}
       onChange={this.handelChange}
       
      />
     <label  className="form-check-label" style={{fontSize:"13px"}}>{ele.name}</label>
     </div>
     
  )
})}
  
  </React.Fragment>
)
}
getCBRating=(arr,value,name,label,)=>{
      
  let values=value?value.split(","):[];
return (
<React.Fragment>
<label className="   ">{label}</label>
{arr.map(ele=>{
  return (
     
      <div className="form-check pb-1 " style={{fontSize:"18px"}} key={ele}>
      <input className="form-check-input"
      type="checkbox"
       name={name}
       value={ele}
       checked={values.findIndex(b=>b==ele)>=0}
       onChange={this.handelChange}
       
      />
     <label className="form-check-label" style={{fontSize:"13px"}}>{ele}  <i className="fas fa-star"></i> & ABOVE</label>
     </div>
     
  )
})}
  
  </React.Fragment>
)
}
getCBs=(str,checked,value)=>{

  let arr=str?str.split(","):[];
  if(checked)
  {
      arr.push(value);

  }
  else
  {
      let index=arr.findIndex(ele=>ele==value);
      if(index>=0)
         {
             arr.splice(index,1);
         }

  }
  return arr.join(",");
}
    render() {
        let {ramList,ratingList,priceList}=this.state;
        let {assured,ram,rating,price}=this.props.options;
      
        return (

           <div  className="row  border-bottom">
               <div className="col-12 pl-3 bg-white  border-bottom border-top py-3">
                 <span className=" fontStyle" style={{fontSize:"20px"}}>Filters</span>
                
               </div>
              
           
                   <div className="col-12 pb-3 py-3 pb-1 pl-4 bg-white  border-bottom ">
                     
     <div className="form-check-inline  py-1" key="assured">
      <input className="form-check-input" 
      type="checkbox"
       name="assured"
       value={assured}
       checked={assured}
         
       onChange={this.handelChange}
       
      />
     <label className="form-check-label"><img width="70" height="21" src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png" /></label>
     </div>             
     

                   </div>
                   <div className="col-12 pl-4 bg-white border-bottom py-2">
                 {this.getCB(ramList,ram,"ram","RAM")}
                
                   </div>
                   <div className="col-12 pl-4 bg-white border-bottom py-2">
                   {this.getCBRating(ratingList,rating,"rating","CUSTOMER RATING")}
                  
                   </div>
                   <div className="col-12 pl-4 bg-white border-bottom py-2">
                   {this.getCB(priceList,price,"price","PRICE")}
                 
                   </div>
           </div>
        )
    }
}
export default Input;