import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import Login from "./login";
import Input from "./input";
import Login2 from "./login2";
import http from "../services/httpServices"

class NavBar extends Component
{
    state={
      search:this.props.search,
      moreList:["Notification","Sell on Flipkart","24X7 Customer Care","Advertise"],
     mobilesList:["Mi","RealMe","Samsung","OPPO","Apple"],
     laptopsList:["Apple","HP","Acer","Dell"],
     camerasList:["Nikon","Canon","Sony"],
     loginshow:false,
     user:{email:"",password:""},
     showDropdown:"",
     errors:"",
     
    }
    showLogin=()=>{
      let s={...this.state};
      s.loginshow=s.loginshow?false:true;
      this.setState(s);
    }
     handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s.search=input.value;
        localStorage.setItem("search","");
        this.setState(s);
    }
    handelSubmit=(e)=>{
      if(e.key == "Enter")
      {
       
        let {search}=this.state;
        this.props.handelSearch(search);
        if(search)
        {
          window.location=`/home/Mobiles?q=${search}`;
        }
       
       
      } 
    }
    handelChangeLogin=(e)=>{
      let {currentTarget:input}=e;
     
      let s={...this.state};
      s.user[input.name]=input.value;
      s.errors="";
      this.setState(s);
  }
  handelSubmitLogin=(e)=>{
    e.preventDefault();
    console.log("login submit");
    let {user}=this.state;
    this.postData("/login",user);
}
  async postData(url,obj) {
      try
      {
          let {data:res,headers}=await http.post(url,obj);
          let usertoken=headers["x-auth-token"];
       
          localStorage.setItem("usertoken",usertoken);
          console.log(res,headers);
          localStorage.setItem("role",res.role);
          localStorage.setItem("id",res.id);
         
         let str=res.role=="admin"?"/admin":"/home";
         window.location=str;
      }
      catch(ex) {
          if(ex.response && ex.response.status == 400)
          {
              let errors="Invalid Email and password!";
             
              this.setState({errors:errors});

          }
          else {
              let errors="";
              errors="Invalid Email and password!";
             
              this.setState({errors:errors});
          }
      }
     
  }
  handelDropdown=()=>{
    let s={...this.state};
    s.showDropdown=s.showDropdown?false:true;
    this.setState(s);
  }
  goToLink=(str)=>{
    window.location=str;
  }
    render() {
      let {search,moreList,mobilesList,laptopsList,camerasList,loginshow,showDropdown}=this.state;
      let city=this.props.city;
       let role=localStorage.getItem("role");
       let {user,errors,show}=this.state;
       let {email,password}=user;
        return(
          <React.Fragment>  
    <div className="row bg-primary text-light py-2 text-center">
         <div className="col-lg-1"></div>
          <div className="col-lg-1">
            <div className="row">
              <div className="col-11">
                <Link to="/">
                <img src="https://i.ibb.co/qs8BK6Y/flipkart-plus-4ee2f9.png" className="flipkartIconStyle" width="75" height="10"/>
                </Link>
             
                <br/>
                <span className="text-white brandIcon font-italic">Explore</span>
                <span className="text-warning brandIcon font-italic">Plus</span>
              <img width="20" className="flipkartExploreStyle" height="10" src="https://i.ibb.co/t2WXyzj/plus-b13a8b.png"/>
              </div>
            </div>
         
      
          </div>
           <div className="col-lg-3">
          
        <input className="form-control searchField" type="search"
     name='search'
    
                    value={search} 
                       onChange={this.handelChange}
                       onKeyDown={this.handelSubmit}
     placeholder="Search for products,brands and more"
     />
        
           </div>
         
           {role? (
             <React.Fragment>
                     <div className="col-lg-2 text-left  navBarItem">
             {role && role == "admin" && (
                     <div className="row">
                      <div className="col-8">
                    
                    <div className="dropdown pb-3 mr-3">
                      <div className=" dropdownbutton">
                      <span style={{fontSize:"16px"}} onClick={()=>this.handelDropdown()} className=" tcolor font-weight-bold  " ><storg>My Account 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"white"}} ></i></storg>
                      </span>
                      </div>
                     
                       <div className="userdropdown mt-3" aria-labelledby="navbarDropdown">
                       <Link onClick={()=>this.goToLink("/admin")}  className="itemstyle">My Profile</Link>
                        <Link onClick={()=>this.goToLink("/admin/products/upload?q=form")}  className="itemstyle">Upload</Link>
                        <Link onClick={()=>this.goToLink("/admin/products/download")} to="/admin/products/download" className="itemstyle">Download</Link>
       
                        <Link to="/logout" className="itemstyle">Logout</Link>
                           </div>
 
                           </div>
                    </div>
                    </div>
            
             )}
             {role && role=="user" && (
                <div className="row">
                      <div className="col-8">
                    
                    <div className="dropdown pb-3 mr-3">
                      <div className=" dropdownbutton">
                      <span style={{fontSize:"16px"}} onClick={()=>this.handelDropdown()} className=" tcolor font-weight-bold  " ><storg>My Account 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"white"}} ></i></storg>
                      </span>
                      </div>
                     
                       <div className="userdropdown mt-3" aria-labelledby="navbarDropdown">
                       <Link onClick={()=>this.goToLink("/dashboard/account")}  className="itemstyle">My Profile</Link>
                  <Link onClick={()=>this.goToLink("/dashboard/account/orders")} className="itemstyle">Orders</Link>
                  <Link onClick={()=>this.goToLink("/dashboard/wishlist")} className="itemstyle">WishList</Link>
                  <Link onClick={()=>this.goToLink("/logout")}  className="itemstyle">Logout</Link>
                           </div>
 
                           </div>
                    </div>
                    </div>
 
               
              )}
            
            </div>
          
             </React.Fragment>
           ):(
                <React.Fragment>
                     <div className="col-lg-2">
                   {!role &&  <span
                  
                   className="searchButton bg-light text-primary loginbutton fontstyle" 
                   type="button" data-target="#exampleModalCenter" data-toggle="modal">Login</span>}
                   </div>
                </React.Fragment>
           )}
         
        <div className="col-lg-1 navBarItem">
     

        <div  className=" moredropdown  pb-3">
           <div className="dropdownbutton">
           <span style={{fontSize:"16px"}} onClick={()=>this.handelDropdown()} className=" tcolor font-weight-bold  " ><storg>More 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"white"}} ></i></storg>
                      </span>
           </div>
          
       
          <div  className="mt-3  moredropdownstyle" >
           
            {moreList.map(ele=>{
              return (
                <div  className=" moreitemstyle">{ele}</div>
              )
            })}
          </div>
        </div>
        </div>
        {this.props.viewList.length?(
           <div className="col-lg-1 navBarItem">
           <Link to="/recentlyviewed"  style={{fontSize:"16px"}} className="tcolor  navbarLink" ><strong>Viewed</strong></Link>
         </div>
        ):""}
         {this.props.compareList.length?(
           <div className="col-lg-1 navBarItem">
           <Link to="/mobiles/compare"  style={{fontSize:"16px"}} className="tcolor  navbarLink" ><strong>Compare</strong></Link>
         </div>
        ):""}
        
        <div className="col-lg-2 navBarItem ">
        <Link to="/cart" style={{fontSize:"16px"}} className="tcolor navbarLink ">  <i style={{display:"inline-block"}} className="fas fa-shopping-cart"></i>
        {this.props.cartItems? <span className="badge badge-pill badge-danger mr-1" style={{fontSize:"10px"}}>{this.props.cartItems}</span>:<span className="ml-1" style={{display:"inline-block"}}></span>}
         
        <strong>Cart</strong></Link>
        </div>
      
      

      </div>  
      <div className="row normalbgcolor   text-center" style={{paddingTop:"11px",paddingBottom:"8px"}}>
     <div className="col-2"></div>
       <div className="col-lg-2 normalbgcolor ">
                      <div className="row">
                      <div className="col-12 text-left">
                    
                    <div className="mobiledropdown  pb-2 mr-3">
                      <div className=" ">
                      <span  onClick={()=>this.handelDropdown()} className="dtitlestyle " >Mobiles 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"lightgray"}} ></i>
                      </span>
                      </div>
                     
                       <div className="mobiledropdownbody mt-2" >
                       {mobilesList.map(ele=>{
                         return (
                          <Link  to={`/home/Mobiles/${ele}?page=1`} className="mobiledropdownitem ditemstyle fontstyle">{ele}</Link>
               
                          )
                           })}
                           </div>
 
                           </div>
                    </div>
                    </div>

                 
       </div>

       
       <div className="col-lg-4 normalbgcolor">

       <div className="row">
                      <div className="col-12 text-center">
                    
                    <div className="mobiledropdown  pb-2 mr-3">
                      <div className=" ">
                      <span  onClick={()=>this.handelDropdown()} className="dtitlestyle " >Laptops 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"lightgray"}} ></i>
                      </span>
                      </div>
                     
                       <div className="mobiledropdownbody mt-2" aria-labelledby="navbarDropdown">
                       {laptopsList.map(ele=>{
                       return (
                       <Link to={`/home/Laptops/${ele}?page=1`} className="mobiledropdownitem ditemstyle  fontstyle">{ele}</Link>
                        )
                              })}
                           </div>
 
                           </div>
                    </div>
                    </div>

                 
         
       </div>
       <div className="col-lg-4 normalbgcolor">
            

       <div className="row">
                      <div className="col-12 text-center">
                    
                    <div className="mobiledropdown pb-2 mr-3">
                      <div className=" ">
                      <span   onClick={()=>this.handelDropdown()} className="dtitlestyle " >Camera 
                       <i class="fa fa-chevron-down mt-1 ml-1" id="onhover" style={{fontSize:"10px",color:"lightgray"}} ></i>
                      </span>
                      </div>
                     
                       <div className="mobiledropdownbody mt-2" aria-labelledby="navbarDropdown">
                       {camerasList.map(ele=>{
              return (
                <Link to={`/home/Cameras/${ele}?page=1`} className="mobiledropdownitem ditemstyle fontstyle">{ele}</Link>
              )
            })}
                           </div>
 
                           </div>
                    </div>
                    </div>
        
              
         
         




       </div>
      </div>  
      







      <div className="modal display-block" id="exampleModalCenter">
  <section className="modal-main">
    <div className="_2hriZF _2rbIyg" tabindex="-1">
      <div className="_2QfC02">
        <button data-dismiss="modal" style={{fontSize:"32px",paddingTop:"0px",background:"transparent",color:"#fff",border:"0px solid black"}}  className="_2KpZ6l disblebutton">✕</button>
        <div className="_2MlkI1">
          <div className="_3wFoIb row">
            <div className="_3oBhRa  col-2-5 _4H6HH5">
              <span className="_36KMOx"><span>Login</span>
              </span><p className="_1-pxlW">
                <span>Get access to your Orders, Wishlist and Recommendations</span></p>
                </div>
                <div className="_36HLxm  col-3-5">
                  <div>
                    <form autocomplete="on">
                    <div className="IiD88i _351hSN">
                      <input type="search" autocomplete="off" 
                      className="_2IX_2- _3umUoc VJZDxU" id="email"
                      value={email}
                      onChange={this.handelChangeLogin}
                       name="email" placeholder="Enter Email"/>
                      <span className="_36T8XR _3umUoc"></span>
                      <label className="_1fqY3P _3umUoc"><span>Enter Email/Mobile number</span></label>
                      </div>
                      <div className="IiD88i _351hSN">
                        <input type="password" 
                        autocomplete="off" className="_2IX_2- _3umUoc _3mctLh VJZDxU" id="password" name="password"
                        onChange={this.handelChangeLogin}
                        placeholder=" Enter Password" value={password}/>
                          <span className="_36T8XR _3umUoc"></span>
                          <label className="_1fqY3P _3umUoc"><span>Enter Password</span>
                          </label>
                          <a className="_2QKxJ- _2_DUc_" tabindex="-1">
                            <span>Forgot?</span>
                            </a>
                           
                            </div>
                            {errors && (
                  
                                 <div className="errorStyle text-danger text-center">
                                 {errors}
                  
                              </div>
                                 )}
                            <div className="_1Ijv5h">By continuing, you agree to Flipkart's<a className="_2ARnXM" target="_blank" href="/pages/terms">Terms of Use</a> and <a className="_2ARnXM" target="_blank" href="/pages/privacypolicy">Privacy Policy</a>.</div>
                            <div className="_1D1L_j"><button className="_2KpZ6l _2HKlqd _3AWRsL" onClick={this.handelSubmitLogin}>
                              <div className="_2YsvKq o8qAfl">
                                <svg className="_2LJFE8" viewBox="25 25 50 50">
                                  <circle stroke="#fff" className="_2XJHnB" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10">
                                    </circle>
                                    </svg>
                                    </div>
                                    <span>Login</span>
                                    </button>
                                    </div>
                                    <div className="_1k3JO2">
                                      <div className="_2XlkPA text-center">OR</div>
                                      <button className="_2KpZ6l _2HKlqd _3NgS1a">Request OTP</button>
                                      </div>
                                      <div className="_1En5li">
                                        <a className="_14Me7y" href="/account/login?signup=true">New to Flipkart? Create an account</a>
                                        </div></form></div></div></div></div></div></div></section></div>

      </React.Fragment>
     
        )
    }
}
export default NavBar;