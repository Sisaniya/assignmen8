import React,{Component} from "react";
import Modal from "react-bootstrap/Modal";
import http from "../services/httpServices";

import "bootstrap/dist/css/bootstrap.min.css";
class Login2 extends Component
{
    state={
     
      user:{email:"",password:""},
      
    }
    handelShow=()=>{
      let s={...this.state};
      s.show=s.show?false:true;
      this.setState(s);
    }
    handelChange=(e)=>{
      let {currentTarget:input}=e;
     
      let s={...this.state};
      s.user[input.name]=input.value;
      this.setState(s);
  }
  async postData(url,obj) {
      try
      {
          let {data:res,headers}=await http.post(url,obj);
          let usertoken=headers["x-auth-token"];
       
          localStorage.setItem("usertoken",usertoken);
          console.log(res,headers);
          localStorage.setItem("role",res.role);
          localStorage.setItem("id",res.id);
         
         let str=res.role=="admin"?"/admin":"/home";
         window.location=str;
      }
      catch(ex) {
          if(ex.response && ex.response.status == 400)
          {
              let errors="Invalid Email and password!";
             
              this.setState({errors:errors});

          }
          else {
              let errors="";
              errors="Invalid Email and password!";
             
              this.setState({errors:errors});
          }
      }
     
  }
  removeInputFocus=(e)=>{
    e.currentTarget.classNameNameName="pincodeFieldStyle";
    console.log(e.currentTarget.classNameNameName);
  }
  handelInputFocus=(e)=>{
    e.currentTarget.classNameNameName="removeBorders";
    console.log(e.currentTarget.classNameNameName);
  }
  handelSubmit=(e)=>{
      e.preventDefault();
      console.log("login submit");
      let {user}=this.state;
      this.postData("/login",user);
  }
    render() {
     
      let {user,errors,show}=this.state;
      let {email,password}=user;
        return (
            <React.Fragment>
        
      
<button type="button" classNameName="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button>

<div className="modal display-block" id="exampleModalCenter">
  <section className="modal-main">
    <div className="_2hriZF _2rbIyg" tabindex="-1">
      <div className="_2QfC02">
        <button data-dismiss="modal" style={{fontSize:"32px",paddingTop:"0px",background:"transparent",color:"#fff",border:"0px solid black"}}  className="_2KpZ6l disblebutton">✕</button>
        <div className="_2MlkI1">
          <div className="_3wFoIb row">
            <div className="_3oBhRa  col-2-5 _4H6HH5">
              <span className="_36KMOx"><span>Login</span>
              </span><p className="_1-pxlW">
                <span>Get access to your Orders, Wishlist and Recommendations</span></p>
                </div>
                <div className="_36HLxm  col-3-5">
                  <div>
                    <form autocomplete="on">
                    <div className="IiD88i _351hSN">
                      <input type="search" autocomplete="off" 
                      className="_2IX_2- _3umUoc VJZDxU" id="email"
                      value={email}
                      onChange={this.handelChange}
                       name="email" placeholder="Enter Email"/>
                      <span className="_36T8XR _3umUoc"></span>
                      <label className="_1fqY3P _3umUoc"><span>Enter Email/Mobile number</span></label>
                      </div>
                      <div className="IiD88i _351hSN">
                        <input type="password" 
                        autocomplete="off" className="_2IX_2- _3umUoc _3mctLh VJZDxU" id="password" name="password"
                        onChange={this.handelChange}
                        placeholder=" Enter Password" value={password}/>
                          <span className="_36T8XR _3umUoc"></span>
                          <label className="_1fqY3P _3umUoc"><span>Enter Password</span>
                          </label>
                          <a className="_2QKxJ- _2_DUc_" tabindex="-1">
                            <span>Forgot?</span>
                            </a>
                           
                            </div>
                            {errors && (
                  
                                 <div className="errorStyle text-danger text-center">
                                 {errors}
                  
                              </div>
                                 )}
                            <div className="_1Ijv5h">By continuing, you agree to Flipkart's<a className="_2ARnXM" target="_blank" href="/pages/terms">Terms of Use</a> and <a className="_2ARnXM" target="_blank" href="/pages/privacypolicy">Privacy Policy</a>.</div>
                            <div className="_1D1L_j"><button className="_2KpZ6l _2HKlqd _3AWRsL" onClick={this.handelSubmit}>
                              <div className="_2YsvKq o8qAfl">
                                <svg className="_2LJFE8" viewBox="25 25 50 50">
                                  <circle stroke="#fff" className="_2XJHnB" cx="50" cy="50" r="20" fill="none" stroke-width="5" stroke-miterlimit="10">
                                    </circle>
                                    </svg>
                                    </div>
                                    <span>Login</span>
                                    </button>
                                    </div>
                                    <div className="_1k3JO2">
                                      <div className="_2XlkPA text-center">OR</div>
                                      <button className="_2KpZ6l _2HKlqd _3NgS1a">Request OTP</button>
                                      </div>
                                      <div className="_1En5li">
                                        <a className="_14Me7y" href="/account/login?signup=true">New to Flipkart? Create an account</a>
                                        </div></form></div></div></div></div></div></div></section></div>


                                        <div className="row">
                     <div className="col-12 my-1 px-2">
                         <strong>What happens when I update my email address (or mobile number)?</strong>
                     </div>
                     <div className="col-12 my-1 px-2">
                        <span>Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</span>
                     </div>
                     <div className="col-12 my-1 px-2">
                         <strong>When will my Flipkart account be updated with the new email address (or mobile number)?</strong>
                     </div>
                     <div className="col-12 my-1 px-2">
                       <span>It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</span>
                     </div>
                     <div className="col-12 my-1 px-2">
                         <strong>What happens to my existing Flipkart account when I update my email address (or mobile number)?</strong>
                     </div>
                     <div className="col-12 my-1 px-2">
                     <span>Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</span>
                     </div>
                     <div className="col-12 my-1 px-2">
                         <strong>Does my Seller account get affected when I update my email address?</strong>
                     </div>
                     <div className="col-12 my-1 px-2">
                       <span>Flipkart has a 'single sign-on' policy. Any changes will reflect in your Seller account also.</span>
                     </div>
                 </div>
                 <div className="row">
                     <div className="col-12">
                         <img className="d-block w-100" src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/myProfileFooter_0cedbe.png"/>
                     </div>
                 </div>
                                        
  </React.Fragment>
        )
    }
}
export default Login2;