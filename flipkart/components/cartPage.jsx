import React,{Component} from "react";
import {Link} from "react-router-dom";
import httpServices from "../services/httpServices";
class CartPage extends Component
{
    state={}
    async addOrder(obj) {
        try {
            let response=await httpServices.put(`/addOrder`,obj);
              window.alert("order Placed");
              this.props.emptyCartList();
              this.props.history.push("/home");
        }
        catch(ex) {
            console.log(ex);
        }
       
         
    }
    placeOrder=()=>{
        let id=localStorage.getItem("id");
        if(id)
        {

          let {cartItems}=this.props;
          let date=new Date();
          let orders=cartItems.map(ele=>{
              let obj={...ele,date:date.toDateString()};
              return obj;
          })
          let obj={userId:id,orders:orders};
        console.log(obj);
          this.addOrder(obj);
         
        }
        else 
        {
            window.alert("Login to place order!");
        }
    }
    render() {
        let {cartItems}=this.props;
        let totalPrice=cartItems.reduce((acc,curr)=>{
            let p= curr.price;
             let tp=p*curr.qty;
             acc=acc+tp;
             return acc;
       
        },0)
        return (
            <React.Fragment>
                <div className="row pt-5 bg-white">
                    <div className="col-12 bg-white mt-6">
                    {cartItems.length?(
                       <div className="row bgcolor mx-2">
                       <div className="col-8 mt-1  normalbgcolor">
                           <span style={{fontSize:"20px",fontWeight:"490"}} className="ml-3 d-block mt-3">My Cart({cartItems.reduce((acc,curr)=>acc+curr.qty,0)})</span>
                           <hr className="bg-light "/>
                           {cartItems.map(ele=>{
                              let {id,name,brand,img,rating,ratingDesc,details=[],
                                 price,assured,discount,emi,exchange,ram,offers=[],popularity,qty,prevPrice}=ele;
                                 return (
                                     <React.Fragment>
                                     <div className="row normalbgcolor">
                                        
                                         <div className="col-lg-3 text-center">
                                         <img src={img} className="d-block rounded mx-auto mb-2"  width="50" height="92"/>
                                         <button onClick={()=>this.props.handelDecrement(id)} className="incrementButtton">-</button>
                                         <span className="border pl-2 pr-2 mt-2 showCartQtyStyle" >{qty}</span>
                                         
                                           <button onClick={()=>this.props.handelIncrement(id)} className="incrementButtton">+</button>
                                         
                                         </div>
                                         
                                         <div className="col-lg-5">
                                             <span className="" style={{fontSize:"18px"}}>{name}</span><br/>
                                             <sapn className="text-muted mr-1">{brand}</sapn> 
                                             {assured == true?<img width="70"  src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png" />:""}
                                             <br/>
                                             <span style={{fontSize:"16px"}} className=" mr-2"> { price}</span>
                                             <span style={{fontSize:"18px"}} className="text-muted cartDiscountStyle  mr-1">{prevPrice} </span>
                                             <span style={{fontSize:"18px",fontWeight:"500",color:"#388e3c"}}>{discount}%</span><br/> 
                                        </div>
                                       
                                        <div className="col-lg-3 ">
                                            <span className="" style={{fontSize:"18px"}}> Delivery in 2 days | Free <span className="cartDiscountStyle">40</span></span>
                                           <br/>
                                            <small className="text-muted">10 Days Replacement Policy</small>
                                        </div>
                                     </div>
                                     <hr className="bg-light"/>
                                     </React.Fragment>
                                 )
                           })}
                          
                           <div className="row">
                               <div className="col-12 text-right">
                                   <button style={{backgroundColor:"rgb(251, 100, 27)",fontSize:"19px",color:"rgb(255, 255, 255)",fontWeight:"490"}} className="btn btn-warning  fontStyle  btn-md" onClick={()=>this.placeOrder()}>Place Order</button>
                               </div>
                           </div>
                          
                       </div>
                       <div className="col-lg-3 ml-2 mt-1">
                           <div className="row border  normalbgcolor">
                                 <div className="col-12 mb-3 mt-2 ">
                                     <span style={{fontSize:"15px"}} className="text-muted ml-1"> Price Details</span>
                                    
                                     </div>
                             
                           </div>
                           <div className="row py-2  border normalbgcolor">
                                 <div style={{fontSize:"14px"}} className="col-6 text-left mb-3 ">Price ({cartItems.reduce((acc,curr)=>acc+curr.qty,0)} items)</div>
                                 <div style={{fontSize:"14px"}} className="col-6 text-right mb-3">{totalPrice}</div>
                                 <div style={{fontSize:"14px"}} className="col-6 text-left ">Delivery</div>
                                 <div style={{fontSize:"14px"}} className="col-6 text-right "><span className="text-success">Free</span></div>
     
                           </div>
                         
                           <div className="row  py-1 pb-2  normalbgcolor ">
                           <div style={{fontSize:"18px",fontWeight:"500"}} className="col-6 text-left fontstyle">Total Payable</div>
                                 <div style={{fontSize:"18px",fontWeight:"500"}} className="col-6 text-right fontstyle">{totalPrice}</div>
                           </div>
                           <div className="row  text-muted">
                               <div className="col-1 pt-1">
                              
                              <img width="35" height="35" src="https://img1a.flixcart.com/www/linchpin/fk-cp-zion/img/shield_435391.svg"/>
                               </div>
                               <div className="col-9 ml-1" style={{fontSize:"14px"}}>
                               Safe and Secure Payments. Easy returns. 100% Authentic Products.
                               </div>
                           </div>
                       </div>
                   </div>
                  ):(
                    <div className="row">
                    <div className="col-12 my-5">
                        <img width="250" height="200" className="d-block mx-auto rounded"
                        src="https://rukminim1.flixcart.com/www/800/800/promos/16/05/2019/d438a32e-765a-4d8b-b4a6-520b560971e8.png?q=90"/>
                    </div>
                    <div className="col-12 mt-1 my-2 text-center">
                        <span className="bigText">Missing Cart Items?</span><br/>
                       <small>Login to see the items you added previously</small>
                    </div>
                </div>
                  )}
             
            
                    </div>
                </div>
                 
            </React.Fragment>
        )
    }
}
export default CartPage;