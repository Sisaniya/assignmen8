import axios from "axios";
import React,{Component} from "react";
import httpServices from "../services/httpServices";
import ExtraInfo from "./extraInfo";

class Product extends Component
{
    state={
        product:{},
        pincode:this.props.spincode,
        f:0,
        reviews:[],
        page:1,
        images:[],
        activeImage:"",
        brandimage:"",
        cartMsg:"ADD TO CART",
        reviewShowList:[],
    }
   showMore=(title)=>{
       let s={...this.state};
        let index=s.reviewShowList.findIndex(ele=>ele == title);
        if(index>=0)
        {
            s.reviewShowList.splice(index,1);
        }
        else
        {
            s.reviewShowList.push(title);
        }
        this.setState(s);
   }
    handelChange=(e)=>{
        let {currentTarget:input}=e;
        let s={...this.state};
        s[input.name]=input.value;
        this.setState(s);
     }
     handelSubmit=(e)=>{

        e.preventDefault();
        let {pincode}=this.state;
        if(pincode)
        {
            this.checkPincodeStatus();
        }
        else
        {
            window.alert("Enter Pincode");
        }
        
     }
    async getReviewsServer() {
        let {id}=this.props.match.params;
        let response=await httpServices.get(`/reviews/${id}`);
        let {data}=response;
        let s={...this.state};
        console.log(data);
        s.reviews=data;
        this.setState(s);
    }
    getReviews=()=> {
      this.getReviewsServer();
    }
     async checkPincodeStatus() {
        let {id}=this.props.match.params;
        let {pincode}=this.state;
        try {
            let response=await httpServices.get(`/pincode/${pincode}/${id}`);
            let {data}=response;
            window.alert(data.display);
        }
        catch(ex) {
            if(ex.response && ex.response.status == 400)
             window.alert(ex.response.data);
        }
        this.props.setPincode(pincode);
     }
   async componentDidMount() {
        let {id}=this.props.match.params;
        let response=await axios.get(`https://gentle-bastion-84264.herokuapp.com/flp/product/${id}`);
        let {data}=response;
        console.log(response);
        let images=data.pics.imgList;
        let brandimage=data.pics.brandImg;
        this.setState({product:data.prod,images:images,activeImage:images[0],brandimage:brandimage});
    }
   addToCart=(id)=>{
       let {cartItems}=this.props;
       let {product}=this.state; 
       let index=cartItems.findIndex(ele=>ele.id == id);
    if(index>=0)
    {
        this.props.history.push("/cart");
    }
    else
    {

        this.props.handelCart(product);
        let s={...this.state};
        s.cartMsg="GO TO CART";
        this.setState(s);
    }
   }
   removeInputFocus=(e)=>{
    e.currentTarget.className="pincodeFieldStyle";
    console.log(e.currentTarget.className);
  }
  handelInputFocus=(e)=>{
    e.currentTarget.className="removeBorders";
    console.log(e.currentTarget.className);
  }
  changeImage=(img)=>{
      let s={...this.state};
      s.activeImage=img;
      this.setState(s);
  }
  clearPincodeField=()=>{
      let s={...this.state};
      s.pincode="";
      this.setState(s);
  }
    render () {
        let {product,pincode,reviews,page,images,activeImage,brandimage,cartMsg,reviewShowList}=this.state;
        let {id,name,brand,img,rating,ratingDesc,details=[],
            price,assured,discount,emi,exchange,ram,offers=[],popularity,prevPrice}=product;
        console.log(product,images);
       let {cartItems}=this.props;
        
        let p=+page;
  let size=3;
  let firstIndex=(p-1)*size;
  let lastIndex=firstIndex+(size-1);
  lastIndex=reviews.length>lastIndex?lastIndex:reviews.length-1;
  let pageReview=reviews.filter((ele,index)=>index>=firstIndex && index<=lastIndex);
  let totalItems=reviews.length;
  
        let pages=Math.ceil(totalItems/size);
        let arr=[];
        for(let i=1;i<=pages;i++)
             arr.push(i);
        console.log(arr);
        return (
            <React.Fragment>
               <div className="row ml-1 pt-1 bg-white">
                   <div className="col-lg-5">
                         <div className="row wrapclass bg-white justify-content-center p-0">
                         <div className="col-lg-2 text-center">
                              
                                   {images.map(ele=>{
                                       return (
                                        <div  onClick={()=>this.changeImage(ele)} style={{cursor:"pointer"}}
                                        className={ele==activeImage?"row wrapclass activeimage  ml-2":"row border  ml-2"} >
                                         
                                               <div style={{display:"inline-block",verticalAlign:"top"}} className="col leftsmallcard">
                                                   <img className="leftimgx" src={ele} />
                                               </div>

                                      </div>
                                         
                                       )
                                   })}
                                    
                               </div>
                             <div className="col-lg-8 secondpart">
                             <img src={activeImage} className="img-fluid " />
                                  
                                </div>
                             <div className="col-2"></div>

                         </div>





                         <div className="row bg-white justify-content-center">
                          <div 
                          onClick={()=>this.addToCart(id)} className="col-6 text-center ">
                             <button style={{backgroundColor:"#ff9f00",cursor:"pointer"}}
                              className="cartbuttonstyle fontstyle">
                             <i className="fas fa-shopping-cart mr-2"></i>
                            
                            {cartMsg}

                             </button>
                              
                            
                          </div>
                          <div
                          onClick={()=>this.addToCart(id)} className="col-6 text-center ">
                              <button  style={{backgroundColor:"#fb641b",cursor:"pointer"}} 
                              className="cartbuttonstyle fontstyle">
                                   <i className="fas fa-bolt mr-2 "></i>
                              BUY NOW

                              </button>
                         
                          </div>


                         </div>
                   </div>
                 
                   <div className="col-lg-7 pl-4">
                       <div className="row">
                           <div className="col-12 tprow">
                          {name}
                           </div>
                       </div>
                       <div className="row ml-2">
                       <div className="col-lg-12">
                       <span className="_3LWZlK">{rating} <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                                               className="_1wB99o"/></span>  
                                <span className="text-muted ml-2 mr-2">{ratingDesc}</span>
                                {assured == true?<img  height="21" src="https://i.ibb.co/t8bPSBN/fa-8b4b59.png" />:""}
                             
                            </div>
                            <div className="col-lg-12">
                            <span style={{fontSize:"28px",fontWeight:"500"}} className=" mr-3"> ₹{ price}</span>
                            <span style={{fontSize:"16px"}} className="text-muted discountStyleProduct mr-2">₹{prevPrice} </span>
                            <span style={{fontSize:"16px",fontWeight:"400",color:"#388e3c"}} className="">{discount} %</span>
                            <img src="https://static-assets-web.flixcart.com/www/linchpin/fk-cp-zion/img/info-basic_6c1a38.svg"  height="18px"/>
                            </div>
                       </div>
                       <div className="row ml-2">
                           <div className="col-1 mr-3">
                               <span className="text-muted " style={{fontSize:"14px",lineHeight:"1.4",display:"inline-block"}}>Delivery</span>
                           </div>
                           <div class="col-4">
                               <span class="hivXmn">
                                   <svg width="12" height="12" viewBox="0 0 9 12" class="_1kbGNj" xmlns="http://www.w3.org/2000/svg">
                                            <path fill="#2874f0" class="_6xm1dD" d="M4.2 5.7c-.828 0-1.5-.672-1.5-1.5 0-.398.158-.78.44-1.06.28-.282.662-.44 1.06-.44.828 0 1.5.672 1.5 1.5 0 .398-.158.78-.44 1.06-.28.282-.662.44-1.06.44zm0-5.7C1.88 0 0 1.88 0 4.2 0 7.35 4.2 12 4.2 12s4.2-4.65 4.2-7.8C8.4 1.88 6.52 0 4.2 0z" fill-rule="evenodd">
                                           </path>
                                 </svg>
                             <form class="N79-rD">
                                <input class="_36yFo0" type="search"
                                 id="pincode"
                                 name="pincode"
                                 onChange={this.handelChange}
                                placeholder="Search for pincode"
                                value={pincode}/>
                             </form>
                             <span class="_2P_LDn">
                                 <span onClick={this.handelSubmit}>
                                     Check
                                     </span>
                            
                             </span>
                          </span>
                         </div>
                         <div className="col-lg-3">
                               <span onClick={this.handelSubmit} 
                               style={{fontSize:"12px",cursor:"pointer"}}
                                className="checkPincodeStyle fontStyle">Check pincode</span>
                           </div>         

                       </div>


                       <div className="row ml-2">
                           <div className="col-12 fontstyle">
                           <span style={{fontSize:"16px",fontWeight:"500",color:"#212121"}}>Available Offers</span>
                           </div>
                     
                       {offers.map(ele=>{
                            let {img,type,detail}=ele;
                            return (
                                <div className="col-lg-10" >
                                    <img className="mr-1" src={img} width="20" height="20"/>
                                    <span className="mr-1">{type}</span>
                                    <span className="text-muted">{detail}</span>
                                </div>
                            )
                       })}
                       </div>
                      <div className="row  my-2">
                          <div className="col-2 brandbox">
                            
                              <img src={brandimage}  className="brandimage "/>
                            
                          </div>
                          <div style={{fontSize:"15px",color:"#212121",lineHeight:"2"}} className="col-9 fontStyle">
                          Brand Warranty of 1 Year Available for Mobile and 6 Months for<br/> Accessories
                          </div>
                      </div>
                       <div className="row my-2 ml-2">
                           <div className="col-1 text-muted">Highlights</div>
                           <div className="col-4">
                               <ul>
                               {details.map(ele=><li>{ele}</li>)}
                               </ul>
                               
                           </div>
                           <div className="col-lg-2 text-muted">
                               <span>Easy</span><br/>
                               <span>Payment Options</span>
                           </div>
                           <div className="col-4">
                               <ul>
                                   {emi && <li>{emi}/month</li>}
                                   
                                   <li>Debit/Flipkart EMI available</li>
                                   <li>Cash on Delivery</li>
                                   <li>Net Banking & Credit/Debit/ATM Card</li>
                              
                               </ul>
                           </div>
                       </div>
                       <div className="row ml-2">
                           <div className="col-1 text-muted">
                               Seller
                           </div>
                           <div className="col-5">
                               <span className="text-primary mr-2">SuperComNet</span>
                               <span className="text-light sellerRatingStyle bg-primary">4.2 <i style={{fontSize:"10px"}} className="fas fa-star"></i></span>
                                <ul>
                                    <li>10 Day Replacement</li>
                                </ul>
                          
                           </div>
                       </div>
                       <div className="row">
                       <div className="col-12">
                           <img height="100px" className="d-block w-70 " src="https://i.ibb.co/j8CMRbn/CCO-PP-2019-07-14.png"  />
                       </div>
                       </div>


                       <div className="col-11 border border-bottom-0">          
                   <div className="row ">
                      
                       <div className="col-12 pb-2 pt-1 border-bottom">
                       <h4>Ratings & Reviews</h4>
                       </div>
                       <div className="col-12 ">
                          {reviews.length?"":this.getReviews()}
                          {pageReview.map((ele,index)=>{
                          let {star,title,description,author}=ele;
                         return (
                          <div className="row   border-bottom">
                              <div className="col-lg-12 ">
                              <span class="reviewrating">
                                  <span style={{marginRight:"2px"}}>{star}</span>
                                  
                              <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMiI+PHBhdGggZmlsbD0iI0ZGRiIgZD0iTTYuNSA5LjQzOWwtMy42NzQgMi4yMy45NC00LjI2LTMuMjEtMi44ODMgNC4yNTQtLjQwNEw2LjUuMTEybDEuNjkgNC4wMSA0LjI1NC40MDQtMy4yMSAyLjg4Mi45NCA0LjI2eiIvPjwvc3ZnPg=="
                               class="reviewstar"/>

                              </span>
                             
                              <span className="reviewtitlestyle">{title}</span>
                              </div>
                              <div className="col-lg-12 my-2 fontstyle" style={{fontSize:"14px",fontWeight:"500"}}>
                                {description.length>45?(<span>
                                    {reviewShowList.findIndex(ele=>ele==index)>=0?(
                                        <span>
                                             {description}
                                    <span onClick={()=>this.showMore(index)} className="text-primary"
                                     style={{color:"#2874f0",fontWeight:"700",fontSize:"12px",cursor:"pointer"}}> Read Less</span>

                                        </span>
                                    ):(
                                        <span>
                                        {description.substring(0,46)}
                               <span onClick={()=>this.showMore(index)} className="text-primary"
                                style={{color:"#2874f0",fontWeight:"700",fontSize:"12px",cursor:"pointer"}}> Read More</span>

                                   </span>
                                        
                                    )}
                                   

                                </span>):
                                    <span>
                                  {reviewShowList.findIndex(ele=>ele==index)>=0?(
                                      <span>
                                      {description}
                             <span onClick={()=>this.showMore(index)} className="text-primary"
                              style={{color:"#2874f0",fontWeight:"700",fontSize:"12px",cursor:"pointer"}}> Read Less</span>

                                 </span>
                                       

                                   ):description}
                                    </span>
                                    }

                              </div>
                              <div style={{fontSize:"12px",color:"#878787"}} className={index==2?"col-lg-12 text-muted":"col-lg-12 mb-3 text-muted"}>
                                  <span className="mr-2">{author}</span>
                                  <i class="fas fa-check-circle mr-1"></i>
                                  <sapn>Certified Buyer</sapn>
                              </div>
                              </div>
                            )
                             })}
                       </div>
                   </div>
                   </div>
                  
            
                  <div className="row my-2 ml-2">
                   <div style={{fontSize:"14px", fontWeight:"500",}} className="col-12 p-1 fontstyle">
                       Page {p} of {arr.length}
                   </div>
                   <div style={{fontSize:"14px"}} className="col-12   ml-3 ">
                   {p>1 && <span style={{cursor:"pointer",margin:"0px 12px"}} onClick={()=>this.handelPageInc(-1)} className="text-primary mr-3">PREVIOUS</span>}
                    {arr.map(ele=>{
                        if(ele==p)
                        {
                            return <span style={{cursor:"pointer"}} onClick={()=>this.handelPage(ele)} 
                            className="bg-primary text-light  pageReviewIndexStyle fontstyle">{ele}</span>
                        }
                        else {
                            return <span style={{display:"inline-block",
                            fontWeight:"500",
                            cursor:"pointer",margin:"0px 12px",color: "#212121"}} 
                            onClick={()=>this.handelPage(ele)} className="fontstyle">{ele}</span>
                        }
                    })}
                    {p<arr.length && <span style={{cursor:"pointer",margin:"0px 12px"}} onClick={()=>this.handelPageInc(1)} className="text-primary ml-2">NEXT</span>}
                   </div>
               </div>
                   </div>

               </div>
               <ExtraInfo/>
            </React.Fragment>
        )
    }
    handelPageInc=(inc)=>{
        let s={...this.state};
        s.page= s.page+inc;
        this.setState(s);
    }
    handelPage=(p)=>{
        let s={...this.state};
        s.page= p;
        this.setState(s);
    }
}
export default Product;