import React,{Component} from "react";
import {Link} from "react-router-dom";
import AdminNavBar from "./adminNavbar";
import FormUpload from "./formUpload";
import QNA from "./qna";
import queryString from "query-string";
import axios from "axios";
import FileDownload from "js-file-download";
class DownloadProducts extends Component
{
    state={
       data:"",
    }
    async fetchData() {
        let user=localStorage.getItem("usertoken")
        console.log(user);
        let response=await axios.get("https://aqueous-dawn-47322.herokuapp.com/downloadAllProducts",{headers: {Authorization: user}});
        let {data}=response;

        this.setState({data:data});
    }
    getAllData=()=>{
        this.fetchData();
    }
    downloadAllData=()=>{
        let {data}=this.state;
        FileDownload(data,'products.csv');
    }
    render() {
      let {data}=this.state;
      console.log(data);
        return (
            <React.Fragment>
                <div className="row backgcolor mb-2 mx-2">
                    <div className="col-lg-3 ml-3 my-2">
                        <AdminNavBar/>
                    </div>
                    <div className="col-lg-8 normalbgcolor pt-3 ml-4 mt-2">
                        {data?(
                        <button onClick={()=>this.downloadAllData()}  className="btn btn-primary btn-sm mt-2 ml-4">Click To Download CSV</button>
                        ):(
                            <button style={{backgroundColor:"rgb(251, 100, 27)"}} onClick={()=>this.getAllData()} className="btn btn-primary btn-sm mt-2 ml-4 text-white">Download Products</button>
                        )}
                      

                        <QNA/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default DownloadProducts;

