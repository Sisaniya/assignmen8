import React,{Component} from "react";
class Product extends Component
{
    state ={
     
    }
   
    render() {
        let {product,increQty}=this.props;
        let {prodid,name,qty}=product;
        return (
            <React.Fragment>
            <div className="row">
                <div className="col-3">{prodid}</div>
                <div className="col-3">{name}</div>
                <div className="col-3">{qty}</div>
                <div className="col-3">  <button className="btn btn-primary btn-sm m-1" onClick={()=>increQty(prodid)}>Increment</button></div>
            </div>
          
           
            </React.Fragment>
        );
    }
}
export default Product;