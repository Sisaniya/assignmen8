import React, {Component} from "react";
import NavBarProduct from "./navBarProduct";
class ProductStore extends Component
{
    state={
       products: [
            {"code":"PEP221","prod":"Pepsi","price":12,"instock":"Yes","category":"Beverages"},
            {"code":"COK113","prod":"Coca Cola","price":18,"instock":"Yes","category":"Beverages"},
            {"code":"MIR646","prod":"Mirinda","price":15,"instock":"No","category":"Beverages"},
            {"code":"SLI874","prod":"Slice","price":22,"instock":"Yes","category":"Beverages"},
            {"code":"MIN654","prod":"Minute Maid","price":25,"instock":"Yes","category":"Beverages"},
            {"code":"APP652","prod":"Appy","price":10,"instock":"No","category":"Beverages"},
            {"code":"FRO085","prod":"Frooti","price":30,"instock":"Yes","category":"Beverages"},
            {"code":"REA546","prod":"Real","price":24,"instock":"No","category":"Beverages"},
            {"code":"DM5461","prod":"Dairy Milk","price":40,"instock":"Yes","category":"Chocolates"},
            {"code":"KK6546","prod":"Kitkat","price":15,"instock":"Yes","category":"Chocolates"},
            {"code":"PER5436","prod":"Perk","price":8,"instock":"No","category":"Chocolates"},
            {"code":"FST241","prod":"5 Star","price":25,"instock":"Yes","category":"Chocolates"},
            {"code":"NUT553","prod":"Nutties","price":18,"instock":"Yes","category":"Chocolates"},
            {"code":"GEM006","prod":"Gems","price":8,"instock":"No","category":"Chocolates"},
            {"code":"GD2991","prod":"Good Day","price":25,"instock":"Yes","category":"Biscuits"},
            {"code":"PAG542","prod":"Parle G","price":5,"instock":"Yes","category":"Biscuits"},
            {"code":"MON119","prod":"Monaco","price":7,"instock":"No","category":"Biscuits"},
            {"code":"BOU291","prod":"Bourbon","price":22,"instock":"Yes","category":"Biscuits"},
            {"code":"MAR951","prod":"MarieGold","price":15,"instock":"Yes","category":"Biscuits"},
            {"code":"ORE188","prod":"Oreo","price":30,"instock":"No","category":"Biscuits"}],
            show:0,
         categoryList:["Beverages","Chocolates","Biscuits"],
          stockList:['Yes','No'],
      rangeList:[{txt:'<10',value:'1'},{txt:'10-20',value:'2'},{txt:'>20',value:'3'}],
      category:"",
      stock:"",
      range:"",
      sort:0,
      bill:[],
    }
    getSort=(i)=>{
        let s={...this.state};
        s.sort=i;   
       let so=i==1?s.products.sort((p1,p2)=>p1.code.localeCompare(p2.code))
        :i==2?s.products.sort((p1,p2)=>p1.prod.localeCompare(p2.prod))
        :i==3?s.products.sort((p1,p2)=>p1.category.localeCompare(p2.category))
        :i==4?s.products.sort((p1,p2)=>p1.price-p2.price)
        :i==5?s.products.sort((p1,p2)=>p1.instock=="Yes"?-1:1):"";
        this.setState(s);

        }
        addToBill=(c)=>{
            let s={...this.state};
            let index=s.bill.findIndex(ele=>ele.code==c);
            console.log(index);
            let pro=s.products.find(ele=>ele.code==c);
            if(index>=0)
            {
              let p=s.bill[index];
               p.qty=p.qty+1;
               s.bill.splice(index,1,p);
             }
             else 
             {
                 let p={...pro,qty:1};
                 s.bill.push(p);
             }
             this.setState(s);
        }
        increaseQty=(c)=>{
            let s={...this.state};
            let pro=s.bill.find(ele=>ele.code==c);
            pro.qty++;
            this.setState(s);
        }
        decreaseQty=(c)=>{
            let s={...this.state};
            let index=s.bill.findIndex(ele=>ele.code==c);
            let pro=s.bill[index];
            console.log(pro);
            if(pro.qty==1){
                s.bill.splice(index,1);
            }
            else 
            {
                pro.qty--;
            }
            this.setState(s);
        }
        removeBill=(c)=>{
            let s={...this.state};
            let index=s.bill.findIndex(ele=>ele.code==c);
            s.bill.splice(index,1);
            this.setState(s);
        }
        closeBill=(c)=>{
            let s={...this.state};
            
            s.bill=[];
            this.setState(s);
        }
        getBill=()=>{
            let {bill}=this.state;
            return (
                <React.Fragment>
                    <h3>Details of Current Bill</h3>
                    Items:{bill.length}, Quantity:{bill.reduce((acc,curr)=>acc+curr.qty,0)}, Amount:{bill.reduce((acc,curr)=>acc+(curr.qty*curr.price),0)}
                {bill.map(ele=>{
                    return (

                        <div className="row border">
                            <div className="col-6">
                                {ele.code} {ele.prod} Price:{ele.price} Quantity:{ele.qty} Value:{ele.qty*ele.price}
                            </div>
                            <div className="col-6">
                                <button className="btn btn-success btn-sm mr-1" onClick={()=>this.increaseQty(ele.code)}>+</button>
                           
                                <button className="btn btn-warning btn-sm mr-1" onClick={()=>this.decreaseQty(ele.code)}>-</button>
                            
                                <button className="btn btn-danger btn-sm" onClick={()=>this.removeBill(ele.code)}>x</button>
                            </div>
                        </div>
                    )
                })}
                {bill.length? <button className="btn btn-primary btn-sm m-1" onClick={()=>this.closeBill()}>Close Bill</button>:""}
                </React.Fragment>
            );
        }
    handleChange=(e)=>{
        let s={...this.state};
        const {currentTarget:input}=e;
        s[input.name]=input.value;
        this.setState(s);
     
        console.log(s[input.name]);
        

    }
    getFilter=()=>{
       let s={...this.state};
       
      let pro1=s.category?s.products.filter(pro => (pro.category == s.category)):s.products;
      let pro2=s.stock?pro1.filter(ele=>ele.instock==s.stock):pro1;
      s.products=pro2;
     console.log(pro2);
      this.setState(s);
    
    }
    getRange=(value)=>{
    let rj=value=='1'?{min:0,max:10}:value=='2'?{min:10,max:20}:value=='3'?{min:20,max:200}:{};
      return rj;
    }
   
    getTable=()=>{
        
        let {products,categoryList,stockList,rangeList,category,stock,range,sort}=this.state;

        let pro1=category?products.filter(pro => pro.category == category):products;
      let pro2=stock?pro1.filter(ele=>ele.instock==stock):pro1;
      let pro3=[];
      if(range==1)pro3=pro2.filter(ele=>ele.price<10);
      else if(range==2)pro3=pro2.filter(ele=>ele.price>=10 && ele.price<=20);
      else if(range==3)pro3=pro2.filter(ele=>ele.price>20);
      else pro3=pro2;

            let fpro=pro3;
        return (
            <React.Fragment>
                <div className="row text-center">
                    <div className="col-12"><h3>Product List</h3></div>
                    <div className="col-3"><h4>Filter Products by :</h4></div>
                    <div className="col-3">{this.getDD("Select Category",categoryList,category,'category')}</div>
                    <div className="col-3">{this.getDD("Select in Stock",stockList,stock,"stock")}</div>
                    <div className="col-3">{this.getDDR("Select Price Range",rangeList,range.value,"range")}</div>
                </div>
                <div className="row bg-dark text-white">
                    <div className="col-2" onClick={()=>this.getSort(1)}>Code{sort==1?"(X)":""}</div>
                    <div className="col-2" onClick={()=>this.getSort(2)}>Product{sort==2?"(X)":""}</div>
                    <div className="col-2" onClick={()=>this.getSort(3)}>Category{sort==3?"(X)":""}</div>
                    <div className="col-2" onClick={()=>this.getSort(4)}>Price{sort==4?"(X)":""}</div>
                    <div className="col-2" onClick={()=>this.getSort(5)}>In Stock{sort==5?"(X)":""}</div>
                    <div className="col-2"></div>
                </div>
                {fpro.map(ele=>{
                    let {code,prod,price,instock,category}=ele;
                    return (
                        <div className="row border">
                    <div className="col-2">{code}</div>
                    <div className="col-2">{prod}</div>
                    <div className="col-2">{category}</div>
                    <div className="col-2">{price}</div>
                    <div className="col-2">{instock}</div>
                    <div className="col-2">
                        <button className="btn btn-secondary btn-sm" onClick={()=>this.addToBill(code)}>Add To Bill</button>
                    </div>
                </div>
                    );
                })}
            </React.Fragment>
        )
    }
hendelShowBill=()=>{
    let s={...this.state};
    s.show=1;
    this.setState(s);
}
    render() {
        let {show}=this.state;
       
        return (
        <React.Fragment>
            <div className="container">
         <NavBarProduct handelData={this.hendelShowBill}/>
         {show?this.getBill():""}
         {show?this.getTable():""}
         </div>
        </React.Fragment>
        );
    }
    getDD=(top,arr,value,name)=>{
        return (
        <div className="form-group">
           
            <select
            className="form-control"
            name={name}
            value={value}
            onChange={this.handleChange}>
             <option value="">{top}</option>
             {arr.map(ele=><option>{ele}</option>)}
            </select>
        </div>
        );
    }
    getDDR=(top,arr,value,name)=>{
        return (
        <div className="form-group">
           
            <select
            className="form-control"
            name={name}
            value={value}
            onChange={this.handleChange}>
             <option value="">{top}</option>
             {arr.map(ele=><option value={ele.value}>{ele.txt}</option>)}
            </select>
        </div>
        );
    }
}
export default ProductStore;