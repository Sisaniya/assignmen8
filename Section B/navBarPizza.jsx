import React, {Component} from "react";
class NavBarPizza extends Component
{
    state={

    }
    render() {
      let {handelData,show}=this.props;
        return (
            <React.Fragment>
           <nav className="navbar navbar-expand-sm bg-light navbar-light">
               <a className="navbar-brand">MyFavPizza</a>
               <div className="" id="45">
                   <ul className="navbar-nav ml-2">
                       <li className="nav-item " onClick={()=>handelData(1)}>
                           <a className={"nav-link "+(show==1?"active":"")}>Veg Pizza
                           
                           </a></li>
                           <li className="nav-item " onClick={()=>handelData(2)}>
                           <a className={"nav-link "+(show==2?"active":"")}>Non-Veg Pizza
                           
                           </a></li>
                           <li className="nav-item " onClick={()=>handelData(3)}>
                           <a className={"nav-link "+(show==3?"active":"")} >Side Dishes
                           
                           </a></li>
                           <li className="nav-item " onClick={()=>handelData(4)}>
                           <a className={"nav-link "+(show==4?"active":"")}>Other Items
                           
                           </a></li>
                           
                   </ul>


               </div>

           </nav>
            </React.Fragment>
        );

    }
   
}
export default NavBarPizza;