import React, {Component} from "react";
import NavBarPizza from "./navBarPizza";

class PizzaOrder extends Component
{
    state={
        show:0,
        data:
        [
            {"id":"MIR101","image":"https://i.ibb.co/SR1Jzpv/mirinda.png","type":"Beverage","name":"Mirinda","desc":"Mirinda","veg":"Yes"},
            {"id":"PEP001","image":"https://i.ibb.co/3vkKqsF/pepsiblack.png","type":"Beverage","name":"Pepsi  Black Can","desc":"Pepsi  Black Can","veg":"Yes"},
            {"id":"LIT281","image":"https://i.ibb.co/27PvTng/lit.png","type":"Beverage","name":"Lipton  Iced  Tea","desc":"Lipton Iced  Tea","veg":"Yes"},
            {"id":"PEP022","image":"https://i.ibb.co/1M9xDZB/pepsi-new20190312.png","type":"Beverage","name":"Pepsi New","desc":"Pepsi  New","veg":"Yes"},
            {"id":"BPCNV1","image":"https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png","type":"Burger Pizza","name":"Classic  Non Veg","desc":"Oven-baked buns  with  cheese, peri-peri chicken,  tomato  & capsicum  in  creamy  mayo","veg":"No"},
            {"id":"BPCV03","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Classic  Veg","desc":"Oven-baked buns  with  cheese, tomato  & capsicum  in  creamy  mayo","veg":"Yes"},
            {"id":"BPPV04","image":"https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel1.png","type":"Burger Pizza","name":"Premium  Veg","desc":"Oven-baked buns  with  cheese, paneer, tomato, capsicum  & red paprika in  creamy  mayo","veg":"Yes"},
            {"id":"DIP033","image":"https://i.ibb.co/0mbBzsw/new-cheesy.png","type":"Side Dish","name":"Cheesy  Dip","desc":"Baked  to  perfection. Your  perfect pizza partner!  Tastes  best  with  dip","veg":"Yes"},
            {"id":"DIP072","image":"https://i.ibb.co/fY52zBw/new-jalapeno.png","type":"Side Dish","name":"Cheesy  Jalapeno  Dip","desc":"A spicy,  tangy flavored  cheese  dip is  a an  absolute  delight with  your  favourite Garlic  Breadsticks","veg":"Yes"},
            {"id":"GAR952","image":"https://i.ibb.co/BNVmfY9/Garlic-bread.png","type":"Side Dish","name":"Garlic  Breadsticks","desc":"Baked  to  perfection. Your  perfect pizza partner!  Tastes  best  with  dip","veg":"Yes"},
            {"id":"PARCH1","image":"https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png","type":"Side  Dish","name":"Chicken Parcel","desc":"Snacky  bites!  Pizza rolls with  chicken sausage & creamy  harissa sauce","veg":"No"},
            {"id":"PARVG7","image":"https://i.ibb.co/JHhrM7d/Parcel-Veg.png","type":"Side Dish","name":"Veg Parcel","desc":"Snacky  bites!  Pizza rolls with  paneer  & creamy  harissa sauce","veg":"Yes"},
            {"id":"PATNV7","image":"https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png","type":"Side  Dish","name":"White Pasta Italiano  Non-Veg","desc":"Creamy white pasta with  pepper  barbecue  chicken","veg":"No"},
            {"id":"PATVG4","image":"https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png","type":"Side  Dish","name":"White Pasta Italiano  Veg","desc":"Creamy white pasta with  herb  grilled mushrooms","veg":"Yes"},
            {"id":"DES044","image":"https://i.ibb.co/gvpDKPv/Butterscotch.png","type":"Dessert","name":"Butterscotch  Mousse  Cake","desc":"Sweet temptation! Butterscotch  flavored  mousse","veg":"Yes"},
            {"id":"DES028","image":"https://i.ibb.co/nm96NZW/ChocoLava.png","type":"Dessert","name":"Choco  Lava  Cake","desc":"Chocolate lovers  delight!  Indulgent,  gooey molten  lava  inside  chocolate cake","veg":"Yes"},
            {"id":"PIZVDV","image":"https://i.ibb.co/F0H0SWG/deluxeveg.png","type":"Pizza","name":"Deluxe Veggie","desc":"Veg delight - onion,  capsicum, grilled mushroom, corn  & paneer","veg":"Yes"},
            {"id":"PIZVFH","image":"https://i.ibb.co/4mHxB5x/farmhouse.png","type":"Pizza","name":"Farmhouse","desc":"Delightful  combination of  onion,  capsicum, tomato  & grilled mushroom","veg":"Yes"},
            {"id":"PIZVIT","image":"https://i.ibb.co/sRH7Qzf/Indian-TandooriPaneer.png","type":"Pizza","name":"Indi Tandoori  Paneer","desc":"It  is  hot.  It  is  spicy.  It  is  oh-soIndian.  Tandoori  paneer  with  capsicum, red paprika & mint  mayo","veg":"Yes"},
            {"id":"PIZVMG","image":"https://i.ibb.co/MGcHnDZ/mexgreen.png","type":"Pizza","name":"Mexican Green Wave","desc":"Mexican herbs sprinkled on  onion,  capsicum, tomato  & jalapeno","veg":"Yes"},
            {"id":"PIZVPP","image":"https://i.ibb.co/cb5vLX9/peppypaneer.png","type":"Pizza","name":"Peppy  Paneer","desc":"Flavorful trio  of  juicy paneer, crisp capsicum  with  spicy red paprika","veg":"Yes"},
            {"id":"PIZVVE","image":"https://i.ibb.co/gTy5DTK/vegextra.png","type":"Pizza","name":"Veg Extravaganza","desc":"Black olives, capsicum, onion,  grilled mushroom, corn, tomato, jalapeno  & extra cheese","veg":"Yes"},
            {"id":"PIZNCP","image":"https://i.ibb.co/b5qBJ9d/cheesepepperoni.png","type":"Pizza","name":"Chicken  Pepperoni","desc":"A  classic American  taste!  Relish  the delectable  flavor  of  Chicken Pepperoni,  topped  with  extra cheese","veg":"No"},
            {"id":"PIZNCD","image":"https://i.ibb.co/GFtkbB1/ChickenDominator10.png","type":"Pizza","name":"Chicken Dominator","desc":"Loaded with  double  pepper  barbecue  chicken,  peri-peri chicken,  chicken tikka & grilled chicken rashers","veg":"No"},
            {"id":"PIZNPB","image":"https://i.ibb.co/GxbtcLK/Pepper-Barbeque-OnionC.png","type":"Pizza","name":"Pepper  Barbecue  & Onion","desc":"A  classic favourite with  pepper  barbeque  chicken & onion","veg":"No"},
            {"id":"PIZNIC","image":"https://i.ibb.co/6Z5wBqr/Indian-Tandoori-ChickenTikka.png","type":"Pizza","name":"Indi  Chicken Tikka","desc":"The  wholesome flavour of  tandoori  masala  with  Chicken tikka,  onion,  red paprika & mint  mayo","veg":"No"}],
            sizes:["Regular","Medium","Large"],
            crusts:["New Hand  Tossed","Wheat  Thin  Crust","Cheese  Burst","Fresh Pan Pizza","Classic Hand Tossed"],
        cart:[],
        info:[],
        }
        handleChange=(e)=>{
            let {currentTarget:input}=e;
            let s={...this.state};
            let index=s.info.findIndex(ele=>ele.id==input.id);
            if(index>=0){
                let ele=s.info[index];
                ele[input.name]=input.value;
                console.log(ele);
            }
            else 
            {
                let json={id:input.id};
                json[input.name]=input.value;
                s.info.push(json);
                console.log(json);
            }
           
            this.setState(s);
            console.log(s[input.name]);
        }
        addToCart=(id)=>{
            let s={...this.state};
            let {size,crust}=s;
            let index=s.data.findIndex(ele => (ele.id==id && (ele.type=='Pizza' || ele.type=='Burger Pizza')));
            let food=s.data.find(ele => ele.id==id);
            if(index>=0)
            {
              let i=s.cart.findIndex(ele => ele.id==id);
              if(i>=0)
              {
                  s.cart[i].qty+=1;
              }
              else
              {
                let details=s.info.find(ele=>ele.id==id);
                let size=details?details.size:"";
                let crust=details?details.crust:"";

               if(size && crust)
                {
               let q=1;
               let cart={...food,size:size,crust:crust,qty:q};
               s.cart.push(cart);
               let index=s.info.findIndex(ele=>ele.id==id);
               s.info.splice(index,1);
                }
                else if(size)
                {
                  window.alert('Choose crust before add to cart');
                 
                }
                else if(crust)
                {
                  window.alert('Choose size before add to cart');
                  
                }
                else
                {
                    window.alert('Choose size and crust  before add to cart');
                }
              }
             }
             else 
             {
                let i=s.cart.findIndex(ele => ele.id==id);
            if(i>=0)
              {
                s.cart[i].qty+=1;
                }
             else
             {
                let q=1;
             let cart={...food,size:'',crust:'',qty:q};
             s.cart.push(cart);
                   } 
             }
           this.setState(s);
        }
      
    hendelShow=(i)=>{
        let s={...this.state};
        s.show=i;
        this.setState(s);
    }
    increaseQty=(id)=>
    {
        let s={...this.state};
     let index=s.cart.findIndex(ele => ele.id==id);
     s.cart[index].qty+=1;
     this.setState(s);
    }
    decreaseQty=(id)=>
    {
        let s={...this.state};
      let index=s.cart.findIndex(ele => ele.id==id);
      let q=s.cart[index].qty;
      if(q==1)
      {
        s.cart.splice(index,1);
      }
      else
      {
        s.cart[index].qty=q-1;
      }
     this.setState(s);
    }
    getCart=()=>{
        let {cart}=this.state;
        if(cart.length>0){
        return (
            <React.Fragment>
                <div className="row container">
                    <div className="col-12 text-center"><h3>Cart List</h3></div>
                </div>
                {cart.map(ele=>{
                    return (
                        <div className="row">
                            <div className="col-4 border">
                                <img style={{width:"98%",height:"auto",marginTop:"10px"}} src={ele.image} width="150" height="100"/>

                            </div>
                            <div className="col-8 border">
                            <h6>{ele.name}</h6>
                                {ele.desc}<br/>
                                {ele.size?<span className="font-weight-bold">{ele.size}/{ele.crust}</span>:""}<br/>
                                <button className="btn btn-danger btn-sm" onClick={()=>this.decreaseQty(ele.id)}>-</button>
                                <button className="btn btn-secondary btn-sm">{ele.qty}</button>
                                <button className="btn btn-success btn-sm" onClick={()=>this.increaseQty(ele.id)}>+</button>
                            </div>
                        </div>
                    )
                })}
          </React.Fragment>
        )
        }
        else 
        {
            return (
            <div className="row">
            <div className="col-12 text-center"><h3>Cart List</h3>Cart Empty</div>
        </div>
          );
        }
    }
    showOthers=()=>
    {
        let {show,data,cart}=this.state;
        let p1=show==3?data.filter(ele => (ele.type=='Side Dish')?true:false):data;
        let p2=show==4?p1.filter(ele => !(ele.type=='Side Dish' || ele.type=='Pizza' || ele.type=='Burger Pizza')):p1;
        return (
            <div className="row text-center">
            {p2.map(ele=>{
                let {id,name,desc,image}=ele;
                let i=cart.findIndex(ele => ele.id==id);
                return (
                 <div className="col-6 border">
                     <div className="row text-center">
                         <div className="col-12">
                             <img style={{width:"95%",height:"auto"}} src={image} width="150" height="80"/>
                 <h6>{name}</h6>{desc}
                          </div>
                
                          {i>=0?(<div className="col-12 text-center">
                            <button className="btn btn-danger btn-sm" onClick={()=>this.decreaseQty(id)}>-</button>
                                <button className="btn btn-secondary btn-sm">{cart[i].qty}</button>
                                <button className="btn btn-success btn-sm" onClick={()=>this.increaseQty(id)}>+</button> </div>)
                                :(<div className="col-12 text-center">
                                    <button className="btn btn-primary btn-md" onClick={()=>this.addToCart(id)}>Add To Cart</button></div> )}
                 
                 </div>
                 </div>
                );
                   
            })}
        </div> 
        );
    }
    getPizza=()=>{
        let {show,data,sizes,crusts,size,info,cart}=this.state;
        let p1=show==1?data.filter(ele => (ele.veg=='Yes' &&  (ele.type=='Pizza' || ele.type=='Burger Pizza'))?true:false):data;
        let p2=show==2?p1.filter(ele => (ele.veg=='No' && (ele.type=='Pizza' || ele.type=='Burger Pizza'))?true:false):p1;
        return (
            
            <div className="row text-center">
               {p2.map(ele=>{
                   let {id,name,desc,image}=ele;
                   let details=info.find(ele=>ele.id==id);
                   let size=details?details.size:"";
                   let crust=details?details.crust:"";
                   let i=cart.findIndex(ele => ele.id==id);

                   if(i>=0)
                   {
                      size=cart[i].size;
                      crust=cart[i].crust;
                   }
                   return (
                    <div className="col-6 border">
                        <div className="row text-center">
                            <div className="col-12 m-1">
                                <img style={{width:"95%",height:"auto"}} src={image} width="150" height="80"/>
                    <h6>{name}</h6>{desc}
                             </div>
                   
                        <div className="col-6 text-center">
                            {this.getDD("Select Size",sizes,size,"size",ele.id)}
                        </div>
                        <div className="col-6 text-center">
                            {this.getDD("Select Crust",crusts,crust,"crust",ele.id)}
                        </div>
                        
                            {i>=0?(<div className="col-12 text-center">
                            <button className="btn btn-danger btn-sm" onClick={()=>this.decreaseQty(ele.id)}>-</button>
                                <button className="btn btn-secondary btn-sm">{cart[i].qty}</button>
                                <button className="btn btn-success btn-sm" onClick={()=>this.increaseQty(ele.id)}>+</button> </div>)
                                :(<div className="col-12 text-center">
                                    <button className="btn btn-primary btn-md" onClick={()=>this.addToCart(id)}>Add To Cart</button></div> )}
                           
                       
                    
                    </div>
                    </div>
                   );
                      
               })}
           </div> 
        );
    }
    render() {
        let {show,data}=this.state;
        return (
            <React.Fragment>
            <NavBarPizza handelData={this.hendelShow} show={show}/>
            
            {show==1 || show==2?<div className="row text-center"><div className="col-8"> {this.getPizza()}</div><div className="col-4">{this.getCart()}</div></div>:""}
            {show==3 || show==4?<div className="row text-center"><div className="col-8"> {this.showOthers()}</div><div className="col-4">{this.getCart()}</div></div>:""}
        
            </React.Fragment>
        )
    }
    getDD=(top,arr,value,name,id)=>{
        return (
        <div className="form-group">
           
            <select
            className="form-control"
            id={id}
            name={name}
            value={value}
            onChange={this.handleChange}>
             <option value="">{top}</option>
             {arr.map(ele=><option>{ele}</option>)}
            </select>
        </div>
        );
    }
}
export default PizzaOrder;